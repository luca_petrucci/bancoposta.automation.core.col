package android;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class BachecaPage extends LoadableComponent<BachecaPage> {
	
	
	private AppiumDriver<MobileElement> driver=null;
	private MobileElement menu;
	
	
	
	public BachecaPage(AppiumDriver<MobileElement> driver) {
		this.driver=driver;
	}
	@Override
	protected void isLoaded() throws Error {
//		menu= (MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.id("//android.widget.ImageButton[@resources-id='posteitaliane.posteapp.appbpol:id/ib_left']")),40);
//		Assert.assertTrue(menu!=null);
	}
	
	public void confirmAuthorization(String actionToPerform,String posteId) throws Exception{
		Thread.sleep(2000);
		MobileElement lastItemVisible=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.RelativeLayout[@resource-id='posteitaliane.posteapp.appbpol:id/rlRow']")),30); 
		lastItemVisible.click();
//		MobileElement confirmAuth= (MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("android.widget.Button[0]")), 30);
		MobileElement confirmAuth= (MobileElement) driver.findElements(By.className("android.widget.Button")).get(0);

		MobileElement cancelAuth= (MobileElement) driver.findElements(By.className("android.widget.Button")).get(1);
		if(actionToPerform.equalsIgnoreCase("confirm")){
			confirmAuth.click();
			MobileElement posteIdField=(MobileElement) driver.findElements(By.className("android.widget.EditText")).get(0);
			posteIdField.sendKeys(posteId);
			driver.hideKeyboard();
			MobileElement nextBtn= (MobileElement) driver.findElements(By.className("android.widget.Button")).get(0);
			nextBtn.click();
			
		}else{
			cancelAuth.click();
		}
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(driver, this);
		
	}

}
