package android;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import test.automation.core.UIUtils;

public class HomePage extends LoadableComponent<HomePage> {
	
	
	private AppiumDriver<MobileElement> driver=null;
	private MobileElement menu;
	
	
	
	public HomePage(AppiumDriver<MobileElement> driver) {
		this.driver=driver;
	}
	@Override
	protected void isLoaded() throws Error {
		System.out.println(driver.getPageSource());
		System.out.println("-------");
		System.out.println(driver.getContext());
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		menu=driver.findElement(By.xpath("//*[@resource-di='posteitaliane.posteapp.appbpol:id/ib_left'"));
		//menu= (MobileElement) driver.findElements(By.className("android.widget.ImageButton")).get(0);
//				menu= (MobileElement) driver.findElement(By.id("//android.widget.ImageButton[@resource-id='posteitaliane.posteapp.appbpol:id/imageButton']"));
//		Assert.assertTrue(menu!=null);
	}
	
	public BachecaPage goToBacheca(){
//		double xCoordinate = 50.0;
//		double yCoordinate = 150.0;
////		driver.tap(1, 50, 150, 800);
//		new TouchAction(driver).tap(50, 150).perform();
		menu.click();
		MobileElement bachecaBtn=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Bacheca']")), 20);
		bachecaBtn.click();
		return new BachecaPage(driver).get();
	}
	
	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}

}
