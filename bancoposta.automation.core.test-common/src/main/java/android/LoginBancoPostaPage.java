package android;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class LoginBancoPostaPage extends LoadableComponent<LoginBancoPostaPage> {
	
	
	private AppiumDriver<MobileElement> driver=null;
	private final static String LOCATOR_BTN_REGISTRATION="posteitaliane.posteapp.appbpol:id/bt_register";
	private final static String LOCATOR_TITLE="posteitaliane.posteapp.appbpol:id/tv_title";
	private MobileElement lblTitle=null;
	private MobileElement btnRegistration=null;
	private TabAccessoPosteITPosteID tabAccessPage=null;
	private PosteITSubAccessPage posteItTabPage=null;
	private PosteIDSubAccessPage posteIDTabPage=null;
	
	
	public PosteITSubAccessPage getPosteItTabPage() {
		return posteItTabPage;
	}
	public PosteIDSubAccessPage getPosteIDTabPage() {
		return posteIDTabPage;
	}
	public LoginBancoPostaPage(AppiumDriver<MobileElement> driver) {
		this.driver=driver;
	}
	@Override
	protected void isLoaded() throws Error {
		tabAccessPage= new TabAccessoPosteITPosteID(driver).get();
		lblTitle= (MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_TITLE)),40);
		btnRegistration= (MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_BTN_REGISTRATION)),40);
		switch (tabAccessPage.getTabNameSelected()) {
		case TabAccessoPosteITPosteID.TEXT_TAB_POSTE_IT:
			posteItTabPage=new PosteITSubAccessPage(driver).get();
			break;
		case TabAccessoPosteITPosteID.TEXT_TAB_POSTEID:
			posteIDTabPage=new PosteIDSubAccessPage(driver).get();
			break;
		default:
			break;
		}
		Assert.assertTrue(lblTitle!=null);
		Assert.assertTrue(btnRegistration!=null);
	}
	
	
	
	
	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}
	
	public void authorizationAcceptfromBacheca(String userName,String password,String posteId,String actionToPerform) throws Exception{
		HomePage homePage= posteItTabPage.login(userName, password);
		BachecaPage bacheca =homePage.goToBacheca();
		bacheca.confirmAuthorization(actionToPerform, posteId);
		
	}
}
