package android;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class PosteITSubAccessPage extends LoadableComponent<PosteITSubAccessPage> {
	private MobileElement userName;
	private MobileElement password;
	private MobileElement loginButton;
	private AppiumDriver<MobileElement> driver=null;

	public PosteITSubAccessPage(AppiumDriver<MobileElement> driver) {
		this.driver=driver;
	}


	@Override
	protected void isLoaded() throws Error {
		userName=(MobileElement)UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.EditText[@resource-id='posteitaliane.posteapp.appbpol:id/et_user_name']")),30);
		password=(MobileElement)UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.EditText[@resource-id='posteitaliane.posteapp.appbpol:id/et_password']")),30);
//		loginButton=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.Button[contains(.,'ACCEDI')]")),30);
		Assert.assertTrue(userName!=null);
		Assert.assertTrue(password!=null);
//		Assert.assertTrue(loginButton!=null);

	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}
	
	public HomePage login(String userName,String password){
		this.userName.sendKeys(userName);
		try {((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception err) {};
		this.password.sendKeys(password);
		try {((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception err) {};
		loginButton=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.Button[@text='ACCEDI' or @text='Accedi']")),30);

		loginButton.click();
		return new HomePage(driver).get();
		
	}
	
}
