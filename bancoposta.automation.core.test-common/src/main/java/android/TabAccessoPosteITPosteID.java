package android;

import org.assertj.core.api.AssertDelegateTarget;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class TabAccessoPosteITPosteID extends LoadableComponent<TabAccessoPosteITPosteID> {
	public final static String TEXT_TAB_POSTEID="POSTEID";
	public final static String TEXT_TAB_POSTE_IT="POSTE.IT";
	public final static String LOCATOR_TAB_SELECTED="//android.widget.LinearLayout/android.widget.TextView[@selected='true']";
	private String tabNameSelected=null;
	private MobileElement tabSelected=null;
	private AppiumDriver<MobileElement> driver=null;
	public String getTabNameSelected(){
		return tabNameSelected;
	}
	public TabAccessoPosteITPosteID(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	public LoadableComponent returnTabSelected(){
		LoadableComponent pageToLoad=null;
		tabSelected=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(LOCATOR_TAB_SELECTED)));
		tabNameSelected=tabSelected.getAttribute("text");
		switch (tabNameSelected) {
		case TEXT_TAB_POSTE_IT:
				pageToLoad=new PosteITSubAccessPage(driver);
			
				break;
		case TEXT_TAB_POSTEID:
			pageToLoad=new PosteIDSubAccessPage(driver);
			break;
		default:
			break;
		}
		return pageToLoad;
	
	}
	
	
	
	@Override
	protected void isLoaded() throws Error {
		returnTabSelected();
		Assert.assertTrue(tabSelected!=null);
	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);
	}

}
