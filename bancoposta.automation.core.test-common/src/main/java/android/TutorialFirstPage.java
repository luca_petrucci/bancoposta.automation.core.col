package android;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class TutorialFirstPage extends LoadableComponent<TutorialFirstPage> {
	private AppiumDriver<MobileElement> driver=null; 
	private final static String LOCATOR_ID_IMG_LOGO="posteitaliane.posteapp.appbpol:id/iv_logo_intro";
	private final static String LOCATOR_ID_BTN_CONTINUE="posteitaliane.posteapp.appbpol:id/access_bt";
	private MobileElement imgLogo=null;
	private MobileElement btnContinue= null;
	public TutorialFirstPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {
		imgLogo=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_ID_IMG_LOGO)),40);
		btnContinue=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_ID_BTN_CONTINUE)),40);
		Assert.assertTrue(imgLogo!=null);
		Assert.assertTrue(btnContinue!=null);


	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}

	public TutorialSecondPage goToNextPage(){
		btnContinue.click();
		return new TutorialSecondPage(driver).get();
	}
	
	
	
}
