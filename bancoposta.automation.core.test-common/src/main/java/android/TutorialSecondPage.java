package android;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class TutorialSecondPage extends LoadableComponent<TutorialSecondPage> {
	private AppiumDriver<MobileElement> driver=null;
	private final static String LOCATOR_ID_IMG_LOGO="posteitaliane.posteapp.appbpol:id/iv_logo_intro";
	private final static String LOCATOR_ID_IMG_TUTORIAL_STEP="posteitaliane.posteapp.appbpol:id/view_pager_indicator";
	private final static String LOCATOR_ID_BTN_CONTINUE="posteitaliane.posteapp.appbpol:id/access_bt";
	private final static String LOCATOR_ID_TEXT_CENTER_UNDER_IMAGE="posteitaliane.posteapp.appbpol:id/tv_header";
	private final static String TEXT_INSIDE_CENTER_TEXT_UNDER_IMAGE="La tua situazione finanziaria, a portata di mano";
	private MobileElement imgLogo=null;
	private MobileElement imgTutorialStep=null;
	private MobileElement btnContinue= null;
	private MobileElement txtViewCenterUnderImage= null;

	public TutorialSecondPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {
		imgLogo=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_ID_IMG_LOGO)),40);
		imgTutorialStep=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_ID_IMG_TUTORIAL_STEP)),40);
		txtViewCenterUnderImage=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_ID_TEXT_CENTER_UNDER_IMAGE)));
		btnContinue=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_ID_BTN_CONTINUE)),40);
//		Assert.assertTrue(txtViewCenterUnderImage.getAttribute("text").equals(TEXT_INSIDE_CENTER_TEXT_UNDER_IMAGE));
		Assert.assertTrue(imgLogo!=null);
		Assert.assertTrue(imgTutorialStep!=null);
		Assert.assertTrue(txtViewCenterUnderImage!=null);
		Assert.assertTrue(btnContinue!=null);


	}
	
	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}

	public TutorialThirdPage goToNextPage(){
		btnContinue.click();
		return new TutorialThirdPage(driver).get();
	}
	
	
	
}
