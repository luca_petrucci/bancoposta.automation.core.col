package android;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

public class TutorialSixthPage extends LoadableComponent<TutorialSixthPage> {
	private AppiumDriver<MobileElement> driver=null;
	private final static String LOCATOR_ID_IMG_LOGO="posteitaliane.posteapp.appbpol:id/iv_logo_intro";
	private final static String LOCATOR_ID_IMG_TUTORIAL_STEP="posteitaliane.posteapp.appbpol:id/iv_tutorial_step";
	private final static String LOCATOR_ID_BTN_CONTINUE="posteitaliane.posteapp.appbpol:id/access_bt";
	private final static String LOCATOR_ID_TEXT_CENTER_UNDER_IMAGE="posteitaliane.posteapp.appbpol:id/tv_header";
	private final static String TEXT_INSIDE_CENTER_TEXT_UNDER_IMAGE="Bollettino da pagare? Scatta una foto!";

	private MobileElement imgLogo=null;
	private MobileElement imgTutorialStep=null;
	private MobileElement btnContinue= null;
	private MobileElement txtViewCenterUnderImage= null;

	public TutorialSixthPage(AppiumDriver<MobileElement> driver){
		this.driver=driver;
	}
	
	@Override
	protected void isLoaded() throws Error {
		imgLogo=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_ID_IMG_LOGO)));
		imgTutorialStep=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_ID_IMG_TUTORIAL_STEP)));
		txtViewCenterUnderImage=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_ID_TEXT_CENTER_UNDER_IMAGE)));
		btnContinue=(MobileElement) UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.id(LOCATOR_ID_BTN_CONTINUE)));
//		Assert.assertTrue(txtViewCenterUnderImage.getAttribute("text").equals(TEXT_INSIDE_CENTER_TEXT_UNDER_IMAGE));
		Assert.assertTrue(imgLogo!=null);
		Assert.assertTrue(imgTutorialStep!=null);
		Assert.assertTrue(txtViewCenterUnderImage!=null);
		Assert.assertTrue(btnContinue!=null);


	}

	@Override
	protected void load() {
		PageFactory.initElements(driver, this);

	}

	public TutorialSixthPage goToNextPage(){
		btnContinue.click();
		return new TutorialSixthPage(driver).get();
	}
	
	
	
}
