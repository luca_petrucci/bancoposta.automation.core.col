package android;

import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.commons.lang.text.StrSubstitutor;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import android.Utility.DIRECTION;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import test.automation.core.UIUtils;
import utils.Entry;
import utils.ObjectFinderLight;

import static java.lang.Math.toIntExact;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;


public class Utility {
	public enum DIRECTION {
	    DOWN, UP, LEFT, RIGHT;
	}
	
	
	
	public static void swipe(MobileDriver driver, DIRECTION direction, long duration) {
	    Dimension size = driver.manage().window().getSize();

	    int startX = 0;
	    int endX = 0;
	    int startY = 0;
	    int endY = 0;

	    switch (direction) {
	        case RIGHT:
	            startY = (int) (size.height / 2);
	            startX = (int) (size.width * 0.90);
	            endX = (int) (size.width * 0.05);
	            new TouchAction(driver)
	                    .press(PointOption.point(startX, startY))
	                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
	                    .moveTo(PointOption.point(endX, startY))
	                    .release()
	                    .perform();
	            break;

	        case LEFT:
	            startY = (int) (size.height / 2);
	            startX = (int) (size.width * 0.05);
	            endX = (int) (size.width * 0.90);
	            new TouchAction(driver)
	                    .press(PointOption.point(startX, startY))
	                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
	                    .moveTo(PointOption.point(endX, startY))
	                    .release()
	                    .perform();

	            break;

	        case UP:
	            endY = (int) (size.height * 0.70);
	            startY = (int) (size.height * 0.30);
	            startX = (size.width / 2);
	            new TouchAction(driver)
	                    .press(PointOption.point(startX, startY))
	                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
	                    .moveTo(PointOption.point(startX, endY ))
	                    .release()
	                    .perform();
	            break;


	        case DOWN:
	            startY = (int) (size.height * 0.70);
	            endY = (int) (size.height * 0.30);
	            startX = (size.width / 2);
	            new TouchAction(driver)
	                    .press(PointOption.point(startX, startY))
	                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
	                    .moveTo(PointOption.point(startX, endY ))
	                    .release()
	                    .perform();

	            break;

	    }
	}
	
	public static void swipeToElement(MobileDriver driver, DIRECTION direction, long duration,String element2FindInPage,int repeats)
	{
		for(int i=0;i<repeats;i++)
		{
			try
			{
				
//				if(driver.getPageSource().contains(element2FindInPage))
//				{
//					return;
//				}
				
				String pageSource=driver.getPageSource();
				
				if(ObjectFinderLight.elementExists(pageSource, element2FindInPage)) return;
				
				Utility.swipe(driver, direction, duration);
				
				Thread.sleep(500);
			}
			catch(Exception err)
			{
				
			}
		}
	}
	
	public static void swipe(MobileDriver driver, DIRECTION direction,float fromDx,float fromDy,float toDx,float toDy, long duration)
	{
		Dimension size = driver.manage().window().getSize();

	    int startX = 0;
	    int endX = 0;
	    int startY = 0;
	    int endY = 0;

	    switch (direction) {
	        case RIGHT:
	            startY = (int) (size.height * fromDy);
	            startX = (int) (size.width * fromDx);
	            endX = (int) (size.width * toDx);
	            new TouchAction(driver)
	                    .press(PointOption.point(startX, startY))
	                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
	                    .moveTo(PointOption.point(endX, startY))
	                    .release()
	                    .perform();
	            break;

	        case LEFT:
	            startY = (int) (size.height * fromDy);
	            startX = (int) (size.width * fromDx);
	            endX = (int) (size.width * toDx);
	            new TouchAction(driver)
	                    .press(PointOption.point(startX, startY))
	                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
	                    .moveTo(PointOption.point(endX, startY))
	                    .release()
	                    .perform();

	            break;

	        case UP:
	            endY = (int) (size.height * toDy);
	            startY = (int) (size.height * fromDy);
	            startX = (int) (size.width * fromDx);
	            new TouchAction(driver)
	                    .press(PointOption.point(startX, startY))
	                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
	                    .moveTo(PointOption.point(startX, endY ))
	                    .release()
	                    .perform();
	            break;


	        case DOWN:
	            startY = (int) (size.height * fromDy);
	            endY = (int) (size.height * toDy);
	            startX = (int) (size.width * fromDx);
	            new TouchAction(driver)
	                    .press(PointOption.point(startX, startY))
	                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(duration)))
	                    .moveTo(PointOption.point(startX, endY ))
	                    .release()
	                    .perform();

	            break;

	    }
	}

	public static String capitalize(String str) 
	{
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}
	
	public static double parseCurrency(final String amount, final Locale locale)
	{  
	    try {
	    	final NumberFormat format = NumberFormat.getNumberInstance(locale);
		    if (format instanceof DecimalFormat) {
		        ((DecimalFormat) format).setParseBigDecimal(true);
		    }
			return format.parse(amount.replaceAll("[^\\d.,]","")).doubleValue();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    return -1;
	}

	public static String getCurrentMonth() 
	{
		Calendar c=Calendar.getInstance();
		
		Date d=c.getTime();
		
		String mese=new DateFormatSymbols(Locale.ITALY).getMonths()[d.getMonth()];
		
		return mese;
	}
	
	public static String replacePlaceHolders(String str, Entry ... placeholders)
	{
		HashMap<String,String> map=new HashMap<String,String>();
		
		for(Entry p : placeholders)
		{
			map.put(p.getKey(), p.getValue());
		}
		
		return StrSubstitutor.replace(str, map);
	}
	
	public static double round(double value, int places) 
	{
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}

	public static void swipeToElementIOS(AppiumDriver<?> driver, DIRECTION direction, float f, float g, float h, float i, int duration,
			String elementXpath, int k) 
	{
		for(int j=0;j<k;j++)
		{
			try
			{
				
				UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath)),1);
				return;
			}
			catch(Exception err)
			{
				
			}
			Utility.swipe(driver, direction,f,g,h,i, duration);
		}
	}

	public static String toCurrency(double d, Locale locale) 
	{
		NumberFormat format = NumberFormat.getNumberInstance(locale);
		
		return format.format(d);
	}

	public static void selectElementFromIOSPickerWeel(WebDriver driver,String list, String element,String element2Find) 
	{
		List<WebElement> PicherWeelList=driver.findElements(By.xpath(list));
		
		for(int i=0;i<PicherWeelList.size();i++)
		{
			String txt=driver.findElements(By.xpath(element)).get(0).getText().trim().toLowerCase();
			
			if(txt.equals(element2Find.toLowerCase()))
			{
				driver.findElement(By.xpath("//*[@text='Fine']")).click();
				return;
			}
			
			Utility.swipe((MobileDriver) driver, Utility.DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.75f, 1000);
		}
	}
	
	public static String getProvincia(String senderProv) 
	{
		InputStream authConfigStream = Utility.class.getClassLoader().getResourceAsStream("it/poste/bancoposta/province.properties");

	    if (authConfigStream != null) {
	      Properties prov = new Properties();
	      try 
	      {
	        prov.load(authConfigStream);
	        
	        return prov.getProperty(senderProv);
	        
	      } catch (Exception e) 
	      {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	        try {
				authConfigStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	      }
	    }
	    
		return null;
	}

}
