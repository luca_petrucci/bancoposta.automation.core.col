package bean.datatable;

public class BollettinoDataBean 
{
	public static final String BOLLETTINO_896 = "Bollettino Precompilato 896";
	public static final String BOLLETTINO_BIANCO = "Bollettino Bianco";
	public static final String MAV = "Bollettino MAV";
	public static final String BOLLETTINO_PA = "Bollettino PA";
	public static final String BOLLETTINO_674 = "Bollettino Precompilato 674";

	private String bollettinoType;
	private String payWith;
	private String bollettinoCode;
	private String cc;
	private String amount;
	private String owner;
	private String description;
	private String expireDate;
	private String senderName;
	private String senderLastName;
	private String senderAdress;
	private String senderCity;
	private String senderCAP;
	private String senderProv;
	private String testIdCheck;
	private String posteid;
	private String paymentMethod;
	private String commissioni;
	private String provinciaCode;


	public String getProvinciaCode() {
		return provinciaCode;
	}
	public void setProvinciaCode(String provinciaCode) {
		this.provinciaCode = provinciaCode;
	}
	public String getCommissioni() {
		return commissioni;
	}
	public void setCommissioni(String commissioni) {
		this.commissioni = commissioni;
	}
	public String getPosteid() {
		return posteid;
	}
	public void setPosteid(String posteid) {
		this.posteid = posteid;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getPosteId() {
		return posteid;
	}
	public void setPosteId(String posteid) {
		this.posteid = posteid;
	}
	public String getBollettinoType() {
		return bollettinoType;
	}
	public void setBollettinoType(String bollettinoType) {
		this.bollettinoType = bollettinoType;
	}
	public String getPayWith() {
		return payWith;
	}
	public void setPayWith(String payWith) {
		this.payWith = payWith;
	}
	public String getBollettinoCode() {
		return bollettinoCode;
	}
	public void setBollettinoCode(String bollettinoCode) {
		this.bollettinoCode = bollettinoCode;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public String getSenderLastName() {
		return senderLastName;
	}
	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}
	public String getSenderAdress() {
		return senderAdress;
	}
	public void setSenderAdress(String senderAdress) {
		this.senderAdress = senderAdress;
	}
	public String getSenderCity() {
		return senderCity;
	}
	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}
	public String getSenderCAP() {
		return senderCAP;
	}
	public void setSenderCAP(String senderCAP) {
		this.senderCAP = senderCAP;
	}
	public String getSenderProv() {
		return senderProv;
	}
	public void setSenderProv(String senderProv) {
		this.senderProv = senderProv;
	}
	public String getTestIdCheck() {
		return testIdCheck;
	}
	public void setTestIdCheck(String testIdCheck) {
		this.testIdCheck = testIdCheck;
	}


}
