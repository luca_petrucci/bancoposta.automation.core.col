package bean.datatable;

public class BonificoDataBean 
{
	private String payWith;
	private String iban;
	private String cc;
	private String city;
	private String owner;
	private String amount;
	private String description;
	private String address;
	private String citySender;
	private String reference;
	private String refecenceEffective;
	private String testIdCheck;
	private String posteid;
	private String commissioni;
	private String bonificoType;
	private String beneficiario;
	private String paymentMethod;
	private String controlloPaeseBonifico;
	
	public String getControlloPaeseBonifico() {
		return controlloPaeseBonifico;
	}
	public void setControlloPaeseBonifico(String controlloPaeseBonifico) {
		this.controlloPaeseBonifico = controlloPaeseBonifico;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getPosteid() {
		return posteid;
	}
	public void setPosteid(String posteid) {
		this.posteid = posteid;
	}
	public String getBeneficiario() {
		return beneficiario;
	}
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}
	public String getBonificoType() {
		return bonificoType;
	}
	public void setBonificoType(String bonificoType) {
		this.bonificoType = bonificoType;
	}

	public String getCommissioni() {
		return commissioni;
	}
	public void setCommissioni(String commissioni) {
		this.commissioni = commissioni;
	}
	public String getPayWith() {
		return payWith;
	}
	public void setPayWith(String payWith) {
		this.payWith = payWith;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCitySender() {
		return citySender;
	}
	public void setCitySender(String citySender) {
		this.citySender = citySender;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getRefecenceEffective() {
		return refecenceEffective;
	}
	public void setRefecenceEffective(String refecenceEffective) {
		this.refecenceEffective = refecenceEffective;
	}
	public String getTestIdCheck() {
		return testIdCheck;
	}
	public void setTestIdCheck(String testIdCheck) {
		this.testIdCheck = testIdCheck;
	}
	
	
}
