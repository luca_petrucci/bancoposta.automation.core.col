package bean.datatable;

public class CredentialAccessBean {
	private String username;
	private String password;
	private String loginType;
	private String testIdCheck;
	private String userHeader;
	private String device;
	private String posteid;
	private String fingerId;
	private String version;
	private String owner;
	private String onboarding;
	private String debug;
	private String bpol;
	private String webDriver;	

	public String getWebDriver() {
		return webDriver;
	}
	public void setWebDriver(String webDriver) {
		this.webDriver = webDriver;
	}
	public String getBpol() {
		return bpol;
	}
	public void setBpol(String bpol) {
		this.bpol = bpol;
	}
	public String getDebug() {
		return debug;
	}
	public void setDebug(String debug) {
		this.debug = debug;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getFingerId() {
		return fingerId;
	}
	public void setFingerId(String fingerId) {
		this.fingerId = fingerId;
	}
	public String getPosteid() {
		return posteid;
	}
	public void setPosteid(String posteid) {
		this.posteid = posteid;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getUserHeader() {
		return userHeader;
	}
	public void setUserHeader(String userHeader) {
		this.userHeader = userHeader;
	}
	public String getTestIdCheck() {
		return testIdCheck;
	}
	public void setTestIdCheck(String testIdCheck) {
		this.testIdCheck = testIdCheck;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getLoginType() {
		return loginType;
	}
	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getOnboarding() {
		return onboarding;
	}
	public void setOnboarding(String onboarding) {
		this.onboarding = onboarding;
	}
	
}
