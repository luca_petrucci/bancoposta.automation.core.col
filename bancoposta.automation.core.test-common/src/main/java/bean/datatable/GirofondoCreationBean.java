package bean.datatable;

public class GirofondoCreationBean {
	private String payWith;
	private String transferTo;
	private String owner;
	private String amount;
	private String testIdCheck;
	private String posteid;
	private String favoreDi;
	private String commissioni;
	private String fromContoDetail;
	private String toContoDetail;
	private String paymentMethod;
	private String checkTransferTo;
		
	public String getFavoreDi() {
		return favoreDi;
	}
	public void setFavoreDi(String favoreDi) {
		this.favoreDi = favoreDi;
	}
	public String getCommissioni() {
		return commissioni;
	}
	public void setCommissioni(String commissioni) {
		this.commissioni = commissioni;
	}
	public String getFromContoDetail() {
		return fromContoDetail;
	}
	public void setFromContoDetail(String fromContoDetail) {
		this.fromContoDetail = fromContoDetail;
	}
	public String getToContoDetail() {
		return toContoDetail;
	}
	public void setToContoDetail(String toContoDetail) {
		this.toContoDetail = toContoDetail;
	}
	public String getPosteid() {
		return posteid;
	}
	public void setPosteid(String posteid) {
		this.posteid = posteid;
	}
	public String getPayWith() {
		return payWith;
	}
	public void setPayWith(String payWith) {
		this.payWith = payWith;
	}
	public String getTransferTo() {
		return transferTo;
	}
	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTestIdCheck() {
		return testIdCheck;
	}
	public void setTestIdCheck(String testIdCheck) {
		this.testIdCheck = testIdCheck;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getCheckTransferTo() {
		return checkTransferTo;
	}
	public void setCheckTransferTo(String checkTransferTo) {
		this.checkTransferTo = checkTransferTo;
	}
	
}
