package bean.datatable;

public class NotificaPushBean 
{
	private String payWith;
	private String iban;
	private String type;
	private String importo;
	private String commissioni;
	private String title;
	private String notificaTitle;
	private String device;
	private String bpol;
	
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getBpol() {
		return bpol;
	}
	public void setBpol(String bpol) {
		this.bpol = bpol;
	}
	public String getNotificaTitle() {
		return notificaTitle;
	}
	public void setNotificaTitle(String notificaTitle) {
		this.notificaTitle = notificaTitle;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPayWith() {
		return payWith;
	}
	public void setPayWith(String payWith) {
		this.payWith = payWith;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getImporto() {
		return importo;
	}
	public void setImporto(String importo) {
		this.importo = importo;
	}
	public String getCommissioni() {
		return commissioni;
	}
	public void setCommissioni(String commissioni) {
		this.commissioni = commissioni;
	}
	
	
}
