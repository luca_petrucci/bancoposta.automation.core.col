package bean.datatable;

public class OnboardingDataBean {
	private String onboardingType;
	private String codeProduct;
	private String secretCode;
	private String posteid;
	private String expiredDate;
	private String cvv;
	private String testIdCheck;
	
	public String getOnboardingType() {
		return onboardingType;
	}
	public void setOnboardingType(String onboardingType) {
		this.onboardingType = onboardingType;
	}
	public String getCodeProduct() {
		return codeProduct;
	}
	public void setCodeProduct(String codeProduct) {
		this.codeProduct = codeProduct;
	}
	public String getSecretCode() {
		return secretCode;
	}
	public void setSecretCode(String secretCode) {
		this.secretCode = secretCode;
	}
	public String getPosteID() {
		return posteid;
	}
	public void setPosteID(String posteid) {
		this.posteid = posteid;
	}
	public String getExpiredDate() {
		return expiredDate;
	}
	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getTestIdCheck() {
		return testIdCheck;
	}
	public void setTestIdCheck(String testIdCheck) {
		this.testIdCheck = testIdCheck;
	}
	
}
