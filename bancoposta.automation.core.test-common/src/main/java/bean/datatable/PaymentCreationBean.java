package bean.datatable;

public class PaymentCreationBean {
	private String iban;
	private String owner;
	private String amount;
	private String description;
	private String testIdCheck;
	private String posteid;
	private String payWith;
	private String beneficiario;
	private String commissioni;
	private String reference;
	private String cc;
	private String city;
	private String address;
	private String citySender;
	private String refecenceEffective;
	private String bonificoType;
	private String paymentMethod;
	

	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getBonificoType() {
		return bonificoType;
	}
	public void setBonificoType(String bonificoType) {
		this.bonificoType = bonificoType;
	}
	public String getRefecenceEffective() {
		return refecenceEffective;
	}
	public void setRefecenceEffective(String refecenceEffective) {
		this.refecenceEffective = refecenceEffective;
	}
	public String getCitySender() {
		return citySender;
	}
	public void setCitySender(String citySender) {
		this.citySender = citySender;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getBeneficiario() {
		return beneficiario;
	}
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}
	public String getCommissioni() {
		return commissioni;
	}
	public void setCommissioni(String commissioni) {
		this.commissioni = commissioni;
	}
	public String getPayWith() {
		return payWith;
	}
	public void setPayWith(String payWith) {
		this.payWith = payWith;
	}
	public String getPosteid() {
		return posteid;
	}
	public void setPosteid(String posteid) {
		this.posteid = posteid;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTestIdCheck() {
		return testIdCheck;
	}
	public void setTestIdCheck(String testIdCheck) {
		this.testIdCheck = testIdCheck;
	}
	
	

}
