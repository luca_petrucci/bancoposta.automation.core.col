package bean.datatable;

public class QuickOperationCreationBean {
	
	private String quickOperationName;
	private String quickOperationName2;
	private String testIdCheck;
	private String transactionType;
	private String payWith;
	private String iban;
	private String city;
	private String owner; 
	private String amount;
	private String description; 
	private String transferTo;
	private String posteid;
	private String phoneNumber;
	private String phoneCompaign;
	private String userHeader;
	private String objectiveName;
	private String useAdb;
	private String device;
	private String fromQuickOperation;
	private String quickOperationSaved;
	
	
	
	public String getQuickOperationSaved() {
		return quickOperationSaved;
	}
	public void setQuickOperationSaved(String quickOperationSaved) {
		this.quickOperationSaved = quickOperationSaved;
	}
	public String getFromQuickOperation() {
		return fromQuickOperation;
	}
	public void setFromQuickOperation(String fromQuickOperation) {
		this.fromQuickOperation = fromQuickOperation;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getUseAdb() {
		return useAdb;
	}
	public void setUseAdb(String useAdb) {
		this.useAdb = useAdb;
	}
	
	
	
	
	public String getPhoneCompaign() {
		return phoneCompaign;
	}
	public void setPhoneCompaign(String phoneCompaign) {
		this.phoneCompaign = phoneCompaign;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPosteid() {
		return posteid;
	}
	public void setPosteid(String posteid) {
		this.posteid = posteid;
	}
	public String getTransferTo() {
		return transferTo;
	}
	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPayWith() {
		return payWith;
	}
	public void setPayWith(String payWith) {
		this.payWith = payWith;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getQuickOperationName() {
		return quickOperationName;
	}
	public void setQuickOperationName(String quickOperationName) {
		this.quickOperationName = quickOperationName;
	}
	public String getTestIdCheck() {
		return testIdCheck;
	}
	public void setTestIdCheck(String testIdCheck) {
		this.testIdCheck = testIdCheck;
	}
	public String getUserHeader() {
		return userHeader;
	}
	public void setUserHeader(String userHeader) {
		this.userHeader = userHeader;
	}
	public String getQuickOperationName2() {
		return quickOperationName2;
	}
	public void setQuickOperationName2(String quickOperationName2) {
		this.quickOperationName2 = quickOperationName2;
	}
	public String getObjectiveName() {
		return objectiveName;
	}
	public void setObjectiveName(String objectiveName) {
		this.objectiveName = objectiveName;
	}
	
}
