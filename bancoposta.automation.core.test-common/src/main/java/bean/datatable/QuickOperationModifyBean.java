package bean.datatable;

public class QuickOperationModifyBean {

	private String editValue;
	private String testIdCheck;
	
	public String getEditValue() {
		return editValue;
	}
	public void setEditValue(String editValue) {
		this.editValue = editValue;
	}
	public String getTestIdCheck() {
		return testIdCheck;
	}
	public void setTestIdCheck(String testIdCheck) {
		this.testIdCheck = testIdCheck;
	}
	
	
}
