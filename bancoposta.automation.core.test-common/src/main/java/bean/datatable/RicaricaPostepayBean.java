package bean.datatable;

public class RicaricaPostepayBean {
	private String payWith;
	private String transferTo;
	private String owner;
	private String amount;
	private String testIdCheck;
	private String transactionType;
	private String description;
	private String posteid;
	private String paymentMethod;
	private String owner2;
	private String saldominimo;
	private String commissione;
	private String quickOperationName;
	private String userHeader;
	
	
	
	public String getPosteid() {
		return posteid;
	}

	public void setPosteid(String posteid) {
		this.posteid = posteid;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getPosteId() {
		return posteid;
	}

	public void setPosteId(String posteid) {
		this.posteid = posteid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setPayWith(String payWith) {
		this.payWith = payWith;
	}
	
	public String getPayWith() {
		return payWith;
	}
	
	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}
	
	public String getTransferTo() {
		return transferTo;
	}
	
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	public String getAmount() {
		return amount;
	}
	
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTestIdCheck() {
		return testIdCheck;
	}
	public void setTestIdCheck(String testIdCheck) {
		this.testIdCheck = testIdCheck;
	}

	public String getOwner2() {
		return owner2;
	}

	public void setOwner2(String owner2) {
		this.owner2 = owner2;
	}

	public String getSaldominimo() {
		return saldominimo;
	}

	public void setSaldominimo(String saldominimo) {
		this.saldominimo = saldominimo;
	}

	public String getCommissione() {
		return commissione;
	}

	public void setCommissione(String commissione) {
		this.commissione = commissione;
	}

	public String getQuickOperationName() {
		return quickOperationName;
	}

	public void setQuickOperationName(String quickOperationName) {
		this.quickOperationName = quickOperationName;
	}

	public String getUserHeader() {
		return userHeader;
	}

	public void setUserHeader(String userHeader) {
		this.userHeader = userHeader;
	}


	
}
