package bean.datatable;

public class TransactionDetailBean {

	private String categoryTransation;
	private String amount;
	private String categoryDestination;
	private String month;
	private String testIdCheck;
	private String subCategoryTransaction;
	private String transactionType;
	private String operation;
	private String csvPage;
	
	
	
	
	public String getCsvPage() {
		return csvPage;
	}
	public void setCsvPage(String csvPage) {
		this.csvPage = csvPage;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getSubCategoryTransaction() {
		return subCategoryTransaction;
	}
	public void setSubCategoryTransaction(String subCategoryTransaction) {
		this.subCategoryTransaction = subCategoryTransaction;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getCategoryDestination() {
		return categoryDestination;
	}
	public void setCategoryDestination(String categoryDestination) {
		this.categoryDestination = categoryDestination;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getCategoryTransation() {
		return categoryTransation;
	}
	public void setCategoryTransation(String categoryTransation) {
		this.categoryTransation = categoryTransation;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTestIdCheck() {
		return testIdCheck;
	}
	public void setTestIdCheck(String testIdCheck) {
		this.testIdCheck = testIdCheck;
	}
	
	

}
