package bean.datatable;

public class TransactionMovementsBean 
{
	private String conto;
	private String amount; 
	private String transactionType; 
	private String transactionDescription;
	private String operation;
	private String commissioni;
	private String paymentMethod;
	private String notifyType;
	
	
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getCommissioni() {
		return commissioni;
	}
	public void setCommissioni(String commissioni) {
		this.commissioni = commissioni;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getConto() {
		return conto;
	}
	public void setConto(String conto) {
		this.conto = conto;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getTransactionDescription() {
		return transactionDescription;
	}
	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}
	public String getNotifyType() {
		return notifyType;
	}
	public void setNotifyType(String notifyType) {
		this.notifyType = notifyType;
	}
	
	
}
