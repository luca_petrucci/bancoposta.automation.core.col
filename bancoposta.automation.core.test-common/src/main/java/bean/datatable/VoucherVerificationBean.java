package bean.datatable;

public class VoucherVerificationBean {
	private String voucherType;
	private String amount;
	private String payWith;
	private String notificationTest;
	private String testIdCheck;
	private String sender;
	private String posteid;
	private String value;
	private String amount2;
	
	
	
	public String getAmount2() {
		return amount2;
	}
	public void setAmount2(String amount2) {
		this.amount2 = amount2;
	}
	public String getPosteid() {
		return posteid;
	}
	public void setPosteid(String posteid) {
		this.posteid = posteid;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getPosteId() {
		return posteid;
	}
	public void setPosteId(String posteid) {
		this.posteid = posteid;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getNotificationTest() {
		return notificationTest;
	}
	public void setNotificationTest(String notificationTest) {
		this.notificationTest = notificationTest;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPayWith() {
		return payWith;
	}
	public void setPayWith(String payWith) {
		this.payWith = payWith;
	}
	public String getTestIdCheck() {
		return testIdCheck;
	}
	public void setTestIdCheck(String testIdCheck) {
		this.testIdCheck = testIdCheck;
	}
	
}
