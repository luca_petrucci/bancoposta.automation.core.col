package utils;

import java.util.HashMap;
import java.util.Map;

public class DinamicData 
{
	private HashMap<String,Object> repositoryData;
	
	private static DinamicData singleton;
	
	private DinamicData()
	{
		repositoryData=new HashMap<String,Object>();
	}
	
	public static DinamicData getIstance()
	{
		if(singleton == null)
			singleton= new DinamicData();
		
		return singleton;
	}
	
	public void set(String key,Object value)
	{
		repositoryData.put(key, value);
	}
	
	public Object get(String key)
	{
		return repositoryData.get(key);
	}
	
	public void clear()
	{
		repositoryData.clear();
	}
}
