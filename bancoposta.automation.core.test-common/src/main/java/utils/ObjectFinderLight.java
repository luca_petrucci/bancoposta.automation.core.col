package utils;

import java.io.StringReader;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import test.automation.core.UIUtils;
import test.automation.core.cmd.adb.AdbCommandPrompt;

public class ObjectFinderLight 
{
	
	public static boolean elementExists(String pageSource,String xpath)
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
		DocumentBuilder builder;  
		try {  
		    builder = factory.newDocumentBuilder();  
		    Document document = builder.parse(new InputSource(new StringReader(pageSource)));
		    
		    XPath xPath = XPathFactory.newInstance().newXPath();
		    
		    return xPath.compile(xpath).evaluate(document,XPathConstants.NODESET) != null;
		    
		} catch (Exception e) {  
		    e.printStackTrace();  
		} 
		
		return false;
	}
	
	public static NodeList getElements(String pageSource,String xpath)
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
		DocumentBuilder builder;  
		try {  
		    builder = factory.newDocumentBuilder();  
		    Document document = builder.parse(new InputSource(new StringReader(pageSource)));
		    
		    XPath xPath = XPathFactory.newInstance().newXPath();
		    
		    return (NodeList) xPath.compile(xpath).evaluate(document,XPathConstants.NODESET);
		    
		} catch (Exception e) {  
		    e.printStackTrace();  
		} 
		
		return null;
	}
	
	public static Node getElement(String pageSource,String xpath)
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
		DocumentBuilder builder;
		System.out.println("xpath:"+xpath);
		try {  
		    builder = factory.newDocumentBuilder();  
		    Document document = builder.parse(new InputSource(new StringReader(pageSource)));
		    
		    XPath xPath = XPathFactory.newInstance().newXPath();
		    
		    return (Node) xPath.compile(xpath).evaluate(document,XPathConstants.NODE);
		    
		} catch (Exception e) {  
		    e.printStackTrace();  
		} 
		
		return null;
	}

	public static void clickOnElement(Node e) 
	{
		//bounds="[42,1906][1038,2032]"
		String bounds=e.getAttributes().getNamedItem("bounds").getNodeValue().trim();
		bounds=bounds.replace("][", ",").replace("[", "").replace("]", "");
		System.out.println("bounds:"+bounds);
		String[] coordinate=bounds.split(",");
		
		int x1=Integer.parseInt(coordinate[0]);
		int y1=Integer.parseInt(coordinate[1]);
		int x2=Integer.parseInt(coordinate[2]);
		int y2=Integer.parseInt(coordinate[3]);
		
		int posX=((x2-x1)/2)+x1;
		int posY=((y2-y1)/2)+y2;
		
		Properties p=UIUtils.ui().getCurrentProperties();
		AdbCommandPrompt cmd=new AdbCommandPrompt(p.getProperty("adb.path"), p.getProperty("adb.device.udid"));
		
		System.out.println(posX+","+posY);
		
		cmd.tap(posX,posY);
		
	}
	
	
	
	public static void iOSClick(WebDriver driver,Node e)
	{
		int x=Integer.parseInt(e.getAttributes().getNamedItem("x").getNodeValue().trim());
		int y=Integer.parseInt(e.getAttributes().getNamedItem("y").getNodeValue().trim());
		int w=Integer.parseInt(e.getAttributes().getNamedItem("width").getNodeValue().trim());
		int h=Integer.parseInt(e.getAttributes().getNamedItem("height").getNodeValue().trim());
		
		System.out.println("x:"+x+",y:"+y+",w:"+w+",h:"+h);
		
		int posX=(int) (x + w*0.5);
		int posY=(int) (y + h*0.5);
		
		TouchAction a=new TouchAction((PerformsTouchActions) driver);
		a.tap(PointOption.point(posX, posY)).perform();
	}
}
