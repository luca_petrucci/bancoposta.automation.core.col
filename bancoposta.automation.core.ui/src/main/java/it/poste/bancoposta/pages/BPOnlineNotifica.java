package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.CredentialAccessBean;
import bean.datatable.NotificaPushBean;
import test.automation.core.UIUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface BPOnlineNotifica extends AbstractPageManager
{
	public void clickAutorizza(CredentialAccessBean b);

	public void clickNega(CredentialAccessBean b);

	public void clickChiudi();

	public void checkNotificaPushPostagiroBpol(NotificaPushBean b);

	public void clickSpese(CredentialAccessBean b);
}
