package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.BollettinoDataBean;
import bean.datatable.BonificoDataBean;
import bean.datatable.GirofondoCreationBean;
import bean.datatable.NotificationReceptionBean;
import bean.datatable.PaymentCreationBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.transaction_messages.BonificoTransactionMessage;
import it.poste.bancoposta.transaction_messages.GirofondoTransactionMessage;
import it.poste.bancoposta.transaction_messages.PostagiroTransactionMessage;
import test.automation.core.UIUtils;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface BachecaMessaggiDetailsPage extends AbstractPageManager
{
	public boolean messageFind(NotificationReceptionBean b);

	public void checkGirofondoMessage(NotificationReceptionBean b, GirofondoCreationBean girofondoData);
	
	public void clickBack();

	public void checkPostagiroMessage(NotificationReceptionBean b, BonificoDataBean bean);

	void checkMessage(String owner,String payWith,String beneficiario,String amount,String iban,String commissioni,String description);

	public void checkBonificoMessage(NotificationReceptionBean b, BonificoDataBean bean);

	public void checkBollettinoMessage(NotificationReceptionBean b, BollettinoDataBean bean);
	
	public void checkPostagiroMessage(NotificationReceptionBean b, PaymentCreationBean bean);
}
