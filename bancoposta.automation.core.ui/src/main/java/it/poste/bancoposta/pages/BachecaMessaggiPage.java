package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.NotificationReceptionBean;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import utils.Entry;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface BachecaMessaggiPage extends AbstractPageManager
{

	public void clickMessaggi();

	public BachecaMessaggiDetailsPage gotoTransactionMessage(NotificationReceptionBean b);

	public HomepageHamburgerMenu openHamburgerMenu();

	public void filtraMessaggi(NotificationReceptionBean b);
}
