package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.BollettinoDataBean;
import bean.datatable.QuickOperationCreationBean;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface BollettinoConfirmPage extends AbstractPageManager
{
	public void check(BollettinoDataBean bean, String type);

	public String getProvincia(String senderProv);

}

