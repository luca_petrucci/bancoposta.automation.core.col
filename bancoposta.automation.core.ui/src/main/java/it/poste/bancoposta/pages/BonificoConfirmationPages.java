package it.poste.bancoposta.pages;

import bean.datatable.BonificoDataBean;
import bean.datatable.QuickOperationCreationBean;

public interface BonificoConfirmationPages 
{
	public void checkData(BonificoDataBean b);
	
	public OKOperationPage submit(BonificoDataBean b); 
}
