package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.BonificoDataBean;
import bean.datatable.BuoniDataBean;
import bean.datatable.CheckTransactionBean;
import bean.datatable.TransactionMovementsBean;
import bean.datatable.VoucherVerificationBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;
import utils.DinamicData;
import utils.Entry;

import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface BuoniLibrettiPage extends AbstractPageManager
{

	public BuoniPostaliPage gotoBuoniPostali();

	public BuoniPostaliPage clickOnConto(VoucherVerificationBean b);

	public BuoniPostaliPage sottoscriviBuono();

	public void clickOnBuono(VoucherVerificationBean b);

	public BuoniPostaliPage changeSubscriptionMethod(VoucherVerificationBean c);

	public BuoniPostaliPage changeAmount(VoucherVerificationBean c);

	public OKOperationPage clickOnConfirm(VoucherVerificationBean b);


	public void navigateToLibretto(String payWith);

	public void searchTransaction (TransactionMovementsBean b);


	public void checkTransaction(TransactionMovementsBean b);


	public HomepageHamburgerMenu openHamburgerMenu();

	public void navigateToRicaricaLaTuaPostepay();

	public CheckTransactionBean clickOnTransaction(TransactionMovementsBean b);
	
	public void clickContinua();
}
