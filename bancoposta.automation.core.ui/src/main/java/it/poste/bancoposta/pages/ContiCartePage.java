package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.BollettinoDataBean;
import bean.datatable.BolloAutoBean;
import bean.datatable.BonificoDataBean;
import bean.datatable.CredentialAccessBean;
import bean.datatable.GirofondoCreationBean;
import bean.datatable.PaymentCreationBean;
import bean.datatable.RicaricaPostepayBean;
import bean.datatable.TransactionMovementsBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;
import utils.DinamicData;
import utils.Entry;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface ContiCartePage extends AbstractPageManager
{
	public BollettinoChooseOperationPage navigateToBollettinoSection(BollettinoDataBean b, String paymentMethod);

	public void retrieveSaldoContabile();

	public void navigateToConto(String payWith);

	public void navigateToCartaPostePay(String payWith);


	public boolean checkUsciteNonContabilizzateButton();

	public HomepageHamburgerMenu openHamburgerMenu();

	public GirofondoFormPage navigateToGirofondo(GirofondoCreationBean b);


	public void searchTransaction (TransactionMovementsBean b);

	public void checkTransaction(TransactionMovementsBean b);

	public SendBonificoPage navigateToBonifico(BonificoDataBean b);

	public SendPostagiroPage navigateToPostagiro(PaymentCreationBean b);

	public RicaricaPostepayPage navigateToRicaricaPostepay(RicaricaPostepayBean b);


	public void checkSaldi(RicaricaPostepayBean b);


	public RiceviSubMenuCartaPage navigateToRicaricaQuestaPostepay(RicaricaPostepayBean b);


	public BolloAutoPage goToBolloAuto(BolloAutoBean b);

	public RicaricaLaTuaPostepayChooseOperationPage goToRicaricaQuestaPostepay();

	public void clickOnPostePayCard();

	public void clickOnDettaglio();

	public void clickOnRicaricheAutomaticheAttive();

	public void clickOnPagaButton();

	public void clickLeftButton();

	public void clickOnTabCarta();

	public void clickOnFirstItemCarta();

	public void checkMovimento();

	public void clickOnFirstItemConto();

	public void checkToggleAcquistoOnLine(CredentialAccessBean b);

	public String getAvailableAmountCarta();

	public void checkSaldo(TransactionMovementsBean transactionMovementsBean);
	
	public void getsaldoVecchio();

	public void clickOnToogleBonificoIstantaneo(CredentialAccessBean b);

	public void clickOnAccettaPropostaUnilateralePopup();

	public void closeFotocamera();


}