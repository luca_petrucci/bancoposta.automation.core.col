package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.CheckTransactionBean;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import utils.Entry;

import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface DettaglioMovimentoPage extends AbstractPageManager
{
	public void checkDettaglio(CheckTransactionBean t);
}
