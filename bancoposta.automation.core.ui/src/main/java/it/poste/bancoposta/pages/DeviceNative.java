package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.CredentialAccessBean;
import io.appium.java_client.android.AndroidDriver;
import test.automation.core.UIUtils;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface DeviceNative extends AbstractPageManager
{

	public void clickOnAutorizzaAccessoNotification(String title);

	public void clickAllow();

	public void resumeApp(CredentialAccessBean access);

	public void aereoModeON();

	public void clickOnBackFromWifiSetting();

	public void aereoModeOFF();

	public void setWifiOFF();

	public void setWifiON();

	public void clickOnNumPadiOS(char c);

	public void clickOnFineFromKeyboard();

	public void controlloHeaderToogleBonifico(String title);

	public boolean controlloDescrizioneToogleBonifico();

	public void clickOnNotification();

}
