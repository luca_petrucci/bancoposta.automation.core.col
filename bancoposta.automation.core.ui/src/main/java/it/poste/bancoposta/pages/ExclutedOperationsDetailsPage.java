package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.w3c.dom.NodeList;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.TransactionDetailBean;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import test.automation.core.cmd.adb.AdbCommandPrompt;
import test.automation.core.cmd.adb.AdbKeyEvent;
import utils.Entry;
import utils.ObjectFinderLight;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.zookeeper.OpResult;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface ExclutedOperationsDetailsPage extends AbstractPageManager
{
	public void checkTransactionIsPresent() throws Exception;

	public boolean findTransaction() throws Exception;
	
	public ExclutedOperationsPage clickBack();

	public List<String[]> readSpeseDettaglio();

	public void readAndCheckCsv(List<String[]> speseDettaglioList) throws Exception;

	public void esportaCsv();

	public void clickRightMenu();

	public void removeCsv();

	public void setBean(TransactionDetailBean bean);
}
