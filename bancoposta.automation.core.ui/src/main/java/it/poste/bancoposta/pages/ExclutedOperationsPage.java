package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.TransactionDetailBean;
import test.automation.core.UIUtils;
import utils.Entry;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface ExclutedOperationsPage extends AbstractPageManager
{
	public ExclutedOperationsDetailsPage gotoSubCategory();

	public YourExpencesDetailsPage clickBack();

	public void setBean(TransactionDetailBean bean);
}
