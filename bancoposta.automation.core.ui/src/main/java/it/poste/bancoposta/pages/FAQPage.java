package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface FAQPage extends AbstractPageManager
{
	public NumeriUtiliPage gotoNumeriUtili();

	public PosteChatPage clickOnChatWithUsButton();
	
	public void clickOnXButton();

	public NumeriUtiliPage clickOnContactUsButton();
	
	public void clickCancelButton();
}