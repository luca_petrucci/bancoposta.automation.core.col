package it.poste.bancoposta.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.AbstractPageManager;
import io.appium.java_client.AppiumDriver;
import test.automation.core.UIUtils;

public interface FingerPrintPopUp extends AbstractPageManager
{
	public void insertFingerPrint(String fingerId)throws Exception;
	
	public void clickCancelButton();
}