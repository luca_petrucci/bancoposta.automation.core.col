package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.GirofondoCreationBean;
import io.appium.java_client.AppiumDriver;
import test.automation.core.UIUtils;
import utils.Entry;

import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface GirofondoFormPage extends AbstractPageManager
{

	public OKOperationPage eseguiGirofondo(GirofondoCreationBean b);

	 void compilaForm(GirofondoCreationBean b);

	public void isEditable(GirofondoCreationBean bean);

	 void checkValues(GirofondoCreationBean b);

	public OKOperationPage eseguiGirofondo(GirofondoCreationBean bean, String campo, String value);

	public void clickTrasferisciSu();

	public void checkTrasferisciSu(String transferTo);

	public void clickAnnulla();
}
