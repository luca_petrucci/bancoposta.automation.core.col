package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.w3c.dom.NodeList;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.BiscottoDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import test.automation.core.MobileUtils;
import test.automation.core.UIUtils;
import utils.Entry;
import utils.ObjectFinderLight;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface HomePage extends AbstractPageManager
{
	public void setUserHeader(String userHeader);

	public HomepageHamburgerMenu openHamburgerMenu();

	public void scrollToBuonieLibrettiSection();

	public BuoniLibrettiPage gotoBuoniELibrettiPage();

	public FAQPage gotoFaq();

	public void clickOnOperazioniVeloci();

	public QuickOperationListPage gotoOperazioniVeloci();

	public YourExpencesPage gotoLeTueSpese();

	public void navigaVersoIlBiscotto(BiscottoDataBean b);

	public void clickOnScopriDaBiscotto();

	public void checkContoInApertura();

	public void clickOnContoInApertura();



}
