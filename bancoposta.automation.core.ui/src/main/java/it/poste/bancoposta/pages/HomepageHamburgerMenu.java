package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.xml.sax.Locator;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.CredentialAccessBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import test.automation.core.UIUtils;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface HomepageHamburgerMenu extends AbstractPageManager
{
	public SettingsPage gotoSettings();

	public HomePage gotoHomePage(String userHeader);

	public ContiCartePage gotoContiCarte();

	public SalvadanaioPage goToSalvadanaio();

	public BachecaMessaggiPage gotoBachecaSectionMessaggi();

	public ScontiPostePage goToSales();

	public SearchPostalOfficePage goToSearchPostalOfficePage();
	
	public BuoniLibrettiPage goToBuoniLibretti();

	public void verifyMenu(List<CredentialAccessBean> bean) throws Throwable;

	public void logout();

	public void clickOnApriunContolibretto();

}
