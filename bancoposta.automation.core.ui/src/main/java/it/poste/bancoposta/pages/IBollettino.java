package it.poste.bancoposta.pages;

import bean.datatable.BollettinoDataBean;

public interface IBollettino 
{

	public void checkData(BollettinoDataBean b);

	public void submit(BollettinoDataBean b);

}
