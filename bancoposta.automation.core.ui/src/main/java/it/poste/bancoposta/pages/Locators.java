package it.poste.bancoposta.pages;

import automation.core.ui.uiobject.UiObjectElementLocator;

public class Locators
{
	public enum SalvadanaioCreateObjRiepilogoMolecola implements UiObjectElementLocator
	{
		HEADER,
		LEFTBTN,
		CATEGORIALABEL,
		LIBRETTOASSOCIATOLABEL,
		PERIODOLABEL,
		IMPORTOLABEL,
		NOMEOBIETTIVOLABEL,
		CATEGORIATEXT,
		LIBRETTOTEXT,
		PERIODOTEXT,
		IMPORTOTEXT,
		NOMETEXT,
		DOCUMENTOINFORMATIVOLINK,
		CONFERMA,
		POPUPERROREIMPORTOMAX
		;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}
	
	public enum SalvadanaioCreateObjNomeMolecola implements UiObjectElementLocator
	{
		HEADER,
		LEFTBTN,
		PERSONALIZZAOBIETTIVO,
		NOMEOBIETTIVO,
		CATEGORYCONTAINER,
		CATEGORYELEMENT,
		CARDSALVADANAIO,
		AVANTI,
		;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}
	
	public enum SalvadanaioCreateObjDataMolecola implements UiObjectElementLocator
	{
		HEADER,
		LEFTBTN,
		DURATAOBIETTIVO,
		EDITDURATA,
		DESCRIPTIONDURATA,
		CARDSALVADANAIO,
		AVANTIBTN,
		DATASELECTOK,
		TITLE,
		CURRENTDATE
		;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}
	
	public enum SalvadanaioCreateObjImportoMolecola implements UiObjectElementLocator
	{
		HEADER,
		BACKBUTTON,
		IMPORTOOBIETTIVO,
		DESCRIPTION,
		MENOBTN,
		PIUBTN,
		IMPORTO,
		CARDINFORMATIVA,
		AVANTIBTN,
		;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}
	
	public enum SelectBonificoTypeMolecola implements UiObjectElementLocator
	{
		DESCRIPTION,
		BONIFICOPOSTAGIROITEM,
		BONIFICOINSTANTANEOITEM,
		BONIFICOSEPATITLE,
		BONIFICOISTANTANEOTITLE,
		BONIFICOSEPADESCRIPTION,
		BONIFICOISTANTANEODESCRIPTION
		;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}
	
	public enum SummaryTargetPageMolecola implements UiObjectElementLocator
	{
		HELPBUTTON,HEADERTARGET,CANCELBUTTON,SUMMARYTARGETTITLE, AMOUNTCONTENT, AMOUNTTARGETNAME, SUMMARYTARGETBUTTON
		;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}
	public enum ObjectiveCreatedMolecola implements UiObjectElementLocator
	{
		OBJECTIVECREATEDTITLE,OBJECTIVEDESCRIPTION,PAYBUTTON,NOTNOWBUTTON
		;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}
	
	public enum ModifyObjectiveMolecola implements UiObjectElementLocator
	{
		MODIFYPAGETITLE, CANCEL, EDITTEXTOBJECTIVE, CATEGORYFIELD, CATEGORYELEMENT, DATEMONTH, SELECTDATE, NEXTARROW, OKBUTTON, PLUSBUTTON, SELECTEDAMOUNT, MODIFYCONFIRMBUTTON, CLOSEBUTTON
		;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}
	
	public enum EnableYourProductMolecola implements UiObjectElementLocator
	{
		ENABLEPRODUCTHEADER,CLOSEBUTTON,DEVICEIMAGE,DESCRIPTIONTITLE,DESCRIPTION,DISCOVERLINK,STARTBUTTON, BTNNONORA,
		NONHAIACCESSODISPOSITIVOBUTTON, NONHAIACCESSODISPOSITIVOBUTTON2, CONTINUABUTTONPOPUP, CONFERMABUTTON, 
		INIZIABUTTONONBOARDING, MESEANNOTEXTFIELD,
		CONTINUABUTTONCARD, CVVINSERTFIELD, CONFERMABUTTONCVV, CODICECONTO1, CODICECONTO2, RICHIEDISMSBUTTON,
		CONFERMABUTTONSMS, CREAPOSTEIDBUTTON, SCEGLIPOSTEIDTEXTFIELD, CONFERMAPOSTEIDTEXTFIELD, CONFERMAPOSTEIDBUTTON, 
		PROSEGUIATTIVAZIONEBUTTON, INIZIABUTTONONBOARDING2, IMAGENOONBOARDING;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}
	
	public enum AmountTargetPageMolecola implements UiObjectElementLocator
	{
		AMOUNTTARGETTITLE,CANCEL,SELECTEDAMOUNT,AMOUNTTARGETBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}
	
	public enum AccessAuthorizationPageMolecola implements UiObjectElementLocator
	{
		HEADER,FINGERPRINTHEADER,INFOAPPHEADER,TOGGLE,FINGERTOGGLE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum AccountDetailPageMolecola implements UiObjectElementLocator
	{
		AUTOMATICACTIVERECHARGEBUTTON,AUTOMATICRECHARGERESULT,DELETEBUTTON,POPUPOKBUTTON,RICARICABUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BPOLAppMolecola implements UiObjectElementLocator
	{
		AREAPERSONALE,USERNAME,PASSWORD,ACCEDI,COOKIEBUTTON,SERVIZIONLINE,BANCOPOSTAONLINELINK,ABILITAHEADER,AUTORIZZABPRADIO,PROSEGUI,MODALPUSHHEADER,COOKIEBP,BPHPHEADER,CONTOECARTELINK,CONTOECARTEHEADER,INVIO_DENARO,POSTAGIRO_DIV,POSTAGIRO_IBAN,POSTAGIRO_INTESTAZIONE,POSTAGIRO_IMPORTO,POSTAGIRO_CAUSALE,POSTAGIRO_CONTINUA,POSTAGIRO_PROSEGUI,COOKIEBUTTON2,CHIUDIMODALE,OPERAZIONEOK,CONTOECARTEFINANZIAMENTI;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BPOnlineNotificheMolecolas implements UiObjectElementLocator
	{
		TITLE,AUTORIZZA,NEGA,SPESE,CHIUDI,NOTIFICA_PAGACON,NOTIFICA_IMPORTO,NOTIFICA_COMMISSIONI,NOTIFICA_OPERAZIONE,NOTIFICA_BENEFICIARIO,NOTIFICA_DATA;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BachecaMessaggiDetailsPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,MESSAGETITLE,MESSAGETITLE2,MESSAGECATEGORY,MESSAGEDATE,MESSAGEHOUR,MESSAGEDELETEBUTTON,MESSAGE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BachecaMessaggiPageMolecola implements UiObjectElementLocator
	{
		HEADER,LEFTMENU,MESSAGGITAB,NOTIFICHETAB,SEARCHMESSAGES,FILTERBUTTON,MESSAGELIST,MESSAGEELEMENT,MESSAGETYPE,MESSAGETITLE,MESSAGEDATE,MESSAGENOTREADFLAG,RICERCATEXTFIELD,STARTDATEBUTTON,POPUPDATEOKBUTTON,ENDDATEBUTTON,FILTERCONFIRMBUTTON,MESSAGEDELETEBUTTON, ERRORPOPUPIOS;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BachecaNotifichePageMolecola implements UiObjectElementLocator
	{
		HEADER,LEFTMENU,MESSAGGITAB,NOTIFICHETAB,NOTIFICHELIST,NOTIFICHEELEMENT,SEARCH,NOTIFICATITLE,NOTIFICAMOVEMENT,NOTIFICADATE,NOTIFICAAMOUNT;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BiscottoHomeMolecola implements UiObjectElementLocator
	{
		TESTO,SCOPRI_BUTTON,PAGE_INDICATOR_PIN,BISCOTTO_IMAGE, cruscottoBiscotti;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum Bollettino896FormPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELLBUTTON,BACKBUTTON,BOLLETTINOTYPEINFO,CHANGEBOLLETTINOTYPE,PAYMENTTYPEINFO,CHANGEPAYMENTTYPE,CODEINPUT,CCPINPUT,AMOUNT,NAMEINPUT,SURNAMEINPUT,ADDRESSINPUT,CITYINPUT,CAPINPUT,PROVINCEINPUT,SAVEPERSON,EXITFROMPAYMENT,CONFIRM;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BollettinoBiancoFormPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELLBUTTON,BOLLETTINOTYPEINFO,CHANGEBOLLETTINOTYPE,PAYMENTTYPEINFO,CHANGEPAYMENTTYPE,OWNERINPUT,CCINPUT,ACCOUNTHEADERINPUT,SAVEACCOUNTHEADER,AMOUNT,DESCRITPION,CASUALEINPUT,NAMEINPUT,SURNAMEINPUT,ADDRESSINPUT,CITYINPUT,CAPINPUT,PROVINCEINPUT,SAVEPERSON,CONFIRM,POPUPCONTINUABUTTON, CANCELPAYMENT;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BollettinoChooseOperationPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELLBUTTON,HEADERIMAGE,TITLE,QRCODEBUTTON,FORMBUTTON,CLOSEBUTTON,CONTINUABUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BollettinoConfirmPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,TITLE,DESCRIPTION,PAYWITHINFO,BOLLETTINOCODEINFO,CCINFO,OWNERINFO,CAUSALE,AMOUNT,COMMISSIONE,EXECUTORINFO,CONFIRM,CONFIRMPOSTEID,CHIUDIBUTTON, EXECUTORINFOPARTE2;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BollettinoPAFormPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELLBUTTON,BOLLETTINOTYPEINFO,CHANGEBOLLETTINOTYPE,PAYMENTTYPEINFO,CHANGEPAYMENTTYPE,CCENTEINPUT,AVVCODEINPUT,DELETEPERSONBUTTON,NAMEPERINPUT,CODFISCINPUT,CONFIRM;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BolloAutoConfirmPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,TOTALEDAPAGARE,ASSISTENZABUTTON,TOTALEIMPORTO,TARGA,VEICOLO,PAGACON,CONFERMABUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BolloAutoPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,ASSISTENZABUTTON,REGIONBUTTON,LICENSEPLATEBUTTON,PREGRESSABUTTON,VEHICLEBUTTON,MONTHBUTTON,YEARBUTTON,VALIDITYMONTHBUTTON,CALCULATEBUTTON,QUADRICICLOBUTTON,CAMPANIABUTTON,MONTPICKERBUTTON,YEARPICKERBUTTON,VALIDITYMONTHSPICKERBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BolloAutoRiepilogoPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,COMMISSIONE,ASSISTENZABUTTON,IMPORTO,TARGA,REGIONE,PAGACON,CONFERMABUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BonificoConfirmPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,TITLE,DESCRIPTION,PAYWITHINFO,IBANINFO,BONIFICOOWNERINFO,COUNTRYOWNERINFO,AMOUNT,COMMISSIONE,CASUALINFO,ADDRESSSENDER,CITYSENDER,SENDER,SENDEREFFECTIVE,CONFIRM,MODALOK,MODIFY;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BuoniLibrettiPageMolecola implements UiObjectElementLocator
	{
		HEADER,LISTATABLIBRETTI,RIGHTNAVIGATIONBUTTON,SALDODISPONIBILELIBRETTO,BOOKLETTAB,VAUCHERTAB,GESTISCIBUTTON,SEARCH,FILTERBUTTON,MOVEMENTLIST,MOVEMENTELEMENT,MOVEMENTDESCRIPTION,MOVEMENTAMOUNT,MOVEMENTDATE,CARDDIV,CARDTITLE,CARDNUMBER,CARDAMOUNT,CONTINUA_TO_SCELTA_BUONO;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BuoniLibrettiSubMenuPageMolecola implements UiObjectElementLocator
	{
		CLOSEBUTTON,BUONIBUTTON,SUPERSMARTBUTTON,GIROFONDOBUTTON,CHARGEYOURPOSTEPAYBUTTON,BONIFICOBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BuoniPostaliPageMolecola implements UiObjectElementLocator
	{
		CONTINUABUTTON,HEADER,MODALLINK,GESTISCIBUTTON,CARICALATUAPOSTEPAYBUTTON,SUBSCRIBEVOUCHERBUTTON,SUBSCRIBEVOUCHERBUTTON_EMPTY,BUONOORDINARIOBUTTON,BUONOFRUTTIFEROPOSTALEBFP3X4BUTTON,BFP4X4BUTTON,BFP3X2BUTTON,CHANGESUBSCRIPTIONBUTTON,AMOUNT,PLUSBUTTON,MINUSBUTTON,CONFIRMBUTTON,SUBSCRIBEVOUCHERBUTTON2,BUONO_BUTTON,CONDIZIONI_GENERALI,SOTTOSCRIVIBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum CloseObjectivePageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCEL,HELP,CLOSEOBJECTIVEPAGEDESCRIPTION,CLOSEOBJECTIVEPAGEDESCRIPTION2,DESCRIPTINCONTAINER,PRODUCTTYPE,PAN,CLOSEOBJECTAMOUNTAVAILABLE,CLOSEOBJECTAMOUNTLABEL;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum ContiCartePageMolecola implements UiObjectElementLocator
	{
		HEADER,MODIFICACONTRATTOHOCAPITOBUTTON,ACCOUNTDETAILBUTTON,TOGGLEACCOUNT,LEFTMENU,TABCONTI,CONTOSETTINGSBUTTON,CONTOTABELEMENT,CONTOTYPE,CONTONUMBER,CARTANUMBER,SALDODISPONIBILE,SALDODISPONIBILECARTA,SALDOCONTABILE,RICEVIBUTTON,PAGABUTTON,PAGABUTTONCARTA,RIGHTNAVIGATIONBUTTON,LEFTNAVIGATIONBUTTON,SEARCH,FILTERS,LISTAMOVIMENTI,MOVIMENTIELEMENT,MOVMENTTYPE,MOVMENTDESCRIPTION,MOVMENTDATE,MOVMENTAMOUNT,RICERCATRANSAZIONI,TABCARTA,RECORDSMOVIMENTI,RECORDSMOVIMENTI2,USCITENONCONTABILIZZATEBUTTON,TOGGLEONLINE,CARDPOSTEPAY,CONTODIV, MOVIMENTO_DATA, MOVIMENTO_IMPORTO, MOVIMENTO_TITOLO, TOOGLEINSTAPAY;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum ContoVolSubPageMolecola implements UiObjectElementLocator
	{
		CONTAINER,TAB_VIDEO,PRODOTTI_CONTAINER,PRODOTTI_OFFERTI,PRODOTTO_IMAGE,PRODOTTO_HEADER,PRODOTTO_SCOPRI_DI_PIU,INTERA_GAMMA_LINK,RICHIEDI_BUTTON,PRODOTTI_OFFERTI_TITLE, ERRORPOPUPIOS;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum DaContabilizzarePageMolecola implements UiObjectElementLocator
	{
		HEADER,THREEPOINTSMENU,BACKBUTTON,TOTALAMOUNT,TOTALAMOUNTLABELTEXT,TOTALAMOUNTMONTH,PAYMENTLIST,AMOUNTSINGLEPAYMENT;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum DettaglioMovimentoMolecola implements UiObjectElementLocator
	{
		TOP,CLOSE_BUTTON,DETTAGLIO_DIV,DETTAGLIO_DESCR,DETTAGLIO_DATA,DETTAGLIO_AMOUNT;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum DeviceNativeElementMolecola implements UiObjectElementLocator
	{
		RESUME_CARD,NOTIFICATION_ELEMENT,NOTIFICATION_TITLE,NOTIFICATION_DESCR,ALLOW_BUTTON,OK_BUTTON, AIR_MODE_TOGGLE, WIFI_LABEL, WIFI_TOGGLE, WIFI_BACK_PAGE, IOSPAD, FINEKEYBOARD, DESCRIZIONETOOGLEATTIVO;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum EditOperationPageMolecola implements UiObjectElementLocator
	{
		HEADER,LEFTMENU,SEARCHINPUT,CATEGORYTAB,CATEGORIESLIST,CATEGORYCURRENT,CATEGORYELEMENT,CATEGORYSELECTEDFLAG;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum ExclutedOperationsDetailsPageMolecola implements UiObjectElementLocator
	{
		HEADER,LEFTMENU,RIGHTMENU,FROMDATE,TODATE,FILTERBUTTON,TOTALAMOUNT,OPERATIONELEMENT,OPERATIONTITLE,OPERATIONDESCRIPTION,OPERATIONDATE,OPERATIONAMOUNTVAL, ESPORTA_CSV, ALLOWPOPUPBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum ExclutedOperationsDetailsViewPageMolecola implements UiObjectElementLocator
	{
		HEADER,LEFTMENU,CATEGORYHEADER,EDITBUTTON,MENUBUTTON,CATEGORYTAG,OPERATIONTITLE,OPERATIONDETAILS,OPERATIONAMMOUNT,DATAVALUTA,DATACONTABILE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum ExclutedOperationsPageMolecola implements UiObjectElementLocator
	{
		HEADER,LEFTMENU,RIGHTMENU,TOTALAMOUNT,MONTHLIST,MONTHELEMENT,OPERATIONLIST,OPERATIONTITLE,OPERATIONELEMENT;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum FAQPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELLBUTTON,HELPSUBSECTION,CONTACTBUTTON,CHATWITHUSBUTTON,APPUFFICIOPOSTALELINK,APPPOSTEPAYLINK;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum FingerPrintPopUpMolecola implements UiObjectElementLocator
	{
		HEADER,DESCRIPTION,ICON,INFOMESSAGE,CANCELBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum GirofondoFormPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELLBUTTON,EXITPAYMENTSNOBUTTON,EXITPAYMENTSYESBUTTON,PAYMENTTYPEINFO,PAYMENTEDITBUTTON,TOCARDINPUT,AMOUNT,CONFIRM,CARDLIST,CARDINFO,CARDSELECTEDFLAG, POP_UP;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum GirofondoSubmitPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELLBUTTON,TITLE,DESCRIPTION,PAYMENTINFO,TOCARDINFO,AMOUNT,CONFIRM,POP_UP,MODIFY,COMMISSIONE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum HomePageMolecola implements UiObjectElementLocator
	{
		WELCOMEHEADER,BANCOPOSTAHEADER,LEFTMENU,YOURPRODUCTTAB,YOURBILLSTABS,QUICKPAIMENTSTAB,QUICKPAIMENTSSHOWALLLINK,QUICKPAIMENTSLIST,QUICKPAIMENTSELEMENT,BILLANDCARDSTAB,BILLANDCARDSSHOWACCOUNTSLINK,BILLANDCARDSLIST,BILLANDCARDELEMENT,BUONIELIBRETTIELEMENT,BUONIELIBRETTIBUTTONLINK,POPUPACCETTOBUTTON,SALESPOSTE,FAQLINK,CONTO_IN_APERTURA;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum HomepageHamburgerMenuCheck implements UiObjectElementLocator
	{
		USER,CLOSEMENU,HOME,ACCOUNTANDCARDS,VOUCHERANDBOOKLET,MONEYBOX,NOTICEBOARD,CONSULENTE,SETTINGS,BPDISCOUNT,POSTOFFICESEARCH,OPENACCOUNT,INVITEYOURFRIENDS,OTHERAPPS,BPOFFICE,PPOFFICE,LOGOUT,FOOTER;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum HomepageHamburgerMenuMolecola implements UiObjectElementLocator
	{
		WELCOMEHEADER,HOME,ACCOUNTANDCARDS,MONEYBOX,VOUCHERANDBOOKLET,NOTICEBOARD,SETTINGS,BPDISCOUNT,EXTRADISCOUNT,POSTOFFICESEARCH,INVITEYOURFRIENDS,BUONILIBRETTIBUTTON,CLOSEHAMBURGERMENU,LOGOUT,SETTINGSMENU, SALESPOSTE, VOLLINK;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum IlTuoAndamentoMolecola implements UiObjectElementLocator
	{
		TOP,CLOSE_BUTTON,USCITE_TAB,ENTRATE_TAB,DIFFERENZA_TAB,RISPETTO_ALLA_MEDIA,HAI_SPESO_TXT,TOTALE_SPESO_TXT,GRAFO,FUMETTO,MESE,MEDIA_GRAFO,MAX_EURO_GRAFO,MIN_EURO_GRAFO,SIDEBAR_GRAFO,RIGHT_BUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum InsertPosteIDPage2Molecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,TITLE,DESCRIPTION,POSTEIDINPUT,NON_SEI_TU,CONFIRM;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum InsertPosteIDPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,TITLE,DESCRIPTION,POSTEIDINPUT,NON_SEI_TU,CONFIRM;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum LibrettoVolSubPageMolecola implements UiObjectElementLocator
	{
		CONTAINER,PRODOTTI_CONTAINER,PRODOTTI_OFFERTI,PRODOTTO_IMAGE,PRODOTTO_HEADER,PRODOTTO_SCOPRI_DI_PIU,INTERA_GAMMA_LINK,PRODOTTI_OFFERTI_TITLE,INTERA_GAMMA;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum LoginPosteIdPageMolecola implements UiObjectElementLocator
	{
		EMAIL,PASSWORD,SHOWMEPASSWORD,REMEMBERME,LOSTCREDENTIAL,SIGNINBUTTON,REGISTERBUTTON,HEADER,BACKBUTTON,TABPOSTEID,POSTEIDIMAGE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum LoginPosteItPageMolecola implements UiObjectElementLocator
	{
		USERNAME,PASSWORD,SHOWMEPASSWORD,REMEMBERME,LOSTCREDENTIAL,SIGNINBUTTON,REGISTERBUTTON,HEADER,BACKBUTTON,TABPOSTEIT,SPIDOKBUTTON,SPIDNOACCOUNTBUTTON,REGISTERACCOUNTBYSITE,REGISTERSITECLOSEBUTTON,DIMENTICATOCREDENZIALI,ERRORPOPUPTXT,RIPROVAERRORPOPUPBUTTON,RECUPERAERRORPOPUPBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum LoginWithPosteidPageMolecola implements UiObjectElementLocator
	{
		HEADER,POSTEIDCODE,REMENBERMELINK,CONFIRM,GOTOLOGINPAGE,OWNERTEXT;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum MAVFormPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELLBUTTON,BOLLETTINOTYPEINFO,CHANGEBOLLETTINOTYPE,PAYMENTTYPEINFO,CHANGEPAYMENTTYPE,MAVCODEINPUT,CCINPUT,AMOUNT,REFERENCE,DATEINPUT,NAMEINPUT,SURNAMEINPUT,ADDRESSINPUT,CITYINPUT,CAPINPUT,PROVINCEINPUT,SAVEPERSON,CONFIRM;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum MovementDetailPage implements UiObjectElementLocator
	{
		MOVEMENTHEADER,IMPORTMOVEMENT,MOVEMENTDATE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum NewLoginNotifichePageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum NewLoginPreLoginPageMolecola implements UiObjectElementLocator
	{
		ACCEDIBUTTONPRELOGIN,SIACCETTOPOPUPBUTTON,ALLOWPOPUPBUTTON,HEADER,OWNERNAMETEXT,ACCEDIBUTTON,NONSEITUBUTTON,SITOWEBTEXT,CAMERAICON,SITOWEBBUTTON,AUTORIZZAZIONITEXT,BELLICON,AUTORIZZAZIONIBUTTON,ENABLEPRODUCTHEADER,CLOSEBUTTON,DEVICEIMAGE,DESCRIPTIONTITLE,DESCRIPTION,DISCOVERLINK,STARTBUTTON,OKPOPUPBUTTON,SCOPRI_PRODOTTI,SIACCETTONOTIFICHEPOPUPBUTTON,POPUPNOTIFICHE,POUPNOTIFICHECHIUDI,ACCEDIPRELOGINPAGE, NOTIFICHEPOPUPELEMENT,NONONACCETTOPOPUPBUTTON,CERCAUFFICIOPRENOTATEXT,CERCAUFFICIOPRENOTAICON, TXTBENVENUTO,
		POPUPCONSENTIUNAVOLTA, POPUPCONSENTI, POPUPOKACCESSOCONTATTI;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum NewLoginQrPageMolecola implements UiObjectElementLocator
	{
		HEADER,BAACKBUTTON,QRTIPTEXT,QRSQUAREPOINTER;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum NewLoginQrPopupApprovePageMolecola implements UiObjectElementLocator
	{
		APPROVEBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum NewLoginScopriQrPageMolecola implements UiObjectElementLocator
	{
		SCOPRIQRTEXT,ICON,QREXPLANATIONTEXT,HOCAPITOBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum NumeriUtiliPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,CALLBANCOPOSTABUTTON,POSTAMATBUTTON,ALERTPOSTEPAYBUTTON,NUMERIUTILI,CLOSEMODAL;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum OKOperationPageMolecola implements UiObjectElementLocator
	{
		TRANSACTIONHEADER,TRANSACTIONDESCRIPTION,QUICKOPERATIONBUTTON,CLOSEBUTTON,OKICON,LINK,NOGRAZIE,NOTNOW;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum OnboardingChoosePageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELBUTTON,TITLE,ENABLEDIV,DISABLEDIV,ENABLEDIVTITLE,ENABLEDIVBUTTON,DISABLEDIVTITLE,DISABLEDIVBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum OnboardingDisableModalMolecola implements UiObjectElementLocator
	{
		HEADER,DESCRIPTION,CANCELBUTTON,CONTINUEBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum OnboardingPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELBUTTON,TITLE,DESCRIPTION,STARTBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum OnboardingSMSPageMolecola implements UiObjectElementLocator
	{
		HEADER,DESCRIPTION,CANCELBUTTON,OTPCODEINPUT,OTPRESENDLINK,HELPLINK,CONFIRMBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum PagaConSubMenuBuoniELibrettiMolecola implements UiObjectElementLocator
	{
		XCLOSEBUTTON,SOTTOSCRIZIONEBUONIBUTTON,ATTIVAOFFERTASUPERSMARTBUTTON,GIROFONDOBUONIELLIBRETTIBUTTON,RICARICALATUAPOSTEPAYBUTTON,RICEVIBONIFICIBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum PagaConSubMenuCartaPageMolecola implements UiObjectElementLocator
	{
		CLOSEBUTTON,RICARICAPOSTEPAYBUTTON,RICARICATELEFONICABUTTON,BONIFICOBUTTON,POSTAGIROBUTTON,BOLLETTINOBUTTON,BOLLOAUTOBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum PagaConSubMenuMolecola implements UiObjectElementLocator
	{
		CLOSEBUTTON,RICARICAPOSTEPAYBUTTON,RICARICATELEFONICABUTTON,BONIFICOBUTTON,POSTAGIROBUTTON,BOLLETTINOBUTTON,GIROFONDOBUTTON,BUONOBUTTON,RICEVIBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum PagoPAMolecola implements UiObjectElementLocator
	{
		TOP,CLOSE_BUTTON,INQUADRA_CODICE,COMPILA_MANUALMENTE,PAGA_CON,PAGA_CON_PULSANTE,BOLLETTINO_POSTALE_SWITCH,BANCHE_ALTRI_SWITCH,CODICE_AVVISO,CCN,RAGIONE_SOCIALE,CODICE_FISCALE,CALCOLA;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum PayWithPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELLBUTTON,OKKOBUTTON,TITLE,PRODUCTLIST,PRODUCTELEMENT,PRODUCTDETAILTYPE,PRODUCTAMOUNT,CARDELEMENT,CARDNUMBER,PRODUCTTYPE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum PostagiroConfirmPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,TITLE,DESCRIPTION,PAYWITHINFO,IBANINFO,OWNERINFO,AMOUNT,COMMISSIONE,CAUSALE,CONFIRM,MODALOK,MODIFY,OPERAZIONEESEGUITACHIUDIBUTTON,OPERAZIONEESEGUITAPOPUPCHIUDIBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum PosteChatPageMolecola implements UiObjectElementLocator
	{
		HEADER,TERMINATEBUTTON,CHATMESSAGE1,CHATMESSAGE2,PHONENUMBER,RESERVECALLBUTTON,ENDBUTTON,POPUPESCBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum PostePayDetailsMolecola implements UiObjectElementLocator
	{
		TOP,CLOSE_BUTTON,CARD_DIV,CARD_TYPE_IMG,CARD_CIRC_IMG,CARD_PAN,AGGIORNATO_AL,SALDO_DISPONIBILE,SALDO_CONTABILE,COORDINATE_BANC_DIV,COORDINATE_IBAN,COORDINATE_BANC_INVIA,SALVADANAIO_DIV,SALVADANAIO_VERSAMENTI_ATTIVI,SALVADANAIO_VERSAMENTI_ATTIVI_NUM,SALVADANAIO_VERSAMENTI_ATTIVI_LINK,SCONTIPOSTE_DIV,SCONTIPOSTE_SCONTI_ACCUMULATI,SCONTIPOSTE_SCONTI_LINK,BLOCCO_CARTA_DIV,BLOCCO_CARTA_SOSPENDI,MASSIMALI_DIV,MASSIMALI_POS_DIV,MASSIMALI_POS_GIORNO_MOD,MASSIMALI_POS_GIORNO_VAL,MASSIMALI_POS_MESE_MOD,MASSIMALI_POS_MESE_VAL,MASSIMALI_ATM_DIV,MASSIMALI_ATM_GIORNO_MOD,MASSIMALI_ATM_GIORNO_VAL,MASSIMALI_ATM_MESE_MOD,MASSIMALI_ATM_MESE_VAL,AREA_GEOGRAFICA_DIV,AERA_GEOGRAFICA_EUROPA,AREA_GEOGRAFICA_MONDO,ABILITA_CONTACTLES_DIV,ABILITA_CONTACTLES_SWITCH,ACQUISTI_ONLINE_DIV,ACQUISTI_ONLINE_SWITCH,ACQUISTI_ONLINE_SELEZIONE_CAT;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum PurchaseVolRequestMolecola implements UiObjectElementLocator
	{
		TOP,CLOSE_BUTTON,CONTENT_LIST,KIT_PRECONTRATTUALE,CONTINUA_BUTTON,ESCI_BUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum QuickOperationDeletePageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,SAVEBURTON,MOVEBUTTON,SELECTOPCHECKBOX,QUICKOPERATIONLIST,QUICKOPERATIONELEMENT,MODALHEADER,MODALCANCELLBUTTON,MODALCONFIRMBUTTON,QUICKOPERATIONELEMENTDESCRIPTION,DELETEBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum QuickOperationGirofondoDetailsFormMolecola implements UiObjectElementLocator
	{
		PAYWITHINFO,CARDTOINFO,AMOUNT;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum QuickOperationListPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,MODIFYBUTTON,QUICKOPERATIONLIST,QUICKOPERATIONELEMENT,QUICKOPERATIONELEMENTDESCRIPTION,QUICKOPERATIONPOPUPOKBUTTON,QUICKOPERATIONELEMENTNAME,POPUPCONTINUEBUTTON,QUICKOPERATIONELEMENTDIN;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum QuickOperationModifyPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,SAVEBURTON,MOVEBUTTON,SELECTOPCHECKBOX,QUICKOPERATIONLIST,QUICKOPERATIONELEMENT;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum RicaricaLaTuaPostepayChooseOperationPageMolecolas implements UiObjectElementLocator
	{
		HEADER,CANCELLBUTTON,HEADERIMAGE,TITLE,TITLEDESCRIPTION,INIZIABUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum RicaricaPostepayConfirmPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,TITLE,DESCRIPTION,PAYWITHINFO,CARDNUMBERINFO,CARDOWNERINFO,AMOUNT,COMMISSIONE,MODALOK,MODIFY,CONFIRM, HEADER_DESCRIPTION;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum RicaricaPostepayPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELLBUTTON,SETSOGLIA,SETTEMPO,SELECTRICO,BIWEEK,MINIMUMBALANCE,UNTILTO,CALDENDAROKBUTTON,AUTOMATICRECHARGENAME,PAYMENTTYPESUMMARY,CHANGEPAYMENTTYPEBUTTON,ADDRESSBOOKLINK,CARDNUMBERINPUT,ACCOUNTHOLDERINPUT,SAVEONADRESSBOOK,AMOUNT,CAUSALEBUTTON,RICARICAAUTOMATICABUTTON,OKBUTTON,CANCELLABUTTON,CONFIRM,NUMEROCARTA, COMMISSIONE, TITLEPOPUPRICARICAAUTO,DESCRIZIONEPOPUPRICARICAAUTO,BTNPOPUPRICARICAAUTO, INIZIABUTTON, MESEANNOTEXTFIELD, CONTINUABUTTON, CVVINSERTFIELD, CONFERMABUTTONCVV, CONFERMABUTTONABILITAPRODOTTO, CONFERMAIDBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum RicaricaSubMenuMolecola implements UiObjectElementLocator
	{
		CLOSEBUTTON,RICARICAUFFICIOBUTTON,RICARICAPOSTEPAYBUTTON,IBANBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum RicaricaTelefonicaConfirmPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,TITLE,DESCRIPTION,PAYWITHINFO,PHONENUMBERINFO,PHONECOMPANYINFO,AMOUNT,CONFIRM;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum RicaricaTelefonicaPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELLBUTTON,PAYMENTTYPESUMMARY,CHANGEPAYMENTTYPEBUTTON,ADDRESSBOOKLINK,PHONENUMBERINPUT,PHONECOMPAIGNINPUT,AMOUNT,OKBUTTON,CONFIRM,AMOUNT_VALUE, OPERATORE_VALUE, NUMEROTELEFONORUBRICA, POPUPCONSENTIBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum RicaricaTelefonicaSelezionaOperatorePageMolecola implements UiObjectElementLocator
	{
		POSTEMOBILIBUTTON,VODAFONEBUTTON,WINDBUTTON,TREBUTTON,TIMBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum RiceviSubMenuCartaPageMolecolas implements UiObjectElementLocator
	{
		CANCELBUTTON,RICARICAINUFFICIOPOSTALEBUTTON,RICARICAQUESTAPOSTEPAYBUTTON,CONDIVIDIIBANBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SalesPostePageMolecola implements UiObjectElementLocator
	{
		HEADER,LEFTMENU,SEARCHBAR,NEARMETAB,ONLINETAB,MAP,NOPOPUPBUTTON,NOGRAZIEPOPUPBUTTON,SHOPONMAPSTRUE,SHOPNAMEDISPLAYED,SHOPDISCOUNTDISPLAYED;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SalvadanaioConfirmPageMolecola implements UiObjectElementLocator
	{
		HEADER,ANNULLABUTTON,ASSISTENZABUTTON,TUTTOCORRETTOTEXT2,SUBDESCRIPTION,TUTTOCORRETTOTEXT,VERSACONCARDNUMBER,VERSACONCARDNUMBER2,OWNER,OWNER2,SALVADANAIONAMEQUICKOPERATION,MODIFYBUTTONQUICKOPERATION,AMOUNT,AMOUNT2,FREQUENCY,STARTDATE,RAGGIUNGIMENTO,CONFIRMBUTTON,CONFIRMBUTTON2,INFORMATIONICON,INFORMATIONMESSAGE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SalvadanaioDepositArrotondamentoPage implements UiObjectElementLocator
	{
		HEADER,LEFT,ARROTONDAMENTO_SWITCH,ARROTONDAMENTO_SEMPLICE,ARROTONDAMENTO_DINAMICO,CONFERMA,VERSA_DA,EDIT_STRUMENTO_PAGAMENTO,REGOLA_ARROTONDAMENTO,EDIT_REGOLA_ARROTONDAMENTO,ARROTONDA_X2,ARROTONDA_X3,ARROTONDA_X4,ARROTONDA_X5;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SalvadanaioDepositPageMolecola implements UiObjectElementLocator
	{
		HEADER,ANNULLABUTTON,ASSISTENZABUTTON,DOMENICAPOPUPOKBUTTON,DOMENICAPOPUPTEXT,DOMENICAPOPUPAVVISOTEXT,PAYWITHCARDNUMBER,AMOUNT,TOGGLE_VERSAMENTO_RICORRENTE,VERSAMENTORICORRENTETRIGGER,FREQUENCYLABEL,STARTDATE,RAGGIUNGIMENTOBUTTON,DATASPECIFICABUTTON,DATASPECIFICATEXTLABEL,DATESELECTION,RAGGIUNGIMENTOTEXTLABEL,TIMEFORENDOBJECTIVETEXTLABEL,SUGGESTIONTEXTLABEL,EVERYDAYPICKEROPTION,EVERYSEVENDAYSPICKEROPTION,EVERYFIFTEENDAYSPICKEROPTION,EVERYTHIRTYDAYSPICKEROPTION,DATEPOPUPOKBUTTON,DATEPOPUPANNULLABUTTON,NEXTMONTHPOPUPARROW,CONFIRMBUTTON,POPUPTEXTMESSAGE,POPUPDELETEBUTTON,POPUPDOMENICAOKBUTTON,DELETEBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SalvadanaioGestisciVersamentoRicorrentePageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,ASSISTENZABUTTON,PAYWITH,AMOUNT,FREQUENCY,STARTDATE,ENDDATE,RICORRENZAATTIVA,PROSSIMOVERSAMENTO,MODIFICAELIMINABUTTON,MODIFICA_ELIMINA_AUTOMATISMO,ELIMINA_AUTOMATISMO,OK_POPUP;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SalvadanaioInfoPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,ASSISTENZABUTTON,SHAREBUTTON,CRONOMETROICONINFO,THREEPOINTSMENUBUTTON,SALVADANAIO_DETTAGLIO_RICORRENTE,GESTISCIVERSAMENTORICORRENTEBUTTON,TITLEDETAIL,CURRENTAMOUNT,TOTALAMOUNT,TEXTLABELRISPARMIO,VERSAMENTIBUTTON,CREATIONDATE,EXPIRATIONDATE,SUGGESTIONLABELTEXT,VERSABUTTON,CLOSEOBJECTIVEBUTTON,CLOSEOBJECTIVEOKBUTTON,EVOLUTIONCARD,EVOLUTIONCARDTWO,OWNER,IMPORT,CONFIRMCLOSEBUTTON, CLOSEBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SalvadanaioPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,CANCEL,OBJECTIVECREATEDTITLE,OBJECTIVEDESCRIPTION,CRONOMETROICON,PAYBUTTON,NOTNOWBUTTON,HELPBUTTON,LABELPAGE,OBJECTIVESCONTAINER,DESCRIPTION,MENUBUTTON,FIRSTAMOUNT,SECONDAMOUNT,EXPIREDDATE,ARROWBUTTON,CREATENEWTARGETBUTTON,HEADERTARGET,CANCELBUTTON,DESCRIPTIONPAGE,CATEGORYIMAGEICON,CATEGORYNAME,DURATIONDESCRIPTIONPAGE,THREEMONTHSFIELD,SIXMONTHSFIELD,NINEMONTHSFIELD,DATESELECTIONFIELD,CONFIRMBUTTON,AMOUNTTARGETTITLE,CATEGORYIMAGEICONTWO,AMOUNTTARGETDESCRIPTION,LESSBUTTON,PLUSBUTTON,SELECTEDAMOUNT,CURRENCY,AMOUNTTARGETBUTTON,SUMMARYTARGETTITLE,SUMMARYCATEGORYLABEL,SUMMARYCATEGORYCONTENT,SUMMARYTARGETCATEGORYICON,PERIODLABEL,PERIODCONTENT,AMOUNTLABEL,AMOUNTCONTENT,AMOUNTTARGETNAME,GENERALCONDITION,SUMMARYTARGETBUTTON,OKBUTTONPOPUP,TEXTCONTENTPOPUP,MODIFYOBJECTIVE,MODIFYPAGETITLE,EDITTEXTOBJECTIVE,CATEGORYFIELD,EXPIREOBJECTIVEFIELD,MODIFYCONFIRMBUTTON,CATEGORYLIST,CATEGORYELEMENT,MENU,DATEMONTH,SELECTDATE,NEXTARROW,OKBUTTON,CLOSEBUTTON, LEFTMENU;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SalvadanaioSelectVersamentoPageMolecola implements UiObjectElementLocator
	{
		HEADER,LEFT,VERSAMENTO_SINGOLO,VERSAMENTO_RICORRENTE,VERSAMENTI_DIV,VERSAMENTO_ARROTONDAMENTO,VERSAMENTO_LETUESPESE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SalvadanaioVersamentiListPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,ASSISTENZABUTTON,DATE,VERSAMENTONAME,AMOUNT;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SaveToQuickOperationPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,DESCRIPTION,QUICKOPNAMEINPUT,CONFIRM,OVERRIDEQUICKOPERATIONYESBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SearchPostalOfficePageMolecola implements UiObjectElementLocator
	{
		POPUPOKBUTTON,HEADER,LISTBUTTON,MAPBUTTON,SEARCHBAR,POSTALOFFICEONMAPSTRUE,POSTALOFFICECITYNAMEDISPLAYED,POSTALOFFICEADDRESSDISPLAYED;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SelectBolletinoTypesPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELLBUTTON,TITLE,BOLLETTINOLIST,BOLLETTINOELEMENT,BOLLETTINOTITLE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SelectOperatoreModalMolecola implements UiObjectElementLocator
	{
		OPERATORILIST,OPERATOREELEMENT;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SelectProvinciaModalMolecola implements UiObjectElementLocator
	{
		PROVINCIELIST,PROVINCIAELEMENT;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SelezionaCartaPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,SEARCHBAR,SEARCHFILTERTEXT,HEADERCARTE,CARDELEMENT,PRODUCTDETAILTYPE,PRODUCTTYPETWO,PRODUCTTYPE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SendBonificoPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELLBUTTON,ESCBUTTON,PAYMENTTYPESUMMARY,CHANGEPAYMENTTYPEBUTTON,ADDRESSBOOKLINK,IBANINPUT,CCINPUT,COUNTRYINPUT,ACCOUNTHOLDERINPUT,SAVEACCOUNTCHECKBOX,AMOUNT,CASUAL,SHOWOTHERINFORMATIONS,ADDRESSINPUT,CITYINPUT,CITYINPUTMODIFY,REFERENCEINPUT,ACTUALREFERENCEINPUT,MODALCONFIRMBUTTON,CONFIRM,EXITMODALBUTTON,AMOUNTMODIFY, POPUPCONTINUABUTTON,LABELRUBRICA, ENABLEBONIFICOINSTANTANEO;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SendPostagiroPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELLBUTTON,PAYMENTTYPESUMMARY,CHANGEPAYMENTTYPEBUTTON,ADDRESSBOOKLINK,IBANINPUT,IBANINPUTMODIFY,ACCOUNTHOLDERINPUT,ACCOUNTHOLDERINPUTMODIFY,SAVEONADRESSBOOK,AMOUNT,AMOUNTMODIFY,CASUAL,CASUALMODIFY,CANCELLADATI,EXITMODALBUTTON,CONFIRM, IOSPAD;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SettingsNotificationEdit1PageMolecola implements UiObjectElementLocator
	{
		HEADER,LEFTMENU,SWITCHBUTTON,MINAMOUNT,EDITAMOUNT;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SettingsNotificationEdit2PageMolecola implements UiObjectElementLocator
	{
		HEADER,LEFTMENU,SWITCHBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SettingsNotificationMinAmountPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELBUTTON,SAVEBUTTON,AMOUNT,DECREASEAMOUNT,INCREASEAMOUNT;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SettingsNotificationPageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,NOTIFICATIONLIST,NOTIFICATIONELEMENT,NOTIFICATIONNAME,NOTIFICATIONSTATUS,SETPREFERITO;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SettingsPageMolecola implements UiObjectElementLocator
	{
		HEADER,LEFTMENU,ACCESSANDAUTH,POSTEIDCODE,NOTIFICATION,PRIVACYANDCONDITION,APPVERSION,DISABLEPRODUCTBUTTON,SETTINGSAUTH;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SubPageWelPageCarousel1Molecola implements UiObjectElementLocator
	{
		DESCRIPTION,CONTINUEBUTTON,DOTNAVIGATIONBAR,DOTNAVIGATIONELEMENT,TITLE,BANCOPOSTAIMAGE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SubPageWelPageCarousel2Molecola implements UiObjectElementLocator
	{
		TITLE,CONTINUEBUTTON,DOTNAVIGATIONBAR,DOTNAVIGATIONELEMENT,DESCRIPTION,BANCOPOSTAIMAGE,STEPIMAGE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SubPageWelPageCarousel3Molecola implements UiObjectElementLocator
	{
		TITLE,CONTINUEBUTTON,DOTNAVIGATIONBAR,DOTNAVIGATIONELEMENT,DESCRIPTION,BANCOPOSTAIMAGE,STEPIMAGE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SubPageWelPageCarousel4Molecola implements UiObjectElementLocator
	{
		TITLE,CONTINUEBUTTON,DOTNAVIGATIONBAR,DOTNAVIGATIONELEMENT,DESCRIPTION,BANCOPOSTAIMAGE,STEPIMAGE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SubPageWelPageCarousel5Molecola implements UiObjectElementLocator
	{
		TITLE,CONTINUEBUTTON,DOTNAVIGATIONBAR,DOTNAVIGATIONELEMENT,DESCRIPTION,BANCOPOSTAIMAGE,STEPIMAGE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SubPageWelPageCarousel6Molecola implements UiObjectElementLocator
	{
		TITLE,ACCESSTOAPPBUTTON,DOTNAVIGATIONBAR,DOTNAVIGATIONELEMENT,DESCRIPTION,BANCOPOSTAIMAGE,STEPIMAGE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SubscribeWithPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELBUTTON,CARDELEMENT,CARDELEMENTTYPE,CARDAMOUNT,CARDNUMBER,MESSAGEHEADER;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum SummaryPurchaseVolMolecola implements UiObjectElementLocator
	{
		TOP,CLOSE_BUTTON,VOL_TYPE_HEADER,DATI_ANAGRAFICI_DIV,PROFILO_FINANZIARIO_DIV,DATI_CC_DIV,FIRMA_CONTRATTI_DIV,RICONOSCIMENTO_DIV,ANNULLA_BUTTON,PROSEGUI_CANCELLAZIONE_BUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum TutorialPopupMolecola implements UiObjectElementLocator
	{
		TESTO,CLOSE_BUTTON,TUTORIAL_IMAGE,TUTORIAL_TEXT_HEADER,TUTORIAL_TEXT_DESCRIPTION,TUTORIAL_ACCESS_BUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum VersaSuObiettivoLeTueSpesePage implements UiObjectElementLocator
	{
		HEADER,LEFT,GESTISCILETUESPESE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum VetrinaVolEvolutaMolecola implements UiObjectElementLocator
	{
		TESTO,CLOSE_BUTTON,CONTO_TAB,LIBRETTO_TAB,BEARER;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum VolLibrettoSmartMolecola implements UiObjectElementLocator
	{
		TESTO,CLOSE_BUTTON,TESTO_CONTAINER,RICHIEDI_BUTTON,TESTO_VOL,SCOPRI_LINK;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum VolProductDetailsMolecola implements UiObjectElementLocator
	{
		TOP,CLOSE_BUTTON,IMAGE_HEADER,CONTENT_LIST,DOCUMENTO_FID_LINK,RICHIEDI_BUTTON,FID_DOC,INTERA_GAMMA;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum VoucherSelectAmoutPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELBUTTON,MODIFYBUTTON,CARDINFO,TITLE,AMOUNT,DECREASEBUTTON,INCREASEBUTTON,DESCRIPTION,AVAILABLEBALANCETXT,CONFIRMBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum VoucherSelectPageMolecola implements UiObjectElementLocator
	{
		HEADER,CANCELBUTTON,VOUCHERTYPEHEADER,VOUCHERTYPEHEADERDESCR,VOUCHERDIV,VOUCHERTITLE,VOUCHERTYPEBUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum VoucherSubscribePageMolecola implements UiObjectElementLocator
	{
		HEADER,BACKBUTTON,TITLE,DESCRIPTION,TYPEINFO,SUBSCRIBEWITHINFO,OWNERINFO,AMOUNTINFO,CHECKBOXINFO,SUBSCRIBE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum WiFiSettingsPageMolecola implements UiObjectElementLocator
	{
		TOGGLE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum YourExpencesDetailsPageMolecola implements UiObjectElementLocator
	{
		HEADER,LEFTMENU,RIGHTMENU,MONTHLIST,MONTHELEMENT,PIECHART,DETAILSLIST,DETAILSELEMENT,DETAILSTITLE,SEEEXCLUDEDMOVEMENTS,DETAILS_VALUE,DETAILS_PERCENTANCE,ESPORTA_CSV,salvaSuFileBtn,btnIPhone,btnSalvaCsv,btnSostituisciCsv,csvFile, csvTable, btnFine, labelCerca;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum YourExpencesPageMolecola implements UiObjectElementLocator
	{
		HEADER,LEFTMENU,YOURPRODUCTTAB,MONTHLIST,MONTHELEMENT,WIDGETCONTAINER,WIDGET,WIDGETHEADER,SEEYOURPROGRESS,SEEALLCATEGORIES,SEEYOURTHRESHOLDS,SETBUDGET,RIEPILOGO_HEADER,ENTRATE_HEADER,USCITE_HEADER,DIFFERENZA_HEADER,ENTRATE_VALUE,USCITE_VALUE,DIFFERENZA_VALUE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum YourExpencesWizardPageMolecola implements UiObjectElementLocator
	{
		SALTABUTTON,CONTINUABUTTON;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


	public enum BonificoConfirmPostePageMolecola implements UiObjectElementLocator
	{
		PAYWITHINFOPOSTE,IBANINFOPOSTE,BONIFICOOWNERINFOPOSTE,AMOUNTPOSTE,COMMISSIONEPOSTE,CASUALINFOPOSTE;
		@Override
		public String getMolecolaId() {
			return this.getClass().getSimpleName();
		}

		@Override
		public String getParticleId() 
		{
			return this.name();
		}}


}