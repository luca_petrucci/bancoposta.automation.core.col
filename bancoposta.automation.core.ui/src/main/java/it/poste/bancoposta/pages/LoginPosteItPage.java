package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.AbstractPageManager;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface LoginPosteItPage extends AbstractPageManager
{
	public void loginToTheApp(String username, String password);

	public void checkErrorPopup();

	public void clickRecuperaPopupErrorLogin();

	public void clickOnSpidButton();

	public void clickOnSignInButton();

	public void clickOnNoAccountPoste();

	public void checkRegistrationPage() throws Exception;
}
