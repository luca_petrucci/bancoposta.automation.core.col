package it.poste.bancoposta.pages;

import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.QuickOperationCreationBean;

public interface OKOperationPage extends AbstractPageManager{

	public void close();

	public void saveAsQuickOperation(QuickOperationCreationBean b) throws Exception;
 
	public boolean checkSaveOperationButton();
	
	public void checkGirofondoOKPage();
	
	public void closePage();

	public void check();

	public void clickNoGrazie();
	
	public void closeFeedback();
	
	public void checkRicaricaQuestaOKPage();

	public void saveQOperation(QuickOperationCreationBean quickOperationCreationBean);

	

}
