package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.AbstractPageManager;
import test.automation.core.UIUtils;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface PayWithPage extends AbstractPageManager
{
		public void clickOnConto(String payWith);

		public void clickBack();
		
		public RicaricaPostepayPage clickOnContodue(String payWith);

		public void clikOnOkButton();

}
