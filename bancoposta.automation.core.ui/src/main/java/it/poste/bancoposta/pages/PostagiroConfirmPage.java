package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.PosteIDSubAccessPage;
import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.BonificoDataBean;
import bean.datatable.PaymentCreationBean;
import bean.datatable.QuickOperationCreationBean;
import test.automation.core.UIUtils;

import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface PostagiroConfirmPage extends AbstractPageManager
{


	void check(String payWith,String iban,String owner,String description,String am);



	OKOperationPage submit(String posteid);


	public void clickOKModal();

	public void checkData(PaymentCreationBean b);

	public OKOperationPage submit(PaymentCreationBean bean);



	void setBean(BonificoDataBean bonificoDataBean);



	void setBean(PaymentCreationBean paymentCreationBean);



	void clikOnOperazioneEseguitaChiudi();
}
