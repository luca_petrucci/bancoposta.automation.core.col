package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.ChattaConNoiBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import test.automation.core.UIUtils;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface PosteChatPage extends AbstractPageManager 
{

	public PosteChatPage insertPhoneNumber(ChattaConNoiBean b);

	public void clickOnRseserveCallButton ();

	public void clickOnEndButton();


}


