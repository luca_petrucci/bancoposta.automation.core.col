package it.poste.bancoposta.pages;

import bean.datatable.QuickOperationCreationBean;

public interface QuickOperationEditable 
{
	public void isEditable(QuickOperationCreationBean b);

	public OKOperationPage submit(QuickOperationCreationBean b, String campo, String value);

	public void clickBack();

	public void setBean(QuickOperationCreationBean quickOperationCreationBean);
}
