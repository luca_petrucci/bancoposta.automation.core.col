package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.BonificoDataBean;
import bean.datatable.GirofondoCreationBean;
import bean.datatable.PaymentCreationBean;
import bean.datatable.QuickOperationCheckBean;
import bean.datatable.QuickOperationCreationBean;
import bean.datatable.QuickOperationModifyBean; 
import test.automation.core.UIUtils;
import utils.DinamicData;
import utils.Entry;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface QuickOperationListPage extends AbstractPageManager{

	public void gotoQuickOperationPage(String quickOperationName, boolean saved) throws InterruptedException;

	public void checkModifyOperation(QuickOperationCreationBean b);

	public HomePage clickBack(String userHeader);

	public OKOperationPage submitTransaction(QuickOperationCreationBean b, String modify);

	public QuickOperationDeletePage clickModify();
	
	public void checkIfQuickOperationIsNotPresent(QuickOperationCreationBean b);

	public void QuickOperationModifyBean(QuickOperationModifyBean b) ;

	public void checkQuickOperationFunnel(QuickOperationCheckBean bean);

	public void clickOnOkPopupButton();
}
