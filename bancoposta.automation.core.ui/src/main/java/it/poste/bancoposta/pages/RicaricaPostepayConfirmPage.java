package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.BonificoDataBean;
import bean.datatable.QuickOperationCreationBean;
import bean.datatable.RicaricaPostepayBean;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;

import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface RicaricaPostepayConfirmPage extends AbstractPageManager{

	public void checkData(RicaricaPostepayBean b);

	public void checkData2(QuickOperationCreationBean b);

	public void checkData3(RicaricaPostepayBean b);

	void check(String payWith,String transferTo,String owner,String description,String am);


	public OKOperationPage submit(RicaricaPostepayBean b);

	OKOperationPage submit(String posteid);

	public void clickOKModal();




}
