package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.RicaricaPostepayBean;
import ch.qos.logback.classic.pattern.Abbreviator;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;

import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface RicaricaPostepayPage extends AbstractPageManager{

	public void check();
	
	public OKOperationPage submitRicarica(RicaricaPostepayBean b);



	public OKOperationPage eseguiRicarica(RicaricaPostepayBean b);

	public void isEditable(RicaricaPostepayBean bean);
	

	
	public OKOperationPage eseguiRicaricaQuestaPostePay(RicaricaPostepayBean b);
	
	public OKOperationPage eseguiRicaricaAutomatica(RicaricaPostepayBean b) throws Exception;
	
	public OKOperationPage eseguiRicaricaAutomatica2(RicaricaPostepayBean b) throws Exception;

	public void clickAnnulla();

	public OKOperationPage eseguiRicaricaAutomaticaFinoAl(RicaricaPostepayBean b);

	public OKOperationPage checkPopupRicaricaAutomatica();
}



