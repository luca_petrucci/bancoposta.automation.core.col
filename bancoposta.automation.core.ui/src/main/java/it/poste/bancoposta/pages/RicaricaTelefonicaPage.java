package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.RicaricaPostepayBean;
import bean.datatable.RicaricaTelefonicaBean;
import io.appium.java_client.AppiumDriver;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface RicaricaTelefonicaPage extends AbstractPageManager{

	public OKOperationPage eseguiRicarica(RicaricaTelefonicaBean b);
	
	public void clickAnnulla();

	public OKOperationPage eseguiRicaricaConClose(RicaricaTelefonicaBean b);
}
