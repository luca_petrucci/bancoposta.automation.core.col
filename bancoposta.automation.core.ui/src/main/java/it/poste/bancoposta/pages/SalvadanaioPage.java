package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.CredentialAccessBean;
import bean.datatable.SalvadanaioDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;

import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface SalvadanaioPage extends AbstractPageManager{

	public SalvadanaioInfoObjectivePage chooseObjective(String objectiveName);

	public void checkCronometro(String objectiveName);

	public SalvadanaioInfoObjectivePage chooseObjectiveTwo(String objectiveNameNew);

	//public void clickOnConto(String payWith);

	public void createNewTargetButton();
	
	public void categoryImage();
	
	public void insertData(List<SalvadanaioDataBean> b);

	public void insertImporto(List<SalvadanaioDataBean> b);

	public void insertNome(List<SalvadanaioDataBean> b);

	public void controlloPopup();

	public void createObjective();
	
	public void createObjective(String objectiveName, String posteID,String evolutionNumber,String owner);

	public void checkObjective(String objectiveName);

	public void checkObjectiveTwo(String objectiveNameNew, String importo2);
	
	public void checkDate(String dt);
	
	public void goToHomepage();

	public void checkModificaObiettivo(SalvadanaioDataBean b);

	public void clickChiudi();

	public SalvadanaioCreateObjRiepilogo checkRiepilogoCreazione(List<SalvadanaioDataBean> b);
	
	
	
}
