package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.w3c.dom.Node;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.QuickOperationCreationBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;
import test.automation.core.cmd.adb.AdbCommandPrompt;
import utils.ObjectFinderLight;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface SaveToQuickOperationPage extends AbstractPageManager{


	public void saveAsQuickOperation(QuickOperationCreationBean b);

	public void saveAsQuickOperationAdb(QuickOperationCreationBean b);

	public SaveToQuickOperationPage getLight() throws Exception;

	public void saveAsQuickOperationLightMode(QuickOperationCreationBean b);

}
