package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.BonificoDataBean;
import bean.datatable.QuickOperationCreationBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import test.automation.core.UIUtils;

import java.util.Locale;

import org.apache.poi.xwpf.usermodel.TOC;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface SendBonificoPage extends AbstractPageManager{


	public OKOperationPage sendBonifico(BonificoDataBean b);

	public void isEditable(BonificoDataBean bean);

	public OKOperationPage sendBonifico(BonificoDataBean bean, String campo, String value);
	
	public void checkPaeseDiResidenza(String city);

	public void clickAnnulla();

	public void chiudiPopup();

	public void chechEntryPoint();
}
