package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.BonificoDataBean;
import bean.datatable.PaymentCreationBean;
import io.appium.java_client.AppiumDriver;
import test.automation.core.UIUtils;
import utils.Entry;

import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface SendPostagiroPage extends AbstractPageManager{


		public void isEditable(BonificoDataBean bean);
		void isEditable(String payWith,String iban,String owner,String importo,String description);

		public OKOperationPage eseguiPostagiro(BonificoDataBean bean, String campo, String value);

		public OKOperationPage sendPostagiro(PaymentCreationBean b);

		public void isEditable(PaymentCreationBean bean);

		public OKOperationPage eseguiPostagiro(PaymentCreationBean bean, String campo, String value);

		public void clickAnnulla();
}
