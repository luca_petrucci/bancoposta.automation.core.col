package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.CredentialAccessBean;
import bean.datatable.NotificationSettingBean;
import test.automation.core.UIUtils;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface SettingsNotificationPage extends AbstractPageManager{


	public void checkNotificationSetting(NotificationSettingBean bean);

	public void setNotification(NotificationSettingBean bean) throws Exception;

	public SettingsPage gotoSettingsPage();

	//-------------------------------ANNUNZIATA------------------------------------------
	public void setPreferito(CredentialAccessBean b);

	//-------------------------------ANNUNZIATA------------------------------------------



}
