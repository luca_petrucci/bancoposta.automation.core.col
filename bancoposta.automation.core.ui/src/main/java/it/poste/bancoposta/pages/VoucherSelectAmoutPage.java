package it.poste.bancoposta.pages;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.AbstractPageManager;
import bean.datatable.VoucherVerificationBean;
import io.appium.java_client.AppiumDriver;
import test.automation.core.UIUtils;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public interface VoucherSelectAmoutPage extends AbstractPageManager{


		public void selectAmount(boolean annulla,String posteId);
}
