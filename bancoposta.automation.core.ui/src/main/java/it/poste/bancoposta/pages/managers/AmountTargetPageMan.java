package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.AmountTargetPage;
import it.poste.bancoposta.pages.Locators;

import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="AmountTargetPage")
@Android
@IOS
public class AmountTargetPageMan extends LoadableComponent<AmountTargetPageMan> implements AmountTargetPage
{
	protected UiPage page;

	@Override
	protected void isLoaded() throws Error {
		//PageFactory.initElements(driver, this);
		load();
	}

	@Override
	protected void load() 
	{
		page.get();
	}


	public void selectAmount()
	{
		WebElement currency = page.getParticle(Locators.AmountTargetPageMolecola.SELECTEDAMOUNT).getElement();
		org.springframework.util.Assert.isTrue(currency.isDisplayed()); 
		
		String curr= currency.getText();
		Assert.assertTrue(curr != null);
		System.out.println(curr);

		WebElement amountTarget= page.getParticle(Locators.AmountTargetPageMolecola.AMOUNTTARGETBUTTON).getElement();
		amountTarget.click();	

	}

	@Override
	public void init(UiPage page) 
	{
		this.page = page;
	}

	@Override
	public void checkPage(Object...params) 
	{
		isLoaded();
	}

}





































































//public class AmountTargetPage extends LoadableComponent<AmountTargetPage>{
//	
//	private WebDriver driver;
//	private int driverType; 
//
//	/**
//		 * @android 
//		 * @ios 
//		 * @web 
//		 */
//	private WebElement categoryImageIconTwo;
//	/**
//		 * @android 
//		 * @ios 
//		 * @web 
//		 */
//	private WebElement lessButton;
//	/**
//		 * @android 
//		 * @ios 
//		 * @web 
//		 */
//	private WebElement plusButton;
//	/**
//		 * @android 
//		 * @ios 
//		 * @web 
//		 */
//	private WebElement currency;
//	/**
//		 * @android 
//		 * @ios 
//		 * @web 
//		 */
//	private WebElement amountTargetButton;
//	public AmountTargetPage(WebDriver driver2, int driverType2) {
//		// TODO Auto-generated constructor stub
//	}
//	/**
//		 * @android 
//		 * @ios 
//		 * @web 
//		 */
//	
//	
//	@Override
//	protected void load() 
//	{
//		switch(this.driverType)
//		{
//			case 0://android driver
//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SalvadanaioPageLocator.LESSBUTTON.getAndroidLocator())));
//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SalvadanaioPageLocator.PLUSBUTTON.getAndroidLocator())));
//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SalvadanaioPageLocator.CURRENCY.getAndroidLocator())));
//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.SalvadanaioPageLocator.AMOUNTTARGETBUTTON.getAndroidLocator())));
//			 
//			break;
//	
//		}
//	}
//	@Override
//	protected void isLoaded() throws Error {
//		// TODO Auto-generated method stub
//		
//	}
//
//	public WebElement getCategoryImageIconTwo() {
//		return categoryImageIconTwo;
//	}
//
//	public void setCategoryImageIconTwo(WebElement categoryImageIconTwo) {
//		this.categoryImageIconTwo = categoryImageIconTwo;
//	}
//
//	public WebElement getLessButton() {
//		return lessButton;
//	}
//
//	public void setLessButton(WebElement lessButton) {
//		this.lessButton = lessButton;
//	}
//
//	public WebElement getPlusButton() {
//		return plusButton;
//	}
//
//	public void setPlusButton(WebElement plusButton) {
//		this.plusButton = plusButton;
//	}
//
//	public WebElement getCurrency() {
//		return currency;
//	}
//
//	public void setCurrency(WebElement currency) {
//		this.currency = currency;
//	}	
//	
//	


//public void selectAmount(){
//		
//		AmountTargetPage a = new AmountTargetPage(driver,driverType);
//		a.get();
//		
//		
//	    
//		currency = driver.findElement(By.xpath(Locators.SalvadanaioPageLocator.SELECTEDAMOUNT.getAndroidLocator()));
//		org.springframework.util.Assert.isTrue(currency.isDisplayed()); 
//		String curr= currency.getText();
//		Assert.assertTrue(curr != null);
//		System.out.println(curr);
//		
//		WebElement amountTarget = driver.findElement(By.xpath(Locators.SalvadanaioPageLocator.AMOUNTTARGETBUTTON.getAndroidLocator()));
//		amountTarget.click();	
//			
//		}
//		
//	}


