package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.Entry;
import automation.core.ui.Utility;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import bean.datatable.NotificaPushBean;
import it.poste.bancoposta.pages.BPOnlineNotifica;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.InsertPosteIDPage2;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="BPOnlineNotifica")
@Android
@IOS
public class BPOnlineNotificaMan extends LoadableComponent<BPOnlineNotificaMan> implements BPOnlineNotifica{

	UiPage page;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public void clickAutorizza(CredentialAccessBean b) 
	{
		WebElement autorizza=page.getParticle(Locators.BPOnlineNotificheMolecolas.AUTORIZZA).getElement();
		autorizza.click();
		
		InsertPosteIDPage2 posteId=(InsertPosteIDPage2) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage2", page.getDriver().getClass(), page.getLanguage());
		
		posteId.insertPosteID(b.getPosteid()).checkPage();
	}

	public void clickNega(CredentialAccessBean b) 
	{
		// TODO Auto-generated method stub
		
	}

	public void clickChiudi() 
	{
		page.getParticle(Locators.BPOnlineNotificheMolecolas.CHIUDI).visibilityOfElement(null);
		page.getParticle(Locators.BPOnlineNotificheMolecolas.CHIUDI).getElement().click();
	}

	public void checkNotificaPushPostagiroBpol(NotificaPushBean b) 
	{
		WebDriver driver=page.getDriver();
		SimpleDateFormat df=new SimpleDateFormat("dd/MM/yyyy");
		String data=df.format(Calendar.getInstance().getTime());
		String xpath=Utility.replacePlaceHolders(page.getParticle(Locators.BPOnlineNotificheMolecolas.NOTIFICA_DATA).getLocator(), new Entry("text",data));
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		
		System.out.println("notifica data OK");
		
		xpath=Utility.replacePlaceHolders(page.getParticle(Locators.BPOnlineNotificheMolecolas.NOTIFICA_PAGACON).getLocator(), new Entry("text",b.getPayWith()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));

		System.out.println("notifica Paga con OK");
		
		xpath=Utility.replacePlaceHolders(page.getParticle(Locators.BPOnlineNotificheMolecolas.NOTIFICA_BENEFICIARIO).getLocator(), new Entry("text",b.getIban()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));

		System.out.println("notifica Beneficiario OK");
		
		xpath=Utility.replacePlaceHolders(page.getParticle(Locators.BPOnlineNotificheMolecolas.NOTIFICA_COMMISSIONI).getLocator(), new Entry("text",b.getCommissioni()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));

		System.out.println("notifica Commissioni OK");
		
		xpath=Utility.replacePlaceHolders(page.getParticle(Locators.BPOnlineNotificheMolecolas.NOTIFICA_IMPORTO).getLocator(), new Entry("text",b.getImporto()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));

		System.out.println("notifica Importo OK");
		
		xpath=Utility.replacePlaceHolders(page.getParticle(Locators.BPOnlineNotificheMolecolas.NOTIFICA_OPERAZIONE).getLocator(), new Entry("text",b.getType()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));

		System.out.println("notifica tipo Operazione OK");
		
	}

	public void clickSpese(CredentialAccessBean b) 
	{
		page.getParticle(Locators.BPOnlineNotificheMolecolas.SPESE).visibilityOfElement(10L).click();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}
}
