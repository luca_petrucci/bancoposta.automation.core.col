package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BollettinoDataBean;
import bean.datatable.BonificoDataBean;
import bean.datatable.GirofondoCreationBean;
import bean.datatable.NotificationReceptionBean;
import bean.datatable.PaymentCreationBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.BachecaMessaggiDetailsPage;
import it.poste.bancoposta.pages.Locators;

import it.poste.bancoposta.transaction_messages.BonificoTransactionMessage;
import it.poste.bancoposta.transaction_messages.GirofondoTransactionMessage;
import it.poste.bancoposta.transaction_messages.PostagiroTransactionMessage;
import test.automation.core.UIUtils;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="BachecaMessaggiDetailsPage")
@Android
@IOS
public class BachecaMessaggiDetailsPageMan extends LoadableComponent<BachecaMessaggiDetailsPageMan> implements BachecaMessaggiDetailsPage
{
	UiPage page;

	private SimpleDateFormat dateFormat;

	private WebElement messageTitle;

	private WebElement message;

	private WebElement backButton;

	@Override
	protected void isLoaded() throws Error {
		//PageFactory.initElements(driver, this);

	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public boolean messageFind(NotificationReceptionBean b) 
	{
		this.messageTitle=page.getParticle(Locators.BachecaMessaggiDetailsPageMolecola.MESSAGETITLE).getElement();
		
		String txt=this.messageTitle.getText().trim();

		return txt.equals(b.getMessageText());
	}

	public void checkGirofondoMessage(NotificationReceptionBean b, GirofondoCreationBean girofondoData) 
	{
		String messaggio=""; 
		Set<String> contextNames = ((AppiumDriver<?>) page.getDriver()).getContextHandles();
		for (String contextName : contextNames) {
			System.out.println(contextName); //prints out something like NATIVE_APP \n WEBVIEW_1
		}
		
		this.message=page.getParticle(Locators.BachecaMessaggiDetailsPageMolecola.MESSAGE).getElement();
		messaggio=this.message.getText().trim();

		Calendar c=Calendar.getInstance();

		Date d=c.getTime();

		//String messaggio=this.message.getText().trim();

		GirofondoTransactionMessage mess=new GirofondoTransactionMessage(messaggio);

		mess.setHeader(girofondoData.getOwner());
		mess.setData(dateFormat.format(d));
		mess.setImporto(girofondoData.getAmount().replace(".", ","));
		mess.setFavore_di(girofondoData.getFavoreDi());
		mess.setFromTipologiaConto(girofondoData.getFromContoDetail());
		mess.setFromConto(girofondoData.getPayWith().split(";")[1]);
		mess.setToTipologiaConto(girofondoData.getToContoDetail());
		mess.setToConto(girofondoData.getTransferTo().split(";")[1]);
		mess.setCommissioni(girofondoData.getCommissioni());

		double importo=Double.parseDouble(girofondoData.getAmount());
		double commissioni=Double.parseDouble(girofondoData.getCommissioni().replace(",", "."));

		NumberFormat format = NumberFormat.getCurrencyInstance(Locale.ITALY);

		String tot=format.format(importo+commissioni);

		mess.setTotale(tot.replace("\u20AC", "").trim());

		mess.checkMessage();
	}

	public void clickBack() 
	{
		WaitManager.get().waitLongTime();
		this.backButton=page.getParticle(Locators.BachecaMessaggiDetailsPageMolecola.BACKBUTTON).getElement();
		this.backButton.click();
	}

	public void checkPostagiroMessage(NotificationReceptionBean b, BonificoDataBean bean) 
	{
		checkMessage(bean.getOwner(),bean.getPayWith(),bean.getBeneficiario(),bean.getAmount(),bean.getIban(),bean.getCommissioni(),bean.getDescription());
	}

	public void checkBonificoMessage(NotificationReceptionBean b, BonificoDataBean bean) 
	{

		String messaggio="";

		messaggio = ((AndroidDriver)page.getDriver()).findElementByAndroidUIAutomator("new UiSelector().className(\"android.webkit.WebView\")").getText();
		System.out.println(messaggio);

		Calendar c=Calendar.getInstance();

		Date d=c.getTime();

		BonificoTransactionMessage message=new BonificoTransactionMessage(messaggio);


		message.setData(dateFormat.format(d));
		message.setPaywith(bean.getPayWith().split(";")[1]);
		message.setIntestazione(bean.getOwner());
		//			message.setIban(bean.getCc());
		//			message.setIbanInt(bean.getOwner());
		//			message.setIndirizzo(bean.getAddress());
		////			message.setRiferimento(bean.getReference());
		//			message.setRiferimentoEffettivo(bean.getOwner());
		////			message.setRiferimentoBeneficiarioEffetivo(bean.getRefecenceEffective());
		//			message.setLocalita(bean.getCitySender());
		////			message.setNazione(Utility.getProvincia(bean.getCity()));
		//			message.setImporto(bean.getAmount().replace(".", ","));
		//			message.setCommissioni(bean.getCommissioni().replace(".", ","));
		//			message.setCasuale(bean.getDescription());

		//			double importo=Double.parseDouble(bean.getAmount());
		//			double commissioni=Double.parseDouble(bean.getCommissioni().replace(",", "."));
		//			
		//			NumberFormat format = NumberFormat.getCurrencyInstance(Locale.ITALY);
		//			
		//			String tot=format.format(importo+commissioni);
		//			
		//			message.setTotale(tot.replace("\u20AC", "").trim());

		message.checkMessage();

	}

	//		public void checkBonificoMessage(NotificationReceptionBean b, BonificoDataBean bean) 
	//		{	
	//			
	//			String messaggio="";
	//			
	//			switch(this.driverType)
	//			{
	//			case 0://android
	////				this.message=driver.findElement(By.xpath(Locators.BachecaMessaggiDetailsPageMolecola.MESSAGE.getAndroidLocator()));
	//				messaggio = ((AndroidDriver)driver).findElementByAndroidUIAutomator("new UiSelector().className(\"android.webkit.WebView\")").getText();
	////				messaggio=this.message.getText().trim();
	//				break;
	//			case 1://ios
	//				//this.message=driver.findElement(By.xpath(Locators.BachecaMessaggiDetailsPageMolecola.MESSAGE.getLocator(driver)));
	//				//a differenza di android il messaggio è suddiviso in n staticLabel ognuna contenente il messaggio
	//				List<WebElement> messTxt=driver.findElements(By.xpath(Locators.BachecaMessaggiDetailsPageMolecola.MESSAGE.getLocator(driver)));
	//				System.out.println(messaggio);
	//				for(WebElement e : messTxt)
	//				{
	//					messaggio+=e.getText().trim()+" ";
	//				}
	//				
	//				break;
	//			}
	//			
	//			Calendar c=Calendar.getInstance();
	//			
	//			Date d=c.getTime();
	//			
	//			BonificoTransactionMessage message=new BonificoTransactionMessage(messaggio);
	//			
	//			message.setHeader(bean.getBeneficiario());
	//			message.setData(dateFormat.format(d));
	//			message.setPaywith(bean.getPayWith().split(";")[1]);
	//			message.setIntestazione(bean.getOwner());
	//			message.setIban(bean.getIban());
	//			message.setIbanInt(bean.getOwner());
	//			message.setIndirizzo(bean.getAddress());
	//			message.setRiferimento(bean.getReference());
	//			message.setRiferimentoEffettivo(bean.getOwner());
	//			message.setRiferimentoBeneficiarioEffetivo(bean.getRefecenceEffective());
	//			message.setLocalita(bean.getCitySender());
	//			message.setNazione(Utility.getProvincia(bean.getCity()));
	//			message.setImporto(bean.getAmount().replace(".", ","));
	//			message.setCommissioni(bean.getCommissioni().replace(".", ","));
	//			message.setCasuale(bean.getDescription());
	//			
	//			double importo=Double.parseDouble(bean.getAmount());
	//			double commissioni=Double.parseDouble(bean.getCommissioni().replace(",", "."));
	//			
	//			NumberFormat format = NumberFormat.getCurrencyInstance(Locale.ITALY);
	//			
	//			String tot=format.format(importo+commissioni);
	//			
	//			message.setTotale(tot.replace("\u20AC", "").trim());
	//			
	//			message.checkMessage();
	//		}

	public void checkBollettinoMessage(NotificationReceptionBean b, BollettinoDataBean bean) 
	{
		String messaggio="";

		this.message=page.getParticle(Locators.BachecaMessaggiDetailsPageMolecola.MESSAGE).getElement();
		messaggio=this.message.getText().trim();

		Calendar c=Calendar.getInstance();

		Date d=c.getTime();

		BonificoTransactionMessage message=new BonificoTransactionMessage(messaggio);
		
		message.setData(dateFormat.format(d));
		message.setPaywith(bean.getPayWith().split(";")[1]);
		message.setIntestazione(bean.getOwner());
		message.setIban(bean.getCc());
		message.setIbanInt(bean.getOwner());
		message.setIndirizzo(bean.getSenderAdress());
		//			message.setRiferimento(bean.getReference());
		message.setRiferimentoEffettivo(bean.getOwner());
		//			message.setRiferimentoBeneficiarioEffetivo(bean.getRefecenceEffective());
		message.setLocalita(bean.getSenderCity());
		//			message.setNazione(Utility.getProvincia(bean.getCity()));
		message.setImporto(bean.getAmount().replace(".", ","));
		message.setCommissioni(bean.getCommissioni().replace(".", ","));
		message.setCasuale(bean.getDescription());

		double importo=Double.parseDouble(bean.getAmount());
		double commissioni=Double.parseDouble(bean.getCommissioni().replace(",", "."));

		NumberFormat format = NumberFormat.getCurrencyInstance(Locale.ITALY);

		String tot=format.format(importo+commissioni);

		message.setTotale(tot.replace("\u20AC", "").trim());

		message.checkMessage();
	}

	public void checkPostagiroMessage(NotificationReceptionBean b, PaymentCreationBean bean) 
	{
		checkMessage(bean.getReference(),bean.getPayWith(),bean.getBeneficiario(),bean.getAmount(),bean.getIban(),bean.getCommissioni(),bean.getDescription());

	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object...params) 
	{
		load();
	}

	@Override
	public void checkMessage(String owner, String payWith, String beneficiario, String amount, String iban,
			String commissioni, String description) 
	{
		String messaggio="";

		messaggio = ((AndroidDriver)page.getDriver()).findElementByAndroidUIAutomator("new UiSelector().className(\"android.webkit.WebView\")").getText();
		//				this.message=driver.findElement(By.xpath(Locators.BachecaMessaggiDetailsPageMolecola.MESSAGE.getAndroidLocator()));
		messaggio=this.message.getText().trim();

		Calendar c=Calendar.getInstance();

		Date d=c.getTime();

		PostagiroTransactionMessage message= new PostagiroTransactionMessage(messaggio);
		message.setHeader(owner);
		message.setData(dateFormat.format(d));
		message.setPayWith(payWith.split(";")[1]);
		message.setIntestazione(owner);
		message.setIntestatarioBeneficiario(beneficiario);
		message.setDataAddebito(dateFormat.format(d));
		message.setImporto(amount.replace(".", ","));
		message.setIban(iban);
		message.setCommissioni(commissioni.replace(".", ","));
		message.setCasuale(description);
		double importo=Double.parseDouble(amount);
		double comms=Double.parseDouble(commissioni.replace(",", "."));

		NumberFormat format = NumberFormat.getCurrencyInstance(Locale.ITALY);

		String tot=format.format(importo+comms);

		message.setTotale(tot.replace("\u20AC", "").trim());

		message.checkMessage();
	}
}
