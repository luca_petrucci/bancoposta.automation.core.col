package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.NotificationReceptionBean;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BachecaMessaggiDetailsPage;
import it.poste.bancoposta.pages.BachecaMessaggiPage;
import it.poste.bancoposta.pages.HomepageHamburgerMenu;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.utils.PageMolecolaImgCreator;
import test.automation.core.UIUtils;
import utils.Entry;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="BachecaMessaggiPage")
@Android
public class BachecaMessaggiPageMan extends LoadableComponent<BachecaMessaggiPageMan> implements BachecaMessaggiPage
{
	protected UiPage page;
	protected WebElement messaggiTab;
	protected WebElement messageList;
	protected SimpleDateFormat dateFormat=new SimpleDateFormat("dd # yyyy");
	protected WebElement leftMenu;
	protected WebElement searchBar;
	protected WebElement filterButton;
	protected WebElement startDateButton;
	protected WebElement popupDateButton;
	protected WebElement endDateButton;
	protected WebElement filterConfirmButton;
	private MobileDriver driver;

	@Override
	protected void isLoaded() throws Error {
		load();
	}

	@Override
	protected void load() 
	{
		WaitManager.get().waitMediumTime();	
		page.get();
		
		WaitManager.get().waitMediumTime();
		
//		byte[] b=((TakesScreenshot)page.getDriver()).getScreenshotAs(OutputType.BYTES);
//		PageMolecolaImgCreator.makeImg(b, page,
//				Locators.BachecaMessaggiPageMolecola.HEADER,
//				Locators.BachecaMessaggiPageMolecola.LEFTMENU,
//				Locators.BachecaMessaggiPageMolecola.MESSAGGITAB,
//				Locators.BachecaMessaggiPageMolecola.NOTIFICHETAB,
//				Locators.BachecaMessaggiPageMolecola.SEARCHMESSAGES,
//				Locators.BachecaMessaggiPageMolecola.FILTERBUTTON,
//				Locators.BachecaMessaggiPageMolecola.MESSAGELIST,
//				Locators.BachecaMessaggiPageMolecola.MESSAGEELEMENT,
//				Locators.BachecaMessaggiPageMolecola.MESSAGETITLE,
//				Locators.BachecaMessaggiPageMolecola.MESSAGETYPE,
//				Locators.BachecaMessaggiPageMolecola.MESSAGEDATE
//				);
	}

	public void clickMessaggi() 
	{
		this.messaggiTab=page.getParticle(Locators.BachecaMessaggiPageMolecola.MESSAGGITAB).getElement();
		this.messaggiTab.click();
	}

	public BachecaMessaggiDetailsPage gotoTransactionMessage(NotificationReceptionBean b) 
	{
		String message="";
		String title="";
		String datatxt="";

		this.messageList=page.getParticle(Locators.BachecaMessaggiPageMolecola.MESSAGELIST).getElement();
		message=page.getParticle(Locators.BachecaMessaggiPageMolecola.MESSAGEELEMENT).getLocator();
		title=page.getParticle(Locators.BachecaMessaggiPageMolecola.MESSAGETITLE).getLocator();
		datatxt=page.getParticle(Locators.BachecaMessaggiPageMolecola.MESSAGEDATE).getLocator();

		Calendar c=Calendar.getInstance();

		Date d=c.getTime();

		String mese=Utility.capitalize(new DateFormatSymbols(Locale.ITALY).getMonths()[d.getMonth()]).substring(0, 3);

		String data=dateFormat.format(d).replace("#", mese).toLowerCase();

		List<WebElement> list=null;
		System.out.println(b.getMessageText());

		String toFind;
		try {
			toFind=b.getMessageText().substring(0, b.getMessageText().indexOf(":")).toLowerCase();
		} catch (Exception e) {
			toFind=b.getMessageText().toLowerCase();
		}

		System.out.println("Questo � tofind" + toFind);
		
		BachecaMessaggiDetailsPage messageDetails=(BachecaMessaggiDetailsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BachecaMessaggiDetailsPage", page.getDriver().getClass(), page.getLanguage());

		list=this.messageList.findElements(By.xpath(message));


		System.out.println(message);
		for(WebElement e : list)
		{
			try
			{
				String txt=e.findElement(By.xpath(title)).getText().trim().toLowerCase();
				String dtxt=e.findElement(By.xpath(datatxt)).getText().trim().toLowerCase();

				if(txt.contains(toFind) && dtxt.contains(data))
				{
					e.click();

					try {
						Thread.sleep(TimeUnit.MINUTES.toMillis(2));
					} catch (Exception r) {

					}
					messageDetails.checkPage();
					break;

					//								if(messageDetails.messageFind(b))
					//								{
					//									return messageDetails;
					//								}
					//								else
					//								{
					//									messageDetails.clickBack();
					//									break;
					//								}
				}
			}
			catch(Exception err)
			{

			}

		}

		try {
			Utility.swipe((MobileDriver<?>) page.getDriver(), Utility.DIRECTION.DOWN, 500);
		} catch (Exception e) {

		}

		return messageDetails;
	}

	public HomepageHamburgerMenu openHamburgerMenu() 
	{
		this.leftMenu=page.getParticle(Locators.BachecaMessaggiPageMolecola.LEFTMENU).getElement();
		this.leftMenu.click();

		HomepageHamburgerMenu p=(HomepageHamburgerMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomepageHamburgerMenu", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();

		return p;
	}

	public void filtraMessaggi(NotificationReceptionBean b) {


		this.searchBar=page.getParticle(Locators.BachecaMessaggiPageMolecola.RICERCATEXTFIELD).getElement();
		searchBar.sendKeys(b.getNotifyType().toLowerCase().trim());

		WaitManager.get().waitShortTime();

		this.filterButton=page.getParticle(Locators.BachecaMessaggiPageMolecola.FILTERBUTTON).getElement();
		filterButton.click();

		WaitManager.get().waitShortTime();


		this.startDateButton=page.getParticle(Locators.BachecaMessaggiPageMolecola.STARTDATEBUTTON).getElement();
		startDateButton.click();

		WaitManager.get().waitShortTime();


		this.popupDateButton=page.getParticle(Locators.BachecaMessaggiPageMolecola.POPUPDATEOKBUTTON).getElement();
		popupDateButton.click();

		WaitManager.get().waitShortTime();


		this.endDateButton=page.getParticle(Locators.BachecaMessaggiPageMolecola.ENDDATEBUTTON).getElement();
		endDateButton.click();

		WaitManager.get().waitShortTime();


		this.popupDateButton=page.getParticle(Locators.BachecaMessaggiPageMolecola.POPUPDATEOKBUTTON).getElement();
		popupDateButton.click();

		WaitManager.get().waitShortTime();


		this.filterConfirmButton=page.getParticle(Locators.BachecaMessaggiPageMolecola.FILTERCONFIRMBUTTON).getElement();
		filterConfirmButton.click();



	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object...params) 
	{
		load();
	}
}
