package it.poste.bancoposta.pages.managers;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import bean.datatable.NotificationReceptionBean;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BachecaMessaggiDetailsPage;
import it.poste.bancoposta.pages.Locators;

@PageManager(page="BachecaMessaggiPage")
@IOS
public class BachecaMessaggiPageManIOS extends BachecaMessaggiPageMan 
{
	protected void load() 
	{
		page.getQuick();
		
		WaitManager.get().waitMediumTime();
		
//		byte[] b=((TakesScreenshot)page.getDriver()).getScreenshotAs(OutputType.BYTES);
//		PageMolecolaImgCreator.makeImg(b, page,
//				Locators.BachecaMessaggiPageMolecola.HEADER,
//				Locators.BachecaMessaggiPageMolecola.LEFTMENU,
//				Locators.BachecaMessaggiPageMolecola.MESSAGGITAB,
//				Locators.BachecaMessaggiPageMolecola.NOTIFICHETAB,
//				Locators.BachecaMessaggiPageMolecola.SEARCHMESSAGES,
//				Locators.BachecaMessaggiPageMolecola.FILTERBUTTON,
//				Locators.BachecaMessaggiPageMolecola.MESSAGELIST,
//				Locators.BachecaMessaggiPageMolecola.MESSAGEELEMENT,
//				Locators.BachecaMessaggiPageMolecola.MESSAGETITLE,
//				Locators.BachecaMessaggiPageMolecola.MESSAGETYPE,
//				Locators.BachecaMessaggiPageMolecola.MESSAGEDATE
//				);
	}
	
	public BachecaMessaggiDetailsPage gotoTransactionMessage(NotificationReceptionBean b) 
	{
		String message="";
		String title="";
		String datatxt="";

		this.messageList=page.getParticle(Locators.BachecaMessaggiPageMolecola.MESSAGELIST).getElement();
		message=page.getParticle(Locators.BachecaMessaggiPageMolecola.MESSAGEELEMENT).getLocator();
		title=page.getParticle(Locators.BachecaMessaggiPageMolecola.MESSAGETITLE).getLocator();
		datatxt=page.getParticle(Locators.BachecaMessaggiPageMolecola.MESSAGEDATE).getLocator();

		Calendar c=Calendar.getInstance();

		Date d=c.getTime();

		String mese=Utility.capitalize(new DateFormatSymbols(Locale.ITALY).getMonths()[d.getMonth()]).substring(0, 3);

		String data=dateFormat.format(d).replace("#", mese).toLowerCase();

		List<WebElement> list=null;
		System.out.println(b.getMessageText());

		String toFind;
		try {
			toFind=b.getMessageText().toLowerCase();
		} catch (Exception e) {
			toFind=b.getMessageText().toLowerCase();
		}

		System.out.println("Questo � tofind" + toFind);
		
		BachecaMessaggiDetailsPage messageDetails=(BachecaMessaggiDetailsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BachecaMessaggiDetailsPage", page.getDriver().getClass(), page.getLanguage());

		list=this.messageList.findElements(By.xpath(message));


		System.out.println(message);
		
		for(int i=0;i<list.size();i++)
		{
			try {
				String txt=page.getDriver().findElement(By.xpath("("+title+")["+(i+1)+"]")).getText().trim().toLowerCase();
				String dtxt=page.getDriver().findElement(By.xpath("("+datatxt+")["+(i+1)+"]")).getText().trim().toLowerCase();
				
				System.out.println("text:"+txt+" date:"+dtxt);
				System.out.println("to Find text:"+toFind+" to find date:"+data);
				
				boolean ok=txt.contains(toFind);
				System.out.println(ok);
				
				if(ok)
				{
					list.get(i).click();
					messageDetails.checkPage();
					break;

					//								if(messageDetails.messageFind(b))
					//								{
					//									return messageDetails;
					//								}
					//								else
					//								{
					//									messageDetails.clickBack();
					//									break;
					//								}
				}
			}
			catch(Exception err)
			{

			}
		}

		return messageDetails;
	}
	
	public void clickMessaggi() 
	{
		boolean popup=false;
		try {
			page.getParticle(Locators.BachecaMessaggiPageMolecola.ERRORPOPUPIOS).visibilityOfElement(15L);
			popup=true;
		}catch(Exception e) {
			
		}
		if(popup==true) {
			throw new RuntimeException("Popup di Errore sulla pagina VOL");
		}
		this.messaggiTab=page.getParticle(Locators.BachecaMessaggiPageMolecola.MESSAGGITAB).getElement();
		this.messaggiTab.click();
		page.getParticle(Locators.BachecaMessaggiPageMolecola.NOTIFICHETAB).getElement().click();
		WaitManager.get().waitShortTime();
		this.messaggiTab.click();
		page.getParticle(Locators.BachecaMessaggiPageMolecola.MESSAGEELEMENT).getElement();
	}
}
