package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BollettinoDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.Bollettino896FormPage;
import it.poste.bancoposta.pages.BollettinoConfirmPage;
import it.poste.bancoposta.pages.IBollettino;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SelectProvinciaModal;
import test.automation.core.UIUtils;

import org.apache.tinkerpop.gremlin.process.traversal.P;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="Bollettino896FormPage")
@Android
@IOS
public class Bollettino896FormPageMan extends LoadableComponent<Bollettino896FormPageMan> implements IBollettino,Bollettino896FormPage
{
	protected UiPage page;
	
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancellButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement bollettinoTypeInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement changeBollettinoType;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement paymentTypeInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement changePaymentType;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement codeInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ccpInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement nameInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement surnameInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement addressInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cityInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement capInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement provinceInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement savePerson;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement confirm;
	private BollettinoDataBean bean;
	private WebElement exitFromPayment;

	

	public void setBean(BollettinoDataBean bean) {
		this.bean = bean;
	}

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		WebDriver driver=page.getDriver();
		
		Utility.swipeToElement((AppiumDriver<?>)driver, Utility.DIRECTION.UP, 300, "posteitaliane.posteapp.appbpol:id/tv_tipologia_value", 10);

		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.HEADER).getXPath()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.CANCELLBUTTON).getXPath()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.BOLLETTINOTYPEINFO).getXPath()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.CHANGEBOLLETTINOTYPE).getXPath()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.PAYMENTTYPEINFO).getXPath()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.CHANGEPAYMENTTYPE).getXPath()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.NAMEINPUT).getXPath()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.CCPINPUT).getXPath()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.AMOUNT).getXPath()));

		this.bollettinoTypeInfo=driver.findElement(page.getParticle(Locators.Bollettino896FormPageMolecola.BOLLETTINOTYPEINFO).getXPath());

		String bltype=this.bollettinoTypeInfo.getText();

		Assert.assertTrue("Controllo header tipologia bollettino; attuale:"+bltype+", atteso:"+bean.getBollettinoType(),bltype.equals(bean.getBollettinoType()));

		this.paymentTypeInfo=driver.findElement(page.getParticle(Locators.Bollettino896FormPageMolecola.PAYMENTTYPEINFO).getXPath());

		String pwInfo=this.paymentTypeInfo.getText();

		Assert.assertTrue("Controllo header paga con; attuale:"+pwInfo+", atteso:"+bean.getPayWith().replace(";", " "),pwInfo.toLowerCase().equals(bean.getPayWith().replace(";", " ")));


		Utility.swipe((AppiumDriver<?>)driver, Utility.DIRECTION.DOWN, 300);

		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.NAMEINPUT).getXPath()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.SURNAMEINPUT).getXPath()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.ADDRESSINPUT).getXPath()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.CITYINPUT).getXPath()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.CAPINPUT).getXPath()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.PROVINCEINPUT).getXPath()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.SAVEPERSON).getXPath()));
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.Bollettino896FormPageMolecola.CONFIRM).getXPath()));

		Utility.swipe((AppiumDriver<?>)driver, Utility.DIRECTION.UP, 300);
	}



	@Override
	public void checkData(BollettinoDataBean b) 
	{ 
		WebDriver driver=page.getDriver();
		//compilo la form con i dati
		Utility.swipe((AppiumDriver<?>)driver, Utility.DIRECTION.UP, 100);

		WaitManager.get().waitMediumTime();

		this.codeInput=page.getParticle(Locators.Bollettino896FormPageMolecola.CODEINPUT).getElement();
		this.ccpInput=page.getParticle(Locators.Bollettino896FormPageMolecola.CCPINPUT).getElement();
		this.amount=page.getParticle(Locators.Bollettino896FormPageMolecola.AMOUNT).getElement();


		this.codeInput.sendKeys(bean.getBollettinoCode());
		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		this.ccpInput.sendKeys(bean.getCc());
		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		this.amount.sendKeys(bean.getAmount());

		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		Utility.swipe((AppiumDriver<?>)driver, Utility.DIRECTION.DOWN, 300);

		WaitManager.get().waitMediumTime();

		this.nameInput=page.getParticle(Locators.Bollettino896FormPageMolecola.NAMEINPUT).getElement();
		this.surnameInput=page.getParticle(Locators.Bollettino896FormPageMolecola.SURNAMEINPUT).getElement();
		this.addressInput=page.getParticle(Locators.Bollettino896FormPageMolecola.ADDRESSINPUT).getElement();
		this.cityInput=page.getParticle(Locators.Bollettino896FormPageMolecola.CITYINPUT).getElement();
		this.capInput=page.getParticle(Locators.Bollettino896FormPageMolecola.CAPINPUT).getElement();
		this.surnameInput=page.getParticle(Locators.Bollettino896FormPageMolecola.SURNAMEINPUT).getElement();
		this.provinceInput=page.getParticle(Locators.Bollettino896FormPageMolecola.PROVINCEINPUT).getElement();
		

		this.nameInput.sendKeys(bean.getSenderName());
		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		this.surnameInput.sendKeys(bean.getSenderLastName());
		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		this.addressInput.sendKeys(bean.getSenderAdress());
		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		this.cityInput.sendKeys(bean.getSenderCity());
		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		this.capInput.sendKeys(bean.getSenderCAP());
		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		this.provinceInput.click();

		SelectProvinciaModal modal=(SelectProvinciaModal) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SelectProvinciaModal", page.getDriver().getClass(), page.getLanguage());

		modal.checkPage();

		modal.selectModalElement(bean.getSenderProv(), 10);

		WaitManager.get().waitMediumTime();
		this.confirm=page.getParticle(Locators.Bollettino896FormPageMolecola.CONFIRM).getElement();
		
		//sottometto
		this.confirm.click();

		BollettinoConfirmPage confirm= (BollettinoConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BollettinoConfirmPage", page.getDriver().getClass(), page.getLanguage());

		confirm.checkPage();

		//controllo che i dati siano uguali a quelli inseriti nella form
		confirm.check(bean,BollettinoDataBean.BOLLETTINO_896);

		//controllo che la form venga riempita con i dati precedentemente inseriti


		//checkFromBackButton();
		//			switch(this.driverType)
		//			{
		//				case 0://android driver
		//					try {
		//						this.cancellButton=driver.findElement(By.xpath(Locators.Bollettino896FormPageMolecola.BACKBUTTON.getAndroidLocator()));
		//						break;
		//						
		//					} catch (Exception e) {
		//						this.cancellButton=driver.findElement(By.xpath(Locators.Bollettino896FormPageMolecola.CANCELLBUTTON.getAndroidLocator()));
		//					}
		//					
		//				case 1://ios driver
		//					this.cancellButton=driver.findElement(By.xpath(Locators.Bollettino896FormPageMolecola.CANCELLBUTTON.getLocator(driver)));
		//				break;
		//				case 2://web driver
		//				
		//				break;
		//			}
		//			
		//			this.cancellButton.click();

		//			switch(this.driverType)
		//			{
		//				case 0://android driver
		//					this.cancellButton=driver.findElement(By.xpath(Locators.Bollettino896FormPageMolecola.CANCELLBUTTON.getAndroidLocator()));
		//					break;
		//				case 1://ios driver
		//					this.exitFromPayment=driver.findElement(By.xpath(Locators.Bollettino896FormPageMolecola.EXITFROMPAYMENT.getLocator(driver)));
		//					this.exitFromPayment.click();
		//					break;
		//				case 2://web driver
		//				
		//				break;
		//			}
	}

	public void checkFromBackButton() 
	{
		WebDriver driver=page.getDriver();
		
		Utility.swipe((AppiumDriver<?>)driver, Utility.DIRECTION.UP, 300);

		WaitManager.get().waitShortTime();

		this.bollettinoTypeInfo=page.getParticle(Locators.Bollettino896FormPageMolecola.BOLLETTINOTYPEINFO).getElement();

		String bltype=this.bollettinoTypeInfo.getText();

		Assert.assertTrue("Controllo header tipologia bollettino; attuale:"+bltype+", atteso:"+bean.getBollettinoType(),bltype.equals(bean.getBollettinoType()));

		this.paymentTypeInfo=page.getParticle(Locators.Bollettino896FormPageMolecola.PAYMENTTYPEINFO).getElement();

		String pwInfo=this.paymentTypeInfo.getText();

		Assert.assertTrue("Controllo header paga con; attuale:"+pwInfo+", atteso:"+bean.getPayWith().replace(";", " "),pwInfo.toLowerCase().equals(bean.getPayWith().replace(";", " ")));

		this.codeInput=page.getParticle(Locators.Bollettino896FormPageMolecola.CODEINPUT).getElement();
		this.ccpInput=page.getParticle(Locators.Bollettino896FormPageMolecola.CCPINPUT).getElement();
		this.amount=page.getParticle(Locators.Bollettino896FormPageMolecola.AMOUNT).getElement();

		String actual=this.codeInput.getText().trim();
		Assert.assertTrue("controllo iban; attuale:"+actual+", attesa:"+bean.getBollettinoCode(),actual.equals(bean.getBollettinoCode()));

		actual=this.ccpInput.getText().trim();
		Assert.assertTrue("controllo c/c; attuale:"+actual+", attesa:"+bean.getCc(),actual.equals(bean.getCc()));

		actual=this.amount.getText().trim();
		Assert.assertTrue("controllo importo; attuale:"+actual+", attesa:"+bean.getAmount().replace(".", ","),actual.equals(bean.getAmount().replace(".", ",")));

		Utility.swipe((AppiumDriver<?>)driver, Utility.DIRECTION.DOWN, 300);

		WaitManager.get().waitMediumTime();

		this.nameInput=page.getParticle(Locators.Bollettino896FormPageMolecola.NAMEINPUT).getElement();
		this.surnameInput=page.getParticle(Locators.Bollettino896FormPageMolecola.SURNAMEINPUT).getElement();
		this.addressInput=page.getParticle(Locators.Bollettino896FormPageMolecola.ADDRESSINPUT).getElement();
		this.cityInput=page.getParticle(Locators.Bollettino896FormPageMolecola.CITYINPUT).getElement();
		this.capInput=page.getParticle(Locators.Bollettino896FormPageMolecola.CAPINPUT).getElement();

		actual=this.nameInput.getText().trim();
		Assert.assertTrue("controllo nome; attuale:"+actual+", attesa:"+bean.getSenderName(),actual.equals(bean.getSenderName()));

		actual=this.surnameInput.getText().trim();
		Assert.assertTrue("controllo cognome; attuale:"+actual+", attesa:"+bean.getSenderLastName(),actual.equals(bean.getSenderLastName()));

		actual=this.addressInput.getText().trim();
		Assert.assertTrue("controllo indirizzo; attuale:"+actual+", attesa:"+bean.getSenderAdress(),actual.equals(bean.getSenderAdress()));

		actual=this.cityInput.getText().trim();
		Assert.assertTrue("controllo citt�; attuale:"+actual+", attesa:"+bean.getSenderCity(),actual.equals(bean.getSenderCity()));

		actual=this.capInput.getText().trim();
		Assert.assertTrue("controllo CAP; attuale:"+actual+", attesa:"+bean.getSenderCAP(),actual.equals(bean.getSenderCAP()));


	}

	@Override
	public void submit(BollettinoDataBean b) 
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object...params) 
	{
		load();
	}
}
