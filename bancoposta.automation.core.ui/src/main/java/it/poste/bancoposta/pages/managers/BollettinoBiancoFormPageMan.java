package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BollettinoDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BollettinoBiancoFormPage;
import it.poste.bancoposta.pages.BollettinoConfirmPage;
import it.poste.bancoposta.pages.IBollettino;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SelectProvinciaModal;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="BollettinoBiancoFormPage")
@Android
@IOS
public class BollettinoBiancoFormPageMan extends LoadableComponent<BollettinoBiancoFormPageMan> implements IBollettino,BollettinoBiancoFormPage{
	
	protected UiPage page;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancellButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement bollettinoTypeInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement changeBollettinoType;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement paymentTypeInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement changePaymentType;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ccInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ownerInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement accountheaderInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement saveAccountheader;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement casualeInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement nameInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement surnameInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement addressInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cityInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement capInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement provinceInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement savePerson;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement confirm;
	private BollettinoDataBean bean;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		WaitManager.get().waitMediumTime();
		WebDriver driver=page.getDriver();
		Utility.swipeToElement((AppiumDriver<?>)driver, Utility.DIRECTION.UP, 300, "posteitaliane.posteapp.appbpol:id/tv_tipologia_value", 10);
//
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.HEADER).getXPath()));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CANCELLBUTTON).getXPath()));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.BOLLETTINOTYPEINFO).getXPath()));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CHANGEBOLLETTINOTYPE).getXPath()));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.PAYMENTTYPEINFO).getXPath()));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CHANGEPAYMENTTYPE).getXPath()));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.OWNERINPUT).getXPath()));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CCINPUT).getXPath()));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.AMOUNT).getXPath()));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.DESCRITPION).getXPath()));

//		this.bollettinoTypeInfo=driver.findElement(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.BOLLETTINOTYPEINFO).getXPath());
//
//		String bltype=this.bollettinoTypeInfo.getText();
//
//		Assert.assertTrue("Controllo header tipologia bollettino; attuale:"+bltype+", atteso:"+bean.getBollettinoType(),bltype.equals(bean.getBollettinoType()));
//
//		this.paymentTypeInfo=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.PAYMENTTYPEINFO).getElement();
//
//		String pwInfo=this.paymentTypeInfo.getText();
//
//		Assert.assertTrue("Controllo header paga con; attuale:"+pwInfo+", atteso:"+bean.getPayWith().replace(";", " "),pwInfo.toLowerCase().equals(bean.getPayWith().replace(";", " ")));


		Utility.swipe((AppiumDriver<?>)driver, Utility.DIRECTION.DOWN, 300);

//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.NAMEINPUT).getXPath()));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.SURNAMEINPUT).getXPath()));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.ADDRESSINPUT).getXPath()));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CITYINPUT).getXPath()));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CAPINPUT).getXPath()));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.PROVINCEINPUT).getXPath()));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CANCELPAYMENT).getXPath()));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CONFIRM).getXPath()));

		Utility.swipe((AppiumDriver<?>)driver, Utility.DIRECTION.UP, 300);

		
	}


	@Override
	public void checkData(BollettinoDataBean b) 
	{
		WebDriver driver=page.getDriver();
		//compilo la form con i dati
		Utility.swipe((AppiumDriver<?>)driver, Utility.DIRECTION.UP, 100);

		WaitManager.get().waitMediumTime();

		this.ownerInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.OWNERINPUT).getElement();
		this.ccInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CCINPUT).getElement();
		this.amount=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.AMOUNT).getElement();
		this.description=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.DESCRITPION).getElement();
		

		this.ccInput.sendKeys(bean.getCc());
		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		this.ownerInput.sendKeys(bean.getOwner());
		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		this.amount.sendKeys(bean.getAmount().replace(".", ","));

		this.description.sendKeys(bean.getDescription());
		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		Utility.swipe((AppiumDriver<?>)driver, Utility.DIRECTION.DOWN, 300);

		WaitManager.get().waitMediumTime();

//		this.nameInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.NAMEINPUT).getElement();
//		this.surnameInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.SURNAMEINPUT).getElement();
//		this.addressInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.ADDRESSINPUT).getElement();
//		this.cityInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CITYINPUT).getElement();
//		this.capInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CAPINPUT).getElement();
//		this.surnameInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.SURNAMEINPUT).getElement();
//		this.provinceInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.PROVINCEINPUT).getElement();
		
//		checkPreFilledData(b);
		
		page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CANCELPAYMENT).getElement().click();
		
		WaitManager.get().waitShortTime();

		this.nameInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.NAMEINPUT).getElement();
		this.surnameInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.SURNAMEINPUT).getElement();
		this.addressInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.ADDRESSINPUT).getElement();
		this.cityInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CITYINPUT).getElement();
		this.capInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CAPINPUT).getElement();
		this.surnameInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.SURNAMEINPUT).getElement();
		this.provinceInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.PROVINCEINPUT).getElement();
		
		this.nameInput.sendKeys(bean.getSenderName());
		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		this.surnameInput.sendKeys(bean.getSenderLastName());
		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		this.addressInput.sendKeys(bean.getSenderAdress());
		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		this.cityInput.sendKeys(bean.getSenderCity());
		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		this.capInput.sendKeys(bean.getSenderCAP());
		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

//		this.provinceInput.click();
//
//		SelectProvinciaModal modal=(SelectProvinciaModal) UiPageManager.get("it.poste.bancoposta.pages.managers")
//				.getPageManager("SelectProvinciaModal", page.getDriver().getClass(), page.getLanguage());
//
//		modal.checkPage();
//
//		modal.selectModalElement(bean.getSenderProv(), 10);

		this.provinceInput.sendKeys(bean.getSenderProv());
		WaitManager.get().waitMediumTime();


		this.confirm=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CONFIRM).getElement();
		

		//sottometto
		this.confirm.click();
		WaitManager.get().waitMediumTime();
		
		try {	
				WebElement popupContinuaButton = page.getParticle(Locators.BollettinoBiancoFormPageMolecola.POPUPCONTINUABUTTON).getElement();
				popupContinuaButton.click();
		} catch (Exception e) {
		}
		
		
		BollettinoConfirmPage confirm=(BollettinoConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BollettinoConfirmPage", page.getDriver().getClass(), page.getLanguage());

		confirm.checkPage();

		//controllo che i dati siano uguali a quelli inseriti nella form
		confirm.check(bean,BollettinoDataBean.BOLLETTINO_896);

		//controllo che la form venga riempita con i dati precedentemente inseriti


		//checkFromBackButton();

		//			switch(this.driverType)
		//			{
		//				case 0://android driver
		//					try {
		//						this.cancellButton=driver.findElement(By.xpath(Locators.BollettinoBiancoFormPageMolecola.BACKBUTTON.getAndroidLocator()));
		//						break;
		//						
		//					} catch (Exception e) {
		//						this.cancellButton=driver.findElement(By.xpath(Locators.BollettinoBiancoFormPageMolecola.CANCELLBUTTON.getAndroidLocator()));
		//					}
		//					
		//				case 1://ios driver
		//					this.cancellButton=driver.findElement(By.xpath(Locators.BollettinoBiancoFormPageMolecola.CANCELLBUTTON.getLocator(driver)));
		//				break;
		//				case 2://web driver
		//				
		//				break;
		//			}
		//			
		//			this.cancellButton.click();

		//			switch(this.driverType)
		//			{
		//				case 0://android driver
		//					this.cancellButton=driver.findElement(By.xpath(Locators.BollettinoBiancoFormPageMolecola.CANCELLBUTTON.getAndroidLocator()));
		//					break;
		//				case 1://ios driver
		//					this.exitFromPayment=driver.findElement(By.xpath(Locators.BollettinoBiancoFormPageMolecola.EXITFROMPAYMENT.getLocator(driver)));
		//					this.exitFromPayment.click();
		//					break;
		//				case 2://web driver
		//				
		//				break;
		//			}
	}

	private void checkPreFilledData(BollettinoDataBean b) 
	{
		System.out.println("AR:" + this.nameInput.getText().toLowerCase().trim());
		System.out.println("ER: "+ b.getSenderName().trim().toLowerCase());
		org.springframework.util.Assert.isTrue(this.nameInput.getText().toLowerCase().trim().equals(b.getSenderName().trim().toLowerCase()), "Campo Nome errato");
		org.springframework.util.Assert.isTrue(this.surnameInput.getText().toLowerCase().trim().equals(b.getSenderLastName().trim().toLowerCase()), "Campo Congnome errato");
		org.springframework.util.Assert.isTrue(this.addressInput.getText().toLowerCase().trim().equals(b.getSenderAdress().trim().toLowerCase()), "Campo Indirizzo errato");
		org.springframework.util.Assert.isTrue(this.cityInput.getText().toLowerCase().trim().equals(b.getSenderCity().trim().toLowerCase()), "Campo Citta non presente");
		org.springframework.util.Assert.isTrue(this.capInput.getText().toLowerCase().trim().equals(b.getSenderCAP().trim().toLowerCase()), "Campo CAP non presente");
		this.provinceInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.PROVINCEINPUT).getElement();
		System.out.println("AR: "+this.provinceInput.getText().toLowerCase()+" ER: "+b.getProvinciaCode().trim().toLowerCase());
		org.springframework.util.Assert.isTrue(this.provinceInput.getText().toLowerCase().trim().contains(b.getProvinciaCode().trim().toLowerCase()), "Campo Provincia non presente");
		
	}

	public void checkFromBackButton() 
	{
		WebDriver driver =page.getDriver();
		
		Utility.swipe((AppiumDriver<?>)driver, Utility.DIRECTION.UP, 300);

		WaitManager.get().waitMediumTime();


		this.bollettinoTypeInfo=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.BOLLETTINOTYPEINFO).getElement();

		String bltype=this.bollettinoTypeInfo.getText();

		Assert.assertTrue("Controllo header tipologia bollettino; attuale:"+bltype+", atteso:"+bean.getBollettinoType(),bltype.equals(bean.getBollettinoType()));

		this.paymentTypeInfo=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.PAYMENTTYPEINFO).getElement();

		String pwInfo=this.paymentTypeInfo.getText();

		Assert.assertTrue("Controllo header paga con; attuale:"+pwInfo+", atteso:"+bean.getPayWith().replace(";", " "),pwInfo.toLowerCase().equals(bean.getPayWith().replace(";", " ")));

		this.ownerInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.OWNERINPUT).getElement();
		this.ccInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CCINPUT).getElement();
		this.amount=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.AMOUNT).getElement();

		String actual=this.ownerInput.getText().trim();
		Assert.assertTrue("controllo owner; attuale:"+actual+", attesa:"+bean.getOwner(),actual.equals(bean.getOwner()));

		actual=this.ccInput.getText().trim();
		Assert.assertTrue("controllo c/c; attuale:"+actual+", attesa:"+bean.getCc(),actual.equals(bean.getCc()));

		actual=this.amount.getText().trim();
		Assert.assertTrue("controllo importo; attuale:"+actual+", attesa:"+bean.getAmount().replace(".", ","),actual.equals(bean.getAmount().replace(".", ",")));

		Utility.swipe((AppiumDriver<?>)driver, Utility.DIRECTION.DOWN, 300);

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		this.nameInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.NAMEINPUT).getElement();
		this.surnameInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.SURNAMEINPUT).getElement();
		this.addressInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.ADDRESSINPUT).getElement();
		this.cityInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CITYINPUT).getElement();
		this.capInput=page.getParticle(Locators.BollettinoBiancoFormPageMolecola.CAPINPUT).getElement();

		actual=this.nameInput.getText().trim();
		Assert.assertTrue("controllo nome; attuale:"+actual+", attesa:"+bean.getSenderName(),actual.equals(bean.getSenderName()));

		actual=this.surnameInput.getText().trim();
		Assert.assertTrue("controllo cognome; attuale:"+actual+", attesa:"+bean.getSenderLastName(),actual.equals(bean.getSenderLastName()));

		actual=this.addressInput.getText().trim();
		Assert.assertTrue("controllo indirizzo; attuale:"+actual+", attesa:"+bean.getSenderAdress(),actual.equals(bean.getSenderAdress()));

		actual=this.cityInput.getText().trim();
		Assert.assertTrue("controllo citt�; attuale:"+actual+", attesa:"+bean.getSenderCity(),actual.equals(bean.getSenderCity()));

		actual=this.capInput.getText().trim();
		Assert.assertTrue("controllo CAP; attuale:"+actual+", attesa:"+bean.getSenderCAP(),actual.equals(bean.getSenderCAP()));


	}

	@Override
	public void submit(BollettinoDataBean b) {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	@Override
	public void setBean(BollettinoDataBean b) {
		this.bean=b;
	}

	
}
