package it.poste.bancoposta.pages.managers;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.BollettinoChooseOperationPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SelectBolletinoTypesPage;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@PageManager(page="BollettinoChooseOperationPage")
@Android
@IOS
public class BollettinoChooseOperationPageMan extends LoadableComponent<BollettinoChooseOperationPageMan> implements BollettinoChooseOperationPage
{
	protected UiPage page;

	/**
	 * @android
	 * @ios
	 * @web
	 */
	private WebElement header;
	/**
	 * @android
	 * @ios
	 * @web
	 */
	private WebElement cancellButton;
	/**
	 * @android
	 * @ios
	 * @web
	 */
	private WebElement headerImage;
	/**
	 * @android
	 * @ios
	 * @web
	 */
	private WebElement title;
	/**
	 * @android
	 * @ios
	 * @web
	 */
	private WebElement qrCodeButton;
	/**
	 * @android
	 * @ios
	 * @web
	 */
	private WebElement formButton;

	/**
	 * @android
	 * @ios
	 * @web
	 */
	private WebElement continueButton;

	/**
	 * @android
	 * @ios
	 * @web
	 */
	private WebElement closeButton;

	/**
	 * @android
	 * @ios
	 * @web
	 */

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() {
		page.get();
	}

	public SelectBolletinoTypesPage gotoCompilaManualmenteSection() 
	{
//		try {
//			//chiude la popup di abilita fotocamera
//			page.getParticle(Locators.BollettinoChooseOperationPageMolecola.CLOSEBUTTON).getElement().click();
//		} catch (Exception e) {
//			
//		}
		WaitManager.get().waitMediumTime();
		try {
			page.getParticle(Locators.BollettinoChooseOperationPageMolecola.FORMBUTTON).visibilityOfElement(20L).click();
		} catch (Exception e) {
			page.getParticle(Locators.BollettinoChooseOperationPageMolecola.CONTINUABUTTON).visibilityOfElement(20L).click();
		}


		SelectBolletinoTypesPage p = (SelectBolletinoTypesPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SelectBolletinoTypesPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();

		return p;
	}

	public void clickAnnulla() 
	{
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoChooseOperationPageMolecola.CANCELLBUTTON).getXPath()));
		page.getParticle(Locators.BollettinoChooseOperationPageMolecola.CANCELLBUTTON).getElement().click();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
//		gotoCompilaManualmenteSection();
		load();
	}
}
