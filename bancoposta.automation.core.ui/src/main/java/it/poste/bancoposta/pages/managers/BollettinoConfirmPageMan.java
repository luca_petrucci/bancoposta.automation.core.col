package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.SoftAssertion;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BollettinoDataBean;
import bean.datatable.QuickOperationCreationBean;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BollettinoConfirmPage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@PageManager(page="BollettinoConfirmPage")
@Android
public class BollettinoConfirmPageMan extends LoadableComponent<BollettinoConfirmPageMan> implements BollettinoConfirmPage{
	protected static final Object DESCRIPTION_TEXT = "Verifica i dati prima di procedere con il pagamento.";
	private static final String TITOLO_TEXT = "\u00E8 tutto corretto?";
	
	UiPage page;
	
	protected BollettinoDataBean b;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement title;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement payWithInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement bollettinoCodeInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement ccInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement ownerInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement causale;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement commissione;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement executorInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement confirm;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoConfirmPageMolecola.HEADER).getXPath()),60);
		page.get();

		this.title=page.getParticle(Locators.BollettinoConfirmPageMolecola.TITLE).getElement();
		this.description=page.getParticle(Locators.BollettinoConfirmPageMolecola.DESCRIPTION).getElement();

		String tit=title.getText();
		System.out.println(tit);

//		Assert.assertTrue("controllo titolo: \u00E8 tutto corretto? ", tit.toLowerCase().equals(TITOLO_TEXT));
        SoftAssertion.get().getAssertions().assertThat(tit.toLowerCase()).withFailMessage("controllo titolo: \u00E8 tutto corretto? ").isEqualTo(TITOLO_TEXT);

		String tit2=description.getText();

//		Assert.assertTrue("controllo titolo: Verifica i dati prima di procedere al pagamento.", tit2.equals(DESCRIPTION_TEXT));
	    SoftAssertion.get().getAssertions().assertThat(tit2).withFailMessage("controllo titolo: Verifica i dati prima di procedere al pagamento.").isEqualTo(DESCRIPTION_TEXT);

	}


	public void check(BollettinoDataBean bean, String type) 
	{
		this.payWithInfo=page.getParticle(Locators.BollettinoConfirmPageMolecola.PAYWITHINFO).getElement();
		try {this.bollettinoCodeInfo=page.getParticle(Locators.BollettinoConfirmPageMolecola.BOLLETTINOCODEINFO).getElement();} catch (Exception err) {}
		this.ccInfo=page.getParticle(Locators.BollettinoConfirmPageMolecola.CCINFO).getElement();
		this.ownerInfo=page.getParticle(Locators.BollettinoConfirmPageMolecola.OWNERINFO).getElement();
		try {this.causale=page.getParticle(Locators.BollettinoConfirmPageMolecola.CAUSALE).getElement();} catch (Exception err) {}
		this.amount=page.getParticle(Locators.BollettinoConfirmPageMolecola.AMOUNT).getElement();
		this.commissione=page.getParticle(Locators.BollettinoConfirmPageMolecola.COMMISSIONE).getElement();
		this.executorInfo=page.getParticle(Locators.BollettinoConfirmPageMolecola.EXECUTORINFO).getElement();
		this.confirm=page.getParticle(Locators.BollettinoConfirmPageMolecola.CONFIRM).getElement();
		this.backButton=page.getParticle(Locators.BollettinoConfirmPageMolecola.BACKBUTTON).getElement();


		//controllo campi
		String pagaCon=this.payWithInfo.getText().trim().toLowerCase(); 
		String fourDigits = pagaCon.substring(pagaCon.length() - 4);
		System.out.println(fourDigits);
		Assert.assertTrue("controllo paga con; attuale:"+fourDigits+", atteso:"+bean.getPayWith().replace(";", " ").substring(bean.getPayWith().length() - 4),fourDigits.equals(bean.getPayWith().replace(";", " ").substring(bean.getPayWith().length() - 4)));

		try 
		{
			String iban=this.bollettinoCodeInfo.getText().trim().toLowerCase();
			Assert.assertTrue("controllo iban; attuale:"+iban+", atteso:"+bean.getBollettinoCode(),iban.equals(bean.getBollettinoCode()));
		} 
		catch (Exception err)
		{
			String cc=this.ccInfo.getText().trim().toLowerCase();
			Assert.assertTrue("controllo c/c; attuale:"+cc+", atteso:"+bean.getCc(),bean.getCc().contains(cc));
		}
		switch(type)
		{
		case BollettinoDataBean.BOLLETTINO_BIANCO:

			String intestato=this.ownerInfo.getText().trim().toLowerCase();
			Assert.assertTrue("controllo intestato a; attuale:"+intestato+", atteso:"+bean.getOwner(),intestato.equals(bean.getOwner()));
			break;

		}

		String amount=this.amount.getText().replace("\u20AC","").replace(",", ".").trim().toLowerCase();
		double actual=Double.parseDouble(amount);
		double expected=Double.parseDouble(bean.getAmount());

		Assert.assertTrue("controllo importo; attuale:"+amount+", atteso:"+expected,actual == expected);

		amount=this.commissione.getText().replace("\u20AC","").replace(",", ".").trim().toLowerCase();
		double commissione=Double.parseDouble(amount);

		//controllo eseguito da
		String eseguito=
				bean.getSenderName()+" "+
						bean.getSenderLastName()+"\n"+
						bean.getSenderAdress()+" - "+
						bean.getSenderCAP()+" "+
						bean.getSenderCity()+" ("+
						bean.getProvinciaCode()+")";

		String executor=this.executorInfo.getText().trim();

		

		Assert.assertTrue("controllo eseguito da; attuale:"+executor+", atteso:"+eseguito,executor.equals(eseguito));

		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 300);

		//controllo tasto paga
		//String totale=""+(actual + commissione);
		//String buttonText="PAGA \u20AC "+totale.replace(".", ",");
		String totale=Utility.toCurrency(actual + commissione,Locale.ITALY);
		totale=(!totale.contains(",")) ? totale+",00" : totale;
		String buttonText="PAGA \u20AC "+totale;


		String paga=this.confirm.getText();
		Assert.assertTrue("controllo testo pulsante paga; attuale:"+paga+", atteso:"+buttonText,paga.equals(buttonText));

		//dopo aver comparato i dati nella confirm page sottometto il bollettino
		this.confirm.click();


		//click su back
		//			this.backButton.click();
	}

	public String getProvincia(String senderProv) 
	{
		InputStream authConfigStream = BollettinoConfirmPage.class.getClassLoader().getResourceAsStream("it/poste/bancoposta/province.properties");

		if (authConfigStream != null) {
			Properties prov = new Properties();
			try 
			{
				prov.load(authConfigStream);

				return prov.getProperty(senderProv);

			} catch (Exception e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					authConfigStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();	
	}

	//		public OKOperationPage submitBollettino(BollettinoDataBean b) 
	//		{
	//		
	//				//clicco su paga
	//				switch(this.driverType)
	//				{
	//				case 0://android driver
	//					this.confirm=driver.findElement(By.xpath(Locators.BollettinoConfirmPageMolecola.CONFIRMPOSTEID.getAndroidLocator()));
	//					break;
	//				case 1://ios
	//					this.confirm=driver.findElement(By.xpath(Locators.BollettinoConfirmPageMolecola.CONFIRMPOSTEID.getLocator(driver)));
	//					break;
	//				}
	//				
	//				this.confirm.click();
	//				
	//				return new InsertPosteIDPage(driver, driverType).get().insertPosteID(b.getPosteId()).get();
	//			}
}

