package it.poste.bancoposta.pages.managers;

import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import android.Utility;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.SoftAssertion;
import bean.datatable.BollettinoDataBean;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;

@PageManager(page="BollettinoConfirmPage")
@IOS
public class BollettinoConfirmPageManIOS extends BollettinoConfirmPageMan {
	
	private WebElement executorInfo2;

	@Override
	protected void load() 
	{
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BollettinoConfirmPageMolecola.HEADER).getXPath()),60);
		page.get();

		this.title=page.getParticle(Locators.BollettinoConfirmPageMolecola.TITLE).getElement();
		this.description=page.getParticle(Locators.BollettinoConfirmPageMolecola.DESCRIPTION).getElement();

		String tit=title.getText();
		System.out.println(tit);

//		Assert.assertTrue("controllo titolo: � tutto corretto? ", tit.toLowerCase().equals(TITOLO_TEXT));

		
		String tit2=description.getText();
		System.out.println(tit2);
		System.out.println(DESCRIPTION_TEXT);

		SoftAssertion.get().getAssertions().assertThat(tit2).withFailMessage("Descrizione errata").isEqualTo(DESCRIPTION_TEXT);
//		Assert.assertTrue("controllo titolo: Verifica i dati prima di procedere al pagamento.", tit2.equals(DESCRIPTION_TEXT));
	}
	
	public void check(BollettinoDataBean bean, String type) 
	{
		this.payWithInfo=page.getParticle(Locators.BollettinoConfirmPageMolecola.PAYWITHINFO).getElement();
		try {this.bollettinoCodeInfo=page.getParticle(Locators.BollettinoConfirmPageMolecola.BOLLETTINOCODEINFO).getElement();} catch (Exception err) {}
		this.ccInfo=page.getParticle(Locators.BollettinoConfirmPageMolecola.CCINFO).getElement();
//		this.ownerInfo=page.getParticle(Locators.BollettinoConfirmPageMolecola.OWNERINFO).getElement();
//		try {this.causale=page.getParticle(Locators.BollettinoConfirmPageMolecola.CAUSALE).getElement();} catch (Exception err) {}
		this.amount=page.getParticle(Locators.BollettinoConfirmPageMolecola.AMOUNT).getElement();
//		this.commissione=page.getParticle(Locators.BollettinoConfirmPageMolecola.COMMISSIONE).getElement();
		this.executorInfo=page.getParticle(Locators.BollettinoConfirmPageMolecola.EXECUTORINFO).getElement();
//		this.executorInfo2=page.getParticle(Locators.BollettinoConfirmPageMolecola.EXECUTORINFOPARTE2).getElement();
		this.confirm=page.getParticle(Locators.BollettinoConfirmPageMolecola.CONFIRM).getElement();
//		this.backButton=page.getParticle(Locators.BollettinoConfirmPageMolecola.BACKBUTTON).getElement();


		//controllo campi
		String pagaCon=this.payWithInfo.getText().trim().toLowerCase(); 
		String fourDigits = pagaCon.substring(pagaCon.length() - 4);
		System.out.println(fourDigits);
		Assert.assertTrue("controllo paga con; attuale:"+fourDigits+", atteso:"+bean.getPayWith().replace(";", " ").substring(bean.getPayWith().length() - 4),fourDigits.equals(bean.getPayWith().replace(";", " ").substring(bean.getPayWith().length() - 4)));

		try 
		{
			String iban=this.bollettinoCodeInfo.getText().trim().toLowerCase();
			Assert.assertTrue("controllo iban; attuale:"+iban+", atteso:"+bean.getBollettinoCode(),iban.equals(bean.getBollettinoCode()));
		} 
		catch (Exception err)
		{
			String cc=this.ccInfo.getText().trim().toLowerCase();
			Assert.assertTrue("controllo c/c; attuale:"+cc+", atteso:"+bean.getCc(),bean.getCc().contains(cc));
		}
//		switch(type)
//		{
//		case BollettinoDataBean.BOLLETTINO_BIANCO:
//
//			String intestato=this.ownerInfo.getText().trim().toLowerCase();
//			Assert.assertTrue("controllo intestato a; attuale:"+intestato+", atteso:"+bean.getOwner(),intestato.equals(bean.getOwner()));
//			break;
//
//		}

		String amount=this.amount.getText().replace("\u20AC","").replace(",", ".").trim().toLowerCase();
		double actual=Double.parseDouble(amount);
		double expected=Double.parseDouble(bean.getAmount());

		Assert.assertTrue("controllo importo; attuale:"+amount+", atteso:"+expected,actual == expected);

//		amount=this.commissione.getText().replace("\u20AC","").replace(",", ".").trim().toLowerCase();
//		double commissione=Double.parseDouble(amount);

		//controllo eseguito da
		String eseguito1=
				bean.getSenderName()+" "+
						bean.getSenderLastName();
		String eseguito2=
				bean.getSenderAdress()+" - "+
				bean.getSenderCAP()+" "+
				bean.getSenderCity()+" ("+
				bean.getProvinciaCode()+")";

		String executor=this.executorInfo.getText().trim();
//		String executor2=this.executorInfo2.getText().trim();
		
		Assert.assertTrue("controllo eseguito da; attuale:"+executor+", atteso:"+eseguito1,executor.equals(eseguito1));
//		Assert.assertTrue("controllo eseguito da; attuale:"+executor2+", atteso:"+eseguito2,executor.equals(eseguito2));

		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 300);

		//controllo tasto paga
		//String totale=""+(actual + commissione);
		//String buttonText="PAGA \u20AC "+totale.replace(".", ",");
//		String totale=Utility.toCurrency(actual + commissione,Locale.ITALY);
//		totale=(!totale.contains(",")) ? totale+",00" : totale;
//		String buttonText="PAGA \u20AC "+totale;
//
//
//		String paga=this.confirm.getText();
//		Assert.assertTrue("controllo testo pulsante paga; attuale:"+paga+", atteso:"+buttonText,paga.equals(buttonText));

		//dopo aver comparato i dati nella confirm page sottometto il bollettino
		this.confirm.click();


		//click su back
		//			this.backButton.click();
	}

}
