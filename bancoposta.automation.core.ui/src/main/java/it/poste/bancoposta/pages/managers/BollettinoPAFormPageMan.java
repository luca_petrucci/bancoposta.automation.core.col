package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BollettinoDataBean;
import it.poste.bancoposta.pages.BollettinoPAFormPage;
import it.poste.bancoposta.pages.IBollettino;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@PageManager(page="BollettinoPAFormPage")
@Android
@IOS
public class BollettinoPAFormPageMan extends LoadableComponent<BollettinoPAFormPageMan> implements IBollettino,BollettinoPAFormPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancellButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement bollettinoTypeInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement changeBollettinoType;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement paymentTypeInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement changePaymentType;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ccEnteInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement avvCodeInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement deletePersonButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement namePerInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement codFiscInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement confirm;
	private BollettinoDataBean bean;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	@Override
	public void checkData(BollettinoDataBean b) {
		// TODO Auto-generated method stub

	}

	@Override
	public void submit(BollettinoDataBean b) {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}
}
