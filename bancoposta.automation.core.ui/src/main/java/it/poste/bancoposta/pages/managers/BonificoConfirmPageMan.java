package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BonificoDataBean;
import bean.datatable.QuickOperationCreationBean;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BonificoConfirmPage;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.BonificoConfirmationPages;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OKOperationPage;
import it.poste.bancoposta.pages.QuickOperationEditable;
import it.poste.bancoposta.pages.SendBonificoPage;
import it.poste.bancoposta.utils.SoftAssertion;
import test.automation.core.UIUtils;

import java.util.Locale;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="BonificoConfirmPage")
@Android
public class BonificoConfirmPageMan extends LoadableComponent<BonificoConfirmPageMan> implements BonificoConfirmationPages,QuickOperationEditable,BonificoConfirmPage
{
	UiPage page;
	
	 static final String TITLE = "� tutto corretto?";
	 static final String DESCRIPTION = "verifica i dati prima di procedere con il pagamento.";
	
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	 WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	 WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	 WebElement title;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	 WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	 WebElement payWithInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	 WebElement ibanInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	 WebElement bonificoOwnerInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	 WebElement countryOwnerInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	 WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	 WebElement commissione;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	 WebElement casualInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	 WebElement confirm;
	 WebElement addressSender;
	 WebElement citySender;
	 WebElement sender;
	 WebElement senderEffective;
	 BonificoDataBean bean;
	 WebElement modalOK;
	 WebElement modify;

	public void BonificoDataBean(BonificoDataBean bonificoDataBean)
	{
		this.bean=bonificoDataBean;
	}

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();

		this.title=page.getParticle(Locators.BonificoConfirmPageMolecola.TITLE).getElement();
		this.description=page.getParticle(Locators.BonificoConfirmPageMolecola.DESCRIPTION).getElement();

		Properties t=page.getLanguage();

		String txt=title.getText().trim().toLowerCase();
		
		System.out.println("header letto:"+txt);
		System.out.println("header ER:"+t.getProperty("eTuttoCorretto").toLowerCase());
		
		SoftAssertion.get().getAssertion().assertThat(txt)
		.withFailMessage("controllo header � tutto corretto")
		.isEqualTo(t.getProperty("eTuttoCorretto").toLowerCase());
		
		System.out.println("check:"+txt.equals(t.getProperty("eTuttoCorretto").toLowerCase()));

		//Assert.assertTrue("controllo header � tutto corretto",txt.equals(TITLE));

		txt=description.getText().trim().toLowerCase();
		
		System.out.println("descr letto:"+txt);
		System.out.println("descr ER:"+t.getProperty("verificaIdatiPrimaDiProcedere").toLowerCase());
		
		SoftAssertion.get().getAssertion().assertThat(txt)
		.withFailMessage("ccontrollo header Verifica i dati prima di procedere con il pagamento.")
		.isEqualTo(t.getProperty("verificaIdatiPrimaDiProcedere").toLowerCase());
		
		System.out.println("check:"+txt.equals(t.getProperty("verificaIdatiPrimaDiProcedere").toLowerCase()));


		//Assert.assertTrue("controllo header Verifica i dati prima di procedere con il pagamento.",txt.equals(DESCRIPTION));

	}

	public void checkData(BonificoDataBean b) 
	{
		WaitManager.get().waitMediumTime();
		
		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.UP,500);
		
		this.payWithInfo=page.getParticle(Locators.BonificoConfirmPageMolecola.PAYWITHINFO).getElement();
		this.ibanInfo=page.getParticle(Locators.BonificoConfirmPageMolecola.IBANINFO).getElement();
		this.bonificoOwnerInfo=page.getParticle(Locators.BonificoConfirmPageMolecola.BONIFICOOWNERINFO).getElement();
		this.countryOwnerInfo=page.getParticle(Locators.BonificoConfirmPageMolecola.COUNTRYOWNERINFO).getElement();
		this.amount=page.getParticle(Locators.BonificoConfirmPageMolecola.AMOUNT).getElement();
		this.commissione=page.getParticle(Locators.BonificoConfirmPageMolecola.COMMISSIONE).getElement();
		this.casualInfo=page.getParticle(Locators.BonificoConfirmPageMolecola.CASUALINFO).getElement();
		

		//controllo paga con
		String txt=this.payWithInfo.getText().trim().toLowerCase().trim();
		String txt4 = txt.substring(txt.length() - 4);
		String atteso = b.getPayWith().toLowerCase().replace(";", " ").trim();
		String atteso4 = atteso.substring(atteso.length() - 4).trim();
		//			System.out.println(payWithInfo.getText().trim().toLowerCase());
		//System.out.println(b + "Questa � la b dopo");
		//			System.out.println("Questa � la stampa" + b.getPayWith().toLowerCase().replace(";", " ") + txt.equals(b.getPayWith().toLowerCase().replace(";", " ")));

		Assert.assertTrue("controllo paga con;attuale:"+txt4+", atteso:"+ atteso4,txt4.equals(atteso4));

		//controllo c/c iban
		txt=this.ibanInfo.getText().trim();

		Assert.assertTrue("controllo iban;attuale:"+txt+", atteso:"+b.getIban(),txt.equals(b.getIban()));

		//controllo intestato a
		txt=this.bonificoOwnerInfo.getText().trim().toLowerCase();

		Assert.assertTrue("controllo intestato a;attuale:"+txt+", atteso:"+b.getOwner().toLowerCase(),txt.equals(b.getOwner().toLowerCase()));

		//controllo importo
		double amount=Utility.parseCurrency(this.amount.getText().trim(), Locale.ITALY);
		double tocheck=Double.parseDouble(b.getAmount());

		Assert.assertTrue("controllo importo;attuale:"+amount+", atteso:"+tocheck,amount==tocheck);

		double commissioni=Utility.parseCurrency(this.commissione.getText().trim(), Locale.ITALY);

		txt=this.casualInfo.getText().trim().toLowerCase();

		Assert.assertTrue("controllo casuale;attuale:"+txt+", atteso:"+b.getDescription().toLowerCase(),txt.equals(b.getDescription().toLowerCase()));

		//controllo paese
		txt=this.countryOwnerInfo.getText().trim().toLowerCase();

		Assert.assertTrue("controllo nazione;attuale:"+txt+", atteso:"+b.getCity().toLowerCase(),txt.equals(b.getCity().toLowerCase()));

		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);

		WaitManager.get().waitShortTime();
		
		this.confirm=page.getParticle(Locators.BonificoConfirmPageMolecola.CONFIRM).getElement();
		

		//controllo che l'interfaccia non mandatory sia presente
		//in quanto essa viene mostrata solo se nella pagina precedente
		//i campi facoltativi vengono inseriti
		BonificoConfirmSubPage subPage=(BonificoConfirmSubPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BonificoConfirmSubPage", page.getDriver().getClass(), page.getLanguage());
		
		subPage.checkPage();

		//pagina presente e campi presenti procedo con la verifica dei dati mostrati
		if(subPage.ok())
		{
			subPage.checkData(b);
		}

		//controlla paga con
		String paga="paga \u20AC "+(Utility.toCurrency(amount+commissioni, Locale.ITALY));

		Assert.assertTrue("controllo paga;attuale:"+this.confirm.getText().trim().toLowerCase()+", atteso:"+paga,this.confirm.getText().trim().toLowerCase().contains(paga));

	}

	@Override
	public OKOperationPage submit(BonificoDataBean b) 
	{
		this.confirm=page.getParticle(Locators.BonificoConfirmPageMolecola.CONFIRM).getElement();
		

		this.confirm.click();

		InsertPosteIDPage posteId=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());

		return posteId.insertPosteID(b.getPosteid());
	}

	@Override
	public void isEditable(QuickOperationCreationBean b) 
	{
		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);

		//clicco su modifica
		this.modify=page.getParticle(Locators.BonificoConfirmPageMolecola.MODIFY).getElement();
		

		this.modify.click();

		SendBonificoPage p=(SendBonificoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SendBonificoPage", page.getDriver().getClass(), page.getLanguage());
		
		p.checkPage();

		p.isEditable(bean);
	}

	@Override
	public OKOperationPage submit(QuickOperationCreationBean b, String campo, String value) 
	{
		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);

		if(campo != null)
		{
			//clicco su modifica
			this.modify=page.getParticle(Locators.BonificoConfirmPageMolecola.MODIFY).getElement();
			

			this.modify.click();

			SendBonificoPage p=(SendBonificoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("SendBonificoPage", page.getDriver().getClass(), page.getLanguage());
			
			p.checkPage();

			return p.sendBonifico(bean,campo,value);
		}
		else
		{
			//clicco su paga
			this.confirm=page.getParticle(Locators.BonificoConfirmPageMolecola.CONFIRM).getElement();
			
			this.confirm.click();

			InsertPosteIDPage p = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
			
			p.checkPage();
			
			return p.insertPosteID(b.getPosteid());
		}
	}

	@Override
	public void clickBack() {
		// TODO Auto-generated method stub

	}

	public void clickOKModal() 
	{
		try
		{
			UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.visibilityOfElementLocated(page.getParticle(Locators.BonificoConfirmPageMolecola.MODALOK).getXPath()),30);
			this.modalOK=page.getParticle(Locators.BonificoConfirmPageMolecola.MODALOK).getElement();
			

			this.modalOK.click();
			
			WaitManager.get().waitShortTime();
		}
		catch(Exception err)
		{

		}
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

	@Override
	public void setBean(QuickOperationCreationBean quickOperationCreationBean) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBean(bean.datatable.BonificoDataBean bonificoDataBean) {
		this.bean=bonificoDataBean;
	}

	@Override
	public void clikOnModifica() {
		WebElement modificaButton =page.getParticle(Locators.BonificoConfirmPageMolecola.MODIFY).getElement();
		modificaButton.click();
	}
}
