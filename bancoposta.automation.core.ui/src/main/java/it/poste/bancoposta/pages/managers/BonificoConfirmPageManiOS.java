package it.poste.bancoposta.pages.managers;

import java.util.Locale;
import java.util.Properties;

import org.junit.Assert;
import org.w3c.dom.Node;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.QuickObjectFinder;
import bean.datatable.BonificoDataBean;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.utils.SoftAssertion;

@PageManager(page="BonificoConfirmPage")
@IOS
public class BonificoConfirmPageManiOS extends BonificoConfirmPageMan 
{
	protected void load() 
	{
		page.getQuick();

		this.title=page.getParticle(Locators.BonificoConfirmPageMolecola.TITLE).getElement();
		this.description=page.getParticle(Locators.BonificoConfirmPageMolecola.DESCRIPTION).getElement();

		Properties t=page.getLanguage();
		
		String txt=title.getText().trim().toLowerCase();
		
		System.out.println("header letto:"+txt);
		System.out.println("header ER:"+t.getProperty("eTuttoCorretto").toLowerCase());
		
		SoftAssertion.get().getAssertion().assertThat(txt)
		.withFailMessage("controllo header � tutto corretto")
		.isEqualTo(t.getProperty("eTuttoCorretto").toLowerCase());
		
		System.out.println("check:"+txt.equals(t.getProperty("eTuttoCorretto").toLowerCase()));

		//Assert.assertTrue("controllo header � tutto corretto",txt.equals(TITLE));

		txt=description.getText().trim().toLowerCase();
		
		System.out.println("descr letto:"+txt);
		System.out.println("descr ER:"+t.getProperty("verificaIdatiPrimaDiProcedere").toLowerCase());
		
		SoftAssertion.get().getAssertion().assertThat(txt)
		.withFailMessage("ccontrollo header Verifica prima di procedere con il pagamento.")
		.isEqualTo(t.getProperty("verificaIdatiPrimaDiProcedere").toLowerCase());
		
		System.out.println("check:"+txt.equals(t.getProperty("verificaIdatiPrimaDiProcedere").toLowerCase()));

		//Assert.assertTrue("controllo header Verifica i dati prima di procedere con il pagamento.",txt.equals(DESCRIPTION));

	}
	
	public void checkData(BonificoDataBean b) 
	{
		System.out.println(b.getControlloPaeseBonifico().trim());
		if(b.getControlloPaeseBonifico().equals("poste".trim()))
		{
			String pageSource=page.getDriver().getPageSource();
			Node payWithInfo=QuickObjectFinder.getElement(pageSource, page.getParticle(Locators.BonificoConfirmPostePageMolecola.PAYWITHINFOPOSTE).getLocator());
			Node ibanInfo=QuickObjectFinder.getElement(pageSource,page.getParticle(Locators.BonificoConfirmPostePageMolecola.IBANINFOPOSTE).getLocator());
			Node bonificoOwnerInfo=QuickObjectFinder.getElement(pageSource,page.getParticle(Locators.BonificoConfirmPostePageMolecola.BONIFICOOWNERINFOPOSTE).getLocator());
			Node am=QuickObjectFinder.getElement(pageSource,page.getParticle(Locators.BonificoConfirmPostePageMolecola.AMOUNTPOSTE).getLocator());
			Node commissione=QuickObjectFinder.getElement(pageSource,page.getParticle(Locators.BonificoConfirmPostePageMolecola.COMMISSIONEPOSTE).getLocator());
			Node casualInfo=QuickObjectFinder.getElement(pageSource,page.getParticle(Locators.BonificoConfirmPostePageMolecola.CASUALINFOPOSTE).getLocator());
			
			System.out.println("payWithInfo:"+payWithInfo.getAttributes().getNamedItem("label").getNodeValue());
			System.out.println("bonificoOwnerInfo:"+bonificoOwnerInfo.getAttributes().getNamedItem("label").getNodeValue());
			System.out.println("am:"+am.getAttributes().getNamedItem("label").getNodeValue());
			System.out.println("commissione:"+commissione.getAttributes().getNamedItem("label").getNodeValue());
			System.out.println("casualInfo:"+casualInfo.getAttributes().getNamedItem("label").getNodeValue());
			
			String txt=payWithInfo.getAttributes().getNamedItem("label").getNodeValue().trim().toLowerCase().trim();
			String txt4 = txt.substring(txt.length() - 4);
			String atteso = b.getPayWith().toLowerCase().replace(";", " ").trim();
			String atteso4 = atteso.substring(atteso.length() - 4).trim();

			Assert.assertTrue("controllo paga con;attuale:"+txt4+", atteso:"+ atteso4,txt4.equals(atteso4));

			//controllo c/c iban
			txt=ibanInfo.getAttributes().getNamedItem("label").getNodeValue().trim();
			Assert.assertTrue("controllo iban;attuale:"+txt+", atteso:"+b.getIban(),txt.equals(b.getIban()));

			//controllo intestato a
			txt=bonificoOwnerInfo.getAttributes().getNamedItem("label").getNodeValue().trim().toLowerCase();
			Assert.assertTrue("controllo intestato a;attuale:"+txt+", atteso:"+b.getOwner().toLowerCase(),txt.equals(b.getOwner().toLowerCase()));

			//controllo importo
			double amount=Utility.parseCurrency(am.getAttributes().getNamedItem("label").getNodeValue().trim(), Locale.ITALY);
			double tocheck=Double.parseDouble(b.getAmount());
			Assert.assertTrue("controllo importo;attuale:"+amount+", atteso:"+tocheck,amount==tocheck);

			double commissioni=Utility.parseCurrency(commissione.getAttributes().getNamedItem("label").getNodeValue().trim(), Locale.ITALY);
			txt=casualInfo.getAttributes().getNamedItem("label").getNodeValue().trim().toLowerCase();
			Assert.assertTrue("controllo casuale;attuale:"+txt+", atteso:"+b.getDescription().toLowerCase(),txt.equals(b.getDescription().toLowerCase()));

			Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);
		}
		else {
		WaitManager.get().waitMediumTime();
		
		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.UP,500);
		
		WaitManager.get().waitShortTime();
		
		String pageSource=page.getDriver().getPageSource();
		
		Node payWithInfo=QuickObjectFinder.getElement(pageSource, page.getParticle(Locators.BonificoConfirmPageMolecola.PAYWITHINFO).getLocator());
		
		Node ibanInfo=QuickObjectFinder.getElement(pageSource,page.getParticle(Locators.BonificoConfirmPageMolecola.IBANINFO).getLocator());
		Node bonificoOwnerInfo=QuickObjectFinder.getElement(pageSource,page.getParticle(Locators.BonificoConfirmPageMolecola.BONIFICOOWNERINFO).getLocator());
//		Node countryOwnerInfo=QuickObjectFinder.getElement(pageSource,page.getParticle(Locators.BonificoConfirmPageMolecola.COUNTRYOWNERINFO).getLocator());
		Node am=QuickObjectFinder.getElement(pageSource,page.getParticle(Locators.BonificoConfirmPageMolecola.AMOUNT).getLocator());
		Node commissione=QuickObjectFinder.getElement(pageSource,page.getParticle(Locators.BonificoConfirmPageMolecola.COMMISSIONE).getLocator());
		Node casualInfo=QuickObjectFinder.getElement(pageSource,page.getParticle(Locators.BonificoConfirmPageMolecola.CASUALINFO).getLocator());
		
		System.out.println("payWithInfo:"+payWithInfo.getAttributes().getNamedItem("label").getNodeValue());
		System.out.println("bonificoOwnerInfo:"+bonificoOwnerInfo.getAttributes().getNamedItem("label").getNodeValue());
//		System.out.println("countryOwnerInfo:"+countryOwnerInfo.getAttributes().getNamedItem("label").getNodeValue());
		System.out.println("am:"+am.getAttributes().getNamedItem("label").getNodeValue());
		System.out.println("commissione:"+commissione.getAttributes().getNamedItem("label").getNodeValue());
		System.out.println("casualInfo:"+casualInfo.getAttributes().getNamedItem("label").getNodeValue());
		
		
		//controllo paga con
		String txt=payWithInfo.getAttributes().getNamedItem("label").getNodeValue().trim().toLowerCase().trim();
		String txt4 = txt.substring(txt.length() - 4);
		String atteso = b.getPayWith().toLowerCase().replace(";", " ").trim();
		String atteso4 = atteso.substring(atteso.length() - 4).trim();
		//			System.out.println(payWithInfo.getText().trim().toLowerCase());
		//System.out.println(b + "Questa � la b dopo");
		//			System.out.println("Questa � la stampa" + b.getPayWith().toLowerCase().replace(";", " ") + txt.equals(b.getPayWith().toLowerCase().replace(";", " ")));

		Assert.assertTrue("controllo paga con;attuale:"+txt4+", atteso:"+ atteso4,txt4.equals(atteso4));

		//controllo c/c iban
		txt=ibanInfo.getAttributes().getNamedItem("label").getNodeValue().trim();

		Assert.assertTrue("controllo iban;attuale:"+txt+", atteso:"+b.getIban(),txt.equals(b.getIban()));

		//controllo intestato a
		txt=bonificoOwnerInfo.getAttributes().getNamedItem("label").getNodeValue().trim().toLowerCase();

		Assert.assertTrue("controllo intestato a;attuale:"+txt+", atteso:"+b.getOwner().toLowerCase(),txt.equals(b.getOwner().toLowerCase()));

		//controllo importo
		double amount=Utility.parseCurrency(am.getAttributes().getNamedItem("label").getNodeValue().trim(), Locale.ITALY);
		double tocheck=Double.parseDouble(b.getAmount());

		Assert.assertTrue("controllo importo;attuale:"+amount+", atteso:"+tocheck,amount==tocheck);

		double commissioni=Utility.parseCurrency(commissione.getAttributes().getNamedItem("label").getNodeValue().trim(), Locale.ITALY);

		txt=casualInfo.getAttributes().getNamedItem("label").getNodeValue().trim().toLowerCase();

		Assert.assertTrue("controllo casuale;attuale:"+txt+", atteso:"+b.getDescription().toLowerCase(),txt.equals(b.getDescription().toLowerCase()));

		//controllo paese
//		txt=countryOwnerInfo.getAttributes().getNamedItem("label").getNodeValue().trim().toLowerCase();
//
//		Assert.assertTrue("controllo nazione;attuale:"+txt+", atteso:"+b.getCity().toLowerCase(),txt.equals(b.getCity().toLowerCase()));
//
		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);
//
//		WaitManager.get().waitShortTime();
//		
//		pageSource=page.getDriver().getPageSource();
//		
//		Node confirm=QuickObjectFinder.getElement(pageSource,page.getParticle(Locators.BonificoConfirmPageMolecola.CONFIRM).getLocator());
//		
//
//		//controllo che l'interfaccia non mandatory sia presente
//		//in quanto essa viene mostrata solo se nella pagina precedente
//		//i campi facoltativi vengono inseriti
//		BonificoConfirmSubPage subPage=(BonificoConfirmSubPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
//				.getPageManager("BonificoConfirmSubPage", page.getDriver().getClass(), page.getLanguage());
//		
//		subPage.checkPage();
//
//		//pagina presente e campi presenti procedo con la verifica dei dati mostrati
//		if(subPage.ok())
//		{
//			subPage.checkData(b);
//		}
//
//		//controlla paga con
//		String paga="paga \u20AC"+(Utility.toCurrency(amount+commissioni, Locale.ITALY));
//
//		Assert.assertTrue("controllo paga;attuale:"+confirm.getAttributes().getNamedItem("label").getNodeValue().trim().toLowerCase()+", atteso:"+paga,confirm.getAttributes().getNamedItem("label").getNodeValue().trim().toLowerCase().contains(paga));
		}
	}
}
