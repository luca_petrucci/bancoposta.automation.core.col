package it.poste.bancoposta.pages.managers;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.w3c.dom.Node;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.QuickObjectFinder;
import bean.datatable.BonificoDataBean;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;

@PageManager(page="BonificoConfirmSubPage")
@IOS
public class BonificoConfirmSubPageManiOS extends BonificoConfirmSubPageMan 
{
	protected void load() 
	{
		String pageSource=page.getDriver().getPageSource();
		
		Assert.assertTrue(QuickObjectFinder.elementExists(pageSource,page.getParticle(Locators.BonificoConfirmPageMolecola.ADDRESSSENDER).getLocator()));
		founds++;
		Assert.assertTrue(QuickObjectFinder.elementExists(pageSource,page.getParticle(Locators.BonificoConfirmPageMolecola.CITYSENDER).getLocator()));
		founds++;
		Assert.assertTrue(QuickObjectFinder.elementExists(pageSource,page.getParticle(Locators.BonificoConfirmPageMolecola.SENDER).getLocator()));
		founds++;
		Assert.assertTrue(QuickObjectFinder.elementExists(pageSource,page.getParticle(Locators.BonificoConfirmPageMolecola.SENDEREFFECTIVE).getLocator()));
		founds++;


	}
	
	public void checkData(BonificoDataBean b) 
	{
		String pageSource=page.getDriver().getPageSource();
		
		Node addressSender=QuickObjectFinder.getElement(pageSource,page.getParticle(Locators.BonificoConfirmPageMolecola.ADDRESSSENDER).getLocator());
		Node citySender=QuickObjectFinder.getElement(pageSource,page.getParticle(Locators.BonificoConfirmPageMolecola.CITYSENDER).getLocator());
		Node sender=QuickObjectFinder.getElement(pageSource,page.getParticle(Locators.BonificoConfirmPageMolecola.SENDER).getLocator());
		Node senderEffective=QuickObjectFinder.getElement(pageSource,page.getParticle(Locators.BonificoConfirmPageMolecola.SENDEREFFECTIVE).getLocator());


		//controllo indirizzo
		String txt=addressSender.getAttributes().getNamedItem("label").getNodeValue().trim();
		String toCheck=b.getAddress();

		Assert.assertTrue("controllo indirizzo;attuale:"+txt+", atteso:"+toCheck,txt.equals(toCheck));

		//controllo localit�
		txt=citySender.getAttributes().getNamedItem("label").getNodeValue().trim();
		toCheck=b.getCitySender();

		Assert.assertTrue("controllo localit�;attuale:"+txt+", atteso:"+toCheck,txt.equals(toCheck));

		//controllo riferimento beneficiario
		txt=sender.getAttributes().getNamedItem("label").getNodeValue().trim();
		toCheck=b.getReference();

		//TODO:da sbloccare al termine del test
		Assert.assertTrue("controllo riferimento beneficiario;attuale:"+txt+", atteso:"+toCheck,txt.equals(toCheck));

		//controllo beneficiario effettivo
		txt=senderEffective.getAttributes().getNamedItem("label").getNodeValue().trim();
		toCheck=b.getRefecenceEffective();

		Assert.assertTrue("controllo beneficiario effettivo;attuale:"+txt+", atteso:"+toCheck,txt.equals(toCheck));
	}
	
	public boolean ok() 
	{
		return founds == 4;
	}
}
