package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BonificoDataBean;
import bean.datatable.BuoniDataBean;
import bean.datatable.CheckTransactionBean;
import bean.datatable.TransactionMovementsBean;
import bean.datatable.VoucherVerificationBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.HidesKeyboard;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.BuoniLibrettiPage;
import it.poste.bancoposta.pages.BuoniPostaliPage;
import it.poste.bancoposta.pages.HomepageHamburgerMenu;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OKOperationPage;
import test.automation.core.UIUtils;
import utils.DinamicData;

import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="BuoniLibrettiPage")
@Android
public class BuoniLibrettiPageMan extends LoadableComponent<BuoniLibrettiPageMan> implements BuoniLibrettiPage{
	private static final String SALDO_CONTABILE = "BUONI_LIBRETTI_SALDO_CONTABILE";
	
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement bookletTab;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement vaucherTab;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement gestisciButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement search;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement filterButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement movementList;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement movementElement;
	/**
	 * @android 
	 * @ios campo dinamico sostituire $(index) con l'indice dell'elemento da ricercare
	 * @web 
	 */
	private WebElement movementDescription;
	/**
	 * @android 
	 * @ios campo dinamico sostituire $(index) con l'indice dell'elemento da ricercare
	 * @web 
	 */
	private WebElement movementAmount;
	/**
	 * @android 
	 * @ios campo dinamico sostituire $(index) con l'indice dell'elemento da ricercare
	 * @web 
	 */
	private WebElement movementDate;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cardDiv;
	/**
	 * @android 
	 * @ios campo dinamico sostituire $(cardName) con il valore della carta da ricercare
	 * @web 
	 */
	private WebElement cardTitle;
	/**
	 * @android 
	 * @ios campo dinamico sostituire $(cardNumber) con il numero della carta da ricercare
	 * @web 
	 */
	private WebElement cardNumber;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cardAmount;
	private WebElement rightNavigationButton;
	private WebElement saldoDisponibile;
	protected WebElement ricercaTransazioniInput;
	private WebElement leftMenu;
	private WebElement ricaricaLaTuaPostepayButton;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public BuoniPostaliPage gotoBuoniPostali() 
	{
		this.vaucherTab=page.getParticle(Locators.BuoniLibrettiPageMolecola.VAUCHERTAB).getElement();
		

		this.vaucherTab.click();

		BuoniPostaliPage p=(BuoniPostaliPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BuoniPostaliPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();

		return p;
	}

	public BuoniPostaliPage clickOnConto(VoucherVerificationBean b) {

		String element="";
		String title="";
		String detail="";

		element=page.getParticle(Locators.PayWithPageMolecola.PRODUCTELEMENT).getLocator();
		title=page.getParticle(Locators.PayWithPageMolecola.PRODUCTTYPE).getLocator();
		detail=page.getParticle(Locators.PayWithPageMolecola.PRODUCTDETAILTYPE).getLocator();

		String payWith = b.getPayWith() ;
		System.out.println(payWith);
		String[] payWithInfo=payWith.split(";");
		System.out.println(payWithInfo);

		List<WebElement> list=page.getDriver().findElements(By.xpath(element));

		for(WebElement e : list)
		{
			String titleTxt=e.findElement(By.xpath(title)).getText().trim().toLowerCase();
			String descrTxt=e.findElement(By.xpath(detail)).getText().trim().toLowerCase(); 
			String fourDigits = descrTxt.substring(descrTxt.length() - 4);
			if(payWithInfo[0].equals(titleTxt) && payWithInfo[1].contains(fourDigits))
			{descrTxt.substring(descrTxt.length() - 4);
			e.click();
			break;
			}
		}

		return null;

	}

	public BuoniPostaliPage sottoscriviBuono() 
	{
		WaitManager.get().waitMediumTime();
		System.out.println(page.getDriver().getPageSource());	
		
		try {
			WebElement sottoscriviBuonoButton = page.getParticle(Locators.BuoniPostaliPageMolecola.SUBSCRIBEVOUCHERBUTTON).getElement();
			sottoscriviBuonoButton.click();
		} catch (Exception e) {
			WebElement sottoscriviBuonoButton = page.getParticle(Locators.BuoniPostaliPageMolecola.SUBSCRIBEVOUCHERBUTTON_EMPTY).getElement();
			sottoscriviBuonoButton.click();
		}

		return null;
	}

	public void clickOnBuono(VoucherVerificationBean b) 
	{
		WaitManager.get().waitShortTime();
		System.out.println(page.getDriver().getPageSource());
		System.out.println(b.getVoucherType());
		WebElement buono=page.getParticle(Locators.BuoniPostaliPageMolecola.BUONO_BUTTON).getElement(new Entry("buono",b.getVoucherType()));
		buono.click();
		WaitManager.get().waitMediumTime();
//		switch(b.getVoucherType())
//		{
//		case "BUONO ORDINARIO"://android driver
//			WebElement buonoOrdinarioButton = ((AndroidDriver)driver).findElementByAndroidUIAutomator("new UiSelector().text(\"#str#\")".replace("#str#",b.getVoucherType()));
//			buonoOrdinarioButton.click();
//			break;
//		case "BUONO FRUTTIFERO POSTALE BFP3X4"://ios driver
//			WebElement buonoFruttiferoButton = ((AndroidDriver)driver).findElementByAndroidUIAutomator("new UiSelector().text(\"#str#\")".replace("#str#",b.getVoucherType()));
//			buonoFruttiferoButton.click();
//			break;
//		case "BFP 4X4"://web driver
//			WebElement bfp4x4Button = ((AndroidDriver)driver).findElementByAndroidUIAutomator("new UiSelector().text(\"#str#\")".replace("#str#",b.getVoucherType()));
//			bfp4x4Button.click();
//			break;
//		case "BFP 3X2":
//			WebElement bfp3x2Button = ((AndroidDriver)driver).findElementByAndroidUIAutomator("new UiSelector().text(\"#str#\")".replace("#str#",b.getVoucherType()));
//			bfp3x2Button.click();
//			break;
//		}	

		
	}

	public BuoniPostaliPage changeSubscriptionMethod(VoucherVerificationBean c) {

		try {
			Thread.sleep(1000);
		} catch (Exception e) {

		}

		WebElement changeSubscriptionButton =page.getParticle(Locators.BuoniPostaliPageMolecola.CHANGESUBSCRIPTIONBUTTON).getElement() ;
		changeSubscriptionButton.click();
		try {
			Thread.sleep(1000);
		} catch (Exception e) {

		}
		//clicco sul metodo di sottoscrizione
		clickOnConto(c);

		return null;
	}

	public BuoniPostaliPage changeAmount(VoucherVerificationBean c) {

		WebElement amount = page.getParticle(Locators.BuoniPostaliPageMolecola.AMOUNT).getElement();
		String importoAttuale = amount.getText().trim().replace(",", ".");
		String getAmount = c.getAmount();
		String getAmount2 = c.getAmount2();

		WaitManager.get().waitMediumTime();

		if (importoAttuale.equals(getAmount)) {
			WebElement plusButton =page.getParticle(Locators.BuoniPostaliPageMolecola.PLUSBUTTON).getElement() ;
			plusButton.click();
			importoAttuale = amount.getText().trim().replace(",", ".");
		}

		WaitManager.get().waitMediumTime();

		if (importoAttuale.equals(getAmount2)) {
			WebElement minusButton =page.getParticle(Locators.BuoniPostaliPageMolecola.MINUSBUTTON).getElement() ;
			minusButton.click();
			importoAttuale = amount.getText().trim().replace(",", ".");
		}
		
		
		WaitManager.get().waitMediumTime();


		return null;
	}

	public OKOperationPage clickOnConfirm(VoucherVerificationBean b) 
	{
		page.getParticle(Locators.BuoniPostaliPageMolecola.CONFIRMBUTTON).visibilityOfElement(10L)
		.click();
		//clicco su condizioni generali di servizio
		page.getParticle(Locators.BuoniPostaliPageMolecola.CONDIZIONI_GENERALI).visibilityOfElement(null)
		.click();
		
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 500);
		
		WaitManager.get().waitShortTime();

		WebElement confirmButton = page.getParticle(Locators.BuoniPostaliPageMolecola.SOTTOSCRIVIBUTTON).getElement();
		confirmButton.click();
		
		WaitManager.get().waitShortTime();
		
		InsertPosteIDPage posteId = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
		OKOperationPage ok=posteId.insertPosteID(b.getPosteId());
		
		ok.checkPage();
		
		return ok;

	}


	public void navigateToLibretto(String payWith) 
	{
//		this.rightNavigationButton=page.getParticle(Locators.BuoniLibrettiPageMolecola.RIGHTNAVIGATIONBUTTON).getElement();		
		String libretto=payWith.split(";")[1];
		libretto = libretto.charAt(8)+libretto.charAt(9)+libretto.charAt(10)+libretto.charAt(11)+"";
		
		List<WebElement> tabList = page.getDriver().findElements(page.getParticle(Locators.BuoniLibrettiPageMolecola.LISTATABLIBRETTI).getXPath());
		System.out.println("Libretto Target: "+libretto);
		
		for (int i = 0; i < tabList.size(); i++) {
			WebElement el = tabList.get(i);
			System.out.println("Libretto presente nel TAB: "+el.getText());
			
			if(el.getText().contains(libretto)) {
				System.out.println("Libretto presente nel TAB: "+el.getText()+ " continene il Libretto Target");
				el.click();
				WaitManager.get().waitShortTime();
				return ;
			}
		}
		
		//navigo fino alla pagina del conto desiderato
//		for(int i=0;i<5;i++)
//		{
//			try 
//			{
//				String conto=payWith.split(";")[1];
//
//				if (((AndroidDriver)page.getDriver()).findElementsByAndroidUIAutomator("new UiSelector().textContains(\"#str#\")".replace("#str#",conto)).size()>0) {
//					WebElement tab = (WebElement) ((AndroidDriver)page.getDriver()).findElementsByAndroidUIAutomator("new UiSelector().textContains(\"#str#\")".replace("#str#",conto)).get(0);
//					tab.click();
//					return ;							
//				}
//
//			} catch (Exception e) {
//				this.rightNavigationButton.click();
//				WaitManager.get().waitShortTime();
//				// TODO Auto-generated catch block
//			}
//		}

//		BuoniLibrettiPage p = (BuoniLibrettiPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
//				.getPageManager("BuoniLibrettiPage", page.getDriver().getClass(), page.getLanguage());
//		p.checkPage();

	}

	public void searchTransaction (TransactionMovementsBean b) {


		this.ricercaTransazioniInput=page.getParticle(Locators.BuoniLibrettiPageMolecola.SEARCH).getElement();
		ricercaTransazioniInput.click();

		ricercaTransazioniInput.sendKeys(b.getTransactionType());
		((HidesKeyboard) page.getDriver()).hideKeyboard();
	}


	public void checkTransaction(TransactionMovementsBean b) 
	{
		page.getParticle(Locators.ContiCartePageMolecola.MOVMENTTYPE).getElement(new Entry("title", b.getTransactionType())).isDisplayed();
//		((AndroidDriver) page.getDriver()).findElementByAndroidUIAutomator("new UiSelector().textContains(\"#str#\")".replace("#str#",b.getTransactionType()));	
	}


	public HomepageHamburgerMenu openHamburgerMenu() 
	{
		//clicco sul menu paga
		this.leftMenu=page.getParticle(Locators.ContiCartePageMolecola.LEFTMENU).getElement();
		

		this.leftMenu.click();

		HomepageHamburgerMenu h=(HomepageHamburgerMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomepageHamburgerMenu", page.getDriver().getClass(), page.getLanguage());

		h.checkPage();

		return h;
	}

	public void navigateToRicaricaLaTuaPostepay() 
	{
		this.gestisciButton=page.getParticle(Locators.BuoniPostaliPageMolecola.GESTISCIBUTTON).getElement();
		
		this.gestisciButton.click();

		this.ricaricaLaTuaPostepayButton=page.getParticle(Locators.BuoniPostaliPageMolecola.CARICALATUAPOSTEPAYBUTTON).getElement();
		
		this.ricaricaLaTuaPostepayButton.click();
	}

	public CheckTransactionBean clickOnTransaction(TransactionMovementsBean b) 
	{
		WebElement desc=page.getParticle(Locators.BuoniLibrettiPageMolecola.MOVEMENTDESCRIPTION).getElement();
		WebElement date=page.getParticle(Locators.BuoniLibrettiPageMolecola.MOVEMENTDATE).getElement();
		WebElement amount=page.getParticle(Locators.BuoniLibrettiPageMolecola.MOVEMENTAMOUNT).getElement();


		CheckTransactionBean t=new CheckTransactionBean();
		t.setDescription(desc.getText().trim());
		t.setDate(date.getText().trim());
		t.setAmount(amount.getText().trim());

		System.out.println("description:"+t.getDescription());
		System.out.println("date:"+t.getDate());
		System.out.println("amount:"+t.getAmount());

		desc.click();


		return t;
	}

	public void clickContinua() 
	{
		page.getParticle(Locators.BuoniLibrettiPageMolecola.CONTINUA_TO_SCELTA_BUONO).visibilityOfElement(null)
		.click();
		
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

	
}
