package it.poste.bancoposta.pages.managers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import bean.datatable.TransactionMovementsBean;
import it.poste.bancoposta.pages.Locators;

@PageManager(page="BuoniLibrettiPage")
@IOS
public class BuoniLibrettiPageManIOS extends BuoniLibrettiPageMan {


	public void searchTransaction (TransactionMovementsBean b) {


		this.ricercaTransazioniInput=page.getParticle(Locators.BuoniLibrettiPageMolecola.SEARCH).getElement();
		ricercaTransazioniInput.click();

		ricercaTransazioniInput.sendKeys(b.getTransactionType());
		WebElement el = null;
		try {
			el = page.getDriver().findElement(By.xpath("//*[contains(@*,'Fine')]"));	
		} catch (Exception err) {
			
		}


	}
}
