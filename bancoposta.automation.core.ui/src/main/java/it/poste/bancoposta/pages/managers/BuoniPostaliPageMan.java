package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.VoucherVerificationBean;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.BuoniPostaliPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SubscribeWithPage;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="BuoniPostaliPage")
@Android
@IOS
public class BuoniPostaliPageMan extends LoadableComponent<BuoniPostaliPageMan> implements BuoniPostaliPage{
	
	UiPage page;

	private WebElement continuaButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement modalLink;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement subscribeVoucherButton;

	private SubscribeWithPage sottoscriviCon;

	@Override
	protected void isLoaded() throws Error {
	}

	@Override
	protected void load() 
	{
		
	}

	public void execAnnullaSottoscrizioneBuono(VoucherVerificationBean b) 
	{
		try
		{
			this.subscribeVoucherButton=page.getParticle(Locators.BuoniPostaliPageMolecola.SUBSCRIBEVOUCHERBUTTON).getElement();
		}
		catch(Exception err)
		{
			this.subscribeVoucherButton=page.getParticle(Locators.BuoniPostaliPageMolecola.SUBSCRIBEVOUCHERBUTTON2).getElement();
		}
		this.subscribeVoucherButton.click();

		try 
		{
			this.continuaButton=page.getParticle(Locators.BuoniPostaliPageMolecola.CONTINUABUTTON).getElement();
			this.continuaButton.click();
		}catch (Exception err) {}
		
//		try 
//		{
//			PayWithPage payWithPage;
//			payWithPage=new PayWithPage(driver, driverType).get();
//			payWithPage.clickOnConto(b.getPayWith());
//		}catch (Exception err) {}
		
		sottoscriviCon=(SubscribeWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SubscribeWithPage", page.getDriver().getClass(), page.getLanguage());
		sottoscriviCon.checkPage();
	
		//procedo con la sottoscrizione del buono
		sottoscriviCon.createBuonoPostale(b,true);

	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
	

}
