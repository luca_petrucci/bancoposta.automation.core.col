package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import bean.datatable.SalvadanaioDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.CloseObjectivePage;
import it.poste.bancoposta.pages.FAQPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SalvadanaioInfoObjectivePage;
import test.automation.core.UIUtils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import javax.imageio.ImageIO;

import org.eclipse.jetty.server.Response.OutputType;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="CloseObjectivePage")
@Android
@IOS
public class CloseObjectivePageMan extends LoadableComponent<CloseObjectivePageMan> implements CloseObjectivePage{

	UiPage page;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancelButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement helpButton;

	private WebElement description;
	private WebElement headerLoc;
	private String headerLoc2;
	private String headerFuture;
	private String cancelButton2;
	private String cancelTwo;
	private String descriptionTwo;
	private WebElement test;
	private Object t;
	private Object test2;
	private String RGB;
	private String closeObjDescriptionTwo;
	private Object description2;
	private WebElement evolutionCard;
	private String evolutionNumber;
	private String evolutionSubString;
	private String evolutionCard2;
	private String evolutionCard3;
	private String evolutionFeature;
	private String evolutionFeature2;
	private WebElement productType;
	private CharSequence paymentMethod2;
	private String productType2;
	private WebElement saldo;
	private String saldo2;
	private WebElement assistance;
	private WebElement xbutton;
	private WebElement h;
	private Object h2;
	private String assistance2;
	private WebElement closeObjectiveButton;

	@Override
	protected void isLoaded() throws Error {

	} 

	@Override

	protected void load() 
	{
		page.get();
	}

	public void closeObjectiveTwo(String header,String cancel,String closeObjDescription,String evolutionNumber,String paymentMethod) 
	{
		load();

		this.headerLoc = page.getParticle(Locators.CloseObjectivePageMolecola.HEADER).getElement();
		headerLoc2 = headerLoc.getText().trim().toLowerCase();

		headerFuture = header.trim().toLowerCase();

		System.out.println(headerLoc2+ " " + headerFuture);

		Assert.assertTrue(headerLoc2.equals(headerFuture));
		System.out.println("L'header atteso della pagina �: "+ " " + headerFuture + " " + "Header dell'app:" + " " + headerLoc2);

		this.cancelButton =page.getParticle(Locators.CloseObjectivePageMolecola.CANCEL).getElement() ;
		cancelButton2 = cancelButton.getText().trim().toLowerCase();
		cancelTwo= cancel.trim().toLowerCase();

		System.out.println(cancelTwo + cancelButton2);
		Assert.assertTrue(cancelButton2.equals(cancelTwo));

		this.helpButton = page.getParticle(Locators.CloseObjectivePageMolecola.HELP).getElement();
		Assert.assertTrue(helpButton.isDisplayed());
		System.out.println("Controllo Help Button andato a buon fine !"); 


		this.description = page.getParticle(Locators.CloseObjectivePageMolecola.CLOSEOBJECTIVEPAGEDESCRIPTION).getElement();
		descriptionTwo = description.getText().trim().toLowerCase();

		description2= closeObjDescription.trim().toLowerCase();

		Assert.assertTrue(descriptionTwo.equals(description2));
		System.out.println("Descrizione corretta : "+ description2 + descriptionTwo);

		this.evolutionCard = page.getParticle(Locators.CloseObjectivePageMolecola.PAN).getElement();
		evolutionCard2= evolutionCard.getText().trim();
		evolutionCard3= evolutionCard2.substring(evolutionCard2.length()-4);
		System.out.println(evolutionCard3);

		evolutionFeature= evolutionNumber.trim();
		evolutionFeature2= evolutionFeature.substring(evolutionFeature.length()-4);
		System.out.println(evolutionFeature2);

		Assert.assertTrue(evolutionCard3.equals(evolutionFeature2));

		this.productType = page.getParticle(Locators.CloseObjectivePageMolecola.PRODUCTTYPE).getElement();
		productType2=productType.getText().trim().toLowerCase();
		paymentMethod2=paymentMethod.trim().toLowerCase();
		Assert.assertTrue(productType2.contains(paymentMethod2));
		System.out.println("Atteso:"+ " " + paymentMethod2+ " "+ "App:" + productType2);

		this.saldo =page.getParticle(Locators.CloseObjectivePageMolecola.CLOSEOBJECTAMOUNTAVAILABLE).getElement() ;
		Assert.assertTrue(saldo.isDisplayed());
		saldo2= saldo.getText();
		System.out.println("Saldo disponibile:"+ " " + saldo2);

	}


	public void clicksOnButtons(){
		load();

		this.helpButton = page.getParticle(Locators.CloseObjectivePageMolecola.HELP).getElement();
		Assert.assertTrue(helpButton.isDisplayed());
		helpButton.click();

		FAQPage f = (FAQPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("FAQPage", page.getDriver().getClass(), page.getLanguage());
		f.checkPage();

		f.clickCancelButton();

//		this.assistance = driver.findElement(By.xpath(Locators.FAQPageLocator.HEADER.getLocator(driver)));
//		assistance2 = assistance.getText().trim();
//
//		Assert.assertTrue(assistance2.equals("Assistenza"));
//		System.out.println("Sei nella pagina:"+ assistance2);
//		WaitManager.get().waitMediumTime();
//
//		this.xbutton = driver.findElement(By.xpath(Locators.FAQPageLocator.CANCELLBUTTON.getLocator(driver)));
//		xbutton.click();

//		CloseObjectivePage p = new CloseObjectivePage(driver, driverType);
//		p.get();

		this.cancelButton = page.getParticle(Locators.CloseObjectivePageMolecola.CANCEL).getElement();
		cancelButton.click();

		SalvadanaioInfoObjectivePage s = (SalvadanaioInfoObjectivePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioInfoObjectivePage", page.getDriver().getClass(), page.getLanguage());
		s.checkPage();
		
		s.clickBack();

//		this.h = driver.findElement(By.xpath(Locators.SalvadanaioInfoPageLocator.HEADER.getLocator(driver)));
//		h2= h.getText().trim();
//		Assert.assertTrue(h2.equals("Dettaglio obiettivo"));
//		System.out.println("Sei nella pagina:"+ h2);
//
//		this.closeObjectiveButton=driver.findElement(By.xpath(Locators.SalvadanaioInfoPageLocator.CLOSEOBJECTIVEBUTTON.getLocator(driver)));
//		closeObjectiveButton.click();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}



}