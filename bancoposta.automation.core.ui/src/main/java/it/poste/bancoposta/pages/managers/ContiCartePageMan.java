package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import io.appium.java_client.HidesKeyboard;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BollettinoDataBean;
import bean.datatable.BolloAutoBean;
import bean.datatable.BonificoDataBean;
import bean.datatable.CredentialAccessBean;
import bean.datatable.GirofondoCreationBean;
import bean.datatable.PaymentCreationBean;
import bean.datatable.RicaricaPostepayBean;
import bean.datatable.TransactionMovementsBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.BollettinoChooseOperationPage;
import it.poste.bancoposta.pages.BolloAutoPage;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.ContiCartePage;
import it.poste.bancoposta.pages.GirofondoFormPage;
import it.poste.bancoposta.pages.HomepageHamburgerMenu;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.MovementDetailPage;
import it.poste.bancoposta.pages.PagaConSubMenu;
import it.poste.bancoposta.pages.PagaConSubMenuCartaPage;
import it.poste.bancoposta.pages.RicaricaLaTuaPostepayChooseOperationPage;
import it.poste.bancoposta.pages.RicaricaPostepayPage;
import it.poste.bancoposta.pages.RicaricheAutomatichePage;
import it.poste.bancoposta.pages.RiceviSubMenuCartaPage;
import it.poste.bancoposta.pages.SendBonificoPage;
import it.poste.bancoposta.pages.SendPostagiroPage;
import it.poste.bancoposta.utils.PageMolecolaImgCreator;
import test.automation.core.UIUtils;
import utils.DinamicData;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="ContiCartePage")
@Android
public class ContiCartePageMan extends LoadableComponent<ContiCartePageMan> implements ContiCartePage{
	protected static final String SALDO_CONTABILE = "CONTI_CARTE_SALDO_CONTABILE";
	
	UiPage page;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement leftMenu;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement tabConti;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement contoTabElement;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement contoType;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement contoNumber;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement saldoDisponibile;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement saldoContabile;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement pagaButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement rightNavigationButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement leftNavigationButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement search;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement filters;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement listaMovimenti;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement movimentiElement;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement movmentType;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement movmentDescription;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement movmentDate;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement modificaContrattoHoCapitoButton;
		/**
		 * @android 
		 * @ios 
		 * @web 
		 */
	protected WebElement movmentAmount;

	protected PagaConSubMenu pagaCon;

	protected PagaConSubMenuCartaPage pagaConCarta;
	protected RiceviSubMenuCartaPage riceviConCarta;

	protected SimpleDateFormat dateFormat=new SimpleDateFormat("dd #");
	protected WebElement ricercaTransazioniInput;
	protected WebElement riceviButton;
	protected WebElement ricaricaQuestaPostepayButton;
	protected WebElement usciteNonContabilizzateButton;
	protected WebElement accountDetailButton;
	protected WebElement automaticActiveRechargeButton;

	protected List<WebElement> record;

	protected String movTitle;

	protected String movImport;

	protected String movDate;

	protected WebElement toggleAccount;

	protected WebElement availableAmount;

	private WebElement toggle;
	

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
		
//		WaitManager.get().waitMediumTime();
//		
//		byte[] b=((TakesScreenshot)page.getDriver()).getScreenshotAs(OutputType.BYTES);
//		PageMolecolaImgCreator.makeImg(b, page,
//				Locators.ContiCartePageMolecola.HEADER,
//				Locators.ContiCartePageMolecola.LEFTMENU,
//				Locators.ContiCartePageMolecola.SALDOCONTABILE,
//				Locators.ContiCartePageMolecola.SALDODISPONIBILE,
//				Locators.ContiCartePageMolecola.PAGABUTTON,
//				//Locators.ContiCartePageMolecola.LISTAMOVIMENTI,
//				Locators.ContiCartePageMolecola.MOVIMENTO_TITOLO,
//				Locators.ContiCartePageMolecola.MOVIMENTO_DATA,
//				Locators.ContiCartePageMolecola.MOVIMENTO_IMPORTO
//				);
	}

	public BollettinoChooseOperationPage navigateToBollettinoSection(BollettinoDataBean b, String paymentMethod) 
	{

		if (b.getPaymentMethod().equals("carta")) {
			navigateToCartaPostePay(b.getPayWith());
			
			this.pagaButton=page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTONCARTA).getElement();
			
			automation.core.ui.WaitManager.get().waitMediumTime();

			//			navigateToContoCarta(b.getPayWith());
			saldoDisponibile = page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILECARTA).getElement();
			String saldo=this.saldoDisponibile.getText().trim().replace("�", "");
			System.out.println(saldo);
			//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

			//double disponibile=Double.parseDouble(saldo);
			double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
			double amount=Double.parseDouble(b.getAmount());

			DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);

			//clicco sul menu paga
			this.pagaButton.click();

			pagaConCarta=(PagaConSubMenuCartaPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagaConSubMenuCartaPage", page.getDriver().getClass(), page.getLanguage());

			pagaConCarta.checkPage();

			return pagaConCarta.gotoBollettino();



		} else {
			WaitManager.get().waitMediumTime();
			this.pagaButton=page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTON).getElement();

			WaitManager.get().waitMediumTime();

			navigateToConto(b.getPayWith());
			saldoDisponibile = page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILE).getElement();
			String saldo=this.saldoDisponibile.getText().trim().replace("�", "");
			System.out.println(saldo);
			//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

			//double disponibile=Double.parseDouble(saldo);
			double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
			double amount=Double.parseDouble(b.getAmount());

			DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);

			//clicco sul menu paga


			this.pagaButton.click();

			pagaCon=(PagaConSubMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagaConSubMenu", page.getDriver().getClass(), page.getLanguage());


			pagaCon.checkPage();

			return pagaCon.gotoBollettino();
		}

	}

	public void retrieveSaldoContabile() 

	{

		saldoDisponibile =page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILE).getElement() ;
		String saldo=this.saldoDisponibile.getText().trim().replace("�", "");
		System.out.println(saldo);
		//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

		//double disponibile=Double.parseDouble(saldo);
		double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);

		DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);
	}




	public void navigateToConto(String payWith) 
	{
		String numeroConto="";
		String saldoConto = "";

		this.rightNavigationButton=page.getParticle(Locators.ContiCartePageMolecola.RIGHTNAVIGATIONBUTTON).getElement();
		numeroConto=page.getParticle(Locators.ContiCartePageMolecola.CONTONUMBER).getLocator();
		saldoConto = page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILE).getLocator();
		
		
		boolean find=false;

		//navigo fino alla pagina del conto desiderato
		String conto=payWith.split(";")[1];
		for(int i=0;i<5;i++)
		{
			try 
			{
				//				if(page.getParticle(numeroConto)).getText().equals(conto))
				//				{
				//					break;
				//				}

//				if (((AndroidDriver)driver).findElementsByAndroidUIAutomator("new UiSelector().textContains(\"#str#\")".replace("#str#",conto)).size()>0) {
//
//					break;
//				}
				
				find=findNumeroConto(conto);
				
				if(find) break;
				
				this.rightNavigationButton=page.getParticle(Locators.ContiCartePageMolecola.RIGHTNAVIGATIONBUTTON).getElement();
				this.rightNavigationButton.click();

				WaitManager.get().waitShortTime();
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
		}
		
		org.springframework.util.Assert.isTrue(find, "Prodotto non trovato "+conto);
	}


	protected boolean findNumeroConto(String conto) 
	{
		if(page.getDriver().findElements(By.xpath("//*[contains(@text,'"+conto+"')]")).size() > 0)
		{
			return true;
			
		}
		else
		{
			//pezza per saltare il problema della rotellina
			if(page.getParticle(Locators.ContiCartePageMolecola.CONTODIV).getListOfElements().size() > 0)
			{
				return true;
			}
		}
		
		return false;
	}

	public void navigateToCartaPostePay(String payWith) 
	{
		String numeroConto="";
		String fourDigits = "";

		this.rightNavigationButton=page.getParticle(Locators.ContiCartePageMolecola.RIGHTNAVIGATIONBUTTON).getElement();
		

		//navigo fino alla pagina del conto desiderato

		boolean find=false;

		String conto=payWith.split(";")[1];
		String conto4 = conto.substring(conto.length() - 4);

		for(int i=0;i<5;i++)
		{
			try 
			{
				
//				if (((AndroidDriver)driver).findElementsByAndroidUIAutomator("new UiSelector().textContains(\"#str#\")".replace("#str#",conto4)).size()>0) {
//					//WebElement tab = (WebElement) ((AndroidDriver)driver).findElementsByAndroidUIAutomator("new UiSelector().textContains(\"#str#\")".replace("#str#",conto4)).get(0);
//					//tab.click();
//					return ;				
//
//				}
//				this.rightNavigationButton.click();
//				Thread.sleep(1000);
				
				if(page.getDriver().findElements(By.xpath("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_pan' and contains(@text,'"+conto4+"')]")).size() > 0)
				{
					find=true;
					break;
				}
				
				this.rightNavigationButton=page.getParticle(Locators.ContiCartePageMolecola.RIGHTNAVIGATIONBUTTON).getElement();
				this.rightNavigationButton.click();

				WaitManager.get().waitShortTime();

			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
		}
		
		org.springframework.util.Assert.isTrue(find, "Prodotto non trovato "+conto);
	}


	public boolean checkUsciteNonContabilizzateButton() 
	{
		this.rightNavigationButton=page.getParticle(Locators.ContiCartePageMolecola.RIGHTNAVIGATIONBUTTON).getElement();
		
		//navigo fino alla pagina del conto desiderato

		try { 
			for(int i=0;i<3;i++)
			{

				try {

					this.usciteNonContabilizzateButton=page.getParticle(Locators.ContiCartePageMolecola.USCITENONCONTABILIZZATEBUTTON).getElement();	
					

				} catch (Exception e ) {
					
				}
				//this.rightNavigationButton.click();
			}
		} catch (Exception e) {
			System.err.println("[WARNING] - USCITE NON CONTABILIZZATE NON PRESENTE");
			
			return false;
		}

		try {
			this.usciteNonContabilizzateButton.click();
		} catch (Exception e) {
			System.err.println("[WARNING] - USCITE NON CONTABILIZZATE NON PRESENTE");
			return false;
		}
		
		return true;
	}


	public HomepageHamburgerMenu openHamburgerMenu() 
	{
		this.leftMenu= page.getParticle(Locators.ContiCartePageMolecola.LEFTMENU).visibilityOfElement(10L);
		
		WaitManager.get().waitLongTime();

		this.leftMenu.click();
		
		WaitManager.get().waitShortTime();

		HomepageHamburgerMenu h=(HomepageHamburgerMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomepageHamburgerMenu", page.getDriver().getClass(), page.getLanguage());

		h.checkPage();

		return h;
	}

	public GirofondoFormPage navigateToGirofondo(GirofondoCreationBean b) 
	{
		WaitManager.get().waitLongTime();
		WaitManager.get().waitLongTime();
		navigateToConto(b.getPayWith());

		this.pagaButton=page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTON).getElement();
		this.saldoDisponibile=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILE).getElement();
		

		//controllo che il saldo disponibile sia superiore all'importo dovuto
		String saldo=this.saldoDisponibile.getText().trim().replace("�", "");
		//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

		//double disponibile=Double.parseDouble(saldo);
		double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
		double amount=Double.parseDouble(b.getAmount());

		DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);

		Assert.assertTrue("controllo che il saldo disponibile sia superiore a "+b.getAmount()+"; saldo disponibile:"+disponibile, disponibile >= amount);

		this.pagaButton.click();

		pagaCon=(PagaConSubMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PagaConSubMenu", page.getDriver().getClass(), page.getLanguage());

		pagaCon.checkPage();
		
		return pagaCon.gotoGirofondo();
	}


	public void searchTransaction (TransactionMovementsBean b) {

		WaitManager.get().waitMediumTime();
		this.ricercaTransazioniInput=page.getParticle(Locators.ContiCartePageMolecola.SEARCH).visibilityOfElement(20L);
		ricercaTransazioniInput.click();

		ricercaTransazioniInput.sendKeys(b.getTransactionType());
//		((HidesKeyboard) page.getDriver()).hideKeyboard();


	}


	public void checkTransaction(TransactionMovementsBean b) 
	{

		String transactionName = page.getParticle(Locators.ContiCartePageMolecola.MOVMENTTYPE).getLocator(new Entry("title",b.getTransactionType().toUpperCase()));
//		((AndroidDriver) page.getDriver()).findElementByAndroidUIAutomator("new UiSelector().textContains(\"#str#\")".replace("#str#",b.getTransactionType()));
		page.getDriver().findElement(By.xpath(transactionName));
	}

	public SendBonificoPage navigateToBonifico(BonificoDataBean b) 
	{

		//clicco sul menu paga
		WaitManager.get().waitMediumTime();

		// Controllo il metodo di pagamento
		if (b.getPaymentMethod().equals("carta")) {
			navigateToCartaPostePay(b.getPayWith());
			
			this.pagaButton=page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTONCARTA).getElement();
			this.saldoDisponibile=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILECARTA).getElement();
			
			//controllo che il saldo disponibile sia superiore all'importo dovuto
			String saldo=this.saldoDisponibile.getText().trim().replace("�", "");
			//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

			//double disponibile=Double.parseDouble(saldo);
			double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
			double amount=Double.parseDouble(b.getAmount());

			DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);
			System.out.println(disponibile);
			System.out.println(amount);
			
			WaitManager.get().waitMediumTime();

			Assert.assertTrue("controllo che il saldo disponibile sia superiore a "+b.getAmount()+"; saldo disponibile:"+disponibile, disponibile >= amount);

			this.pagaButton.click();

			pagaConCarta=(PagaConSubMenuCartaPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagaConSubMenuCartaPage", page.getDriver().getClass(), page.getLanguage());

			pagaConCarta.checkPage();


		}  else {
			
			this.pagaButton=page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTON).getElement();
			this.saldoDisponibile=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILE).getElement();
			

			//controllo che il saldo disponibile sia superiore all'importo dovuto
			String saldo=this.saldoDisponibile.getText().trim().replace("�", "");
			//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

			//double disponibile=Double.parseDouble(saldo);
			double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
			double amount=Double.parseDouble(b.getAmount());

			DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);

			Assert.assertTrue("controllo che il saldo disponibile sia superiore a "+b.getAmount()+"; saldo disponibile:"+disponibile, disponibile >= amount);

			this.pagaButton.click();

			pagaCon=(PagaConSubMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagaConSubMenu", page.getDriver().getClass(), page.getLanguage());

			pagaCon.checkPage();
		}

		if (b.getPaymentMethod().equals("carta")) {
			return pagaConCarta.gotoBonifico();
		} else {
			return pagaCon.gotoBonifico();
		}


	}

	public SendPostagiroPage navigateToPostagiro(PaymentCreationBean b) 
	{
		if (b.getPaymentMethod().equals("carta")) {
			navigateToCartaPostePay(b.getPayWith());
			
			this.pagaButton=page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTONCARTA).getElement();
			this.saldoDisponibile=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILECARTA).getElement();
			
			//controllo che il saldo disponibile sia superiore all'importo dovuto
			String saldo=this.saldoDisponibile.getText().trim().replace("�", "");
			//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

			//double disponibile=Double.parseDouble(saldo);
			double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
			double amount=Double.parseDouble(b.getAmount());

			DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);
			System.out.println(disponibile);
			System.out.println(amount);
			
			WaitManager.get().waitShortTime();

			Assert.assertTrue("controllo che il saldo disponibile sia superiore a "+b.getAmount()+"; saldo disponibile:"+disponibile, disponibile >= amount);

			this.pagaButton.click();

			pagaConCarta=(PagaConSubMenuCartaPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagaConSubMenuCartaPage", page.getDriver().getClass(), page.getLanguage());

			pagaConCarta.checkPage();


		}  else {
			navigateToConto(b.getPayWith());
			
			this.pagaButton=page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTON).getElement();
			this.saldoDisponibile=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILE).getElement();
			

			//controllo che il saldo disponibile sia superiore all'importo dovuto
			String saldo=this.saldoDisponibile.getText().trim().replace("\u20AC", "");
			//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

			//double disponibile=Double.parseDouble(saldo);
			double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
			double amount=Double.parseDouble(b.getAmount());

			DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);

			Assert.assertTrue("controllo che il saldo disponibile sia superiore a "+b.getAmount()+"; saldo disponibile:"+disponibile, disponibile >= amount);

			this.pagaButton.click();

			pagaCon=(PagaConSubMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagaConSubMenu", page.getDriver().getClass(), page.getLanguage());

			pagaCon.checkPage();
		}

		if (b.getPaymentMethod().equals("carta")) {
			return pagaConCarta.gotoPostagiro();
		} else {
			return pagaCon.gotoPostagiro();
		}
	}

	public RicaricaPostepayPage navigateToRicaricaPostepay(RicaricaPostepayBean b) 
	{
		WaitManager.get().waitMediumTime();

		// Controllo il metodo di pagamento
		if (b.getPaymentMethod().equals("carta")) {
			navigateToCartaPostePay(b.getPayWith());
			
			this.pagaButton=page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTONCARTA).getElement();
			this.saldoDisponibile=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILECARTA).getElement();
			
			
			//controllo che il saldo disponibile sia superiore all'importo dovuto
			String saldo=this.saldoDisponibile.getText().trim().replace("�", "");
			//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

			//double disponibile=Double.parseDouble(saldo);
			double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
			double amount=Double.parseDouble(b.getAmount());

			DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);
			System.out.println(disponibile);
			System.out.println(amount);
			
			WaitManager.get().waitMediumTime();

			//Assert.assertTrue("controllo che il saldo disponibile sia superiore a "+b.getAmount()+"; saldo disponibile:"+disponibile, disponibile >= amount);

			this.pagaButton.click();

			pagaConCarta=(PagaConSubMenuCartaPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagaConSubMenuCartaPage", page.getDriver().getClass(), page.getLanguage());

			pagaConCarta.checkPage();


		}  else {
			
			this.pagaButton=page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTON).getElement();
			this.saldoDisponibile=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILE).getElement();
			

			//controllo che il saldo disponibile sia superiore all'importo dovuto
			String saldo=this.saldoDisponibile.getText().trim().replace("�", "");
			//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

			//double disponibile=Double.parseDouble(saldo);
			double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
			double amount=Double.parseDouble(b.getAmount());

			DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);

			Assert.assertTrue("controllo che il saldo disponibile sia superiore a "+b.getAmount()+"; saldo disponibile:"+disponibile, disponibile >= amount);

			this.pagaButton.click();

			pagaCon=(PagaConSubMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagaConSubMenu", page.getDriver().getClass(), page.getLanguage());

			pagaCon.checkPage();
		}

		if (b.getPaymentMethod().equals("carta")) {
			return pagaConCarta.goToRicaricaAltraPostepay();
		} else {
			return pagaCon.goToRicaricaPostepay();
		}
	}


	public void checkSaldi(RicaricaPostepayBean b) {
		
		WaitManager.get().waitMediumTime();

		if (b.getPaymentMethod().equals("carta")) {
			navigateToConto(b.getPayWith());
			navigateToCartaPostePay(b.getTransferTo());


		}
	}


	public RiceviSubMenuCartaPage navigateToRicaricaQuestaPostepay(RicaricaPostepayBean b) {
		
		WaitManager.get().waitMediumTime();

		// Controllo il metodo di pagamento
		if (b.getPaymentMethod().equals("carta")) {
			navigateToConto(b.getPayWith());
			navigateToCartaPostePay(b.getTransferTo());
			
			this.riceviButton=page.getParticle(Locators.ContiCartePageMolecola.RICEVIBUTTON).getElement();
//			this.saldoDisponibile=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILECARTA).getElement();
//			
//
//			String saldo = saldoDisponibile.getText();
			this.riceviButton.click();

			riceviConCarta = (RiceviSubMenuCartaPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("RiceviSubMenuCartaPage", page.getDriver().getClass(), page.getLanguage());

			riceviConCarta.checkPage();


		} 		
		if (b.getPaymentMethod().equals("carta")) {

		}
		return riceviConCarta; 
	}


	public BolloAutoPage goToBolloAuto(BolloAutoBean b) 
	{
		//		BolloAutoBean b = bean.get(0);
		//clicco sul menu paga
		WaitManager.get().waitMediumTime();

		// Controllo il metodo di pagamento
		if (b.getPaymentMethod().equals("carta")) {
			navigateToCartaPostePay(b.getPayWith());
			
			this.pagaButton=page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTONCARTA).getElement();
			this.saldoDisponibile=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILECARTA).getElement();
			
			//controllo che il saldo disponibile sia superiore all'importo dovuto
			String saldo=this.saldoDisponibile.getText().trim().replace("�", "");
			//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

			//double disponibile=Double.parseDouble(saldo);
			double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
			double amount=Double.parseDouble(b.getAmount());

			DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);
			System.out.println(disponibile);
			System.out.println(amount);
			try {
				Thread.sleep(1000);
			} catch (Exception e) {

			}

			Assert.assertTrue("controllo che il saldo disponibile sia superiore a "+b.getAmount()+"; saldo disponibile:"+disponibile, disponibile >= amount);

			this.pagaButton.click();

			pagaConCarta=(PagaConSubMenuCartaPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagaConSubMenuCartaPage", page.getDriver().getClass(), page.getLanguage());

			pagaConCarta.checkPage();


		}  else {
			
			this.pagaButton=page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTON).getElement();
			this.saldoDisponibile=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILE).getElement();
			

			//controllo che il saldo disponibile sia superiore all'importo dovuto
			String saldo=this.saldoDisponibile.getText().trim().replace("�", "");
			//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

			//double disponibile=Double.parseDouble(saldo);
			double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
			double amount=Double.parseDouble(b.getAmount());

			DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);

			Assert.assertTrue("controllo che il saldo disponibile sia superiore a "+b.getAmount()+"; saldo disponibile:"+disponibile, disponibile >= amount);

			this.pagaButton.click();

			pagaCon=(PagaConSubMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagaConSubMenu", page.getDriver().getClass(), page.getLanguage());

			pagaCon.checkPage();
		}

		if (b.getPaymentMethod().equals("carta")) {
			return pagaConCarta.goToBolloAuto();
		} else {
			return pagaCon.goToBolloAuto();
		}


	}

	public RicaricaLaTuaPostepayChooseOperationPage goToRicaricaQuestaPostepay() 
	{
		//this.ricaricaQuestaPostepayButton=page.getParticle(Locators.RiceviSubMenuCartaPageLocators.RICARICAQUESTAPOSTEPAYBUTTON).getElement();
		RiceviSubMenuCartaPage ric=(RiceviSubMenuCartaPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RiceviSubMenuCartaPage", page.getDriver().getClass(), page.getLanguage());
		
		ric.goToRicaricaQuestaPostepay();

		WaitManager.get().waitMediumTime();


		RicaricaLaTuaPostepayChooseOperationPage p = (RicaricaLaTuaPostepayChooseOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaLaTuaPostepayChooseOperationPage", page.getDriver().getClass(), page.getLanguage());
		p.checkPage();

		return p;	
	}

	public void clickOnPostePayCard() 
	{
		page.getParticle(Locators.ContiCartePageMolecola.CARDPOSTEPAY).visibilityOfElement(null).click();
	}

	public void clickOnDettaglio() 
	{
		List<WebElement> list = page.getParticle(Locators.ContiCartePageMolecola.ACCOUNTDETAILBUTTON).getListOfElements();
		if(list.size() > 0) {
			this.accountDetailButton=page.getParticle(Locators.ContiCartePageMolecola.ACCOUNTDETAILBUTTON).getElement();
			accountDetailButton.click();
		}
		else {
			page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILE).getElement().click();
		}
	}

	public void clickOnRicaricheAutomaticheAttive() 
	{
		this.automaticActiveRechargeButton=page.getParticle(Locators.AccountDetailPageMolecola.AUTOMATICACTIVERECHARGEBUTTON).getElement();
		automaticActiveRechargeButton.click(); 
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

	@Override
	public void clickOnPagaButton() {
		WebElement pagaButton = page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTON).getElement();
		pagaButton.click();
	}

	@Override
	public void clickLeftButton() {
		leftMenu= page.getParticle(Locators.ContiCartePageMolecola.LEFTMENU).visibilityOfElement(10L);
		leftMenu.click();
	}

	@Override
	public void clickOnTabCarta() {
		WebElement postepay = page.getParticle(Locators.ContiCartePageMolecola.TABCARTA).getElement();
		postepay.click();
	}

	@Override
	public void clickOnFirstItemCarta() {
		this.record =page.getParticle(Locators.ContiCartePageMolecola.RECORDSMOVIMENTI).getListOfElements();
		System.out.println(page.getParticle(Locators.ContiCartePageMolecola.RECORDSMOVIMENTI));
		System.out.println(record);

		//TODO:TAA-303
		if (this.record.size() > 0) {

			WebElement mov = this.record.get(0);
			WebElement movTitle = mov.findElement(page.getParticle(Locators.ContiCartePageMolecola.MOVIMENTO_TITOLO).getXPath());
			WebElement movImport = mov.findElement(page.getParticle(Locators.ContiCartePageMolecola.MOVIMENTO_IMPORTO).getXPath());
			WebElement movDate = mov.findElement(page.getParticle(Locators.ContiCartePageMolecola.MOVIMENTO_DATA).getXPath());

			System.out.println(movTitle.getText());
			System.out.println(movImport.getText());
			System.out.println(movDate.getText());

			this.movTitle = movTitle.getText().trim();
			this.movImport = movImport.getText();
			this.movDate = movDate.getText();

			System.out.println(this.movTitle);
			movTitle.click();
			System.out.println(this.movImport);
			System.out.println(this.movDate);
		}
	}

	@Override
	public void checkMovimento() {
		MovementDetailPage movement = (MovementDetailPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("MovementDetailPage", page.getDriver().getClass(), null);
		WaitManager.get().waitShortTime();
		movement.verifyHeader(this.movTitle, (AndroidDriver<WebElement>) page.getDriver());
		movement.verifyImport(this.movImport, (AndroidDriver<WebElement>) page.getDriver());
		movement.verifyDate(this.movDate, (AndroidDriver<WebElement>) page.getDriver());
	}

	@Override
	public void clickOnFirstItemConto() {
		this.record =page.getParticle(Locators.ContiCartePageMolecola.RECORDSMOVIMENTI2).getListOfElements();
		System.out.println(page.getParticle(Locators.ContiCartePageMolecola.RECORDSMOVIMENTI2).getLocator());
		System.out.println(record);
		if (this.record.size() > 0) {

			WebElement mov = this.record.get(0);
			WebElement movTitle = mov.findElement(page.getParticle(Locators.ContiCartePageMolecola.MOVIMENTO_TITOLO).getXPath());
			WebElement movImport = mov.findElement(page.getParticle(Locators.ContiCartePageMolecola.MOVIMENTO_IMPORTO).getXPath());
			WebElement movDate = mov.findElement(page.getParticle(Locators.ContiCartePageMolecola.MOVIMENTO_DATA).getXPath());

			System.out.println(movTitle.getText());
			System.out.println(movImport.getText());
			System.out.println(movDate.getText());

			this.movTitle = movTitle.getText().trim();
			this.movImport = movImport.getText();
			this.movDate = movDate.getText();

			System.out.println(this.movTitle);
			movTitle.click();
			System.out.println(this.movImport);
			System.out.println(this.movDate);

		}
	}

	@Override
	public void checkToggleAcquistoOnLine(CredentialAccessBean b) {
		
		it.poste.bancoposta.utils.Utility.scrollVersoElemento(
				page.getDriver(), 
				page.getParticle(Locators.ContiCartePageMolecola.TOGGLEACCOUNT), 
				UIUtils.SCROLL_DIRECTION.DOWN, 
				10);
		
		this.toggleAccount = page.getParticle(Locators.ContiCartePageMolecola.TOGGLEACCOUNT).getElement();

		if (toggleAccount.getText().trim().equals("OFF")) {
			// Utility.swipe((MobileDriver<?>) driver, Utility.DIRECTION.DOWN, 500);

			WaitManager.get().waitMediumTime();
			
			this.toggleAccount = page.getParticle(Locators.ContiCartePageMolecola.TOGGLEACCOUNT).getElement();
			toggleAccount.click();
			
			WaitManager.get().waitMediumTime();
			
			InsertPosteIDPage i=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), null);
			

			i.insertPosteId(b.getPosteid());
			
			WaitManager.get().waitHighTime();
			
			
			this.toggleAccount = page.getParticle(Locators.ContiCartePageMolecola.TOGGLEACCOUNT).getElement();
			Assert.assertTrue("Toggle OFF", toggleAccount.getText().trim().equals("ON"));

		} else {
			Utility.swipe((MobileDriver<?>) page.getDriver(), Utility.DIRECTION.DOWN, 500);
			
			WaitManager.get().waitShortTime();
			
			this.toggleAccount = page.getParticle(Locators.ContiCartePageMolecola.TOGGLEACCOUNT).getElement();
			toggleAccount.click();
			WaitManager.get().waitMediumTime();
			

			InsertPosteIDPage i=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), null);
			

			i.insertPosteId(b.getPosteid());
			WaitManager.get().waitHighTime();
			
			this.toggleAccount = page.getParticle(Locators.ContiCartePageMolecola.TOGGLEACCOUNT).getElement();
			WaitManager.get().waitShortTime();
			this.toggleAccount = page.getParticle(Locators.ContiCartePageMolecola.TOGGLEACCOUNT).getElement();
			Assert.assertTrue("TOGGLE ON", toggleAccount.getText().trim().equals("OFF"));
		}
	}

	@Override
	public String getAvailableAmountCarta() {
		this.availableAmount=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILECARTA).getElement();	

		String availableAmount2 = availableAmount.getText().replace("�", "");	

		System.out.println(availableAmount2);
		
		return availableAmount2;
	}

	@Override
	public void checkSaldo(TransactionMovementsBean b) {
		double saldoVecchio=(double) DinamicData.getIstance().get(SALDO_CONTABILE);
		double amount=Double.parseDouble(b.getAmount().replace(",", "."));
		double commissioni=0;
		
		if(b.getTransactionType().equals("POSTAGIRO"))
		{
			commissioni=0.50;
		}
		
		this.saldoDisponibile=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILE).getElement();
		
		//controllo che il saldo disponibile sia superiore all'importo dovuto
		String saldo=this.saldoDisponibile.getText().trim();
		//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

		//double disponibile=Double.parseDouble(saldo);
		double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
		
		double totale=Utility.round(disponibile + amount + commissioni, 2);
		
		System.out.println(saldo);
		System.out.println("totale disponibile:"+disponibile);
		
		//L'assert di seguito � commentato perch� in Collaudo il saldo non si aggiorna subito
		//Assert.assertTrue("saldo non decrementato: attuale:"+disponibile+", precedente:"+saldoVecchio, disponibile < saldoVecchio);
	}
	
	@Override
	public void getsaldoVecchio() {
		this.saldoDisponibile=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILE).getElement();
		
		String saldo=this.saldoDisponibile.getText().trim().replace("�", "");
		//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");
		//double disponibile=Double.parseDouble(saldo);
		double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
		
		DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);
		System.out.println(disponibile);
	}

	@Override
	public void clickOnToogleBonificoIstantaneo(CredentialAccessBean b) {
		try {
			this.modificaContrattoHoCapitoButton = page.getParticle(Locators.ContiCartePageMolecola.MODIFICACONTRATTOHOCAPITOBUTTON).getElement();
		    this.modificaContrattoHoCapitoButton.click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		this.toggle = page.getParticle(Locators.ContiCartePageMolecola.TOOGLEINSTAPAY).getElement();
		String t = toggle.getAttribute("checked");
		if (t.equals("false")) {
			try {	
				WaitManager.get().waitShortTime();	
				toggle.click();	
			}catch(Exception e) {	
				toggle.click();	
			}

		} else {
			try {	
				WaitManager.get().waitShortTime();	
				toggle.click();	
			}catch(Exception e) {	
				toggle.click();	
			}
			InsertPosteIDPage i=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), null);
			i.insertPosteId(b.getPosteid());
			WaitManager.get().waitShortTime();
			try {	
				WaitManager.get().waitShortTime();	
				toggle.click();	
			}catch(Exception e) {	
				toggle.click();	
			}
		}
	}

	@Override
	public void clickOnAccettaPropostaUnilateralePopup() 
	{
		page.getParticle(Locators.ContiCartePageMolecola.MODIFICACONTRATTOHOCAPITOBUTTON).visibilityOfElement(20L).click();
	}

	@Override
	public void closeFotocamera() {
		page.getParticle(Locators.BollettinoChooseOperationPageMolecola.CLOSEBUTTON).visibilityOfElement(20L).click();
	}
}