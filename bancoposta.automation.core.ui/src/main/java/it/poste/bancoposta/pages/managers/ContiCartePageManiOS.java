package it.poste.bancoposta.pages.managers;

import static io.appium.java_client.MobileCommand.setSettingsCommand;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.w3c.dom.Node;

import android.Utility;
import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import bean.datatable.BonificoDataBean;
import bean.datatable.CredentialAccessBean;
import bean.datatable.PaymentCreationBean;
import bean.datatable.RicaricaPostepayBean;
import bean.datatable.TransactionMovementsBean;
import io.appium.java_client.CommandExecutionHelper;
import io.appium.java_client.ExecutesMethod;
import io.appium.java_client.HasSettings;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.Setting;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.HomepageHamburgerMenu;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.PagaConSubMenu;
import it.poste.bancoposta.pages.PagaConSubMenuCartaPage;
import it.poste.bancoposta.pages.RicaricaPostepayPage;
import it.poste.bancoposta.pages.SendBonificoPage;
import it.poste.bancoposta.pages.SendPostagiroPage;
import test.automation.core.UIUtils;
import utils.DinamicData;
import utils.ObjectFinderLight;

@PageManager(page="ContiCartePage")
@IOS
public class ContiCartePageManiOS extends ContiCartePageMan 
{
	private String pageSource;
	
	public void checkPage(Object... params) 
	{
		//load();
		WaitManager.get().waitMediumTime();
		
		setCustomSettings();
		
		page.getQuick();
		
		pageSource=page.getPageSource();
		
		restoreDefaultSettings();
		
	}

	private void restoreDefaultSettings() {
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("customSnapshotTimeout",15));
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("snapshotMaxDepth",50));
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("useFirstMatch",false));
	}

	private void setCustomSettings() {
		System.out.println("Set Custom Setting for conti e carte");
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("customSnapshotTimeout",0));
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("snapshotMaxDepth",20));
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("useFirstMatch",true));
	}
	
	public SendBonificoPage navigateToBonifico(BonificoDataBean b) 
	{

		//clicco sul menu paga
		WaitManager.get().waitMediumTime();
		
		setCustomSettings();

		// Controllo il metodo di pagamento
		if (b.getPaymentMethod().equals("carta")) {
			navigateToCartaPostePay(b.getPayWith());
			
			this.pagaButton=page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTONCARTA).getElement();
			this.saldoDisponibile=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILECARTA).getElement();
			
			//controllo che il saldo disponibile sia superiore all'importo dovuto
			String saldo=this.saldoDisponibile.getText().trim().replace("�", "");
			//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

			//double disponibile=Double.parseDouble(saldo);
			double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
			double amount=Double.parseDouble(b.getAmount());

			DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);
			System.out.println(disponibile);
			System.out.println(amount);
			
			WaitManager.get().waitMediumTime();

			Assert.assertTrue("controllo che il saldo disponibile sia superiore a "+b.getAmount()+"; saldo disponibile:"+disponibile, disponibile >= amount);

			this.pagaButton.click();
			
			restoreDefaultSettings();

			pagaConCarta=(PagaConSubMenuCartaPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagaConSubMenuCartaPage", page.getDriver().getClass(), page.getLanguage());

			pagaConCarta.checkPage();


		}  else {
			
			this.pagaButton=page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTON).getElement();
			this.saldoDisponibile=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILE).getElement();
			

			//controllo che il saldo disponibile sia superiore all'importo dovuto
			String saldo=this.saldoDisponibile.getText().trim().replace("�", "");
			//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

			//double disponibile=Double.parseDouble(saldo);
			double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
			double amount=Double.parseDouble(b.getAmount());

			DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);

			Assert.assertTrue("controllo che il saldo disponibile sia superiore a "+b.getAmount()+"; saldo disponibile:"+disponibile, disponibile >= amount);

			this.pagaButton.click();
			
			restoreDefaultSettings();

			pagaCon=(PagaConSubMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagaConSubMenu", page.getDriver().getClass(), page.getLanguage());

			pagaCon.checkPage();
		}

		if (b.getPaymentMethod().equals("carta")) {
			return pagaConCarta.gotoBonifico();
		} else {
			return pagaCon.gotoBonifico();
		}


	}
	
	public HomepageHamburgerMenu openHamburgerMenu() 
	{
		setCustomSettings();
		this.leftMenu= page.getParticle(Locators.ContiCartePageMolecola.LEFTMENU).visibilityOfElement(10L);
		
		WaitManager.get().waitLongTime();

		this.leftMenu.click();
		
		WaitManager.get().waitShortTime();
		
		restoreDefaultSettings();

		HomepageHamburgerMenu h=(HomepageHamburgerMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomepageHamburgerMenu", page.getDriver().getClass(), page.getLanguage());

		h.checkPage();

		return h;
	}

	public void navigateToConto(String payWith) 
	{
		String numeroConto="";
		String saldoConto = "";
		
		//pageSource=page.getDriver().getPageSource();
		
		setCustomSettings();
		
		Node rightNavigationButton=ObjectFinderLight.getElement(pageSource, page.getParticle(Locators.ContiCartePageMolecola.RIGHTNAVIGATIONBUTTON).getLocator());
		
		boolean find=false;

		//navigo fino alla pagina del conto desiderato
		String conto=payWith.split(";")[1];
		for(int i=0;i<5;i++)
		{
			try 
			{
				//				if(page.getParticle(numeroConto)).getText().equals(conto))
				//				{
				//					break;
				//				}

//				if (((AndroidDriver)driver).findElementsByAndroidUIAutomator("new UiSelector().textContains(\"#str#\")".replace("#str#",conto)).size()>0) {
//
//					break;
//				}
				
				find=findNumeroConto(conto);
				
				if(find) break;
				
				ObjectFinderLight.iOSClick(page.getDriver(), rightNavigationButton);

				WaitManager.get().waitShortTime();
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
		}
		
		restoreDefaultSettings();
		
		org.springframework.util.Assert.isTrue(find, "Prodotto non trovato "+conto);
	}
	
	protected boolean findNumeroConto(String conto)
	{
		String xpath=page.getParticle(Locators.ContiCartePageMolecola.CONTONUMBER).getLocator(new Entry("contoNumber",conto));
		return ObjectFinderLight.elementExists(pageSource, xpath);
	}
	
	public SendPostagiroPage navigateToPostagiro(PaymentCreationBean b) 
	{
		setCustomSettings();
		
		if (b.getPaymentMethod().equals("carta")) {
			navigateToCartaPostePay(b.getPayWith());
			
			Node pagaButton=ObjectFinderLight.getElement(pageSource, page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTONCARTA).getLocator());
			Node saldoDisponibile=ObjectFinderLight.getElement(pageSource, page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILECARTA).getLocator());
			
			//controllo che il saldo disponibile sia superiore all'importo dovuto
			String saldo=saldoDisponibile.getAttributes().getNamedItem("value").getNodeValue().trim();
			//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

			//double disponibile=Double.parseDouble(saldo);
			double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
			double amount=Double.parseDouble(b.getAmount());

			DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);
			System.out.println(disponibile);
			System.out.println(amount);
			
			WaitManager.get().waitShortTime();

			Assert.assertTrue("controllo che il saldo disponibile sia superiore a "+b.getAmount()+"; saldo disponibile:"+disponibile, disponibile >= amount);

			ObjectFinderLight.iOSClick(page.getDriver(), pagaButton);

			pagaConCarta=(PagaConSubMenuCartaPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagaConSubMenuCartaPage", page.getDriver().getClass(), page.getLanguage());

			pagaConCarta.checkPage();


		}  else {
			navigateToConto(b.getPayWith());
			
			Node pagaButton=ObjectFinderLight.getElement(pageSource, page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTON).getLocator());
			Node saldoDisponibile=ObjectFinderLight.getElement(pageSource, page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILE).getLocator());
			

			//controllo che il saldo disponibile sia superiore all'importo dovuto
			String saldo=saldoDisponibile.getAttributes().getNamedItem("value").getNodeValue().trim();
			//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

			//double disponibile=Double.parseDouble(saldo);
			double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
			double amount=Double.parseDouble(b.getAmount());

			DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);

			Assert.assertTrue("controllo che il saldo disponibile sia superiore a "+b.getAmount()+"; saldo disponibile:"+disponibile, disponibile >= amount);

			ObjectFinderLight.iOSClick(page.getDriver(), pagaButton);

			pagaCon=(PagaConSubMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagaConSubMenu", page.getDriver().getClass(), page.getLanguage());

			pagaCon.checkPage();
		}
		
		restoreDefaultSettings();

		if (b.getPaymentMethod().equals("carta")) {
			return pagaConCarta.gotoPostagiro();
		} else {
			return pagaCon.gotoPostagiro();
		}
	}
	
	public void checkTransaction(TransactionMovementsBean b) 
	{
		//page.getParticle(Locators.ContiCartePageMolecola.MOVMENTTYPE).visibilityOfElement(10L, new Entry("title",b.getTransactionType()));
		WaitManager.get().waitMediumTime();
		ObjectFinderLight.elementExists(page.getPageSource(), page.getParticle(Locators.ContiCartePageMolecola.MOVMENTTYPE).getLocator(new Entry("title",b.getTransactionType().toUpperCase())));
	}
	
	public void clickLeftButton() {
		page.getParticle(Locators.ContiCartePageMolecola.LEFTMENU).getElement().click();
	}
	
	public void clickOnPagaButton() {
//		page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTON).clickOffline(page.getPageSource());
		page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTON).getElement().click();
	}
	
	public RicaricaPostepayPage navigateToRicaricaPostepay(RicaricaPostepayBean b) 
	{
		WaitManager.get().waitMediumTime();
		WaitManager.get().waitMediumTime();
		// Controllo il metodo di pagamento
		if (b.getPaymentMethod().equals("carta")) {
			navigateToCartaPostePay(b.getPayWith());
			
			this.pagaButton=page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTONCARTA).getElement();
			this.saldoDisponibile=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILECARTA).getElement();
			
			
			//controllo che il saldo disponibile sia superiore all'importo dovuto
			String saldo=this.saldoDisponibile.getText().trim().replace("�", "");
			//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

			//double disponibile=Double.parseDouble(saldo);
			double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
			double amount=Double.parseDouble(b.getAmount());

			DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);
			System.out.println(disponibile);
			System.out.println(amount);
			
			WaitManager.get().waitMediumTime();

			//Assert.assertTrue("controllo che il saldo disponibile sia superiore a "+b.getAmount()+"; saldo disponibile:"+disponibile, disponibile >= amount);

			this.pagaButton.click();

			pagaConCarta=(PagaConSubMenuCartaPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagaConSubMenuCartaPage", page.getDriver().getClass(), page.getLanguage());

			pagaConCarta.checkPage();


		}  else {
			WaitManager.get().waitMediumTime();
			this.pagaButton=page.getParticle(Locators.ContiCartePageMolecola.PAGABUTTON).getElement();
			this.saldoDisponibile=page.getParticle(Locators.ContiCartePageMolecola.SALDODISPONIBILE).getElement();
			

			//controllo che il saldo disponibile sia superiore all'importo dovuto
			String saldo=this.saldoDisponibile.getText().trim().replace("�", "");
			//saldo=saldo.replace("\u20AC", "").replace(".", "").replace(",", ".");

			//double disponibile=Double.parseDouble(saldo);
			double disponibile=Utility.parseCurrency(saldo, Locale.ITALY);
			double amount=Double.parseDouble(b.getAmount().replace(",", "."));

			DinamicData.getIstance().set(SALDO_CONTABILE, disponibile);

			Assert.assertTrue("controllo che il saldo disponibile sia superiore a "+b.getAmount()+"; saldo disponibile:"+disponibile, disponibile >= amount);

			this.pagaButton.click();

			pagaCon=(PagaConSubMenu) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagaConSubMenu", page.getDriver().getClass(), page.getLanguage());

			pagaCon.checkPage();
		}

		if (b.getPaymentMethod().equals("carta")) {
			return pagaConCarta.goToRicaricaAltraPostepay();
		} else {
			return pagaCon.goToRicaricaPostepay();
		}
	}
	
	@Override
	public void checkToggleAcquistoOnLine(CredentialAccessBean b) {
		
		it.poste.bancoposta.utils.Utility.scrollVersoElemento(
				page.getDriver(), 
				page.getParticle(Locators.ContiCartePageMolecola.TOGGLEACCOUNT), 
				UIUtils.SCROLL_DIRECTION.DOWN, 
				10);
		
		this.toggleAccount = page.getParticle(Locators.ContiCartePageMolecola.TOGGLEACCOUNT).getElement();

		if (toggleAccount.getAttribute("value").trim().equals("0")) {
			// Utility.swipe((MobileDriver<?>) driver, Utility.DIRECTION.DOWN, 500);

			WaitManager.get().waitMediumTime();
			
			this.toggleAccount = page.getParticle(Locators.ContiCartePageMolecola.TOGGLEACCOUNT).getElement();
			toggleAccount.click();
			
			WaitManager.get().waitMediumTime();
			
			InsertPosteIDPage i=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), null);
			

			i.insertPosteId(b.getPosteid());
			
			WaitManager.get().waitHighTime();
			WaitManager.get().waitMediumTime();
			
			UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 200);
			UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 200);
			
			this.toggleAccount = page.getParticle(Locators.ContiCartePageMolecola.TOGGLEACCOUNT).visibilityOfElement(10L);
			Assert.assertTrue("TOGGLE OFF", toggleAccount.getAttribute("value").trim().equals("1"));

		} else {
			Utility.swipe((MobileDriver<?>) page.getDriver(), Utility.DIRECTION.DOWN, 500);
			
			WaitManager.get().waitShortTime();
			
			this.toggleAccount = page.getParticle(Locators.ContiCartePageMolecola.TOGGLEACCOUNT).getElement();
			toggleAccount.click();
			WaitManager.get().waitMediumTime();
			

			InsertPosteIDPage i=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), null);
			

			i.insertPosteId(b.getPosteid());
			WaitManager.get().waitHighTime();
			WaitManager.get().waitMediumTime();
			
			UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 200);
			UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 200);
			
			//this.toggleAccount = page.getParticle(Locators.ContiCartePageMolecola.TOGGLEACCOUNT).getElement();
			WaitManager.get().waitShortTime();
			this.toggleAccount = page.getParticle(Locators.ContiCartePageMolecola.TOGGLEACCOUNT).visibilityOfElement(10L);
			Assert.assertTrue("TOGGLE ON", toggleAccount.getAttribute("value").trim().equals("0"));

		}
	}


}
