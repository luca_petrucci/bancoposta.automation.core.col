package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BiscottoDataBean;
import bean.datatable.TutorialPopUpBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.ContoVolSubPage;
import it.poste.bancoposta.pages.DeviceNative;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;
import utils.ObjectFinderLight;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@PageManager(page="ContoVolSubPage")
@Android
public class ContoVolSubPageMan extends LoadableComponent<ContoVolSubPageMan> implements ContoVolSubPage
{
	UiPage page;
	private String pageSource;
	
	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
//		page.get();
		page.getParticle(Locators.ContoVolSubPageMolecola.CONTAINER).getElement();
		page.getParticle(Locators.ContoVolSubPageMolecola.PRODOTTI_CONTAINER).getElement();
		page.getParticle(Locators.ContoVolSubPageMolecola.PRODOTTI_OFFERTI).getElement();
	}

	public void checkSubPage(BiscottoDataBean b) 
	{
		
	}

	public void navigateToProduct(String prodotto, String pageSource2) 
	{
		WaitManager.get().waitLongTime();
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.5f, 0.5f, 0.2f, 300);
		WaitManager.get().waitMediumTime();
		
		pageSource=page.getDriver().getPageSource();
		
		System.out.println("prodotto da trovare:"+prodotto);
		
//		List<WebElement> listEl=page.getParticle(Locators.ContoVolSubPageMolecola.PRODOTTI_OFFERTI_TITLE).getListOfElements();
//		//-------------------------VERSIONE Luca --------------------------------
//		for (int i = 0; i < listEl.size(); i++) {
//			WebElement el = listEl.get(i);
//			if(el.getText().contains(prodotto)) {
//				el.click();
//				WaitManager.get().waitShortTime();
//			}
//		}
		
		//-------------------------VERSIONE LIGHT VELOCIZZATA--------------------------------
		for(int i=0;i<2;i++)
		{
			NodeList el=ObjectFinderLight.getElements(pageSource, page.getParticle(Locators.ContoVolSubPageMolecola.PRODOTTI_OFFERTI_TITLE).getLocator());
			
			for(int j=0;j<el.getLength();j++)
			{
				Node e=el.item(j);
				
				String txt=e.getAttributes().getNamedItem("text").getNodeValue().trim();
				
				System.out.println("prodotto:"+txt);
				System.out.println(e);
				
				if(txt.contains(prodotto))
				{
					ObjectFinderLight.clickOnElement(e);
					return;
				}
			}
			
			UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.LEFT, 0.5f, 0.5f, 0.2f, 0.5f, 1000);
			WaitManager.get().waitMediumTime();
			
//			pageSource=page.getDriver().getPageSource();
//			
//			System.out.println(pageSource);
			
		}
		
//		for(int i=0;i<3;i++)
//		{
//			List<WebElement> el=driver.findElements(By.xpath(Locators.ContoVolSubPageMolecola.PRODOTTI_OFFERTI_TITLE.getLocator(driver)));
//			System.out.println("prodotti trovati:"+el.size());
//			
//			for(WebElement e : el)
//			{
//				String txt=e.getText().trim();
//				System.out.println(txt);
//				
//				boolean find=false;
//				
//				for(String s : prodVisitato)
//				{
//					if(s.equals(txt))
//					{
//						find=true;
//						break;
//					}
//				}
//				
//				if(!find)
//				{
//					if(txt.contains(prodotto))
//					{
//						e.click();
//						return;
//					}
//					
//					UIUtils.mobile().swipe((MobileDriver) driver, UIUtils.SCROLL_DIRECTION.LEFT, 0.5f, 0.5f, 0.2f, 0.5f, 1000);
//					WaitManager.get().waitMediumTime();
//					
//					prodVisitato.add(txt);
//				}
//				
//				
//				
//			}
//		}
	}

	public void clickRichiedi() 
	{
		WebDriver driver=page.getDriver();
		try {
			page.getParticle(Locators.ContoVolSubPageMolecola.RICHIEDI_BUTTON).visibilityOfElement(null);
			
		} catch (Exception e) {
			UIUtils.mobile().swipe((MobileDriver) driver, UIUtils.SCROLL_DIRECTION.DOWN, 500);
			WaitManager.get().waitMediumTime();
		}
		
		page.getParticle(Locators.ContoVolSubPageMolecola.RICHIEDI_BUTTON).visibilityOfElement(null).click();
		
		WaitManager.get().waitMediumTime();
		
		for(int i=0;i<4;i++)
		{
			try {
				DeviceNative n=(DeviceNative) UiPageManager.get("it.poste.bancoposta.pages.managers")
						.getPageManager("DeviceNative", page.getDriver().getClass(), page.getLanguage());
				n.clickAllow();
//				UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.DeviceNativeElementLocator.ALLOW_BUTTON.getLocator(driver))),3);
//				driver.findElement(By.xpath(Locators.DeviceNativeElementLocator.ALLOW_BUTTON.getLocator(driver))).click();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	public void getLight(String p) throws Exception 
	{
		pageSource=p;
		
		if(!ObjectFinderLight.elementExists(pageSource,page.getParticle(Locators.ContoVolSubPageMolecola.CONTAINER).getLocator()))
			throw new Exception("element not find in light mode "+page.getParticle(Locators.ContoVolSubPageMolecola.CONTAINER).getName());
		
		if(!ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.ContoVolSubPageMolecola.PRODOTTI_CONTAINER).getLocator()))
			throw new Exception("element not find in light mode "+page.getParticle(Locators.ContoVolSubPageMolecola.PRODOTTI_CONTAINER).getName());
		
		if(!ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.ContoVolSubPageMolecola.PRODOTTI_OFFERTI).getLocator()))
			throw new Exception("element not find in light mode "+page.getParticle(Locators.ContoVolSubPageMolecola.PRODOTTI_OFFERTI).getName());
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

	

}
