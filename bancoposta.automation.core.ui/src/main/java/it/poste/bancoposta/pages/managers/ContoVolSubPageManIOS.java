package it.poste.bancoposta.pages.managers;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;
import utils.ObjectFinderLight;

@PageManager(page="ContoVolSubPage")
@IOS
public class ContoVolSubPageManIOS extends ContoVolSubPageMan {
	
	protected void load() 
	{
//		page.get();
		boolean popup=false;
		try {
			page.getParticle(Locators.ContoVolSubPageMolecola.ERRORPOPUPIOS).visibilityOfElement(15L);
			popup=true;
		}catch(Exception e) {
			
		}
		if(popup==true) {
			throw new RuntimeException("Popup di Errore sulla pagina VOL");
		}
		page.getParticle(Locators.ContoVolSubPageMolecola.CONTAINER).getElement();
		page.getParticle(Locators.ContoVolSubPageMolecola.PRODOTTI_CONTAINER).getElement();
		page.getParticle(Locators.ContoVolSubPageMolecola.PRODOTTI_OFFERTI).getElement();
	} 
	
	public void navigateToProduct(String prodotto, String pageSource2) 
	{
		WaitManager.get().waitLongTime();
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.5f, 0.5f, 0.2f, 300);
		WaitManager.get().waitMediumTime();
		
		System.out.println("prodotto da trovare:"+prodotto);
		
//		for(int i=0;i<3;i++)
//		{
//			List<WebElement> el= page.getParticle(Locators.ContoVolSubPageMolecola.PRODOTTI_OFFERTI_TITLE).getListOfElements();
//			
//			for(int j=0;j<el.size();j++)
//			{
//				WebElement e=el.get(j);
//				
//				String txt=e.getText().trim();
//				
//				System.out.println("prodotto:"+txt);
//				
//				if(txt.contains(prodotto))
//				{
//					e.click();
//					return;
//				}
//			}
//			UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.LEFT, 0.5f, 0.5f, 0.2f, 0.5f, 1000);
//			WaitManager.get().waitMediumTime();
//		}
		page.getParticle(Locators.ContoVolSubPageMolecola.PRODOTTI_OFFERTI_TITLE).getElement(new Entry("title", prodotto)).click();;
		
	}

}
