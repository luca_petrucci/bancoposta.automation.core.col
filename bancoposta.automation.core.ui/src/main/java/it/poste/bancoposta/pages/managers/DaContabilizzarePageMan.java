package it.poste.bancoposta.pages.managers;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.DaContabilizzarePage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
@PageManager(page="DaContabilizzarePage")
@Android
@IOS
public class DaContabilizzarePageMan extends LoadableComponent<DaContabilizzarePageMan> implements DaContabilizzarePage 
{
	UiPage page;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement threePointsMenu;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement totalAmount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement totalAmountMonth;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement paymentList;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement amountSinglePayment;
	private String amount;
	private String amount2;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	
	@Override
	protected void isLoaded() throws Error {
		
	}


	@Override
	protected void load() 
	{
		page.get();
	}
	
	
	
	
	public void checkAmounts() 
	{
		this.totalAmount = page.getParticle(Locators.DaContabilizzarePageMolecola.TOTALAMOUNT).getElement();
		this.amount = totalAmount.getText().trim().toLowerCase();
		
		Assert.assertTrue(totalAmount.isDisplayed());
		
		this.totalAmountMonth=page.getParticle(Locators.DaContabilizzarePageMolecola.TOTALAMOUNTMONTH).getElement();	
		this.amount2 = totalAmountMonth.getText().trim().toLowerCase();
		System.out.println(amount2);
		
		Assert.assertTrue(totalAmountMonth.isDisplayed());
		System.out.println("La pagina � giusta 2");
		
//		for(int i=0;i<3;i++)
//		{
//			try {
				
				
				
//			} catch (Exception e ) {
//				
//				
//			}
//		}
//		
		
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}


}