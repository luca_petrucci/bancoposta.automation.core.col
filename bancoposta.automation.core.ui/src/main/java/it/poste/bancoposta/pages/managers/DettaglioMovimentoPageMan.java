package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CheckTransactionBean;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.DettaglioMovimentoPage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;
import utils.Entry;

import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="DettaglioMovimentoPage")
@Android
@IOS
public class DettaglioMovimentoPageMan extends LoadableComponent<DettaglioMovimentoPageMan> implements DettaglioMovimentoPage
{
	UiPage page;



	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public void checkDettaglio(CheckTransactionBean t) 
	{
		WebElement desc=page.getParticle(Locators.DettaglioMovimentoMolecola.DETTAGLIO_DESCR).getElement();
		WebElement data=page.getParticle(Locators.DettaglioMovimentoMolecola.DETTAGLIO_DATA).getElement();
		WebElement amount=page.getParticle(Locators.DettaglioMovimentoMolecola.DETTAGLIO_AMOUNT).getElement();
		
		String d=desc.getText().trim();
		String dt=data.getText().trim();
		String a=amount.getText().trim().replaceAll(" ", "");
		
		org.springframework.util.Assert.isTrue(d.equals(t.getDescription()), "messaggio transazione KO atteso:"+t.getDescription()+", attuale:"+d);
		System.out.println(d+" = "+t.getDescription());
		
		org.springframework.util.Assert.isTrue(dt.equals(t.getDate()), "data transazione KO atteso:"+t.getDate()+", attuale:"+dt);
		System.out.println(dt+" = "+t.getDate());
		
		org.springframework.util.Assert.isTrue(a.equals(t.getAmount().replaceAll(" ", "")), "importo transazione KO atteso:"+t.getAmount()+", attuale:"+a);
		System.out.println(a+" = "+t.getAmount().replaceAll(" ", ""));
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}
}
