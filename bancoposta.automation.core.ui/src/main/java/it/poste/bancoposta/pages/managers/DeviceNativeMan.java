package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.LocksDevice;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.android.nativekey.PressesKey;
import it.poste.bancoposta.pages.DeviceNative;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;

import static org.junit.Assert.assertTrue;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="DeviceNative")
@Android
public class DeviceNativeMan extends LoadableComponent<DeviceNativeMan> implements DeviceNative{

	UiPage page;
	private WebElement notifica;
	private String notificaAttiva;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		
	}

	public void clickOnAutorizzaAccessoNotification(String title) 
	{
		System.out.println("provo a cercare la notifica "+page.getParticle(Locators.DeviceNativeElementMolecola.NOTIFICATION_TITLE)
		.getXPath(new Entry("notification",title)));
		page.getParticle(Locators.DeviceNativeElementMolecola.NOTIFICATION_TITLE).getElement( 
				new Entry("notification",title)).click();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

	@Override
	public void clickAllow() 
	{
		page.getParticle(Locators.DeviceNativeElementMolecola.ALLOW_BUTTON).visibilityOfElement(10L).click();
	}

	@Override
	public void resumeApp(CredentialAccessBean access) {
		WebDriver driver=page.getDriver();
		
		((LocksDevice) driver).unlockDevice();

		WaitManager.get().waitLongTime();

		((PressesKey) driver).pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
		
		WaitManager.get().waitMediumTime();

		WebElement card = page.getParticle(Locators.DeviceNativeElementMolecola.RESUME_CARD).getElement();

		card.click();

		WaitManager.get().waitMediumTime();
	}

	@Override
	public void aereoModeON() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clickOnBackFromWifiSetting() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aereoModeOFF() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setWifiOFF() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setWifiON() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clickOnNumPadiOS(char c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clickOnFineFromKeyboard() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void controlloHeaderToogleBonifico(String title) {
//		page.getParticle(Locators.DeviceNativeElementMolecola.NOTIFICATION_TITLE).getElement( 
//		new Entry("notification",title)).isDisplayed();
//WaitManager.get().waitShortTime();
WebDriverWait w=new WebDriverWait(page.getDriver(), TimeUnit.MINUTES.toSeconds(2));
w.until(ExpectedConditions.presenceOfElementLocated(
		page.getParticle(Locators.DeviceNativeElementMolecola.NOTIFICATION_TITLE).getXPath(new Entry("notification",title))));
	}

	@Override
	public boolean controlloDescrizioneToogleBonifico() {
		Properties texts= page.getLanguage();
		this.notifica=page.getParticle(Locators.DeviceNativeElementMolecola.DESCRIZIONETOOGLEATTIVO).getElement();
		notificaAttiva=notifica.getText().trim();
		System.out.println("text: "+notificaAttiva);
		return notificaAttiva.contains(texts.getProperty("descrizioneToogleAttivaBonifico"));
		
	}

	@Override
	public void clickOnNotification() {
		WebElement el = page.getParticle(Locators.DeviceNativeElementMolecola.NOTIFICATION_TITLE).getElement( 
				new Entry("notification","SERVIZIO ATTIVATO"));
		
		el.click();
		WaitManager.get().waitShortTime();
		
	}


}
