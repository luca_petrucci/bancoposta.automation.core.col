package it.poste.bancoposta.pages.managers;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import bean.datatable.CredentialAccessBean;
import io.appium.java_client.InteractsWithApps;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;

@PageManager(page="DeviceNative")
@IOS
public class DeviceNativeManIOS extends DeviceNativeMan 
{
	public void aereoModeON() {
		WebElement toggle=page.getParticle(Locators.DeviceNativeElementMolecola.AIR_MODE_TOGGLE).visibilityOfElement(10L);
		
		if(!toggle.getAttribute("value").equals("1")) {
			toggle.click();
		}
		
		WaitManager.get().waitMediumTime();
	}
	
	public void clickOnBackFromWifiSetting() {
		page.getParticle(Locators.DeviceNativeElementMolecola.WIFI_BACK_PAGE).visibilityOfElement(10L).click();
		
	}
	
	public void aereoModeOFF() {
		WebElement toggle=page.getParticle(Locators.DeviceNativeElementMolecola.AIR_MODE_TOGGLE).visibilityOfElement(10L);
		
		if(toggle.getAttribute("value").equals("1")) {
			toggle.click();
		}
		
		WaitManager.get().waitShortTime();
		
		try
		{
			UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@*='OK']")),3).click();
		}
		catch(Exception err)
		{
			
		}
		
		WaitManager.get().waitShortTime();
	}
	
	public void resumeApp(CredentialAccessBean access)
	{
		Properties pro = UIUtils.ui().getCurrentProperties();
		
		((InteractsWithApps) page.getDriver()).activateApp(pro.getProperty("ios.bundle.id"));
		WaitManager.get().waitShortTime();
	}
	
	public void setWifiOFF()
	{
		page.getParticle(Locators.DeviceNativeElementMolecola.WIFI_LABEL).visibilityOfElement(10L).click();
		
		WaitManager.get().waitShortTime();
		
		WebElement toggle=page.getParticle(Locators.DeviceNativeElementMolecola.WIFI_TOGGLE).visibilityOfElement(10L);
		
		if(toggle.getAttribute("value").equals("1")) {
			toggle.click();
		}
		
		WaitManager.get().waitShortTime();
		
		
	}
	
	public void setWifiON()
	{
		page.getParticle(Locators.DeviceNativeElementMolecola.WIFI_LABEL).visibilityOfElement(10L).click();
		
		WaitManager.get().waitShortTime();
		
		WebElement toggle=page.getParticle(Locators.DeviceNativeElementMolecola.WIFI_TOGGLE).visibilityOfElement(10L);
		
		if(!toggle.getAttribute("value").equals("1")) {
			toggle.click();
		}
		
		WaitManager.get().waitShortTime();
	}
	
	@Override
	public void clickOnNumPadiOS(char c) {
		System.out.println("click on pad "+page.getParticle(Locators.DeviceNativeElementMolecola.IOSPAD).getLocator(new Entry("padNum",""+c)));
		page.getParticle(Locators.DeviceNativeElementMolecola.IOSPAD).getElement(new Entry("padNum",""+c)).click();
		
	}
	
	@Override
	public void clickOnFineFromKeyboard() {
		page.getParticle(Locators.DeviceNativeElementMolecola.FINEKEYBOARD).visibilityOfElement(10L).click();
	}
}
