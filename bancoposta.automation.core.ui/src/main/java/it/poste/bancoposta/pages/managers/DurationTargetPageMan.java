package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.DurationTargetPage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="DurationTargetPage")
@Android
@IOS
public class DurationTargetPageMan extends LoadableComponent<DurationTargetPageMan> implements DurationTargetPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement headerTarget;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancelButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement durationDescriptionPage;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement threeMonthsField;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement sixMonthsField;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement nineMonthsField;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement dateSelectionField;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement confirmButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	@Override
	protected void isLoaded() throws Error {

	}

	@Override
	protected void load() 
	{
		page.getParticle(Locators.SalvadanaioPageMolecola.DURATIONDESCRIPTIONPAGE).visibilityOfElement(10L);
		page.getParticle(Locators.SalvadanaioPageMolecola.THREEMONTHSFIELD).visibilityOfElement(null);
		page.getParticle(Locators.SalvadanaioPageMolecola.SIXMONTHSFIELD).visibilityOfElement(null);
		page.getParticle(Locators.SalvadanaioPageMolecola.NINEMONTHSFIELD).visibilityOfElement(null);
	}


	public void durationAndDateSelection()
	{
		threeMonthsField = page.getParticle(Locators.SalvadanaioPageMolecola.THREEMONTHSFIELD).getElement();
		

		threeMonthsField.click();
		dateSelectionField= page.getParticle(Locators.SalvadanaioPageMolecola.DATESELECTIONFIELD).getElement();
		org.springframework.util.Assert.isTrue(dateSelectionField.isDisplayed()); 

		String o= dateSelectionField.getText();
		System.out.println(o);
		//dateSelectionField.click();

		confirmButton= page.getParticle(Locators.SalvadanaioPageMolecola.CONFIRMBUTTON).getElement();
		confirmButton.click();





	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}



}
