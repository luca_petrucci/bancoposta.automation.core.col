package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import automation.core.ui.uiobject.UiParticle;
import bean.datatable.CredentialAccessBean;
import bean.datatable.SalvadanaioDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.EnableYourProductApp;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.utils.PageMolecolaImgCreator;
import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="EnableYourProductApp")
@Android
public class EnableYourProductAppMan extends LoadableComponent<EnableYourProductAppMan> implements EnableYourProductApp
{

	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web  
	 */
	protected WebElement enableProductheader;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */ 

	protected WebElement closeButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	protected WebElement deviceImage;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	protected WebElement descriptionTitle;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	protected WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	protected WebElement discoverLink;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	protected WebElement startButton;
	protected String enableProductHeadertwo;
	protected String descriptionTitleTwo;
	protected String descriptionTwo;
	protected String discoverLinkTwo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	protected WebElement nonHaiAccessoDispositivoButton;
	protected WebElement continuaButtonPopup;
	protected WebElement confermaButton;
	protected WebElement iniziaButtonNonOnboarding;
	protected WebElement meseAnnoTextField;
	protected WebElement continuaButtonCard;
	protected WebElement cvvInsertField;
	protected WebElement confermaButtonCVV;
	protected WebElement codiceConto1;
	protected WebElement codiceConto2;
	protected WebElement richiediSmsButton;
	protected WebElement confermaButtonSms;
	protected WebElement creaPosteIdButton;
	protected WebElement scegliPosteIdTextField;
	protected WebElement confermaPosteIdTextField;
	protected WebElement confermaPosteIdButton;
	protected WebElement proseguiAttivazioneButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	@Override
	protected void isLoaded() throws Error {

	}  

	@Override
	protected void load() 
	{
		page.get();



	}

	public void checkEnableYourProductPage(){

		this.enableProductheader =page.getParticle(Locators.EnableYourProductMolecola.ENABLEPRODUCTHEADER).getElement();
		enableProductHeadertwo= enableProductheader.getText().trim();
		System.out.println(enableProductHeadertwo);

		Assert.assertTrue("Abilita il tuo prodotto in App! errato -->"+enableProductHeadertwo,enableProductHeadertwo.equals("Abilita il tuo prodotto in App!"));


		this.closeButton =page.getParticle(Locators.EnableYourProductMolecola.CLOSEBUTTON).getElement();

		Assert.assertTrue("close button non presente",closeButton.isDisplayed());

		this.deviceImage =page.getParticle(Locators.EnableYourProductMolecola.IMAGENOONBOARDING).getElement();

		Assert.assertTrue("device image non presente",deviceImage.isDisplayed());

		try {
			this.descriptionTitle =page.getParticle(Locators.EnableYourProductMolecola.DESCRIPTIONTITLE).getElement();
			descriptionTitleTwo=descriptionTitle.getText().trim();

			System.out.println(descriptionTitleTwo);

			Assert.assertTrue("Abilita anche questo dispositivo ad operare! errato -->"+descriptionTitleTwo,descriptionTitleTwo.equals("Abilita il tuo prodotto in App!"));

			this.description =page.getParticle(Locators.EnableYourProductMolecola.DESCRIPTION).getElement();
			descriptionTwo=description.getText().trim();

			Assert.assertTrue("Descrizione errata -->"+descriptionTwo,descriptionTwo.contains("Abilita il tuo conto e la tua carta di debito per operare direttamente in App"));

			this.discoverLink =page.getParticle(Locators.EnableYourProductMolecola.DISCOVERLINK).getElement();

			Assert.assertTrue("scopri di piu non presente",discoverLink.isDisplayed());
		} catch (Exception e) {
			// TODO: handle exception
		}

		this.startButton =page.getParticle(Locators.EnableYourProductMolecola.INIZIABUTTONONBOARDING2).getElement();

		Assert.assertTrue("inizia non presente",startButton.isDisplayed());

		System.out.println("I controlli della pagina sono terminati correttamente!");

	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

	@Override
	public void clickOnNonOra() {
		page.readPageSource();
		page.getParticle(Locators.EnableYourProductMolecola.BTNNONORA).visibilityOfElement(10L).click();
	}

	public void checkIniziaButton(){
		try {
			page.getParticle(Locators.EnableYourProductMolecola.INIZIABUTTONONBOARDING).visibilityOfElement(30L);
		} catch (Exception e) {
			page.getParticle(Locators.EnableYourProductMolecola.INIZIABUTTONONBOARDING2).visibilityOfElement(30L);
			System.out.println(page.getParticle(Locators.EnableYourProductMolecola.INIZIABUTTONONBOARDING2).getElement());
		}

	}

	@Override
	public void doOnboarding(String username) {
		String file = username.replace("@", "");
		try {
			page.getParticle(Locators.EnableYourProductMolecola.INIZIABUTTONONBOARDING2).visibilityOfElement(20L).click();
			
			page.getParticle(Locators.EnableYourProductMolecola.NONHAIACCESSODISPOSITIVOBUTTON).visibilityOfElement(20L).click();
					
			page.getParticle(Locators.EnableYourProductMolecola.CONTINUABUTTONPOPUP).visibilityOfElement(20L).click();
				
			page.getParticle(Locators.EnableYourProductMolecola.CONFERMABUTTON).visibilityOfElement(20L).click();
				
			page.getParticle(Locators.EnableYourProductMolecola.INIZIABUTTONONBOARDING).visibilityOfElement(20L).click();
				
			Properties t=page.getLanguage();
			page.getParticle(Locators.EnableYourProductMolecola.MESEANNOTEXTFIELD).visibilityOfElement(20L).sendKeys(t.getProperty("mese"));
			try {
				((AppiumDriver)page.getDriver()).hideKeyboard();
			} catch (Exception e) {
				// TODO: handle exception
			}
					
			page.getParticle(Locators.EnableYourProductMolecola.CONTINUABUTTONCARD).visibilityOfElement(20L).click();
			page.getParticle(Locators.EnableYourProductMolecola.CVVINSERTFIELD).visibilityOfElement(20L).sendKeys(t.getProperty("CVV"));
			
			try {
				((AppiumDriver)page.getDriver()).hideKeyboard();
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			page.getParticle(Locators.EnableYourProductMolecola.CONFERMABUTTONCVV).visibilityOfElement(20L).click();
			page.getParticle(Locators.EnableYourProductMolecola.CODICECONTO1).visibilityOfElement(20L).sendKeys(t.getProperty("CodiceConto"));
			page.getParticle(Locators.EnableYourProductMolecola.RICHIEDISMSBUTTON).visibilityOfElement(20L).click();	
			
			WaitManager.get().waitMediumTime();
			
			page.getParticle(Locators.EnableYourProductMolecola.CODICECONTO2).visibilityOfElement(20L).sendKeys(t.getProperty("CodiceConto"));
			page.getParticle(Locators.EnableYourProductMolecola.CONFERMABUTTONSMS).visibilityOfElement(20L).click();
			try {
				page.getParticle(Locators.EnableYourProductMolecola.CREAPOSTEIDBUTTON).visibilityOfElement(20L).click();
				page.getParticle(Locators.EnableYourProductMolecola.SCEGLIPOSTEIDTEXTFIELD).visibilityOfElement(20L).sendKeys(t.getProperty("PosteID"));
				page.getParticle(Locators.EnableYourProductMolecola.CONFERMAPOSTEIDTEXTFIELD).visibilityOfElement(20L).sendKeys(t.getProperty("PosteID"));
				page.getParticle(Locators.EnableYourProductMolecola.CONFERMAPOSTEIDBUTTON).visibilityOfElement(20L).click();
			} catch (Exception r) {

			}		
			
			WaitManager.get().waitLongTime();
			
			System.out.println(page.getDriver().getPageSource());
			page.getParticle(Locators.EnableYourProductMolecola.PROSEGUIATTIVAZIONEBUTTON).visibilityOfElement(20L).click();
			
			WaitManager.get().waitFor(TimeUnit.SECONDS, 20L);
			
		} catch (Exception e) {
			e.printStackTrace();
			page.getParticle(Locators.EnableYourProductMolecola.INIZIABUTTONONBOARDING).visibilityOfElement(20L).click();
			Properties t=page.getLanguage();
			page.getParticle(Locators.EnableYourProductMolecola.MESEANNOTEXTFIELD).visibilityOfElement(20L).sendKeys(t.getProperty("mese"));
			
			try {
				((AppiumDriver)page.getDriver()).hideKeyboard();
			} catch (Exception r) {

			}		
			page.getParticle(Locators.EnableYourProductMolecola.CONTINUABUTTONCARD).visibilityOfElement(20L).click();
			page.getParticle(Locators.EnableYourProductMolecola.CVVINSERTFIELD).visibilityOfElement(20L).sendKeys(t.getProperty("CVV"));
			
			try {
				((AppiumDriver)page.getDriver()).hideKeyboard();
			} catch (Exception r) {

			}
			page.getParticle(Locators.EnableYourProductMolecola.CONFERMABUTTONCVV).visibilityOfElement(20L).click();
			page.getParticle(Locators.EnableYourProductMolecola.CODICECONTO1).visibilityOfElement(20L).sendKeys(t.getProperty("CodiceConto"));
			page.getParticle(Locators.EnableYourProductMolecola.RICHIEDISMSBUTTON).visibilityOfElement(20L).click();
			
			WaitManager.get().waitMediumTime();
			
			page.getParticle(Locators.EnableYourProductMolecola.CODICECONTO2).visibilityOfElement(20L).click();
			page.getParticle(Locators.EnableYourProductMolecola.CODICECONTO2).visibilityOfElement(20L).sendKeys(t.getProperty("CodiceConto"));
			page.getParticle(Locators.EnableYourProductMolecola.CONFERMABUTTONSMS).visibilityOfElement(20L).click();
			try {
				page.getParticle(Locators.EnableYourProductMolecola.CREAPOSTEIDBUTTON).visibilityOfElement(20L).click();
				page.getParticle(Locators.EnableYourProductMolecola.SCEGLIPOSTEIDTEXTFIELD).visibilityOfElement(20L).sendKeys(t.getProperty("PosteID"));
				page.getParticle(Locators.EnableYourProductMolecola.CONFERMAPOSTEIDTEXTFIELD).visibilityOfElement(20L).sendKeys(t.getProperty("PosteID"));
				page.getParticle(Locators.EnableYourProductMolecola.CONFERMAPOSTEIDBUTTON).visibilityOfElement(20L).click();
			} catch (Exception r) {

			}
			WaitManager.get().waitLongTime();
			
			
			page.getParticle(Locators.EnableYourProductMolecola.PROSEGUIATTIVAZIONEBUTTON).visibilityOfElement(20L).click();
			
			WaitManager.get().waitFor(TimeUnit.SECONDS, 20L);

		}
	}

	@Override
	public boolean isCreaPosteIdVisible() {
		boolean isDisplayed = false;
		UiParticle s = page.getParticle(Locators.EnableYourProductMolecola.CREAPOSTEIDBUTTON);
		if(s != null) {
			try {
				s.visibilityOfElement(15L);
				isDisplayed=true;
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return isDisplayed;
	}

	@Override
	public void createPosteID(String posteid) {
		// Clicco su Crea BTN 
		page.getParticle(Locators.EnableYourProductMolecola.CREAPOSTEIDBUTTON).visibilityOfElement(10L).click();

		// Inserisco il primo PosteId
		page.getParticle(Locators.EnableYourProductMolecola.SCEGLIPOSTEIDTEXTFIELD).visibilityOfElement(10L).sendKeys(posteid);
		try {page.getDriver().findElement(By.xpath("//*[@*='Fine' and @*='XCUIElementTypeStaticText']")).click();} catch (Exception e) {}
		// Inserisco il secondo PosteId
		page.getParticle(Locators.EnableYourProductMolecola.CONFERMAPOSTEIDTEXTFIELD).visibilityOfElement(10L).sendKeys(posteid);
		try {page.getDriver().findElement(By.xpath("//*[@*='Fine' and @*='XCUIElementTypeStaticText']")).click();} catch (Exception e) {}

		//		clicco su conferma
		page.getParticle(Locators.EnableYourProductMolecola.CONFERMAPOSTEIDBUTTON).visibilityOfElement(10L).click();

		// Attendo che la TYP di creazione sia mostrata per cliccare su accedi
		page.getParticle(Locators.EnableYourProductMolecola.PROSEGUIATTIVAZIONEBUTTON).visibilityOfElement(10L).click();
	}

}
