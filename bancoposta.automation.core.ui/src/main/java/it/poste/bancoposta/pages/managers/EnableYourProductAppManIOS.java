package it.poste.bancoposta.pages.managers;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.HideKeyboardStrategy;
import io.appium.java_client.touch.offset.PointOption;
import it.poste.bancoposta.pages.EnableYourProductApp;
import it.poste.bancoposta.pages.Locators;

@PageManager(page="EnableYourProductApp")
@IOS
public class EnableYourProductAppManIOS extends EnableYourProductAppMan 
{
	private By pickers1 = MobileBy.xpath("//*[@*='XCUIElementTypePickerWheel'][1]"); 
	private By pickers2 = MobileBy.xpath("//*[@*='XCUIElementTypePickerWheel'][2]"); 
	public void checkEnableYourProductPage()
	{
		this.enableProductheader =page.getParticle(Locators.EnableYourProductMolecola.ENABLEPRODUCTHEADER).getElement();
		enableProductHeadertwo= enableProductheader.getText().trim();
		System.out.println(enableProductHeadertwo);

		Assert.assertTrue("Abilita anche questo dispositivo ad operare! errato -->"+enableProductHeadertwo,enableProductHeadertwo.equals("Abilita anche questo dispositivo ad operare!"));


		this.closeButton =page.getParticle(Locators.EnableYourProductMolecola.CLOSEBUTTON).getElement();

		Assert.assertTrue("close button non presente",closeButton.isDisplayed());

		this.deviceImage =page.getParticle(Locators.EnableYourProductMolecola.DEVICEIMAGE).getElement();

		Assert.assertTrue("device image non presente",deviceImage.isEnabled());

		//		try {
		//			this.descriptionTitle =page.getParticle(Locators.EnableYourProductMolecola.DESCRIPTIONTITLE).getElement();
		//			descriptionTitleTwo=descriptionTitle.getText().trim();
		//			
		//			System.out.println(descriptionTitleTwo);
		//			
		//			Assert.assertTrue("Abilita il tuo prodotto in App! errato -->"+descriptionTitleTwo,descriptionTitleTwo.equals("Abilita il tuo prodotto in App!"));
		//			
		//			this.description =page.getParticle(Locators.EnableYourProductMolecola.DESCRIPTION).getElement();
		//			descriptionTwo=description.getText().trim();
		//			
		//			Assert.assertTrue("Descrizione errata -->"+descriptionTwo,descriptionTwo.contains("Abilita il tuo conto e la tua carta di debito per operare direttamente in App"));
		//			
		//			this.discoverLink =page.getParticle(Locators.EnableYourProductMolecola.DISCOVERLINK).getElement();
		//			
		//			Assert.assertTrue("scopri di piu non presente",discoverLink.isDisplayed());
		//		} catch (Exception e) {
		//			// TODO: handle exception
		//		}
		this.discoverLink =page.getParticle(Locators.EnableYourProductMolecola.DISCOVERLINK).getElement();
		Assert.assertTrue("scopri di piu non presente",discoverLink.isDisplayed());

		this.startButton =page.getParticle(Locators.EnableYourProductMolecola.STARTBUTTON).getElement();

		Assert.assertTrue("inizia non presente",startButton.isDisplayed());

		System.out.println("I controlli della pagina sono terminati correttamente!");
	}

	@Override
	public void doOnboarding(String username) {
		String file = username.replace("@", "");
		try {
			page.getParticle(Locators.EnableYourProductMolecola.INIZIABUTTONONBOARDING2).visibilityOfElement(20L).click();
			WaitManager.get().waitMediumTime();
			page.readPageSource();
			System.out.println("pagina"+page.getPageSource());
//			Dimension size = page.getDriver().manage().window().getSize();
//			 new TouchAction((PerformsTouchActions) page.getDriver())
//	         .press(PointOption.point(155, 530))
//	         .release()
//	         .perform();
			page.getParticle(Locators.EnableYourProductMolecola.NONHAIACCESSODISPOSITIVOBUTTON).visibilityOfElement(20L).click();
				
			page.getParticle(Locators.EnableYourProductMolecola.CONTINUABUTTONPOPUP).visibilityOfElement(20L).click();
				
			page.getParticle(Locators.EnableYourProductMolecola.CONFERMABUTTON).visibilityOfElement(20L).click();
					
			page.getParticle(Locators.EnableYourProductMolecola.INIZIABUTTONONBOARDING).visibilityOfElement(20L).click();
					
			Properties t=page.getLanguage();
			char num1=t.getProperty("mese").charAt(0);
			char numero=t.getProperty("mese").charAt(1);
			String s=String.valueOf(num1);
			String s1=String.valueOf(numero);
			String finale=s.concat(s1);		
			WebDriverWait wait = new WebDriverWait(page.getDriver(), 25);
			List<WebElement> pickerEls = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(pickers1));
			pickerEls.get(0).sendKeys(finale);

			pickerEls = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(pickers2));
			char num2=t.getProperty("mese").charAt(2);
			char num3=t.getProperty("mese").charAt(3);
			String s2=String.valueOf(num2);
			String s3=String.valueOf(num3);
			String s4=s2.concat(s3);
			String anno="20";
			pickerEls.get(0).sendKeys(anno.concat(s4));
			try {page.getDriver().findElement((By.xpath("//*[@*='Fine' and @*='XCUIElementTypeButton']"))).click();} catch (Exception d) {}
			
			page.getParticle(Locators.EnableYourProductMolecola.CONTINUABUTTONCARD).visibilityOfElement(20L).click();
			page.getParticle(Locators.EnableYourProductMolecola.CVVINSERTFIELD).visibilityOfElement(20L).sendKeys(t.getProperty("CVV"));
			try {page.getDriver().findElement((By.xpath("//*[@*='Fine' and @*='XCUIElementTypeButton']"))).click();} catch (Exception d) {}
			page.getParticle(Locators.EnableYourProductMolecola.CONFERMABUTTONCVV).visibilityOfElement(20L).click();
			page.getParticle(Locators.EnableYourProductMolecola.CODICECONTO1).visibilityOfElement(20L).sendKeys(t.getProperty("CodiceConto"));
			page.getParticle(Locators.EnableYourProductMolecola.RICHIEDISMSBUTTON).visibilityOfElement(20L).click();	
			
			WaitManager.get().waitMediumTime();
			
			page.getParticle(Locators.EnableYourProductMolecola.CODICECONTO2).visibilityOfElement(20L).sendKeys(t.getProperty("CodiceConto"));
			page.getParticle(Locators.EnableYourProductMolecola.CONFERMABUTTONSMS).visibilityOfElement(20L).click();
			try {
				page.getParticle(Locators.EnableYourProductMolecola.CREAPOSTEIDBUTTON).visibilityOfElement(20L).click();
			} catch (Exception r) {

			}	
			page.getParticle(Locators.EnableYourProductMolecola.SCEGLIPOSTEIDTEXTFIELD).visibilityOfElement(20L).sendKeys(t.getProperty("PosteID"));
			page.getParticle(Locators.EnableYourProductMolecola.CONFERMAPOSTEIDTEXTFIELD).visibilityOfElement(20L).sendKeys(t.getProperty("PosteID"));
			try {page.getDriver().findElement((By.xpath("//*[@*='Fine' and @*='XCUIElementTypeButton']"))).click();} catch (Exception d) {}
			page.getParticle(Locators.EnableYourProductMolecola.CONFERMAPOSTEIDBUTTON).visibilityOfElement(20L).click();
			
			WaitManager.get().waitLongTime();
			
			System.out.println(page.getDriver().getPageSource());
			page.getParticle(Locators.EnableYourProductMolecola.PROSEGUIATTIVAZIONEBUTTON).visibilityOfElement(20L).click();
			
			WaitManager.get().waitFor(TimeUnit.SECONDS, 20L);
			
		} catch (Exception e) {
			e.printStackTrace();
//			page.getParticle(Locators.EnableYourProductMolecola.INIZIABUTTONONBOARDING).visibilityOfElement(20L).click();
			Properties t=page.getLanguage();
			char num1=t.getProperty("mese").charAt(0);
			char numero=t.getProperty("mese").charAt(1);
			String s=String.valueOf(num1);
			String s1=String.valueOf(numero);
			String finale=s.concat(s1);		
			WebDriverWait wait = new WebDriverWait(page.getDriver(), 25);
			List<WebElement> pickerEls = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(pickers1));
			pickerEls.get(0).sendKeys(finale);


			// Inserisco la scadenza(anno)
			pickerEls = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(pickers2));
			char num2=t.getProperty("mese").charAt(2);
			char num3=t.getProperty("mese").charAt(3);
			String s2=String.valueOf(num2);
			String s3=String.valueOf(num3);
			String s4=s2.concat(s3);
			String anno="20";
			pickerEls.get(0).sendKeys(anno.concat(s4));
			try {page.getDriver().findElement((By.xpath("//*[@*='Fine' and @*='XCUIElementTypeButton']"))).click();} catch (Exception d) {}
			page.getParticle(Locators.EnableYourProductMolecola.CONTINUABUTTONCARD).visibilityOfElement(20L).click();
			page.getParticle(Locators.EnableYourProductMolecola.CVVINSERTFIELD).visibilityOfElement(20L).sendKeys(t.getProperty("CVV"));
			try {page.getDriver().findElement((By.xpath("//*[@*='Fine' and @*='XCUIElementTypeButton']"))).click();} catch (Exception d) {}
			
			page.getParticle(Locators.EnableYourProductMolecola.CONFERMABUTTONCVV).visibilityOfElement(20L).click();
			page.getParticle(Locators.EnableYourProductMolecola.CODICECONTO1).visibilityOfElement(20L).sendKeys(t.getProperty("CodiceConto"));
			page.getParticle(Locators.EnableYourProductMolecola.RICHIEDISMSBUTTON).visibilityOfElement(20L).click();
			
			page.getParticle(Locators.EnableYourProductMolecola.CODICECONTO2).visibilityOfElement(20L).click();
			page.getParticle(Locators.EnableYourProductMolecola.CODICECONTO2).visibilityOfElement(20L).sendKeys(t.getProperty("CodiceConto"));
			page.getParticle(Locators.EnableYourProductMolecola.CONFERMABUTTONSMS).visibilityOfElement(20L).click();
			try {
				page.getParticle(Locators.EnableYourProductMolecola.CREAPOSTEIDBUTTON).visibilityOfElement(20L).click();
			} catch (Exception r) {

			}
			page.getParticle(Locators.EnableYourProductMolecola.SCEGLIPOSTEIDTEXTFIELD).visibilityOfElement(20L).sendKeys(t.getProperty("PosteID"));
			page.getParticle(Locators.EnableYourProductMolecola.CONFERMAPOSTEIDTEXTFIELD).visibilityOfElement(20L).sendKeys(t.getProperty("PosteID"));
			try {page.getDriver().findElement((By.xpath("//*[@*='Fine' and @*='XCUIElementTypeButton']"))).click();} catch (Exception d) {}
			page.getParticle(Locators.EnableYourProductMolecola.CONFERMAPOSTEIDBUTTON).visibilityOfElement(20L).click();
			WaitManager.get().waitLongTime();
			
			page.getParticle(Locators.EnableYourProductMolecola.PROSEGUIATTIVAZIONEBUTTON).visibilityOfElement(20L).click();
			
			WaitManager.get().waitFor(TimeUnit.SECONDS, 20L);

		}
	}

}
