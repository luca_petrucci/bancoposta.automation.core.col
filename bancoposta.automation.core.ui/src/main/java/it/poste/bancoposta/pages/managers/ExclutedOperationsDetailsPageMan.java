package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.w3c.dom.NodeList;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.TransactionDetailBean;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.ExclutedOperationsDetailsPage;
import it.poste.bancoposta.pages.ExclutedOperationsPage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;
import test.automation.core.cmd.adb.AdbCommandPrompt;
import test.automation.core.cmd.adb.AdbKeyEvent;
import utils.Entry;
import utils.ObjectFinderLight;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.zookeeper.OpResult;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="ExclutedOperationsDetailsPage")
@Android
@IOS
public class ExclutedOperationsDetailsPageMan extends LoadableComponent<ExclutedOperationsDetailsPageMan> implements ExclutedOperationsDetailsPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement leftmenu;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement rightMenu;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement fromDate;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement toDate;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement filterButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement totalAmount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement operationElement;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement operationAmountVal;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement operationTitle;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement operationDescription;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement operationDate;

	private TransactionDetailBean bean;
	private SimpleDateFormat dateFormat=new SimpleDateFormat("dd #");

	public void setTransactionDetails(TransactionDetailBean b)
	{
		this.bean=b;
	}

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public void checkTransactionIsPresent() throws Exception
	{
		boolean find=findTransaction();

		if(!find)
		{
			System.err.println("WARN: la transazione non risulta ancora categorizzata nelle liste "+bean.getCategoryTransation()+" - "+bean.getSubCategoryTransaction());
		}
	}

	public boolean findTransaction() throws Exception 
	{
		String list="";
		String title="";
		String descr="";
		String date="";
		String amount="";

		list=page.getParticle(Locators.ExclutedOperationsDetailsPageMolecola.OPERATIONELEMENT).getLocator();
		title=page.getParticle(Locators.ExclutedOperationsDetailsPageMolecola.OPERATIONTITLE).getLocator();
		descr=page.getParticle(Locators.ExclutedOperationsDetailsPageMolecola.OPERATIONDESCRIPTION).getLocator();
		date=page.getParticle(Locators.ExclutedOperationsDetailsPageMolecola.OPERATIONDATE).getLocator();
		amount=page.getParticle(Locators.ExclutedOperationsDetailsPageMolecola.OPERATIONAMOUNTVAL).getLocator();

		//ricerco la transazione nella lista se la trovo controllo la pagina di dettaglio
		List<WebElement> L=page.getDriver().findElements(By.xpath(list));

		boolean find=false;

		//for(WebElement e : L)
		for(int i=0;i<L.size();i++)
		{

			try {
				this.operationTitle=L.get(i).findElement(By.xpath(title));
				this.operationDescription=L.get(i).findElement(By.xpath(descr));
				this.operationAmountVal=L.get(i).findElement(By.xpath(amount));
				this.operationDate=L.get(i).findElement(By.xpath(date));

			} catch (Exception e) {
				Utility.swipe((MobileDriver<?>) page.getDriver(), Utility.DIRECTION.DOWN, 500);
			}

			String mese=Utility.getCurrentMonth().toUpperCase().substring(0, 3);

			Calendar c=Calendar.getInstance();
			Date d=c.getTime();

			mese=dateFormat.format(d).replace("#", mese);

			String importo=this.operationAmountVal.getText().trim();

			boolean titleOK=this.operationTitle.getText().trim().toLowerCase().equals(bean.getCategoryTransation().toLowerCase());
			boolean descOK=this.operationDescription.getText().trim().toLowerCase().equals(bean.getTransactionType().toLowerCase());
			boolean dataOK=this.operationDate.getText().trim().equals(mese);
			boolean importoOK=
					((bean.getOperation().equals("-")) ? importo.startsWith("-") : importo.startsWith("+")) 
					&& 
					Utility.parseCurrency(importo, Locale.ITALY) == Double.parseDouble(bean.getAmount());

			find=titleOK && descOK && dataOK && importoOK;

			if(find)
				break;
		}

		return find;
	}

	public ExclutedOperationsPage clickBack() 
	{
		this.leftmenu=page.getParticle(Locators.ExclutedOperationsDetailsPageMolecola.LEFTMENU).getElement();
		

		this.leftmenu.click();

		ExclutedOperationsPage p=(ExclutedOperationsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ExclutedOperationsPage", page.getDriver().getClass(), page.getLanguage());
		
		p.setBean(bean);
		
		p.checkPage();

		return p;
	}

	public List<String[]> readSpeseDettaglio() 
	{
		//M0027
		String pageSource=page.getDriver().getPageSource();
		NodeList operation=ObjectFinderLight.getElements(pageSource, page.getParticle(Locators.ExclutedOperationsDetailsPageMolecola.OPERATIONDESCRIPTION).getLocator());
		NodeList opDate=ObjectFinderLight.getElements(pageSource, page.getParticle(Locators.ExclutedOperationsDetailsPageMolecola.OPERATIONDATE).getLocator());
		NodeList opValue=ObjectFinderLight.getElements(pageSource, page.getParticle(Locators.ExclutedOperationsDetailsPageMolecola.OPERATIONAMOUNTVAL).getLocator());

		ArrayList<String[]> L=new ArrayList<>();

		org.springframework.util.Assert.isTrue(operation.getLength() == opDate.getLength() && operation.getLength() == opValue.getLength(), "lista con valori non uguali");

		for(int i=0;i<operation.getLength();i++)
		{
			String op=operation.item(i).getAttributes().getNamedItem("text").getNodeValue().trim();
			String date=opDate.item(i).getAttributes().getNamedItem("text").getNodeValue().trim();
			String val=opValue.item(i).getAttributes().getNamedItem("text").getNodeValue().trim();

			String[] v=new String[4];
			v[0]=bean.getCsvPage();
			v[1]=op;
			v[2]=date;
			v[3]=val;

			System.out.println(Arrays.toString(v));

			L.add(v);
		}

		return L;
	}

	public void readAndCheckCsv(List<String[]> speseDettaglioList) throws Exception 
	{
		Reader reader =new BufferedReader(new FileReader(bean.getCsvPage().replaceAll(" ", "_")+".csv"));
//		Reader reader = Files.newBufferedReader(Paths.get(bean.getCsvPage().replaceAll(" ", "_")+".csv"));
		CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader());

		int row=0;

		for(CSVRecord csvRecord : csvParser)
		{
			String category=csvRecord.get(0).trim().toLowerCase();
			String esercente=csvRecord.get(1).trim().toLowerCase();
			String data=csvRecord.get(2).trim();
			String value=csvRecord.get(3).trim();

			//boolean find=false;

			System.out.println("check row:"+category+","+esercente+","+data+","+value);

			if(row >= speseDettaglioList.size())
				break;

			String[] el=speseDettaglioList.get(row);

			boolean cat=category.equals(el[0].toLowerCase());
			boolean eser=esercente.contains(el[1].toLowerCase());
			boolean dt=data.equals(el[2]);
			boolean v=value.equals(el[3]);

			System.out.println("categoria:"+cat);
			System.out.println("esercente:"+eser);
			System.out.println("data:"+dt);
			System.out.println("value:"+v);

			row++;

			org.springframework.util.Assert.isTrue(cat && eser && dt && v, category+","+esercente+","+data+","+value+":"+Arrays.toString(el)+" --> [KO]");

		}

		System.out.println("csv check [OK]");
	}

	public void esportaCsv() 
	{
		page.getParticle(Locators.ExclutedOperationsDetailsPageMolecola.ESPORTA_CSV).visibilityOfElement(10L).click();

		WaitManager.get().waitMediumTime();

		try {
			page.getParticle(Locators.ExclutedOperationsDetailsPageMolecola.ALLOWPOPUPBUTTON).visibilityOfElement(10L).click();

		} catch (Exception e) {
			// TODO: handle exception
		}

		WaitManager.get().waitMediumTime();


		Properties p=UIUtils.ui().getCurrentProperties();

		try {
			AdbCommandPrompt c=new AdbCommandPrompt(p.getProperty("adb.path"), p.getProperty("ios.udid"));

			c.keyEvent(AdbKeyEvent.KEYCODE_BACK);
		} catch (Exception e) {
			// TODO: handle exception
		}

		String csvFile="/storage/emulated/0/Download/"+bean.getCsvPage().replaceAll(" ", "_")+".csv";
		System.out.println(csvFile);

		AdbCommandPrompt c=new AdbCommandPrompt(p.getProperty("adb.path"), p.getProperty("ios.udid"));

		c.pull(csvFile, bean.getCsvPage().replaceAll(" ", "_")+".csv");
	}

	public void clickRightMenu() 
	{
		page.getParticle(Locators.ExclutedOperationsDetailsPageMolecola.RIGHTMENU).visibilityOfElement(null).click();
	}

	public void removeCsv() 
	{
		String csvFile="/storage/emulated/0/Download/"+bean.getCsvPage().replaceAll(" ", "_")+".csv";
		System.out.println("delete "+csvFile);
		Properties p=UIUtils.ui().getCurrentProperties();

		AdbCommandPrompt c=new AdbCommandPrompt(p.getProperty("adb.path"), p.getProperty("ios.udid"));

		try {
			c.remove(csvFile);

			File f=new File(bean.getCsvPage().replaceAll(" ", "_")+".csv");

			f.delete();

		} catch (Exception e) {
			System.err.println("[WARNING] - file csv non cancellato:"+csvFile);
		}
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

	@Override
	public void setBean(TransactionDetailBean bean) 
	{
		this.bean=bean;
	}
}
