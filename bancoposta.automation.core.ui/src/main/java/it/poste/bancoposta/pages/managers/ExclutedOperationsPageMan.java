package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.TransactionDetailBean;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.ExclutedOperationsDetailsPage;
import it.poste.bancoposta.pages.ExclutedOperationsPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.YourExpencesDetailsPage;
import test.automation.core.UIUtils;
import utils.Entry;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="ExclutedOperationsPage")
@Android
public class ExclutedOperationsPageMan extends LoadableComponent<ExclutedOperationsPageMan> implements ExclutedOperationsPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement leftmenu;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement rightMenu;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement totalAmount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement monthList;
	/**
	 * @android campo dinamico sostituire $(month) con il nome del mese da ricercare
	 * @ios campo dinamico sostituire $(month) con il mese da ricercare
	 * @web 
	 */
	private WebElement monthElement;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement operationList;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement operationElement;
	protected TransactionDetailBean bean;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();

		String header=page.getParticle(Locators.ExclutedOperationsPageMolecola.HEADER).getElement().getText().trim().toLowerCase();

		Assert.assertTrue("controllo header",header.equals(bean.getCategoryTransation().toLowerCase()));

	}

	public ExclutedOperationsDetailsPage gotoSubCategory() 
	{

		//			try {
		//				Thread.sleep(20000);
		//			} catch (Exception e) {
		//				
		//			}
		String list="";
		String title="";


		list=page.getParticle(Locators.ExclutedOperationsPageMolecola.OPERATIONELEMENT).getLocator();
		title=page.getParticle(Locators.ExclutedOperationsPageMolecola.OPERATIONTITLE).getLocator();
		

		List<WebElement> L=page.getDriver().findElements(By.xpath(list));

		//for(WebElement e : L)
		for(int i=0;i<L.size();i++)
		{
			String tit=null;

			tit=L.get(i).findElement(By.xpath(title)).getText().trim().toLowerCase();
			
			if(tit.equals(bean.getSubCategoryTransaction().toLowerCase()))
			{
				L.get(i).click();
				break;
			}
		}

		ExclutedOperationsDetailsPage p=(ExclutedOperationsDetailsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ExclutedOperationsDetailsPage", page.getDriver().getClass(), page.getLanguage());;

		p.setBean(bean);
		p.checkPage();

		return p;
	}

	public YourExpencesDetailsPage clickBack() 
	{
		this.leftmenu=page.getParticle(Locators.ExclutedOperationsPageMolecola.LEFTMENU).getElement();
		

		this.leftmenu.click();
		
		YourExpencesDetailsPage p=(YourExpencesDetailsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("YourExpencesDetailsPage", page.getDriver().getClass(), page.getLanguage());;

		p.checkPage();
		return p;
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}


	@Override
	public void setBean(TransactionDetailBean bean) {
		this.bean=bean;

	}
}
