package it.poste.bancoposta.pages.managers;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import automation.core.ui.Entry;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import it.poste.bancoposta.pages.ExclutedOperationsDetailsPage;
import it.poste.bancoposta.pages.Locators;

@PageManager(page="ExclutedOperationsPage")
@IOS
public class ExclutedOperationsPageManIOS extends ExclutedOperationsPageMan {

	
	public ExclutedOperationsDetailsPage gotoSubCategory() 
	{


		page.getParticle(Locators.ExclutedOperationsPageMolecola.OPERATIONTITLE).getElement(new Entry("title", bean.getSubCategoryTransaction())).click();
		
		ExclutedOperationsDetailsPage p=(ExclutedOperationsDetailsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ExclutedOperationsDetailsPage", page.getDriver().getClass(), page.getLanguage());;

		p.setBean(bean);
		p.checkPage();

		return p;
	}
}
