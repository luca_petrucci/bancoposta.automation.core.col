package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.GirofondoCreationBean;
import io.appium.java_client.AppiumDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.GirofondoFormPage;
import it.poste.bancoposta.pages.GirofondoSubmitPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OKOperationPage;
import it.poste.bancoposta.pages.PayWithPage;

import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="GirofondoFormPage")
@Android
public class GirofondoFormPageMan extends LoadableComponent<GirofondoFormPageMan> implements GirofondoFormPage
{
	private static final String IMPORTO = "importo";
	
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancellButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement paymentTypeInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement paymentEditButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement popup;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement toCardInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement confirm;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cardList;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cardInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cardSelectedFlag;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement exitPaymentNOButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement exitPaymentYESButton;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		WaitManager.get().waitLongTime();
		page.get();
	}

	public OKOperationPage eseguiGirofondo(GirofondoCreationBean b) 
	{
		this.paymentTypeInfo=page.getParticle(Locators.GirofondoFormPageMolecola.PAYMENTTYPEINFO).getElement();
		this.toCardInput=page.getParticle(Locators.GirofondoFormPageMolecola.TOCARDINPUT).getElement();
		this.amount=page.getParticle(Locators.GirofondoFormPageMolecola.AMOUNT).getElement();
		

		//controllo che paga con sia uguale al desiderato
		String payWith=this.paymentTypeInfo.getText().trim();
		String expPayWith=b.getPayWith().replace(";", " ");

		Assert.assertTrue("controllo paga con; attuale:"+payWith+", atteso:"+expPayWith,payWith.toLowerCase().equals(expPayWith));

		compilaForm(b);
		
		try 
		{
			this.popup=page.getParticle(Locators.GirofondoFormPageMolecola.POP_UP).visibilityOfElement(10L);
			this.popup.click();
		} catch (Exception err) {}
		
		//controllo riepilogo e confermo pagamento
		GirofondoSubmitPage submit=(GirofondoSubmitPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("GirofondoSubmitPage", page.getDriver().getClass(), page.getLanguage());
		
		submit.setBean(b);

		submit.checkPage();

	

		return submit.submitGirofondo();
	}

	public void compilaForm(GirofondoCreationBean b) 
	{
		this.paymentTypeInfo=page.getParticle(Locators.GirofondoFormPageMolecola.PAYMENTTYPEINFO).getElement();
		this.toCardInput=page.getParticle(Locators.GirofondoFormPageMolecola.TOCARDINPUT).getElement();
		this.amount=page.getParticle(Locators.GirofondoFormPageMolecola.AMOUNT).getElement();
		

		//seleziono trasferisci tu
		this.toCardInput.click();

		String cardInfo="";
		/*
		 * TODO:
		 * 19/07/2019
		 * bug JIRA PBPAPP-2530 in analisi,
		 * il nuovo comportamento prevede che qualora sia presente un solo elemento nella lista
		 * esso non compaia, in pi� esso deve essere prepopolato all'accesso
		 */
		try
		{
			this.cardList=page.getParticle(Locators.GirofondoFormPageMolecola.CARDLIST).getElement();
			cardInfo=page.getParticle(Locators.GirofondoFormPageMolecola.CARDINFO).getLocator();
			List<WebElement> list=this.cardList.findElements(By.xpath(cardInfo));

			for(WebElement e : list)
			{
				String name=e.getText().trim().toLowerCase();

				if(name.equals(b.getTransferTo().replace(";", " ").toLowerCase()))
				{
					e.click();
					break;
				}
			}
		}
		catch(Exception err)
		{

		}

		this.amount=page.getParticle(Locators.GirofondoFormPageMolecola.AMOUNT).getElement();
		this.confirm=page.getParticle(Locators.GirofondoFormPageMolecola.CONFIRM).getElement();
		
		//inserisco l'importo
		this.amount.clear();
		try {Thread.sleep(500);}catch(Exception err) {};
		this.amount.sendKeys(b.getAmount());
		try {Thread.sleep(500);}catch(Exception err) {};
		this.amount.clear();
		try {Thread.sleep(500);}catch(Exception err) {};
		this.amount.sendKeys(b.getAmount());

		try {((AppiumDriver<?>) page.getDriver()).hideKeyboard();}catch(Exception err) {};

		//clicco conferma
		this.confirm.click();
	}

	public void isEditable(GirofondoCreationBean bean) 
	{
		this.paymentTypeInfo=page.getParticle(Locators.GirofondoFormPageMolecola.PAYMENTTYPEINFO).getElement();
		this.toCardInput=page.getParticle(Locators.GirofondoFormPageMolecola.TOCARDINPUT).getElement();
		this.amount=page.getParticle(Locators.GirofondoFormPageMolecola.AMOUNT).getElement();
		this.paymentEditButton=page.getParticle(Locators.GirofondoFormPageMolecola.PAYMENTEDITBUTTON).getElement();
		

		//controllo che il pulsante modifica paga con funzioni
//		this.paymentEditButton.click();
//
//		PayWithPage p=(PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
//				.getPageManager("PayWithPage", page.getDriver().getClass(), page.getLanguage());
//		
//		p.checkPage();
//
//		p.clickBack();

		//memorizzo temporanemanete il valore precedente
		double amount=Double.parseDouble(bean.getAmount());

		bean.setAmount(""+(amount*2));

		compilaForm(bean);

		//controllo riepilogo e clicco back
		GirofondoSubmitPage submit=(GirofondoSubmitPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("GirofondoSubmitPage", page.getDriver().getClass(), page.getLanguage());
		submit.setBean(bean);
		submit.checkPage();

		submit.clickBack();

		//ricontrollo la form
		this.get();
		WaitManager.get().waitLongTime();

		this.paymentTypeInfo=page.getParticle(Locators.GirofondoFormPageMolecola.PAYMENTTYPEINFO).getElement();
		this.toCardInput=page.getParticle(Locators.GirofondoFormPageMolecola.TOCARDINPUT).getElement();
		this.amount=page.getParticle(Locators.GirofondoFormPageMolecola.AMOUNT).getElement();
		this.paymentEditButton=page.getParticle(Locators.GirofondoFormPageMolecola.PAYMENTEDITBUTTON).getElement();
		this.cancellButton=page.getParticle(Locators.GirofondoFormPageMolecola.CANCELLBUTTON).getElement();
		

		//controllo form con i valori settati correttamente
		checkValues(bean);

		//ripristino il vecchio valore
		bean.setAmount(""+amount);

		//clicco back
		this.cancellButton.click();

		page.getParticle(Locators.GirofondoFormPageMolecola.EXITPAYMENTSYESBUTTON).visibilityOfElement(10L);
		this.exitPaymentYESButton=page.getParticle(Locators.GirofondoFormPageMolecola.EXITPAYMENTSYESBUTTON).getElement();
		
		this.exitPaymentYESButton.click();
	}

	public void checkValues(GirofondoCreationBean b) 
	{
		//controllo che paga con sia uguale al desiderato
		String payWith=this.paymentTypeInfo.getText().trim();
		String expPayWith=b.getPayWith().replace(";", " ");


		Assert.assertTrue("controllo paga con; attuale:"+payWith+", atteso:"+expPayWith,payWith.toLowerCase().equals(expPayWith));

		try {
			double amount=Utility.parseCurrency(this.amount.getText().trim(), Locale.ITALY);

			Assert.assertTrue("controllo importo; attuale:"+amount+", atteso:"+b.getAmount(),amount == Double.parseDouble(b.getAmount()));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String trans=this.toCardInput.getText().trim().toLowerCase();
		String toCheck=b.getTransferTo().replace(";", " ").toLowerCase();

		Assert.assertTrue("controllo trasferici su; attuale:"+trans+", atteso:"+toCheck,trans.equals(toCheck));

	}

	public OKOperationPage eseguiGirofondo(GirofondoCreationBean bean, String campo, String value) 
	{
		String oldValue=null;

		this.amount=page.getParticle(Locators.GirofondoFormPageMolecola.AMOUNT).getElement(new Entry("importo",bean.getAmount().replace(".", ",")));
		this.amount.clear();

		//memorizzo temporaneamente il vecchio valore
		switch(campo)
		{
		case IMPORTO:
			oldValue=bean.getAmount();
			bean.setAmount(value);
			break;
		}

		//eseguo il girofondo
		OKOperationPage ok=this.eseguiGirofondo(bean);

		return ok;
	}

	public void clickTrasferisciSu() 
	{
		this.toCardInput=page.getParticle(Locators.GirofondoFormPageMolecola.TOCARDINPUT).getElement();
		toCardInput.click();
	}

	public void checkTrasferisciSu(String transferTo) 
	{
		List<WebElement> el=page.getParticle(Locators.GirofondoFormPageMolecola.CARDINFO).getListOfElements();

		boolean find=false;

		if(el.size() > 0)
		{
			for(WebElement e : el)
			{
				String card=e.getText().trim();
				if(card.equals(transferTo))
				{
					find=true;
					break;
				}
			}
		}
		else
		{
			String txt=page.getParticle(Locators.GirofondoFormPageMolecola.TOCARDINPUT).getElement().getText().trim().toLowerCase();
			find=txt.equals(transferTo.toLowerCase());
		}

		org.springframework.util.Assert.isTrue(find, "card "+transferTo+" non trovata nella lista");
	}

	public void clickAnnulla() 
	{
		page.getParticle(Locators.GirofondoFormPageMolecola.CANCELLBUTTON).visibilityOfElement(10L).click();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}
}
