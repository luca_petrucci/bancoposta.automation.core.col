package it.poste.bancoposta.pages.managers;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import bean.datatable.GirofondoCreationBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;

@PageManager(page="GirofondoFormPage")
@IOS
public class GirofondoFormPageManIOS extends GirofondoFormPageMan {
	
	private By pickers = MobileBy.className("XCUIElementTypePickerWheel"); 
	
	public void checkTrasferisciSu(String transferTo) 
	{
		
		List<WebElement> pickerEls = UIUtils.ui().waitForCondition(page.getDriver(),ExpectedConditions.presenceOfAllElementsLocatedBy(pickers));
        System.out.println(pickerEls.size());
        pickerEls.get(0).sendKeys(transferTo);
	
			String txt=page.getParticle(Locators.GirofondoFormPageMolecola.TOCARDINPUT).getElement().getText().trim().toLowerCase();
			Assert.assertTrue("card "+transferTo+" non trovato",txt.equals(transferTo.toLowerCase()));
	}
	
	public void compilaForm(GirofondoCreationBean b) 
	{
		this.paymentTypeInfo=page.getParticle(Locators.GirofondoFormPageMolecola.PAYMENTTYPEINFO).getElement();
		this.toCardInput=page.getParticle(Locators.GirofondoFormPageMolecola.TOCARDINPUT).getElement();
		this.amount=page.getParticle(Locators.GirofondoFormPageMolecola.AMOUNT).getElement();

		//seleziono trasferisci tu
		this.toCardInput.click();
		
		List<WebElement> pickerEls = UIUtils.ui().waitForCondition(page.getDriver(),ExpectedConditions.presenceOfAllElementsLocatedBy(pickers));
        System.out.println(pickerEls.size());
        pickerEls.get(0).sendKeys(b.getTransferTo());

		//inserisco l'importo
		this.amount.sendKeys(b.getAmount().replace(".", ","));
		
//		try {((AppiumDriver<?>) page.getDriver()).hideKeyboard();}catch(Exception err) {};
		this.paymentTypeInfo.click();
		//clicco conferma
		page.getParticle(Locators.GirofondoFormPageMolecola.CONFIRM).getElement().click();
		WaitManager.get().waitShortTime();
	}

}
