package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.GirofondoCreationBean;
import bean.datatable.QuickOperationCreationBean;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.GirofondoFormPage;
import it.poste.bancoposta.pages.GirofondoSubmitPage;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OKOperationPage;
import it.poste.bancoposta.pages.PayWithPage;
import it.poste.bancoposta.pages.QuickOperationEditable;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="GirofondoSubmitPage")
@Android
@IOS
public class GirofondoSubmitPageMan extends LoadableComponent<GirofondoSubmitPageMan> implements QuickOperationEditable,GirofondoSubmitPage
{
	private static final String TUTTO_CORRETTO = "\u00E8 tutto corretto?";
	private static final String VERIFICA_DATI = "verifica prima di procedere";
	private static final String TRASFERISCI = "trasferisci \u20AC";
	
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancellButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement title;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement paymentInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement toCardInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement confirm;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement popup;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement modify;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement commissione;
	private GirofondoCreationBean bean;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();

		//controllo headers
		this.title=page.getParticle(Locators.GirofondoSubmitPageMolecola.TITLE).getElement();
		this.description=page.getParticle(Locators.GirofondoSubmitPageMolecola.DESCRIPTION).getElement();

		String actual=this.title.getText().trim().toLowerCase();

		Assert.assertTrue("controllo header \"\u00E8 tutto corretto?\"",actual.equals(TUTTO_CORRETTO.toLowerCase()));

		actual=this.description.getText().trim().toLowerCase();
		System.out.println(actual);
		System.out.println(VERIFICA_DATI);
		Assert.assertTrue("controllo header \"verifica prima di procedere.\"",actual.contains(VERIFICA_DATI.toLowerCase().trim()));

		//controllo paga con
		this.paymentInfo=page.getParticle(Locators.GirofondoSubmitPageMolecola.PAYMENTINFO).getElement();
		actual=this.paymentInfo.getText().trim().toLowerCase();
		String expected=bean.getPayWith().replace(";", " ");

		Assert.assertTrue("controllo paga con; attuale:"+actual+", atteso:"+expected,actual.equals(expected));

		//controllo trasferisci su
		this.toCardInfo=page.getParticle(Locators.GirofondoSubmitPageMolecola.TOCARDINFO).getElement();
		actual=this.toCardInfo.getText().trim().toLowerCase();
		expected=bean.getTransferTo().replace(";", " ").toLowerCase();

		Assert.assertTrue("controllo trasferisci su; attuale:"+actual+", atteso:"+expected,actual.equals(expected));

		//controllo importo
		this.amount=page.getParticle(Locators.GirofondoSubmitPageMolecola.AMOUNT).getElement();
		actual=this.amount.getText().replace("\u20AC", "").trim().replace(".", "").replace(",", ".");
		expected=bean.getAmount();
		double act=Double.parseDouble(actual);
		double exp=Double.parseDouble(expected);

		Assert.assertTrue("controllo importo; attuale:"+act+", atteso:"+exp,act == exp);

		//controllo stringa contenuta nel pulsante paga
		this.commissione=page.getParticle(Locators.GirofondoSubmitPageMolecola.COMMISSIONE).getElement();
		actual=this.commissione.getText().replace("\u20AC", "").trim().replace(".", "").replace(",", ".");

		double commissione=Double.parseDouble(actual);

		double total=act + commissione;
		this.confirm=page.getParticle(Locators.GirofondoSubmitPageMolecola.CONFIRM).getElement();

		actual=this.confirm.getText().trim().toLowerCase();

		boolean paga=actual.contains(TRASFERISCI);

		Assert.assertTrue("controllo presenza stringa \"trasferisci �\"",paga);

		actual=actual.replace(TRASFERISCI, "").trim().replace(".", "").replace(",", ".");
		System.out.println(TRASFERISCI);
		System.out.println(actual);
		act=Double.parseDouble(actual);

		Assert.assertTrue("controllo valore totale mostrato nel pulsante paga; attuale:"+act+", atteso:"+total,act == total);


	}

	public OKOperationPage submitGirofondo() 
	{
		this.confirm=page.getParticle(Locators.GirofondoSubmitPageMolecola.CONFIRM).getElement();
		

		this.confirm.click();


		//inserisco posteid
		InsertPosteIDPage insert=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
		insert.checkPage();

		OKOperationPage ok=insert.insertPosteID(bean.getPosteid());
		ok.checkPage();
		return ok;
	}

	@Override
	public void isEditable(QuickOperationCreationBean b) 
	{
		//clicco su modifica
		WaitManager.get().waitShortTime();
		this.modify=page.getParticle(Locators.GirofondoSubmitPageMolecola.MODIFY).getElement();
		

		this.modify.click();
		
		WaitManager.get().waitShortTime();
		
		PayWithPage conto = (PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PayWithPage", page.getDriver().getClass(), page.getLanguage());
		conto.checkPage();
		conto.clickOnConto(b.getPayWith());

		GirofondoFormPage p=(GirofondoFormPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("GirofondoFormPage", page.getDriver().getClass(), page.getLanguage());
		
		p.checkPage();
		p.isEditable(bean);
	}

	public void clickBack() 
	{
		//clicco su modifica
		this.cancellButton=page.getParticle(Locators.GirofondoSubmitPageMolecola.CANCELLBUTTON).getElement();
		

		this.cancellButton.click();
	}

	@Override
	public OKOperationPage submit(QuickOperationCreationBean b, String campo, String value) 
	{
		if(campo != null)
		{
			//clicco su modifica
			this.modify=page.getParticle(Locators.GirofondoSubmitPageMolecola.MODIFY).getElement();
			
			this.modify.click();

			GirofondoFormPage p=(GirofondoFormPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("GirofondoFormPage", page.getDriver().getClass(), page.getLanguage());
			
			p.checkPage();

			return p.eseguiGirofondo(bean,campo,value);
		}
		else
		{
			//clicco su paga
			this.confirm=page.getParticle(Locators.GirofondoSubmitPageMolecola.CONFIRM).getElement();
			

			this.confirm.click();

			InsertPosteIDPage p=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
			
			p.checkPage();

			
			OKOperationPage ok=p.insertPosteID(bean.getPosteid());
			ok.checkPage();
			return ok;
		}
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

	@Override
	public void setBean(GirofondoCreationBean b) {
		this.bean=b;
		
	}

	@Override
	public void setBean(QuickOperationCreationBean quickOperationCreationBean) {
		
	}

}
