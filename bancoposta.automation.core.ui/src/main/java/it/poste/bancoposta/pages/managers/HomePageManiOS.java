package it.poste.bancoposta.pages.managers;

import org.junit.Assert;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import it.poste.bancoposta.pages.Locators;

@PageManager(page="HomePage")
@IOS
public class HomePageManiOS extends HomePageMan 
{
	protected void load() 
	{
		page.getQuick();

		//controllo che il welcome header contenga la Stringa "Ciao userHeader!"
		String welcHeader=page.getParticle(Locators.HomePageMolecola.WELCOMEHEADER).getElement().getText();
		String checkWelcHeader="Ciao "+this.userHeader+"!";

		Assert.assertTrue("controllo header home page",welcHeader.equals(checkWelcHeader));
		
//		byte[] b=((TakesScreenshot)page.getDriver()).getScreenshotAs(OutputType.BYTES);
//		PageMolecolaImgCreator.makeImg(b, page, 
//				Locators.HomePageMolecola.WELCOMEHEADER,
//				Locators.HomePageMolecola.BANCOPOSTAHEADER,
//				Locators.HomePageMolecola.LEFTMENU,
//				Locators.HomePageMolecola.YOURPRODUCTTAB,
//				Locators.HomePageMolecola.YOURBILLSTABS,
//				Locators.HomePageMolecola.QUICKPAIMENTSTAB,
//				Locators.HomePageMolecola.QUICKPAIMENTSSHOWALLLINK,
//				Locators.HomePageMolecola.QUICKPAIMENTSLIST,
//				Locators.HomePageMolecola.BILLANDCARDSSHOWACCOUNTSLINK,
//				Locators.HomePageMolecola.BILLANDCARDSLIST,
//				Locators.HomePageMolecola.FAQLINK);
	}
	public void clickOnScopriDaBiscotto() 
	{
		WaitManager.get().waitShortTime();
		page.getParticle(Locators.BiscottoHomeMolecola.BISCOTTO_IMAGE).visibilityOfElement(10L).click();
	}
}
