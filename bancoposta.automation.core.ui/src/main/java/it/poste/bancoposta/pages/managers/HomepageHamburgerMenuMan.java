package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.xml.sax.Locator;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import it.poste.bancoposta.pages.BachecaMessaggiPage;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.BuoniLibrettiPage;
import it.poste.bancoposta.pages.ContiCartePage;
import it.poste.bancoposta.pages.HomePage;
import it.poste.bancoposta.pages.HomepageHamburgerMenu;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SalvadanaioPage;
import it.poste.bancoposta.pages.ScontiPostePage;
import it.poste.bancoposta.pages.SearchPostalOfficePage;
import it.poste.bancoposta.pages.SettingsPage;
import it.poste.bancoposta.utils.PageMolecolaImgCreator;
import test.automation.core.UIUtils;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="HomepageHamburgerMenu")
@Android
public class HomepageHamburgerMenuMan extends LoadableComponent<HomepageHamburgerMenuMan> implements HomepageHamburgerMenu
{
	protected static final String HOME_TEXT = "Home";
	protected static final String ACCOUNT_TEXT = "Conti e carte";
	protected static final String VOUCHER_TEXT = "Buoni e Libretti";
	protected static final String NOTICE_TEXT = "Bacheca";
	protected static final String SETTING_TEXT = "Impostazioni";
	protected static final String BP_TEXT = "Sconti BancoPosta";
	protected static final String EXTRA_TEXT = "Extra Sconti";
	protected static final String POSTOFF_TEXT = "Cerca Ufficio Postale";
	protected static final String INVITE_TEXT = "Invita i tuoi amici";
	protected static final String LOGOUT_TEXT = "Logout";

	UiPage page;

	/**
	 * @android 
	 * @ios campo dinamico sostituire $(quickOpName) con l'operazione veloce da ricercare
	 * @web 
	 */
	protected WebElement welcomeHeader;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement home;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement accountAndCards;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement voucherAndBooklet;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement noticeBoard;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement settings;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement bpDiscount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement extraDiscount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement postOfficeSearch;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement inviteYourFriends;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement logout;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement salesButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement searchPostalOfficeButton;
	protected WebElement buoniLibrettiButton;
	protected HomepageHamburgerMenu hamburgerMenu;
	protected WebElement text;
	protected WebElement conti;
	protected WebElement buoni;
	protected WebElement salvadanaio;
	protected WebElement account;
	protected WebElement voucher;
	protected WebElement moneybox;
	protected WebElement bacheca;
	protected WebElement discount;
	protected WebElement otherApps;
	protected WebElement bpoffice;
	protected WebElement footer;
	protected WebElement ppoffice;
	protected WebElement closemenu;
	protected WebElement consultant;
	protected WebElement openAcoount;
	private WebElement noPopupButton;
	private MobileDriver driver;

	@Override
	protected void isLoaded() throws Error {

	}

	@Override
	protected void load() 
	{
		WaitManager.get().waitMediumTime();

		/*
		driver.findElement(MobileBy.AndroidUIAutomator(
		        "new UiScrollable(new UiSelector().className(\"android.widget.ScrollView\")).getChildByText("
		        + "new UiSelector().resourceId(\"posteitaliane.posteapp.appbpol:id/tv_text\"), \""+HOME_TEXT+"\")"));
		 */

//		WaitManager.get().waitMediumTime();
//		
//		byte[] b=((TakesScreenshot)page.getDriver()).getScreenshotAs(OutputType.BYTES);
//		PageMolecolaImgCreator.makeImg(b, page,
//				Locators.HomepageHamburgerMenuMolecola.WELCOMEHEADER,
//				Locators.HomepageHamburgerMenuMolecola.HOME,
//				Locators.HomepageHamburgerMenuMolecola.ACCOUNTANDCARDS,
//				Locators.HomepageHamburgerMenuMolecola.BUONILIBRETTIBUTTON,
//				Locators.HomepageHamburgerMenuMolecola.MONEYBOX,
//				Locators.HomepageHamburgerMenuMolecola.VOUCHERANDBOOKLET,
//				Locators.HomepageHamburgerMenuMolecola.NOTICEBOARD,
//				Locators.HomepageHamburgerMenuMolecola.SETTINGS,
//				Locators.HomepageHamburgerMenuMolecola.BPDISCOUNT,
//				Locators.HomepageHamburgerMenuMolecola.POSTOFFICESEARCH
//				);
// fa schifo al cazzo
//		page.getDriver().findElement(MobileBy.AndroidUIAutomator(
//				"new UiScrollable(new UiSelector().resourceId(\"posteitaliane.posteapp.appbpol:id/fragment_drawer\").childSelector(new UiSelector().className(\"android.widget.ScrollView\")))"
//						+ ".getChildByText("
//						+ "new UiSelector().resourceId(\"posteitaliane.posteapp.appbpol:id/tv_text\"), "
//						+ "\""+HOME_TEXT+"\")"));

		WaitManager.get().waitMediumTime();
		
		


		//UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.WELCOMEHEADER.getAndroidLocator())));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.HOME.getAndroidLocator())));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.ACCOUNTANDCARDS.getAndroidLocator())));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.VOUCHERANDBOOKLET.getAndroidLocator())));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.NOTICEBOARD.getAndroidLocator())));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.SETTINGS.getAndroidLocator())));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.BPDISCOUNT.getAndroidLocator())));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.EXTRADISCOUNT.getAndroidLocator())));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.POSTOFFICESEARCH.getAndroidLocator())));
		//			
		//			this.home=driver.findElement(By.xpath(Locators.HomepageHamburgerMenuMolecola.HOME.getAndroidLocator()));
		//			this.accountAndCards=driver.findElement(By.xpath(Locators.HomepageHamburgerMenuMolecola.ACCOUNTANDCARDS.getAndroidLocator()));
		//			this.voucherAndBooklet=driver.findElement(By.xpath(Locators.HomepageHamburgerMenuMolecola.VOUCHERANDBOOKLET.getAndroidLocator()));
		//			this.noticeBoard=driver.findElement(By.xpath(Locators.HomepageHamburgerMenuMolecola.NOTICEBOARD.getAndroidLocator()));
		//			this.settings=driver.findElement(By.xpath(Locators.HomepageHamburgerMenuMolecola.SETTINGS.getAndroidLocator()));
		//			this.bpDiscount=driver.findElement(By.xpath(Locators.HomepageHamburgerMenuMolecola.BPDISCOUNT.getAndroidLocator()));
		//			this.extraDiscount=driver.findElement(By.xpath(Locators.HomepageHamburgerMenuMolecola.EXTRADISCOUNT.getAndroidLocator()));
		//			this.postOfficeSearch=driver.findElement(By.xpath(Locators.HomepageHamburgerMenuMolecola.POSTOFFICESEARCH.getAndroidLocator()));
		//			
		//			Assert.assertTrue("controllo tasto home",this.home.findElement(By.xpath("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_text']")).getText().trim().equals(HOME_TEXT));
		//			Assert.assertTrue("controllo tasto conti e carte",this.accountAndCards.findElement(By.xpath("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_text']")).getText().trim().equals(ACCOUNT_TEXT));
		//			Assert.assertTrue("controllo tasto buoni e libretti",this.voucherAndBooklet.findElement(By.xpath("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_text']")).getText().trim().equals(VOUCHER_TEXT));
		//			Assert.assertTrue("controllo tasto bacheca",this.noticeBoard.findElement(By.xpath("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_text']")).getText().trim().equals(NOTICE_TEXT));
		//			Assert.assertTrue("controllo tasto impostazioni",this.settings.findElement(By.xpath("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_text']")).getText().trim().equals(SETTING_TEXT));
		//			Assert.assertTrue("controllo tasto sconti bancoposta",this.bpDiscount.findElement(By.xpath("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_text']")).getText().trim().equals(BP_TEXT));
		//			Assert.assertTrue("controllo tasto extra sconti",this.extraDiscount.findElement(By.xpath("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_text']")).getText().trim().equals(EXTRA_TEXT));
		//			Assert.assertTrue("controllo tasto cerca ufficio",this.postOfficeSearch.findElement(By.xpath("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_text']")).getText().trim().equals(POSTOFF_TEXT));
		//			
		//			
		//			driver.findElement(MobileBy.AndroidUIAutomator(
		//			        "new UiScrollable(new UiSelector().resourceId(\"posteitaliane.posteapp.appbpol:id/fragment_drawer\").childSelector(new UiSelector().className(\"android.widget.ScrollView\")))"
		//			        + ".getChildByText("
		//			        + "new UiSelector().resourceId(\"posteitaliane.posteapp.appbpol:id/tv_text\"), "
		//			        + "\""+LOGOUT_TEXT+"\")"));
		//			
		//			try {
		//				Thread.sleep(1000);
		//			} catch (InterruptedException e) {
		//				// TODO Auto-generated catch block
		//				e.printStackTrace();
		//			}
		//
		//			
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.INVITEYOURFRIENDS.getAndroidLocator())));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.LOGOUT.getAndroidLocator())));
		//
		//			this.inviteYourFriends=driver.findElement(By.xpath(Locators.HomepageHamburgerMenuMolecola.INVITEYOURFRIENDS.getAndroidLocator()));
		//			this.logout=driver.findElement(By.xpath(Locators.HomepageHamburgerMenuMolecola.LOGOUT.getAndroidLocator()));
		//			
		//			Assert.assertTrue("controllo tasto invita i tuoi amici",this.inviteYourFriends.findElement(By.xpath("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_text']")).getText().trim().equals(INVITE_TEXT));
		//			Assert.assertTrue("controllo tasto logout",this.logout.findElement(By.xpath("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_text']")).getText().trim().equals(LOGOUT_TEXT));
		//			
		//			
		//			driver.findElement(MobileBy.AndroidUIAutomator(
		//			        "new UiScrollable(new UiSelector().resourceId(\"posteitaliane.posteapp.appbpol:id/fragment_drawer\").childSelector(new UiSelector().className(\"android.widget.ScrollView\")))"
		//			        + ".getChildByText("
		//			        + "new UiSelector().resourceId(\"posteitaliane.posteapp.appbpol:id/tv_text\"), "
		//			        + "\""+HOME_TEXT+"\")"));
		//			
		//			
		//			break;
		//			case 1://ios driver
		//				
		//			Utility.swipe((MobileDriver) driver, Utility.DIRECTION.UP, 0.3f, 0.3f, 0.3f, 0.9f, 200);
		//				
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.HOME.getLocator(driver))));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.ACCOUNTANDCARDS.getLocator(driver))));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.VOUCHERANDBOOKLET.getLocator(driver))));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.NOTICEBOARD.getLocator(driver))));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.SETTINGS.getLocator(driver))));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.BPDISCOUNT.getLocator(driver))));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.EXTRADISCOUNT.getLocator(driver))));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.POSTOFFICESEARCH.getLocator(driver))));
		//			
		//			Utility.swipe((MobileDriver) driver, Utility.DIRECTION.DOWN, 0.3f, 0.9f, 0.3f, 0.3f, 200);
		//			
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.INVITEYOURFRIENDS.getLocator(driver))));
		//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.HomepageHamburgerMenuMolecola.LOGOUT.getLocator(driver))));
		//
		//			Utility.swipe((MobileDriver) driver, Utility.DIRECTION.UP, 0.3f, 0.3f, 0.3f, 0.9f, 200);



	}

	public SettingsPage gotoSettings() 
	{
		this.settings=page.getParticle(Locators.HomepageHamburgerMenuMolecola.SETTINGS).getElement();


		this.settings.click();

		SettingsPage s=(SettingsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SettingsPage", page.getDriver().getClass(), page.getLanguage());

		s.checkPage();

		return s;
	}

	public HomePage gotoHomePage(String userHeader) 
	{
		WaitManager.get().waitShortTime();

		try {


			page.getDriver().findElement(MobileBy.AndroidUIAutomator(
					"new UiScrollable(new UiSelector().resourceId(\"posteitaliane.posteapp.appbpol:id/fragment_drawer\").childSelector(new UiSelector().className(\"android.widget.ScrollView\")))"
							+ ".getChildByText("
							+ "new UiSelector().resourceId(\"posteitaliane.posteapp.appbpol:id/tv_text\"), "
							+ "\""+HOME_TEXT+"\")"));
		} catch (Exception e) {

		}

		WaitManager.get().waitShortTime();

		this.home=page.getParticle(Locators.HomepageHamburgerMenuMolecola.HOME).getElement();
		this.home.click();

		HomePage h=(HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", page.getDriver().getClass(), page.getLanguage());

		h.setUserHeader(userHeader);

		h.checkPage();

		return h;
	}

	public ContiCartePage gotoContiCarte() 
	{
		this.accountAndCards=page.getParticle(Locators.HomepageHamburgerMenuMolecola.ACCOUNTANDCARDS).getElement();


		this.accountAndCards.click();

		ContiCartePage h=(ContiCartePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ContiCartePage", page.getDriver().getClass(), page.getLanguage());

		h.checkPage();

		return h;
	}

	public SalvadanaioPage goToSalvadanaio() 
	{
		this.salvadanaio=page.getParticle(Locators.HomepageHamburgerMenuMolecola.MONEYBOX).getElement();


		this.salvadanaio.click();

		SalvadanaioPage h=(SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioPage", page.getDriver().getClass(), page.getLanguage());

		h.checkPage();

		return h;
	}

	public BachecaMessaggiPage gotoBachecaSectionMessaggi() 
	{
		this.noticeBoard=page.getParticle(Locators.HomepageHamburgerMenuMolecola.NOTICEBOARD).getElement();


		this.noticeBoard.click();

		BachecaMessaggiPage p=(BachecaMessaggiPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BachecaMessaggiPage", page.getDriver().getClass(), page.getLanguage());

		WaitManager.get().waitHighTime();	
		WaitManager.get().waitHighTime();
		WaitManager.get().waitMediumTime();

		p.clickMessaggi();

		p.checkPage();

		return p;
	}

	public ScontiPostePage goToSales() 
	{
		
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 1000);
		this.salesButton=page.getParticle(Locators.HomepageHamburgerMenuMolecola.SALESPOSTE).getElement();

		this.salesButton.click();
		//TODO:TAA-301
		try {
			page.getParticle(Locators.SalesPostePageMolecola.NOPOPUPBUTTON).visibilityOfElement(10L)
			.click();
		} catch (Exception e) {
			
		}
		try {
			page.getParticle(Locators.SalesPostePageMolecola.NOGRAZIEPOPUPBUTTON).visibilityOfElement(10L)
			.click();
		} catch (Exception e) {
			
		}
		//			try{Thread.sleep(2000);}catch(Exception err){}
		//			try {
		//				switch(this.driverType)
		//				{
		//				case 0://android
		////					this.noPopupButton=driver.findElement(By.xpath(Locators.SalesPostePageMolecola.NOPOPUPBUTTON.getAndroidLocator()));
		//					WebElement noPopupButton = driver.findElement(By.xpath(Locators.SalesPostePageMolecola.NOPOPUPBUTTON.getLocator(driver)));
		//					noPopupButton.click(); 
		//					break;
		//				case 1://ios
		////					this.noPopupButton=driver.findElement(By.xpath(Locators.SalesPostePageMolecola.NOPOPUPBUTTON.getLocator(driver)));
		//					noPopupButton = driver.findElement(By.xpath(Locators.SalesPostePageMolecola.NOPOPUPBUTTON.getLocator(driver)));
		//					noPopupButton.click(); 
		//					break;
		//				}
		//			} catch (Exception e) {
		//				
		//			}
		ScontiPostePage p=(ScontiPostePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ScontiPostePage", page.getDriver().getClass(), page.getLanguage());
		p.checkPage();
		return p;

	}

	public SearchPostalOfficePage goToSearchPostalOfficePage() {
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 1000);
		this.searchPostalOfficeButton=page.getParticle(Locators.HomepageHamburgerMenuMolecola.POSTOFFICESEARCH).getElement();

		this.searchPostalOfficeButton.click();

		SearchPostalOfficePage p=(SearchPostalOfficePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SearchPostalOfficePage", page.getDriver().getClass(), page.getLanguage());

		//TODO:TAA-302
		try {
			p.clickOkPoup();
		} catch (Exception e) {
			// TODO: handle exception
		}
		//			try{Thread.sleep(1500);}catch(Exception err){}
		//			
		//			try {
		//				switch(this.driverType)
		//				{
		//				case 0://android
		//					WebElement okPopupButton = driver.findElement(By.xpath(Locators.SearchPostalOfficePageLocator.POPUPOKBUTTON.getLocator(driver)));
		//					okPopupButton.click(); 
		//					break;
		//				case 1://ios
		//					okPopupButton = driver.findElement(By.xpath(Locators.SearchPostalOfficePageLocator.POPUPOKBUTTON.getLocator(driver)));
		//					okPopupButton.click(); 
		//					break;
		//				}
		//			} catch (Exception e) {
		//				
		//			}
		WaitManager.get().waitMediumTime();
		p.checkPage();
		return p;

	}



	public BuoniLibrettiPage goToBuoniLibretti() {

		this.buoniLibrettiButton=page.getParticle(Locators.HomepageHamburgerMenuMolecola.BUONILIBRETTIBUTTON).getElement();

		this.buoniLibrettiButton.click();
		WaitManager.get().waitMediumTime();
		BuoniLibrettiPage p=(BuoniLibrettiPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BuoniLibrettiPage", page.getDriver().getClass(), page.getLanguage());
		p.checkPage();
		return p;

	}



	public void verifyMenu(List<CredentialAccessBean> bean) throws Throwable {
		CredentialAccessBean b = bean.get(0);

		String userHeader = bean.get(0).getUserHeader();
		HomePage homePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", page.getDriver().getClass(), page.getLanguage());

		homePage.setUserHeader(userHeader);

		homePage.checkPage();

		this.hamburgerMenu=homePage.openHamburgerMenu();

		this.text=page.getParticle(Locators.HomepageHamburgerMenuCheck.USER).getElement();
		String text2 = text.getText().trim();
		System.out.println(text2);
		String o = b.getOwner().trim();
		System.out.println(o);

		org.springframework.util.Assert.isTrue(text2.equals(o),"hamburger menu user");

		System.out.println(text2+"ok"+o);

		this.home=page.getParticle(Locators.HomepageHamburgerMenuCheck.HOME).getElement();

		String h = home.getText().trim();
		System.out.println(h);

		org.springframework.util.Assert.isTrue(h.equals("Home"),"hamburger menu home");

		System.out.println(h+ " ok" + "Home");

		this.account=page.getParticle(Locators.HomepageHamburgerMenuCheck.ACCOUNTANDCARDS).getElement();

		String acc = account.getText().trim();
		System.out.println(acc);

		org.springframework.util.Assert.isTrue(acc.equals("Conti e carte"),"hamburger menu conti e carte");

		System.out.println(acc+ " ok" + "Conti e carte");

		this.voucher=page.getParticle(Locators.HomepageHamburgerMenuCheck.VOUCHERANDBOOKLET).getElement();

		String vou = voucher.getText().trim();
		System.out.println(vou);

		org.springframework.util.Assert.isTrue(vou.equals("Buoni e Libretti"),"hamburger menu buoni e libretti");

		System.out.println(vou +" ok" + "Buoni e Libretti");

		this.moneybox=page.getParticle(Locators.HomepageHamburgerMenuCheck.MONEYBOX).getElement();

		String money = moneybox.getText().trim();
		System.out.println(money); 

		org.springframework.util.Assert.isTrue(money.equals("Salvadanaio"),"hamburger menu salvadanaio");
		System.out.println(money+" ok" + "Salvadanaio"); 

		this.bacheca=page.getParticle(Locators.HomepageHamburgerMenuCheck.NOTICEBOARD).getElement();

		String board = bacheca.getText().trim();
		System.out.println(board); 

		org.springframework.util.Assert.isTrue(board.equals("Bacheca"),"hamburger menu bacheca");
		System.out.println(board +" ok " + "Bacheca");

		this.settings=page.getParticle(Locators.HomepageHamburgerMenuCheck.SETTINGS).getElement();

		String set = settings.getText().trim();
		System.out.println(set); 

		org.springframework.util.Assert.isTrue(set.equals("Impostazioni"),"hamburger menu impostazioni");
		System.out.println(set +" ok " + "Impostazioni");
		this.discount=page.getParticle(Locators.HomepageHamburgerMenuCheck.BPDISCOUNT).getElement();

		String dis = discount.getText().trim();
		System.out.println(dis); 

		org.springframework.util.Assert.isTrue(dis.equals("ScontiPoste"),"hamburger menu sconti poste");
		System.out.println(dis +" ok " + "ScontiPoste");
		this.postOfficeSearch=page.getParticle(Locators.HomepageHamburgerMenuCheck.POSTOFFICESEARCH).getElement();

		String office = postOfficeSearch.getText().trim();
		System.out.println(office); 

		org.springframework.util.Assert.isTrue(office.equals("Cerca Ufficio Postale"),"hamburger menu ufficio postale");
		System.out.println(office +" ok " + "Cerca Ufficio Postale");

		try {
			this.inviteYourFriends=page.getParticle(Locators.HomepageHamburgerMenuCheck.INVITEYOURFRIENDS).getElement();   
		} catch (Exception e) {
			Utility.swipe((MobileDriver<?>) page.getDriver(), Utility.DIRECTION.DOWN, 500);
		}
		this.inviteYourFriends=page.getParticle(Locators.HomepageHamburgerMenuCheck.INVITEYOURFRIENDS).getElement();

		String friends = inviteYourFriends.getText().trim();
		System.out.println(friends); 

		org.springframework.util.Assert.isTrue(friends.equals("Invita i tuoi amici"),"hamburger menu invita amici");
		System.out.println(friends +" ok " + "Invita i tuoi amici");

		try {
			this.otherApps=page.getParticle(Locators.HomepageHamburgerMenuCheck.OTHERAPPS).getElement();   
		} catch (Exception e) {
			Utility.swipe((MobileDriver<?>) page.getDriver(), Utility.DIRECTION.DOWN, 500);
		}
		this.otherApps=page.getParticle(Locators.HomepageHamburgerMenuCheck.OTHERAPPS).getElement();

		String other = otherApps.getText().trim();
		System.out.println(other); 

		org.springframework.util.Assert.isTrue(other.equals("LE ALTRE APP DI POSTE"),"hamburger menu altre app");
		System.out.println(other +" ok " + "LE ALTRE APP DI POSTE");	

		try {
			this.bpoffice=page.getParticle(Locators.HomepageHamburgerMenuCheck.BPOFFICE).getElement();
		} catch (Exception e) {
			Utility.swipe((MobileDriver<?>) page.getDriver(), Utility.DIRECTION.DOWN, 500);
		}
		this.bpoffice=page.getParticle(Locators.HomepageHamburgerMenuCheck.BPOFFICE).getElement();

		String bp = bpoffice.getText().trim();
		System.out.println(bp); 

		org.springframework.util.Assert.isTrue(bp.equals("Ufficio Postale"),"hamburger menu Ufficio Postale");
		System.out.println(bp +" ok " + "Ufficio Postale"); 	

		try {
			this.ppoffice=page.getParticle(Locators.HomepageHamburgerMenuCheck.PPOFFICE).getElement();      
		} catch (Exception e) {
			Utility.swipe((MobileDriver<?>) page.getDriver(), Utility.DIRECTION.DOWN, 500);
		}

		this.ppoffice=page.getParticle(Locators.HomepageHamburgerMenuCheck.PPOFFICE).getElement(); 
		String pp = ppoffice.getText().trim();
		System.out.println(pp); 
		org.springframework.util.Assert.isTrue(pp.equals("Postepay"),"hamburger menu PostePay");
		System.out.println(bp +" ok " + "Postepay"); 

		try {
			this.logout=page.getParticle(Locators.HomepageHamburgerMenuCheck.LOGOUT).getElement();
		} catch (Exception e) {
			Utility.swipe((MobileDriver<?>) page.getDriver(), Utility.DIRECTION.DOWN, 500);
		}
		this.logout=page.getParticle(Locators.HomepageHamburgerMenuCheck.LOGOUT).getElement();

		String logOut = logout.getText().trim();
		System.out.println(logOut); 

		org.springframework.util.Assert.isTrue(logOut.equals("Logout"),"hamburger menu logout");
		System.out.println(logOut +" ok " + "Logout");  
		this.footer=page.getParticle(Locators.HomepageHamburgerMenuCheck.FOOTER).getElement();

		//String footer2 = footer.getText().trim();
		//System.out.println(footer2); 

		Assert.assertTrue("hamburger menu footer",footer.isDisplayed());
		//org.springframework.util.Assert;
		System.out.println(footer +" ok ");  	




	}







	public void logout() 
	{
		WebDriver driver=page.getDriver();
		UIUtils.ui().mobile().swipe((MobileDriver)driver, UIUtils.SCROLL_DIRECTION.DOWN,0.3f,0.8f,0.3f,0.3f, 500);
		WaitManager.get().waitShortTime();
		UIUtils.ui().mobile().swipe((MobileDriver)driver, UIUtils.SCROLL_DIRECTION.DOWN,0.3f,0.8f,0.3f,0.3f, 500);
		WaitManager.get().waitMediumTime();
		WebElement logout=page.getParticle(Locators.HomepageHamburgerMenuMolecola.LOGOUT).getElement();
		logout.click();
		WaitManager.get().waitHighTime();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

	@Override
	public void clickOnApriunContolibretto() {
		Utility.swipe((MobileDriver<?>) page.getDriver(), Utility.DIRECTION.DOWN, 500);
		WaitManager.get().waitShortTime();
		page.getParticle(Locators.HomepageHamburgerMenuMolecola.VOLLINK).getElement().click();
		WaitManager.get().waitMediumTime();
	}

	
}
