package it.poste.bancoposta.pages.managers;

import static io.appium.java_client.MobileCommand.setSettingsCommand;

import org.openqa.selenium.WebElement;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import io.appium.java_client.CommandExecutionHelper;
import io.appium.java_client.ExecutesMethod;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BachecaMessaggiPage;
import it.poste.bancoposta.pages.BuoniLibrettiPage;
import it.poste.bancoposta.pages.ContiCartePage;
import it.poste.bancoposta.pages.HomePage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SalvadanaioPage;
import it.poste.bancoposta.pages.ScontiPostePage;
import it.poste.bancoposta.pages.SearchPostalOfficePage;
import it.poste.bancoposta.pages.SettingsPage;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;

@PageManager(page="HomepageHamburgerMenu")
@IOS
public class HomepageHamburgerMenuManiOS extends HomepageHamburgerMenuMan 
{
	private String pageSource;

	protected void load()
	{
		setCustomSettings();
		
		page.getQuick();
		pageSource=page.getPageSource();
		
		WebElement home=page.getParticle(Locators.HomepageHamburgerMenuMolecola.HOME).visibilityOfElement(10L);
		
		String txt=home.getText().trim();
		
		restoreDefaultSettings();
		
	}
	
	private void restoreDefaultSettings() {
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("customSnapshotTimeout",15));
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("snapshotMaxDepth",50));
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("useFirstMatch",false));
	}

	private void setCustomSettings() {
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("customSnapshotTimeout",0));
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("snapshotMaxDepth",15));
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("useFirstMatch",true));
	}
	
	public SettingsPage gotoSettings() 
	{
		setCustomSettings();
			
		try {
			UIUtils.ui().mobile().swipe((MobileDriver) page.getDriver(), SCROLL_DIRECTION.DOWN, 1000);
		} catch (Exception e) {
			// TODO: handle exception
		}
//		page.getParticle(Locators.HomepageHamburgerMenuMolecola.SETTINGS).clickOffline(pageSource);
		page.getParticle(Locators.HomepageHamburgerMenuMolecola.SETTINGS).getElement().click();
		WaitManager.get().waitMediumTime();
		
		restoreDefaultSettings();
		
		SettingsPage s=(SettingsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SettingsPage", page.getDriver().getClass(), page.getLanguage());

		s.checkPage();
		

		return s;
	}

	public HomePage gotoHomePage(String userHeader) 
	{
		setCustomSettings();
		
		WaitManager.get().waitMediumTime();
		UIUtils.ui().mobile().swipe((MobileDriver) page.getDriver(), SCROLL_DIRECTION.UP, 600);

//		page.getParticle(Locators.HomepageHamburgerMenuMolecola.HOME).clickOffline(pageSource);
		page.getParticle(Locators.HomepageHamburgerMenuMolecola.HOME).getElement().click();
		
		restoreDefaultSettings();
		
		HomePage h=(HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", page.getDriver().getClass(), page.getLanguage());

		h.setUserHeader(userHeader);

		h.checkPage();

		return h;
	}

	public ContiCartePage gotoContiCarte() 
	{
		
		page.getParticle(Locators.HomepageHamburgerMenuMolecola.ACCOUNTANDCARDS).clickOffline(pageSource);

		ContiCartePage h=(ContiCartePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ContiCartePage", page.getDriver().getClass(), page.getLanguage());

		h.checkPage();

		return h;
	}

	public SalvadanaioPage goToSalvadanaio() 
	{
		
		page.getParticle(Locators.HomepageHamburgerMenuMolecola.MONEYBOX).clickOffline(pageSource);

		SalvadanaioPage h=(SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioPage", page.getDriver().getClass(), page.getLanguage());

		h.checkPage();

		return h;
	}

	public BachecaMessaggiPage gotoBachecaSectionMessaggi() 
	{
		setCustomSettings();
		
		page.getParticle(Locators.HomepageHamburgerMenuMolecola.NOTICEBOARD).getElement().click();
		
		restoreDefaultSettings();

		BachecaMessaggiPage p=(BachecaMessaggiPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BachecaMessaggiPage", page.getDriver().getClass(), page.getLanguage());

		WaitManager.get().waitHighTime();

		p.clickMessaggi();

		p.checkPage();

		return p;
	}

	public ScontiPostePage goToSales() 
	{
		page.getParticle(Locators.HomepageHamburgerMenuMolecola.SALESPOSTE).clickOffline(pageSource);
		//TODO:TAA-301
		try {
			page.getParticle(Locators.SalesPostePageMolecola.NOPOPUPBUTTON).visibilityOfElement(10L)
			.click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		//			try{Thread.sleep(2000);}catch(Exception err){}
		//			try {
		//				switch(this.driverType)
		//				{
		//				case 0://android
		////					this.noPopupButton=driver.findElement(By.xpath(Locators.SalesPostePageMolecola.NOPOPUPBUTTON.getAndroidLocator()));
		//					WebElement noPopupButton = driver.findElement(By.xpath(Locators.SalesPostePageMolecola.NOPOPUPBUTTON.getLocator(driver)));
		//					noPopupButton.click(); 
		//					break;
		//				case 1://ios
		////					this.noPopupButton=driver.findElement(By.xpath(Locators.SalesPostePageMolecola.NOPOPUPBUTTON.getLocator(driver)));
		//					noPopupButton = driver.findElement(By.xpath(Locators.SalesPostePageMolecola.NOPOPUPBUTTON.getLocator(driver)));
		//					noPopupButton.click(); 
		//					break;
		//				}
		//			} catch (Exception e) {
		//				
		//			}
		ScontiPostePage p=(ScontiPostePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ScontiPostePage", page.getDriver().getClass(), page.getLanguage());
		p.checkPage();
		return p;

	}

	public SearchPostalOfficePage goToSearchPostalOfficePage() {

		UIUtils.ui().mobile().swipe((MobileDriver) page.getDriver(), SCROLL_DIRECTION.DOWN, 600);
		page.getParticle(Locators.HomepageHamburgerMenuMolecola.POSTOFFICESEARCH).getElement().click();
		SearchPostalOfficePage p=(SearchPostalOfficePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SearchPostalOfficePage", page.getDriver().getClass(), page.getLanguage());

		//TODO:TAA-302
		try {
			p.clickOkPoup();
		} catch (Exception e) {
			// TODO: handle exception
		}
		//			try{Thread.sleep(1500);}catch(Exception err){}
		//			
		//			try {
		//				switch(this.driverType)
		//				{
		//				case 0://android
		//					WebElement okPopupButton = driver.findElement(By.xpath(Locators.SearchPostalOfficePageLocator.POPUPOKBUTTON.getLocator(driver)));
		//					okPopupButton.click(); 
		//					break;
		//				case 1://ios
		//					okPopupButton = driver.findElement(By.xpath(Locators.SearchPostalOfficePageLocator.POPUPOKBUTTON.getLocator(driver)));
		//					okPopupButton.click(); 
		//					break;
		//				}
		//			} catch (Exception e) {
		//				
		//			}
		WaitManager.get().waitMediumTime();
		p.checkPage();
		return p;

	}



	public BuoniLibrettiPage goToBuoniLibretti() {

//		page.getParticle(Locators.HomepageHamburgerMenuMolecola.BUONILIBRETTIBUTTON).clickOffline(pageSource);
//		WaitManager.get().waitMediumTime();
//		BuoniLibrettiPage p=(BuoniLibrettiPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
//				.getPageManager("BuoniLibrettiPage", page.getDriver().getClass(), page.getLanguage());
//		p.checkPage();
//		return p;

		page.getParticle(Locators.HomepageHamburgerMenuMolecola.BUONILIBRETTIBUTTON).getElement().click();
		WaitManager.get().waitMediumTime();
		BuoniLibrettiPage p=(BuoniLibrettiPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BuoniLibrettiPage", page.getDriver().getClass(), page.getLanguage());
		p.checkPage();
		return p;
	}
}
