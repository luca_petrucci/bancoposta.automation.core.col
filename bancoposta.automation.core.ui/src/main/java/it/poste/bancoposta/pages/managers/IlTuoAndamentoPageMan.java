package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.TutorialPopUpBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import it.poste.bancoposta.pages.IlTuoAndamentoPage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;
import test.automation.core.cmd.adb.AdbCommandPrompt;
import test.automation.core.cmd.adb.AdbKeyEvent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="IlTuoAndamentoPage")
@Android
public class IlTuoAndamentoPageMan extends LoadableComponent<IlTuoAndamentoPageMan> implements IlTuoAndamentoPage
{
	UiPage page;
	
	private WebElement testo;
	private WebElement header;
	private WebElement description;
	private WebElement apriButton;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public void checkEntrateTab() 
	{
		page.getParticle(Locators.IlTuoAndamentoMolecola.ENTRATE_TAB).visibilityOfElement(null).click();
		//
		checkSubPage();
	}

	public void checkSubPage() 
	{
		page.getParticle(Locators.IlTuoAndamentoMolecola.RISPETTO_ALLA_MEDIA).visibilityOfElement(null);
		try {
			page.getParticle(Locators.IlTuoAndamentoMolecola.HAI_SPESO_TXT).visibilityOfElement(2L);
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			page.getParticle(Locators.IlTuoAndamentoMolecola.TOTALE_SPESO_TXT).visibilityOfElement(2L);
		} catch (Exception e) {
			// TODO: handle exception
		}
		page.getParticle(Locators.IlTuoAndamentoMolecola.GRAFO).visibilityOfElement(null);
		page.getParticle(Locators.IlTuoAndamentoMolecola.FUMETTO).visibilityOfElement(null);
		page.getParticle(Locators.IlTuoAndamentoMolecola.MESE).visibilityOfElement(null);
		page.getParticle(Locators.IlTuoAndamentoMolecola.MEDIA_GRAFO).visibilityOfElement(null);
		page.getParticle(Locators.IlTuoAndamentoMolecola.MAX_EURO_GRAFO).visibilityOfElement(null);
		page.getParticle(Locators.IlTuoAndamentoMolecola.MIN_EURO_GRAFO).visibilityOfElement(null);
		page.getParticle(Locators.IlTuoAndamentoMolecola.SIDEBAR_GRAFO).visibilityOfElement(null);
		
	}

	public void checkUsciteTab() 
	{
		page.getParticle(Locators.IlTuoAndamentoMolecola.USCITE_TAB).visibilityOfElement(null).click();
		//
		checkSubPage();
	}

	public void checkDifferenzaTab() 
	{
		page.getParticle(Locators.IlTuoAndamentoMolecola.DIFFERENZA_TAB).visibilityOfElement(null).click();
		//
		checkSubPage();
	}

	public void checkGraphSeekBarEntrate() 
	{
		page.getParticle(Locators.IlTuoAndamentoMolecola.ENTRATE_TAB).getElement().click();
		checkSideBar(false);
	}


	public void checkGraphSeekBarUscite() 
	{
		page.getParticle(Locators.IlTuoAndamentoMolecola.USCITE_TAB).getElement().click();
		checkSideBar(false);
	}

	public void checkGraphSeekBarAndamento() 
	{
		page.getParticle(Locators.IlTuoAndamentoMolecola.DIFFERENZA_TAB).getElement().click();
		checkSideBar(true);
	}

	public void checkSideBar(boolean andamento) 
	{
		if(!andamento)
		{
			WebElement fumetto=page.getParticle(Locators.IlTuoAndamentoMolecola.FUMETTO).getElement();
			WebElement mese=page.getParticle(Locators.IlTuoAndamentoMolecola.MESE).getElement();
			WebElement subtitle1=page.getParticle(Locators.IlTuoAndamentoMolecola.HAI_SPESO_TXT).getElement();
			WebElement subtitle2=page.getParticle(Locators.IlTuoAndamentoMolecola.TOTALE_SPESO_TXT).getElement();
			
			String fm=fumetto.getText().trim();
			String ms=mese.getText().trim();
			String sb1=subtitle1.getText().trim();
			String sb2=subtitle2.getText().trim();
			
			clickSideBar(0.5);
			checkGrafoAfterClickOnSidebar(fm,ms,sb1,sb2);
			
			fumetto=page.getParticle(Locators.IlTuoAndamentoMolecola.FUMETTO).getElement();
			mese=page.getParticle(Locators.IlTuoAndamentoMolecola.MESE).getElement();
			subtitle1=page.getParticle(Locators.IlTuoAndamentoMolecola.HAI_SPESO_TXT).getElement();
			subtitle2=page.getParticle(Locators.IlTuoAndamentoMolecola.TOTALE_SPESO_TXT).getElement();
			
			fm=fumetto.getText().trim();
			ms=mese.getText().trim();
			sb1=subtitle1.getText().trim();
			sb2=subtitle2.getText().trim();
			
			clickSideBar(0.3);
			checkGrafoAfterClickOnSidebar(fm,ms,sb1,sb2);
		}
		else
		{
			WebElement fumetto=page.getParticle(Locators.IlTuoAndamentoMolecola.FUMETTO).getElement();
			WebElement mese=page.getParticle(Locators.IlTuoAndamentoMolecola.MESE).getElement();
			
			String fm=fumetto.getText().trim();
			String ms=mese.getText().trim();
			
			clickSideBar(0.5);
			checkGrafoAfterClickOnSidebar(fm,ms,null,null);
			
			fumetto=page.getParticle(Locators.IlTuoAndamentoMolecola.FUMETTO).getElement();
			mese=page.getParticle(Locators.IlTuoAndamentoMolecola.MESE).getElement();
			
			fm=fumetto.getText().trim();
			ms=mese.getText().trim();
			
			clickSideBar(0.3);
			checkGrafoAfterClickOnSidebar(fm,ms,null,null);
		}
	}

	public void checkGrafoAfterClickOnSidebar(String fm, String ms, String sb1, String sb2) 
	{
		WebElement fumetto=page.getParticle(Locators.IlTuoAndamentoMolecola.FUMETTO).getElement();
		WebElement mese=page.getParticle(Locators.IlTuoAndamentoMolecola.MESE).getElement();
		WebElement subtitle1=null;
		WebElement subtitle2=null;
		
		if(sb1 != null)
			subtitle1=page.getParticle(Locators.IlTuoAndamentoMolecola.HAI_SPESO_TXT).getElement();
		if(sb2 != null)
			subtitle2=page.getParticle(Locators.IlTuoAndamentoMolecola.TOTALE_SPESO_TXT).getElement();
		
		org.springframework.util.Assert.isTrue(!fumetto.getText().trim().equals(fm), "fumetto non cambiato dopo click su sidebar precedente:"+fm+" attuale:"+fumetto.getText());
		org.springframework.util.Assert.isTrue(!mese.getText().trim().equals(ms), "mese non cambiato dopo click su sidebar precedente:"+ms+" attuale:"+mese.getText());
		
		if(sb1 != null)
			org.springframework.util.Assert.isTrue(!subtitle1.getText().trim().equals(sb1), "subtitle1 non cambiato dopo click su sidebar precedente:"+sb1+" attuale:"+subtitle1.getText());
		
		if(sb2 != null)
			org.springframework.util.Assert.isTrue(!subtitle2.getText().trim().equals(sb2), "subtitle2 non cambiato dopo click su sidebar precedente:"+sb2+" attuale:"+subtitle2.getText());
	}

	public void clickSideBar(double d) 
	{
		WebElement seekBar=page.getParticle(Locators.IlTuoAndamentoMolecola.SIDEBAR_GRAFO).getElement();
		Point l=seekBar.getLocation();
		Rectangle r=seekBar.getRect();
		
		int xPos=(int) (r.width * d) + l.x;
		int yPos=(int) (r.height * 0.5) + l.y;
		
		System.out.println("click "+xPos+","+yPos);
		
//		TouchAction t=new TouchAction((PerformsTouchActions) driver);
//		t.press(PointOption.point(xPos, yPos));
		
		Properties p=UIUtils.ui().getCurrentProperties();
		
		AdbCommandPrompt c=new AdbCommandPrompt(p.getProperty("adb.path"), p.getProperty("ios.udid"));
		
		c.tap(xPos, yPos);
		
		WaitManager.get().waitShortTime();
		
		
	}

	public void clickUsciteTab() 
	{
		page.getParticle(Locators.IlTuoAndamentoMolecola.USCITE_TAB).getElement().click();
		
	}

	public void clickEntrateTab() 
	{
		page.getParticle(Locators.IlTuoAndamentoMolecola.ENTRATE_TAB).getElement().click();
		
	}

	public void clickDifferenzaTab() 
	{
		page.getParticle(Locators.IlTuoAndamentoMolecola.DIFFERENZA_TAB).getElement().click();
		
	}

	public void clickRightMenu() 
	{
		page.getParticle(Locators.IlTuoAndamentoMolecola.RIGHT_BUTTON).getElement().click();
		
	}

	public void esportaCsv() 
	{
//		page.readPageSource();
//		System.out.println(page.getPageSource());
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.ESPORTA_CSV).visibilityOfElement(10L).click();
		
		WaitManager.get().waitMediumTime();
		
		try {
			page.getParticle(Locators.NewLoginPreLoginPageMolecola.ALLOWPOPUPBUTTON).visibilityOfElement(10L).click();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		WaitManager.get().waitMediumTime();
		
		
		Properties p=UIUtils.ui().getCurrentProperties();
		
		try {
			AdbCommandPrompt c=new AdbCommandPrompt(p.getProperty("adb.path"), p.getProperty("ios.udid"));
			
			c.keyEvent(AdbKeyEvent.KEYCODE_BACK);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		SimpleDateFormat s=new SimpleDateFormat("dd_MMM_yyyy");
		String date=s.format(Calendar.getInstance().getTime()).toUpperCase();
		
		String csvFile="/storage/emulated/0/Download/Il_tuo_andamento_"+date+".csv";
		System.out.println(csvFile);
		
		AdbCommandPrompt c=new AdbCommandPrompt(p.getProperty("adb.path"), p.getProperty("ios.udid"));
		
		try {
			c.pull(csvFile, "Il_tuo_andamento_"+date+".csv");
		} catch (Exception e) 
		{
			s=new SimpleDateFormat("dd_MMM_yyyy",Locale.ENGLISH);
			date=s.format(Calendar.getInstance().getTime()).toUpperCase();
			csvFile="/storage/emulated/0/Download/Il_tuo_andamento_"+date+".csv";
			System.out.println(csvFile);
			
			c.pull(csvFile, "Il_tuo_andamento_"+date+".csv");
		}
	}

	public void readAndCheckCsv() throws Exception 
	{
		SimpleDateFormat s=new SimpleDateFormat("dd_MMM_yyyy");
		String date=s.format(Calendar.getInstance().getTime()).toUpperCase();
		Reader reader = null;
		
		try {
			 reader =new BufferedReader(new FileReader("Il_tuo_andamento_"+date+".csv"));
//			reader=Files.newBufferedReader(Paths.get("Il_tuo_andamento_"+date+".csv"));
		} catch (Exception e) {
			s=new SimpleDateFormat("dd_MMM_yyyy",Locale.ENGLISH);
			date=s.format(Calendar.getInstance().getTime()).toUpperCase();
			reader =new BufferedReader(new FileReader("Il_tuo_andamento_"+date+".csv"));
//			reader=Files.newBufferedReader(Paths.get("Il_tuo_andamento_"+date+".csv"));
		}
		
        CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader());
        
        int row=0;
        
        for(CSVRecord csvRecord : csvParser)
        {
        	row++;
        	String tipo=csvRecord.get(0);
        	String media=csvRecord.get(1);
        	String[] mesi=new String[12];
        	
        	int mesiIdx=0;
        	
        	for(int i=2;i<14;i++)
        	{
        		mesi[mesiIdx]=csvRecord.get(i).trim();
        		mesiIdx++;
        	}
        	
        	org.springframework.util.Assert.isTrue(!tipo.isEmpty(), "tipo vuoto");
        	org.springframework.util.Assert.isTrue(!media.isEmpty(), "media vuoto");
        	
        	double md=0;
        	
        	for(String s1 : mesi)
        	{
        		double val=Double.parseDouble(s1.replace("\u20AC", "").replace(",", ".").replace(" ", ""));
        		System.out.println(val);
        		md+=val;
        	}
        	
        	md = md / 12.0;
        	md=new BigDecimal(md).setScale(2, RoundingMode.HALF_UP).doubleValue();
        	
        	double _media=Double.parseDouble(media.trim().replace("\u20AC", "").replace(",", ".").replace(" ", ""));
        	
        	org.springframework.util.Assert.isTrue(_media == md, "media errata attuale:"+_media+" attesa:"+md);
        	System.out.println("----------------------------------");
        	System.out.println("media errata attuale:"+_media+" attesa:"+md);
        }
        
        org.springframework.util.Assert.isTrue(row > 0, "file csv vuoto");
	}

	public void removeCsv() 
	{
		SimpleDateFormat s=new SimpleDateFormat("dd_MMM_yyyy");
		String date=s.format(Calendar.getInstance().getTime()).toUpperCase();
		String csvFile="/storage/emulated/0/Download/Il_tuo_andamento_"+date+".csv";
		System.out.println("delete "+csvFile);
		Properties p=UIUtils.ui().getCurrentProperties();
		
		AdbCommandPrompt c=new AdbCommandPrompt(p.getProperty("adb.path"), p.getProperty("ios.udid"));
		
		c.remove(csvFile);
		
		File f=new File("Il_tuo_andamento_"+date+".csv");
		
		f.delete();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

	@Override
	public void clickOnBack() {
		page.getParticle(Locators.IlTuoAndamentoMolecola.CLOSE_BUTTON).visibilityOfElement(10L).click();
	}

}
