package it.poste.bancoposta.pages.managers;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebElement;

import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import io.appium.java_client.InteractsWithApps;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;
import test.automation.core.cmd.adb.AdbCommandPrompt;

@PageManager(page="IlTuoAndamentoPage")
@IOS
public class IlTuoAndamentoPageManIOS extends IlTuoAndamentoPageMan {
	protected static String[] mesi=new String[] {
			"Gen",
			"Feb",
			"Mar",
			"Apr",
			"Mag",
			"Giu",
			"Lug",
			"Ago",
			"Set",
			"Ott",
			"Nov",
			"Dic"
	};

	
	public void checkSubPage() 
	{
		page.getParticle(Locators.IlTuoAndamentoMolecola.RISPETTO_ALLA_MEDIA).visibilityOfElement(null);
		try {
			page.getParticle(Locators.IlTuoAndamentoMolecola.HAI_SPESO_TXT).visibilityOfElement(2L);
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			page.getParticle(Locators.IlTuoAndamentoMolecola.TOTALE_SPESO_TXT).visibilityOfElement(2L);
		} catch (Exception e) {
			// TODO: handle exception
		}
		page.getParticle(Locators.IlTuoAndamentoMolecola.GRAFO).visibilityOfElement(null);
		page.getParticle(Locators.IlTuoAndamentoMolecola.FUMETTO).visibilityOfElement(null);
		page.getParticle(Locators.IlTuoAndamentoMolecola.MESE).visibilityOfElement(null);
//		page.getParticle(Locators.IlTuoAndamentoMolecola.MEDIA_GRAFO).visibilityOfElement(null);
//		page.getParticle(Locators.IlTuoAndamentoMolecola.MAX_EURO_GRAFO).visibilityOfElement(null);
//		page.getParticle(Locators.IlTuoAndamentoMolecola.MIN_EURO_GRAFO).visibilityOfElement(null);
		page.getParticle(Locators.IlTuoAndamentoMolecola.SIDEBAR_GRAFO).visibilityOfElement(null);
		
	}
	public void clickSideBar(double d) 
	{
		WebElement mese=page.getParticle(Locators.IlTuoAndamentoMolecola.MESE).getElement();
		Point l=mese.getLocation();
		Rectangle r=mese.getRect();
		
		int xPos=(int) (r.width * 0.5) + l.x;
		int yPos=(int) (r.height * 0.5) + l.y;
		System.out.println("click "+xPos+","+yPos);
		WebElement seekBar=page.getParticle(Locators.IlTuoAndamentoMolecola.SIDEBAR_GRAFO).getElement();
		l=seekBar.getLocation();
		r=seekBar.getRect();
		int xPos1=(int) (r.width * d) + l.x;
		int yPos2=(int) (r.height * 0.5) + l.y;
		System.out.println("click num2 "+xPos1+","+yPos2);
		TouchAction action = new TouchAction((PerformsTouchActions) page.getDriver());
		action.longPress(PointOption.point(xPos, yPos)).waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
		.moveTo(PointOption.point(xPos1, yPos2))
		.release()	
		.perform();
		WaitManager.get().waitShortTime();
	}
	
	public void esportaCsv() 
	{
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.ESPORTA_CSV).visibilityOfElement(10L);
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.ESPORTA_CSV).getElement().click();
		
		WaitManager.get().waitMediumTime();
		
		int m=Calendar.getInstance().get(Calendar.MONTH);
		int y=Calendar.getInstance().get(Calendar.YEAR);
		int d=Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		
		String csvFile="Il_tuo_Andamento_"+d+"_"+mesi[m].toUpperCase()+"_"+y;
		System.out.println(csvFile);
		
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.salvaSuFileBtn).getElement().click();
		WaitManager.get().waitShortTime();
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.btnIPhone).getElement().click();
		WaitManager.get().waitShortTime();
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.btnSalvaCsv).getElement().click();
		WaitManager.get().waitShortTime();
		try {
			page.getParticle(Locators.YourExpencesDetailsPageMolecola.btnSostituisciCsv).getElement().click();
			WaitManager.get().waitShortTime();
		}catch(Exception e)
		{
			//TODO
		}
	
	}
	public void readAndCheckCsv() throws Exception 
	{
		((InteractsWithApps) page.getDriver()).activateApp("com.apple.DocumentsApp");
		WaitManager.get().waitShortTime();
		
		int m=Calendar.getInstance().get(Calendar.MONTH);
		int y=Calendar.getInstance().get(Calendar.YEAR);
		int d=Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		String controllo=""+d;
		if(controllo.length()==1)
			controllo="0"+controllo;
		String csvFile="Il_tuo_Andamento_"+controllo+"_"+mesi[m].toUpperCase()+"_"+y;
		try {
			page.getParticle(Locators.YourExpencesDetailsPageMolecola.btnFine).getElement().click();;
		}catch(Exception e)
		{
			
		}
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.csvFile).getElement(new Entry("title",csvFile)).click();
		WaitManager.get().waitShortTime();
		
		int pos=0;
		String locator= page.getParticle(Locators.YourExpencesDetailsPageMolecola.csvTable).getLocator(new Entry("title",csvFile));
		List<WebElement> table= page.getDriver().findElements(By.xpath(locator));
		for(int i=2;i<table.size();i++)
		{
			System.out.println(table.get(i).getText());
			if(table.get(i).getText().equals("USCITE"))
			{
				String mediaTotale=table.get(i+1).getText();
				double sommaMedie=0;
				for(int j=i+2;j<=(i+13);j++)
				{
					String media=table.get(j).getText();
					double val=Double.parseDouble(media.replace("\u20AC", "").replace(",", ".").replace(" ", ""));
	        		System.out.println(val);
	        		sommaMedie+=val;
				}
				sommaMedie/=12.0;
				BigDecimal bd = BigDecimal.valueOf(sommaMedie);
			    bd = bd.setScale(2, RoundingMode.HALF_UP);
			    sommaMedie=bd.doubleValue();
				double val=Double.parseDouble(mediaTotale.replace("\u20AC", "").replace(",", ".").replace(" ", ""));
				assertTrue("controllo media uscite. Valore "+val+ " Valore Calcolato "+ sommaMedie,Math.abs(val-sommaMedie) <= 0.01);
			}
			
			if(table.get(i).getText().equals("ENTRATE"))
			{
				String mediaTotale=table.get(i+1).getText();
				double sommaMedie=0;
				for(int j=i+2;j<=(i+13);j++)
				{
					String media=table.get(j).getText();
					double val=Double.parseDouble(media.replace("\u20AC", "").replace(",", ".").replace(" ", ""));
	        		System.out.println(val);
	        		sommaMedie+=val;
				}
				sommaMedie/=12.0;
				BigDecimal bd = BigDecimal.valueOf(sommaMedie);
			    bd = bd.setScale(2, RoundingMode.HALF_UP);
			    sommaMedie=bd.doubleValue();
				double val=Double.parseDouble(mediaTotale.replace("\u20AC", "").replace(",", ".").replace(" ", ""));
				assertTrue("controllo media entrate. Valore "+val+ " Valore Calcolato "+ sommaMedie,Math.abs(val-sommaMedie) <= 0.01);
			}
			
			if(table.get(i).getText().equals("DIFFERENZE"))
			{
				String mediaTotale=table.get(i+1).getText();
				double sommaMedie=0;
				for(int j=i+2;j<=(i+13);j++)
				{
					String media=table.get(j).getText();
					double val=Double.parseDouble(media.replace("\u20AC", "").replace(",", ".").replace(" ", ""));
	        		System.out.println(val);
	        		sommaMedie+=val;
				}
				sommaMedie/=12.0;
				System.out.println("Somma Medie"+sommaMedie);
				BigDecimal bd = BigDecimal.valueOf(sommaMedie);
			    bd = bd.setScale(2, RoundingMode.HALF_UP);
			    sommaMedie=bd.doubleValue();
				double val=Double.parseDouble(mediaTotale.replace("\u20AC", "").replace(",", ".").replace(" ", ""));
				assertTrue("controllo media differenze. Valore "+val+ " Valore Calcolato "+ sommaMedie, Math.abs(val-sommaMedie) <= 0.01);
			}
		}
		Properties pro = UIUtils.ui().getCurrentProperties();
		((InteractsWithApps) page.getDriver()).activateApp(pro.getProperty("ios.bundle.id"));
	}
	
	public void removeCsv() 
	{
	
	}
}
