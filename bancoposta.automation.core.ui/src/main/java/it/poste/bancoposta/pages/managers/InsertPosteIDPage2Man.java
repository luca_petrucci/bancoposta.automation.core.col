package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import io.appium.java_client.AppiumDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.InsertPosteIDPage2;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OKOperationPage;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="InsertPosteIDPage2")
@Android
@IOS
public class InsertPosteIDPage2Man extends LoadableComponent<InsertPosteIDPage2Man> implements InsertPosteIDPage2
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement title;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement posteIdInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement confirm;

	private WebElement nonSeituButton;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.getParticle(Locators.InsertPosteIDPageMolecola.HEADER).visibilityOfElement(20L);
		page.get();
	}

	public OKOperationPage insertPosteID(String posteId) 
	{
		this.posteIdInput=page.getParticle(Locators.InsertPosteIDPage2Molecola.POSTEIDINPUT).getElement();
		try {((AppiumDriver<?>) page.getDriver()).hideKeyboard();}catch(Exception err) {};
		try {((AppiumDriver<?>) page.getDriver()).hideKeyboard();}catch(Exception err) {};
		this.confirm=page.getParticle(Locators.InsertPosteIDPage2Molecola.CONFIRM).getElement();

		this.posteIdInput.clear();
		this.posteIdInput.sendKeys(posteId);

		try {((AppiumDriver<?>) page.getDriver()).hideKeyboard();}catch(Exception err) {};


		this.confirm.click();

		OKOperationPage tnkPage=(OKOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("OKOperationPage", page.getDriver().getClass(), page.getLanguage());

		tnkPage.checkPage();

		return tnkPage;
	}
	//-------------------------------ANNUNZIATA------------------------------------------

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}
}
