package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import io.appium.java_client.HidesKeyboard;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import io.appium.java_client.AppiumDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OKOperationPage;
import it.poste.bancoposta.utils.PageMolecolaImgCreator;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="InsertPosteIDPage")
@Android
@IOS
public class InsertPosteIDPageMan extends LoadableComponent<InsertPosteIDPageMan> implements InsertPosteIDPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement title;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement posteIdInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement confirm;

	private WebElement nonSeituButton;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.getParticle(Locators.InsertPosteIDPageMolecola.HEADER).visibilityOfElement(20L);
//		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}
//		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}
		page.getParticle(Locators.InsertPosteIDPageMolecola.DESCRIPTION).visibilityOfElement(20L).click();
		//UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.InsertPosteIDPageMolecola.BACKBUTTON.getAndroidLocator())));
		page.get();
		
//		WaitManager.get().waitMediumTime();
//		
//		byte[] b=((TakesScreenshot)page.getDriver()).getScreenshotAs(OutputType.BYTES);
//		PageMolecolaImgCreator.makeImg(b, page,
//				Locators.InsertPosteIDPageMolecola.HEADER,
//				Locators.InsertPosteIDPageMolecola.TITLE,
//				Locators.InsertPosteIDPageMolecola.DESCRIPTION,
//				Locators.InsertPosteIDPageMolecola.POSTEIDINPUT,
//				Locators.InsertPosteIDPageMolecola.CONFIRM
//				);
	}

	public OKOperationPage insertPosteID(String posteId) 
	{
		this.posteIdInput=page.getParticle(Locators.InsertPosteIDPageMolecola.POSTEIDINPUT).getElement();
//		try {((AppiumDriver<?>) page.getDriver()).hideKeyboard();}catch(Exception err) {};
//		try {((AppiumDriver<?>) page.getDriver()).hideKeyboard();}catch(Exception err) {};
		this.confirm=page.getParticle(Locators.InsertPosteIDPageMolecola.CONFIRM).getElement();
		
//		this.posteIdInput.clear();
		this.posteIdInput.sendKeys(posteId);

		page.getParticle(Locators.InsertPosteIDPageMolecola.DESCRIPTION).visibilityOfElement(20L).click();
//		try {((AppiumDriver<?>) page.getDriver()).hideKeyboard();}catch(Exception err) {};


		this.confirm.click();

		OKOperationPage tnkPage=(OKOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("OKOperationPage", page.getDriver().getClass(), page.getLanguage());

		tnkPage.clickNoGrazie();
		
//		tnkPage.checkPage();

		return tnkPage;
	}

	public void clickNonSeiTu() {

		nonSeituButton = page.getParticle(Locators.InsertPosteIDPageMolecola.NON_SEI_TU).getElement();
		nonSeituButton.click();

	}

	//-------------------------------ANNUNZIATA------------------------------------------
	public void insertPosteId(String posteId)
	{
		this.posteIdInput=page.getParticle(Locators.InsertPosteIDPageMolecola.POSTEIDINPUT).getElement();
		try {((AppiumDriver<?>) page.getDriver()).hideKeyboard();}catch(Exception err) {};
//		try {((AppiumDriver<?>) page.getDriver()).hideKeyboard();}catch(Exception err) {};
		this.confirm=page.getParticle(Locators.InsertPosteIDPageMolecola.CONFIRM).getElement();
		

		this.posteIdInput.clear();
		this.posteIdInput.sendKeys(posteId);
		WaitManager.get().waitShortTime();
		try {((AppiumDriver<?>) page.getDriver()).hideKeyboard();}catch(Exception err) {};

		//chiusura tastiera
		page.getParticle(Locators.InsertPosteIDPageMolecola.TITLE).getElement().click();
		this.confirm.click();
	}
	//-------------------------------ANNUNZIATA------------------------------------------

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}
}
