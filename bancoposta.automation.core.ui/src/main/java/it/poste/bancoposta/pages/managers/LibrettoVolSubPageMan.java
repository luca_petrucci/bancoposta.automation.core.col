package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.w3c.dom.Node;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BiscottoDataBean;
import bean.datatable.TutorialPopUpBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import it.poste.bancoposta.pages.LibrettoVolSubPage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;
import utils.ObjectFinderLight;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="LibrettoVolSubPage")
@Android
public class LibrettoVolSubPageMan extends LoadableComponent<LibrettoVolSubPageMan> implements LibrettoVolSubPage
{
	UiPage page;
	
	private String pageSource;
	private WebElement fid;
	
	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
//		String pageSource=page.getDriver().getPageSource();
//		WaitManager.get().waitLongTime();
		page.getParticle(Locators.LibrettoVolSubPageMolecola.CONTAINER).getElement();
		page.getParticle(Locators.LibrettoVolSubPageMolecola.PRODOTTI_CONTAINER).getElement();
		page.getParticle(Locators.LibrettoVolSubPageMolecola.PRODOTTI_OFFERTI).getElement();
		page.getParticle(Locators.LibrettoVolSubPageMolecola.INTERA_GAMMA_LINK).getElement();
//		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.LibrettoVolSubPageMolecola.CONTAINER).getLocator()), "Container not found");
//		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.LibrettoVolSubPageMolecola.PRODOTTI_CONTAINER).getLocator()), "PRODOTTI_CONTAINER not found");
//		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.LibrettoVolSubPageMolecola.PRODOTTI_OFFERTI).getLocator()), "PRODOTTI_OFFERTI not found");
//		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.LibrettoVolSubPageMolecola.INTERA_GAMMA_LINK).getLocator()), "INTERA_GAMMA_LINK not found");
		
//		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.5f, 0.5f, 0.2f, 300);
//	    WaitManager.get().waitMediumTime();
//	    
//	    fid=page.getParticle(Locators.LibrettoVolSubPageMolecola.INTERA_GAMMA).visibilityOfElement(10L);
//		
		
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LibrettoVolSubPageMolecola.CONTAINER.getLocator(driver))));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LibrettoVolSubPageMolecola.PRODOTTI_CONTAINER.getLocator(driver))));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LibrettoVolSubPageMolecola.PRODOTTI_OFFERTI.getLocator(driver))));
//		UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LibrettoVolSubPageMolecola.INTERA_GAMMA_LINK.getLocator(driver))));
		
	}

	public void checkSubPage(BiscottoDataBean b) 
	{
		// TODO Auto-generated method stub
		
	}

	public void navigateToProduct(String prodotto) 
	{
		WaitManager.get().waitLongTime();
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.5f, 0.5f, 0.2f, 300);
		WaitManager.get().waitMediumTime();
		
		System.out.println("prodotto da trovare:"+prodotto);
		
		List<String> prodVisitato=new ArrayList();
		
		for(int i=0;i<3;i++)
		{
			List<WebElement> el=page.getParticle(Locators.LibrettoVolSubPageMolecola.PRODOTTI_OFFERTI_TITLE).getListOfElements();
			System.out.println("prodotti trovati:"+el.size());
			
			for(WebElement e : el)
			{
				String txt=e.getText().trim();
				System.out.println(txt);
				
				boolean find=false;
				
				for(String s : prodVisitato)
				{
					if(s.equals(txt))
					{
						find=true;
						break;
					}
				}
				
				if(!find)
				{
					if(txt.contains(prodotto))
					{
						e.click();
						return;
					}
					
					UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.LEFT, 0.5f, 0.5f, 0.2f, 0.5f, 1000);
					WaitManager.get().waitMediumTime();
					
					prodVisitato.add(txt);
				}
				
				
				
			}
		}
	}

	public void clickOnInteraGamma(String pageSource) 
	{
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.5f, 0.5f, 0.2f, 300);
	    WaitManager.get().waitMediumTime();
//	    UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.LibrettoVolSubPageMolecola.INTERA_GAMMA.getLocator(driver))));
//		WebElement fid=driver.findElement(By.xpath(Locators.LibrettoVolSubPageMolecola.INTERA_GAMMA.getLocator(driver)));
		
	    //String pageSource=driver.getPageSource();
//		if(ObjectFinderLight.elementExists(pageSource, Locators.LibrettoVolSubPageMolecola.INTERA_GAMMA.getLocator(driver)))
//		{
//			Node e=ObjectFinderLight.getElement(pageSource, Locators.LibrettoVolSubPageMolecola.INTERA_GAMMA.getLocator(driver));
//			String bounds=e.getAttributes().getNamedItem("bounds").getNodeValue().trim();
//			bounds=bounds.replace("][", ",").replace("[", "").replace("]", "");
//			System.out.println("bounds:"+bounds);
//			String[] coordinate=bounds.split(",");
//			
//			int x1=Integer.parseInt(coordinate[0]);
//			int y1=Integer.parseInt(coordinate[1]);
//			int x2=Integer.parseInt(coordinate[2]);
//			int y2=Integer.parseInt(coordinate[3]);
//			
//			Rectangle r=new Rectangle(x1, y1, x2-x1, y2-y1);
//			int x=(int) (x1 + (r.getWidth()*0.5));
//			int y=y1;
//			System.out.println("x:"+x+", y:"+y);
//			TouchAction touchAction=new TouchAction((PerformsTouchActions) driver);
//			touchAction.tap(PointOption.point(x, y)).perform();
//		}
	    //fid.click();
		Rectangle r=fid.getRect();
		Point p=fid.getLocation();
		int x=(int) (p.x + (r.getWidth()*0.5));
		int y=p.y;
		System.out.println("x:"+x+", y:"+y);
		TouchAction touchAction=new TouchAction((PerformsTouchActions) page.getDriver());
		touchAction.tap(PointOption.point(x, y)).perform();
	}

	public void getLight(String p) throws Exception 
	{
		pageSource=p;
		
		if(!ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.LibrettoVolSubPageMolecola.PRODOTTI_CONTAINER).getLocator()))
			throw new Exception("element not find in light mode "+page.getParticle(Locators.LibrettoVolSubPageMolecola.PRODOTTI_CONTAINER).getName());
		
		if(!ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.LibrettoVolSubPageMolecola.PRODOTTI_OFFERTI).getLocator()))
			throw new Exception("element not find in light mode "+page.getParticle(Locators.LibrettoVolSubPageMolecola.PRODOTTI_OFFERTI).getName());
		
		if(!ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.LibrettoVolSubPageMolecola.INTERA_GAMMA_LINK).getLocator()))
			throw new Exception("element not find in light mode "+page.getParticle(Locators.LibrettoVolSubPageMolecola.INTERA_GAMMA_LINK).getName());
		
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

	

}
