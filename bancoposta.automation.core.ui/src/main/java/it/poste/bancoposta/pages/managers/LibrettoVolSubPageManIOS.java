package it.poste.bancoposta.pages.managers;

import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import it.poste.bancoposta.pages.Locators;

@PageManager(page="LibrettoVolSubPage")
@IOS
public class LibrettoVolSubPageManIOS extends LibrettoVolSubPageMan {
	protected void load() 
	{	
		boolean popup=false;
		try {
			page.getParticle(Locators.ContoVolSubPageMolecola.ERRORPOPUPIOS).visibilityOfElement(15L);
			popup=true;
		}catch(Exception e) {

		}
		if(popup==true) {
			throw new RuntimeException("Popup di Errore sulla pagina VOL");
		}
		page.getParticle(Locators.LibrettoVolSubPageMolecola.CONTAINER).getElement();
		page.getParticle(Locators.LibrettoVolSubPageMolecola.PRODOTTI_CONTAINER).getElement();
		page.getParticle(Locators.LibrettoVolSubPageMolecola.PRODOTTI_OFFERTI).getElement();
		page.getParticle(Locators.LibrettoVolSubPageMolecola.INTERA_GAMMA_LINK).getElement();

	}
}
