package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import io.appium.java_client.HidesKeyboard;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import io.appium.java_client.AppiumDriver;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.LoginPosteIdPage;
import it.poste.bancoposta.utils.PageMolecolaImgCreator;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="LoginPosteIdPage")
@Android
@IOS
public class LoginPosteIdPageMan extends LoadableComponent<LoginPosteIdPageMan> implements LoginPosteIdPage
{
	UiPage page;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement email;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement password;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement showMePassword;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement rememberMe;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement lostCredential;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement signInButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement registerButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement tabPosteID;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement posteIdImage;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		WaitManager.get().waitMediumTime();
		try
		{
			((HidesKeyboard) page.getDriver()).hideKeyboard();
		}
		catch(Exception err)
		{

		}
		WaitManager.get().waitShortTime();
		page.get();
		
	}

	//		public void clickPosteIdTab() 
	//		{
	//			switch(driverType)
	//			{
	//			case 0://android
	//				this.tabPosteID=driver.findElement(By.xpath(Locators.LoginPosteIdPageMolecola.TABPOSTEID.getAndroidLocator()));
	//				break;
	//			case 1://ios
	//				this.tabPosteID=driver.findElement(By.xpath(Locators.LoginPosteIdPageMolecola.TABPOSTEID.getLocator(driver)));
	//				break;
	//			}
	//			
	//			this.tabPosteID.click();
	//		}
	//		
	public void loginToTheApp(String username, String password) 
	{
		this.email=page.getParticle(Locators.LoginPosteIdPageMolecola.EMAIL).getElement();
		this.password=page.getParticle(Locators.LoginPosteIdPageMolecola.PASSWORD).getElement();
		this.signInButton=page.getParticle(Locators.LoginPosteIdPageMolecola.SIGNINBUTTON).getElement();
		

		this.email.sendKeys(username);

		try
		{
			((HidesKeyboard) page.getDriver()).hideKeyboard();
		}
		catch(Exception err)
		{

		}

		this.password.sendKeys(password);

		try
		{
			((HidesKeyboard) page.getDriver()).hideKeyboard();
		}
		catch(Exception err)
		{

		}

		this.signInButton.click();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}
}
