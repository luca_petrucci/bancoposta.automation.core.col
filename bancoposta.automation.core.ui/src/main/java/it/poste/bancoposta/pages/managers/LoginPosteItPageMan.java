package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import io.appium.java_client.HidesKeyboard;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.LoginPosteItPage;
import it.poste.bancoposta.utils.PageMolecolaImgCreator;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="LoginPosteItPage")
@Android
public class LoginPosteItPageMan extends LoadableComponent<LoginPosteItPageMan> implements LoginPosteItPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement userName;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement password;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement showMePassword;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement rememberMe;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement lostCredential;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement signInButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement registerButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement tabPosteIT;

	private WebElement spidOkButton;

	private WebElement signButton;

	private WebElement noAccountButton;

	private WebElement registerAccountSite;

	private WebElement registerCloseButton;

	@Override
	protected void isLoaded() throws Error {

	}

	@Override
	protected void load() 
	{
		page.get();
//		byte[] b=((TakesScreenshot)page.getDriver()).getScreenshotAs(OutputType.BYTES);
//		PageMolecolaImgCreator.makeImg(b, page, 
//				Locators.LoginPosteItPageMolecola.USERNAME,
//				Locators.LoginPosteItPageMolecola.PASSWORD,
//				Locators.LoginPosteItPageMolecola.REMEMBERME,
//				Locators.LoginPosteItPageMolecola.LOSTCREDENTIAL,
//				Locators.LoginPosteItPageMolecola.SIGNINBUTTON,
//				Locators.LoginPosteItPageMolecola.REGISTERBUTTON,
//				Locators.LoginPosteItPageMolecola.BACKBUTTON);
	}

	public void loginToTheApp(String username, String password) 
	{
		this.userName=page.getParticle(Locators.LoginPosteItPageMolecola.USERNAME).getElement();
		this.password=page.getParticle(Locators.LoginPosteItPageMolecola.PASSWORD).getElement();
		this.signInButton=page.getParticle(Locators.LoginPosteItPageMolecola.SIGNINBUTTON).getElement();		


		this.userName.sendKeys(username);

		try
		{
			((HidesKeyboard) page.getDriver()).hideKeyboard();
		}
		catch(Exception err)
		{

		}

		this.password.sendKeys(password);

		try
		{
			((HidesKeyboard) page.getDriver()).hideKeyboard();
		}
		catch(Exception err)
		{

		}

		this.signInButton.click();

	}

	public void checkErrorPopup() 
	{
		page.getParticle(Locators.LoginPosteItPageMolecola.DIMENTICATOCREDENZIALI).visibilityOfElement(10L);
		page.getParticle(Locators.LoginPosteItPageMolecola.ERRORPOPUPTXT).visibilityOfElement(null);
		page.getParticle(Locators.LoginPosteItPageMolecola.RIPROVAERRORPOPUPBUTTON).visibilityOfElement(null);
		page.getParticle(Locators.LoginPosteItPageMolecola.RECUPERAERRORPOPUPBUTTON).visibilityOfElement(null);

		String content=page.getParticle(Locators.LoginPosteItPageMolecola.ERRORPOPUPTXT).visibilityOfElement(null)
				.getText();

		System.out.println(content);
	}

	public void clickRecuperaPopupErrorLogin() 
	{
		page.getParticle(Locators.LoginPosteItPageMolecola.RECUPERAERRORPOPUPBUTTON).visibilityOfElement(null)
		.click();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

	@Override
	public void clickOnSpidButton() {
		this.spidOkButton = page.getParticle(Locators.LoginPosteItPageMolecola.SPIDOKBUTTON).getElement();
		this.spidOkButton.click();

	}

	@Override
	public void clickOnSignInButton() {
		this.signButton = page.getParticle(Locators.LoginPosteItPageMolecola.SIGNINBUTTON).getElement();
		signButton.click();
	}

	@Override
	public void clickOnNoAccountPoste() {
		this.noAccountButton = page.getParticle(Locators.LoginPosteItPageMolecola.SPIDNOACCOUNTBUTTON).getElement();
		noAccountButton.click();
	}

	@Override
	public void checkRegistrationPage() throws Exception {
		String text;
		text = "Registrati all'account di Poste".toLowerCase().trim();

		this.registerAccountSite = page.getParticle(Locators.LoginPosteItPageMolecola.REGISTERACCOUNTBYSITE).getElement();

		String text2 = registerAccountSite.getText().toLowerCase().trim();

		if (text2.equals(text)) {
			System.out.println("la pagina � corretta");

			this.registerCloseButton = page.getParticle(Locators.LoginPosteItPageMolecola.REGISTERSITECLOSEBUTTON).getElement();
			registerCloseButton.click();
			WaitManager.get().waitShortTime();
		} else {
			throw new Exception("La pagina non � corretta " + text2);

		}
	}
}
