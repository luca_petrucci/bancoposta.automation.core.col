package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.HidesKeyboard;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.LoginWithPosteidPage;
import it.poste.bancoposta.utils.PageMolecolaImgCreator;
import test.automation.core.UIUtils;

import org.codehaus.groovy.transform.tailrec.GotoRecurHereException;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="LoginWithPosteidPage")
@Android
@IOS
public class LoginWithPosteidPageMan extends LoadableComponent<LoginWithPosteidPageMan> implements LoginWithPosteidPage
{
	UiPage page;
	
	private WebElement posteIdCode;
	private WebElement confirm;

	@Override
	protected void isLoaded() throws Error {
	}

	@Override
	protected void load() 
	{
		page.getParticle(Locators.LoginWithPosteidPageMolecola.POSTEIDCODE).visibilityOfElement(60L);
		((HidesKeyboard) page.getDriver()).hideKeyboard();
		WaitManager.get().waitShortTime();
		((HidesKeyboard) page.getDriver()).hideKeyboard();
		page.get();
		
	}



	public void loginToTheApp(String posteid) 
	{
		//	try
		//	{
		//		Thread.sleep(3000);
		//		driver.findElement(By.xpath(Locators.FingerPrintPopUpLocator.CANCELBUTTON.getLocator(driver))).click();
		//	}
		//	catch(Exception err)
		//	{
		//		System.out.println("il popup di fingerprint non � presente");
		//
		//	}
		this.posteIdCode=page.getParticle(Locators.LoginWithPosteidPageMolecola.POSTEIDCODE).getElement();
//		((AppiumDriver)page.getDriver()).hideKeyboard();
		this.confirm=page.getParticle(Locators.LoginWithPosteidPageMolecola.CONFIRM).getElement();
		

		this.posteIdCode.sendKeys(posteid);
		page.getParticle(Locators.LoginWithPosteidPageMolecola.OWNERTEXT).getElement().click();
//		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}
		WaitManager.get().waitShortTime();
		this.confirm.click();
	}







	//		public void loginToTheApp(String posteid)
	//		{
	//			switch(driverType)
	//			{
	//			case 0://android
	//				this.posteIdCode=driver.findElement(By.xpath(Locators.LoginWithPosteidPageMolecola.POSTEIDCODE.getAndroidLocator()));
	//				((AppiumDriver)driver).hideKeyboard();
	//				this.confirm=driver.findElement(By.xpath(Locators.LoginWithPosteidPageMolecola.CONFIRM.getAndroidLocator()));
	//				break;
	//			case 1://ios
	//				this.posteIdCode=driver.findElement(By.xpath(Locators.LoginWithPosteidPageMolecola.POSTEIDCODE.getLocator(driver)));
	//				this.confirm=driver.findElement(By.xpath(Locators.LoginWithPosteidPageMolecola.CONFIRM.getLocator(driver)));
	//				break;
	//			}
	//			
	//			this.posteIdCode.sendKeys(posteid);
	//			try {((AppiumDriver<?>)driver).hideKeyboard();} catch(Exception err) {}
	//			
	//			this.confirm.click();
	//		}

	public void clickNonSeiTu() {
		WebElement goToLoginPage = page.getParticle(Locators.LoginWithPosteidPageMolecola.GOTOLOGINPAGE).getElement();
		goToLoginPage.click();

	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}


}
