package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BollettinoDataBean;
import it.poste.bancoposta.pages.IBollettino;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.MAVFormPage;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="MAVFormPage")
@Android
@IOS
public class MAVFormPageMan extends LoadableComponent<MAVFormPageMan> implements IBollettino,MAVFormPage
{
	UiPage page;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancellButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement bollettinoTypeInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement changeBollettinoType;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement paymentTypeInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement changePaymentType;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement mavCodeInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ccInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement reference;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement dateInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement nameInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement surnameInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement addressInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cityInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement capInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement provinceInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement savePerson;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement confirm;
	private BollettinoDataBean bean;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();

	}

	@Override
	public void checkData(BollettinoDataBean b) {
		// TODO Auto-generated method stub

	}

	@Override
	public void submit(BollettinoDataBean b) {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}
}
