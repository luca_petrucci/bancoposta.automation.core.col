package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.ModifyObjectivePage;

import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="ModifyObjectivePage")
@Android
@IOS
public class ModifyObjectivePageMan extends LoadableComponent<ModifyObjectivePageMan> implements ModifyObjectivePage
{
	UiPage page;
	
	private WebElement eitTextObjective;
	private String categoryField;
	private WebElement category;
	private WebElement categorySelection;
	private WebElement expireObjectiveField;
	private WebElement selectDate;
	private WebElement nextArrow;
	private WebElement okButton;
	private WebElement plusButton;
	private WebElement amount;
	private WebElement importTwo;
	private WebElement amount2;
	private WebElement okButton2;
	private WebElement confirmButton;
	private WebElement closeButton;
	private String dt; 


	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override 
	protected void load() 
	{
		page.get();

	}

	public String modifyObjective(String ObjectiveNameNew,String Category,String importo, String importo2){

		eitTextObjective =page.getParticle(Locators.ModifyObjectiveMolecola.EDITTEXTOBJECTIVE).getElement();

		eitTextObjective.sendKeys(ObjectiveNameNew); 


		categorySelection =page.getParticle(Locators.ModifyObjectiveMolecola.CATEGORYFIELD).getElement();
		categorySelection.click();

		WaitManager.get().waitMediumTime();

		categoryField=page.getParticle(Locators.ModifyObjectiveMolecola.CATEGORYELEMENT).getLocator(new Entry("title",Category));
		category =page.getDriver().findElement(By.xpath(categoryField));
		category.click();

		expireObjectiveField =page.getParticle(Locators.ModifyObjectiveMolecola.DATEMONTH).getElement();
		expireObjectiveField.click();

		WaitManager.get().waitShortTime();
		try {
			selectDate =page.getParticle(Locators.ModifyObjectiveMolecola.SELECTDATE).getElement();
			selectDate.click();


		} catch (Exception e) {
			nextArrow =page.getParticle(Locators.ModifyObjectiveMolecola.NEXTARROW).getElement();
			nextArrow.click();

			WaitManager.get().waitMediumTime();

			selectDate =page.getParticle(Locators.ModifyObjectiveMolecola.SELECTDATE).getElement();
			selectDate.click();

		}



		okButton =page.getParticle(Locators.ModifyObjectiveMolecola.OKBUTTON).getElement();
		okButton.click();

		expireObjectiveField =page.getParticle(Locators.ModifyObjectiveMolecola.DATEMONTH).getElement();
		String ex = expireObjectiveField.getText();

		plusButton =page.getParticle(Locators.ModifyObjectiveMolecola.PLUSBUTTON).getElement();
		plusButton.click();

		amount=page.getParticle(Locators.ModifyObjectiveMolecola.SELECTEDAMOUNT).getElement();
		String a =amount.getText();
		System.out.println(a);

		Assert.assertTrue(a.equals(importo));
		System.out.println(a+" "+importo);


		importTwo =page.getParticle(Locators.ModifyObjectiveMolecola.SELECTEDAMOUNT).getElement();
		importTwo.sendKeys(importo2);


		String a2 =importTwo.getText();
		System.out.println(a2);

		Assert.assertTrue(a2.equals(importo2));
		System.out.println(a2+" "+importo2);

		confirmButton =page.getParticle(Locators.ModifyObjectiveMolecola.MODIFYCONFIRMBUTTON).getElement();
		confirmButton.click();

		WaitManager.get().waitLongTime();

		closeButton =page.getParticle(Locators.ModifyObjectiveMolecola.CLOSEBUTTON).getElement();
		closeButton.click(); 

		return ex;
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}


}
