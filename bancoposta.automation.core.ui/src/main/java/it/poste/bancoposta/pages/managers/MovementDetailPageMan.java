package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.HomePage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.MovementDetailPage;
import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="MovementDetailPage")
@Android
@IOS
public class MovementDetailPageMan extends LoadableComponent<MovementDetailPageMan> implements MovementDetailPage
{

	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement movementHeader;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement importMovement;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement movementDate;
	private List<WebElement> record;
	private String movTitle;
	private String movImport;
	private String movDate;
	
	@Override
	protected void isLoaded() throws Error {
		
	}

	//@Override
	//	protected void load() 
	//	{
	//		switch(this.driverType)
	//		{
	//			case 0://android driver
	//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MovementDetailPage.MOVEMENTHEADER.getAndroidLocator())));
	//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MovementDetailPage.IMPORTMOVEMENT.getAndroidLocator())));
	//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MovementDetailPage.MOVEMENTDATE.getAndroidLocator())));
	//				
	//			
	//			break;
	//			case 1://ios driver
	//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MovementDetailPage.MOVEMENTHEADER.getLocator(driver))));
	//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MovementDetailPage.IMPORTMOVEMENT.getLocator(driver))));
	//			UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.MovementDetailPage.MOVEMENTDATE.getLocator(driver))));
	//			
	//			
	//			break;
	//			case 2://web driver
	//			
	//			break;
	//		}
	//		
	//		
	//	}

	//Recupera il titolo,l'importo e la data dalla pagina Conti e carte



	public void verifyHeader(String header, AppiumDriver<WebElement> driver){

		// Implementa l'errore nello step di Junit

		WaitManager.get().waitMediumTime();
		String xpath=page.getParticle(Locators.MovementDetailPage.MOVEMENTHEADER).getLocator(new Entry("value",header));
		WebElement newHeader = page.getParticle(Locators.MovementDetailPage.MOVEMENTHEADER).getElement(new Entry("value",header));
		String newHead = newHeader.getText().trim();

		org.springframework.util.Assert.isTrue(newHead.equals(header));
		System.out.println(newHead);		


	}

	public void verifyImport(String imp, AppiumDriver<WebElement> driver){

		WebElement newImport = page.getParticle(Locators.MovementDetailPage.IMPORTMOVEMENT).getElement(new Entry("value",imp));
		String newImp = newImport.getText().trim();

		System.out.print(newImp);
		org.springframework.util.Assert.isTrue(newImp.equals(imp));

	}


	public void verifyDate(String date, AppiumDriver<WebElement> driver){

		//Recupero l'anno in corso	
		//	 	int year = Calendar.getInstance().get(Calendar.YEAR);
		//	    String y = Integer.toString(year);

		//		String conc= date.concat(" "+ y);
		WebElement n = page.getParticle(Locators.MovementDetailPage.MOVEMENTDATE).getElement(new Entry("value",date));
		String newDat = n.getText().trim();
		System.out.println(newDat); 



		org.springframework.util.Assert.isTrue(newDat.equals(date));
		System.out.println(newDat+ date );

		try {
			Thread.sleep(1000);
		} catch (Exception e) {

		}
	}


	public HomePage gotoHomePage(String userHeader) 
	{
		WebElement e=page.getParticle(Locators.SettingsPageMolecola.LEFTMENU).getElement();
		e.click();
		WaitManager.get().waitMediumTime();
		e=page.getParticle(Locators.SettingsPageMolecola.LEFTMENU).getElement();
		e.click();
		WaitManager.get().waitMediumTime();
		e=page.getParticle(Locators.HomepageHamburgerMenuMolecola.HOME).getElement();
		e.click();

		HomePage h=(HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", page.getDriver().getClass(), page.getLanguage());
		
		h.setUserHeader(userHeader);
		
		h.checkPage();
		
		return h;
	}

	@Override
	protected void load() 
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}


}
