package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import bean.datatable.SalvadanaioDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.android.nativekey.PressesKey;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.NewLoginPreLoginPage;
import it.poste.bancoposta.utils.PageMolecolaImgCreator;
import test.automation.core.UIUtils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import javax.imageio.ImageIO;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import automation.core.ui.uiobject.UiParticle;

@PageManager(page="NewLoginPreLoginPage")
@Android
public class NewLoginPreLoginPageMan extends LoadableComponent<NewLoginPreLoginPageMan> implements NewLoginPreLoginPage
{

	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web  
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */ 

	private WebElement ownerNameText;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement accediButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	protected WebElement nonSeiTuButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement sitoWebText;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement cameraIcon;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement sitoWebButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement autorizzazioniText;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement bellIcon;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement autorizzazioniButton;

	private WebElement okpopupbutton;

	private WebElement siAccettoButton;

	private WebElement noNonAccetto;

	private WebElement allowPopupButton;

	private WebElement ufficioIcon;

	private WebElement ufficioText;
	
	protected WebElement txtBenvenuto;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	@Override
	protected void isLoaded() throws Error {

	}  

	@Override
	protected void load() 
	{
		page.get();
		//		byte[] b=((TakesScreenshot)page.getDriver()).getScreenshotAs(OutputType.BYTES);
		//		PageMolecolaImgCreator.makeImg(b, page, 
		//				Locators.NewLoginPreLoginPageMolecola.HEADER,
		//				Locators.NewLoginPreLoginPageMolecola.OWNERNAMETEXT,
		//				Locators.NewLoginPreLoginPageMolecola.ACCEDIBUTTON,
		//				Locators.NewLoginPreLoginPageMolecola.NONSEITUBUTTON,
		//				Locators.NewLoginPreLoginPageMolecola.SITOWEBTEXT,
		//				Locators.NewLoginPreLoginPageMolecola.CAMERAICON,
		//				Locators.NewLoginPreLoginPageMolecola.SITOWEBBUTTON,
		//				Locators.NewLoginPreLoginPageMolecola.AUTORIZZAZIONITEXT);
	}

	public void makeAnewLogin(){

		this.header =page.getParticle(Locators.NewLoginPreLoginPageMolecola.HEADER).getElement();
		Assert.assertTrue(header.isDisplayed());

		this.ownerNameText =page.getParticle(Locators.NewLoginPreLoginPageMolecola.OWNERNAMETEXT).getElement();
		String ownerNameText2 = ownerNameText.getText().trim();
		//Assert.assertTrue(ownerNameText2.equals("Ciao Vincenzo")||ownerNameText2.equals("Ciao Gennaro")||ownerNameText2.equals("Ciao Luca")||ownerNameText2.equals("Ciao Salvatore"));

		System.out.println("Owner locator app :" + " " + ownerNameText2);

		this.nonSeiTuButton =page.getParticle(Locators.NewLoginPreLoginPageMolecola.NONSEITUBUTTON).getElement();
		Assert.assertTrue(nonSeiTuButton.isDisplayed());

		this.sitoWebText =page.getParticle(Locators.NewLoginPreLoginPageMolecola.SITOWEBTEXT).getElement();
		Assert.assertTrue(sitoWebText.isDisplayed());

		this.cameraIcon =page.getParticle(Locators.NewLoginPreLoginPageMolecola.CAMERAICON).getElement();
		Assert.assertTrue(cameraIcon.isDisplayed());

//		this.sitoWebButton =page.getParticle(Locators.NewLoginPreLoginPageMolecola.SITOWEBBUTTON).getElement();
//		Assert.assertTrue(sitoWebButton.isDisplayed());

		this.autorizzazioniText =page.getParticle(Locators.NewLoginPreLoginPageMolecola.AUTORIZZAZIONITEXT).getElement();
		Assert.assertTrue(autorizzazioniText.isDisplayed());

		this.bellIcon =page.getParticle(Locators.NewLoginPreLoginPageMolecola.BELLICON).getElement();
		Assert.assertTrue(bellIcon.isDisplayed());
		
		this.ufficioIcon =page.getParticle(Locators.NewLoginPreLoginPageMolecola.CERCAUFFICIOPRENOTAICON).getElement();
		Assert.assertTrue(ufficioIcon.isDisplayed());
		
		this.ufficioText =page.getParticle(Locators.NewLoginPreLoginPageMolecola.CERCAUFFICIOPRENOTATEXT).getElement();
		Assert.assertTrue(ufficioText.isDisplayed());

//		this.autorizzazioniButton =page.getParticle(Locators.NewLoginPreLoginPageMolecola.AUTORIZZAZIONIBUTTON).getElement();
//		Assert.assertTrue(autorizzazioniButton.isDisplayed());

		this.nonSeiTuButton=page.getParticle(Locators.NewLoginPreLoginPageMolecola.NONSEITUBUTTON).getElement();
		Assert.assertTrue(nonSeiTuButton.isDisplayed());
		nonSeiTuButton.click();
		WaitManager.get().waitShortTime(); 

	}



	public void makeAnewLoginAccess(){

		this.header =page.getParticle(Locators.NewLoginPreLoginPageMolecola.HEADER).getElement();
		Assert.assertTrue(header.isDisplayed());

		this.ownerNameText =page.getParticle(Locators.NewLoginPreLoginPageMolecola.OWNERNAMETEXT).getElement();
		String ownerNameText2 = ownerNameText.getText().trim();
		//Assert.assertTrue(ownerNameText2.equals("Ciao Vincenzo")||ownerNameText2.equals("Ciao Gennaro")||ownerNameText2.equals("Ciao Luca"));

		System.out.println("Owner locator app :" + " " + ownerNameText2);

		this.nonSeiTuButton =page.getParticle(Locators.NewLoginPreLoginPageMolecola.NONSEITUBUTTON).getElement();
		Assert.assertTrue(nonSeiTuButton.isDisplayed());

		this.sitoWebText =page.getParticle(Locators.NewLoginPreLoginPageMolecola.SITOWEBTEXT).getElement();
		Assert.assertTrue(sitoWebText.isDisplayed());

		this.cameraIcon =page.getParticle(Locators.NewLoginPreLoginPageMolecola.CAMERAICON).getElement();
		Assert.assertTrue(cameraIcon.isDisplayed());

		this.sitoWebButton =page.getParticle(Locators.NewLoginPreLoginPageMolecola.SITOWEBBUTTON).getElement();
		Assert.assertTrue(sitoWebButton.isDisplayed());

		this.autorizzazioniText =page.getParticle(Locators.NewLoginPreLoginPageMolecola.AUTORIZZAZIONITEXT).getElement();
		Assert.assertTrue(autorizzazioniText.isDisplayed());

		this.bellIcon =page.getParticle(Locators.NewLoginPreLoginPageMolecola.BELLICON).getElement();
		Assert.assertTrue(bellIcon.isDisplayed());

		this.autorizzazioniButton =page.getParticle(Locators.NewLoginPreLoginPageMolecola.AUTORIZZAZIONIBUTTON).getElement();
		Assert.assertTrue(autorizzazioniButton.isDisplayed());

		this.accediButton=page.getParticle(Locators.NewLoginPreLoginPageMolecola.ACCEDIBUTTON).getElement();
		Assert.assertTrue(accediButton.isDisplayed());
		accediButton.click();
		WaitManager.get().waitShortTime();

	}

	public void clickScopriProdotti() 
	{
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 1000);
		page.getParticle(Locators.NewLoginPreLoginPageMolecola.SCOPRI_PRODOTTI).visibilityOfElement(10L).click();
		WaitManager.get().waitMediumTime();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) 
	{
		load();
	}

	@Override
	public void clickOnAccediPrelogin() {
		page.getParticle(Locators.NewLoginPreLoginPageMolecola.ACCEDIPRELOGINPAGE).visibilityOfElement(60L)
		.click();
	
	}

	@Override
	public void clickOnOkPopup() {
		page.getParticle(Locators.NewLoginPreLoginPageMolecola.OKPOPUPBUTTON).visibilityOfElement(10L);
		this.okpopupbutton =page.getParticle(Locators.NewLoginPreLoginPageMolecola.OKPOPUPBUTTON).getElement();
		this.okpopupbutton.click();

		WaitManager.get().waitHighTime();
	}

	@Override
	public void closeNotifichePopup() {
		try {
			page.getParticle(Locators.NewLoginPreLoginPageMolecola.NOTIFICHEPOPUPELEMENT).visibilityOfElement(10L);
			((PressesKey) page.getDriver()).pressKey(new KeyEvent(AndroidKey.BACK));

		} catch (Exception e) {

		}
	}

	@Override
	public void closePersonalizzaEsperienzaInApp() {
		try {
			this.siAccettoButton = page.getParticle(Locators.NewLoginPreLoginPageMolecola.SIACCETTOPOPUPBUTTON).getElement();
			this.siAccettoButton.click();
			this.allowPopupButton = page.getParticle(Locators.NewLoginPreLoginPageMolecola.ALLOWPOPUPBUTTON).getElement();
			this.allowPopupButton.click();	
			//			fatto = true;					
		} catch (Exception r) {

		}
	}
	@Override
	public void closeFingerPrintPopup() {

		try {
			UiParticle p = page.getParticle(Locators.NewLoginPreLoginPageMolecola.NONONACCETTOPOPUPBUTTON);
			System.out.println(p);
			p.visibilityOfElement(5L).click();
			WaitManager.get().waitShortTime();
//			((AndroidDriver<?>) page.getDriver()).pressKey(new KeyEvent(AndroidKey.BACK));
		} catch (Exception r) {
			r.printStackTrace();

		}

	}

	@Override
	public boolean isUpdated() {
		boolean isUp = true;
		
		String btnTxt = page.getParticle(Locators.NewLoginPreLoginPageMolecola.OKPOPUPBUTTON).visibilityOfElement(20L).getText();
		if(btnTxt.toLowerCase().contains("aggiorna")) {
			isUp = false;
		}
		return isUp;
	}

	@Override
	public void clickOnNotUpdate() {
		page.getParticle(Locators.NewLoginPreLoginPageMolecola.NONONACCETTOPOPUPBUTTON).visibilityOfElement(20L).click();
		
	}

	@Override
	public void clickOnConsentiUnaVolta() {
		page.getParticle(Locators.NewLoginPreLoginPageMolecola.POPUPCONSENTIUNAVOLTA).visibilityOfElement(30L).click();
	}

	@Override
	public void clickOnConsenti() {
		page.getParticle(Locators.NewLoginPreLoginPageMolecola.POPUPCONSENTI).visibilityOfElement(20L).click();		
	}

	@Override
	public void clickOnConsentiAccessoContatti() {
		page.getParticle(Locators.NewLoginPreLoginPageMolecola.POPUPOKACCESSOCONTATTI).visibilityOfElement(10L).click();
		
	}

	@Override
	public void closeApplepay() {
		try {
		page.getParticle(Locators.NewLoginPreLoginPageMolecola.ALLOWPOPUPBUTTON).visibilityOfElement(10L).click();
		}catch(Exception e ) {
			
		}
	}

	@Override
	public void checkBenvenuto() {		
		page.getParticle(Locators.NewLoginPreLoginPageMolecola.TXTBENVENUTO).visibilityOfElement(20L);
	}
}
