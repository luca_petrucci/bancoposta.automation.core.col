package it.poste.bancoposta.pages.managers;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.android.nativekey.PressesKey;
import it.poste.bancoposta.pages.Locators;

@PageManager(page="NewLoginPreLoginPage")
@IOS
public class NewLoginPreLoginPageManIOS extends NewLoginPreLoginPageMan 
{
	public void makeAnewLogin()
	{

		this.nonSeiTuButton=page.getParticle(Locators.NewLoginPreLoginPageMolecola.NONSEITUBUTTON).getElement();
		//Assert.//AssertTrue(nonSeiTuButton.isDisplayed());
		nonSeiTuButton.click();
		WaitManager.get().waitShortTime(); 

	}
	
	@Override
	public void clickOnAccediPrelogin() {
		this.txtBenvenuto=page.getParticle(Locators.NewLoginPreLoginPageMolecola.TXTBENVENUTO).visibilityOfElement(15L);
		if(txtBenvenuto.isDisplayed()) {
		page.getParticle(Locators.NewLoginPreLoginPageMolecola.ACCEDIPRELOGINPAGE).visibilityOfElement(15L)
		.click();
		}
		else {
			System.out.println("Step successivo");
		}
	}
	
	@Override
	public void closeNotifichePopup() {
		try {
			page.getParticle(Locators.NewLoginPreLoginPageMolecola.NOTIFICHEPOPUPELEMENT).visibilityOfElement(10L).click();
		} catch (Exception e) {

		}
	}

}
