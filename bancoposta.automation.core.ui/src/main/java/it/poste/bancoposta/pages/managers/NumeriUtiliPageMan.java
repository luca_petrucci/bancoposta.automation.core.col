package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.NumeriUtiliPage;
import test.automation.core.UIUtils;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="NumeriUtiliPage")
@Android
@IOS
public class NumeriUtiliPageMan extends LoadableComponent<NumeriUtiliPageMan> implements NumeriUtiliPage
{
	private static final String NUMERO_ITALIA = "800 00 33 22 (Italia)";
	private static final String NUMERO_ESTERO = "+39 02 82 44 33 33 (Estero)";
	
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement callBancoPostaButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement postamatButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement alertPostepayButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement numeriUtili;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement closeModal;

	
	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public void checkNumeriUtili() 
	{
		this.callBancoPostaButton=page.getParticle(Locators.NumeriUtiliPageMolecola.CALLBANCOPOSTABUTTON).getElement();
		

		this.callBancoPostaButton.click();

		checkModal();

		this.postamatButton=page.getParticle(Locators.NumeriUtiliPageMolecola.POSTAMATBUTTON).getElement();
		

		this.postamatButton.click();

		checkModal();

		this.alertPostepayButton=page.getParticle(Locators.NumeriUtiliPageMolecola.ALERTPOSTEPAYBUTTON).getElement();
		

		this.alertPostepayButton.click();

		checkModal();
	}

	public void checkModal() 
	{
		String numeriUtiliXpath="";

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.closeModal=page.getParticle(Locators.NumeriUtiliPageMolecola.CLOSEMODAL).getElement();
		
		List<WebElement> numbers=page.getParticle(Locators.NumeriUtiliPageMolecola.NUMERIUTILI).getListOfElements();

		Assert.assertTrue("controllo che i numeri mostrati siano due",numbers.size() == 2);
		Assert.assertTrue("controllo presenza numero "+NUMERO_ITALIA,numbers.get(0).getText().trim().equals(NUMERO_ITALIA));
		Assert.assertTrue("controllo presenza numero "+NUMERO_ESTERO,numbers.get(1).getText().trim().equals(NUMERO_ESTERO));

		this.closeModal.click();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}