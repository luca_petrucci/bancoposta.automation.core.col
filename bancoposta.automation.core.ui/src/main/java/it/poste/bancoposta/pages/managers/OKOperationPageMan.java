package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.QuickOperationCreationBean;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OKOperationPage;
import it.poste.bancoposta.pages.SaveToQuickOperationPage;
import it.poste.bancoposta.utils.PageMolecolaImgCreator;
import test.automation.core.UIUtils;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import automation.core.ui.uiobject.UiParticle;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

@PageManager(page="OKOperationPage")
@Android
public class OKOperationPageMan extends LoadableComponent<OKOperationPageMan> implements OKOperationPage
{
	private static final String HEADER = "operazione eseguita!";
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement transactionHeader;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement transactionDescription;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement quickOperationButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement closeButton;
	private WebElement okicon;
	private WebElement description;
	private String descriptionTwo;
	private WebElement header;
	private String headerTwo;
	private WebElement link;
	private WebElement quickButton;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
		
//		WaitManager.get().waitMediumTime();
//		
//		byte[] b=((TakesScreenshot)page.getDriver()).getScreenshotAs(OutputType.BYTES);
//		PageMolecolaImgCreator.makeImg(b, page,
//				Locators.OKOperationPageMolecola.TRANSACTIONHEADER,
//				Locators.OKOperationPageMolecola.TRANSACTIONDESCRIPTION,
//				Locators.OKOperationPageMolecola.CLOSEBUTTON,
//				Locators.OKOperationPageMolecola.QUICKOPERATIONBUTTON
//				);
	}

	public void close() 
	{
		try {
			page.getParticle(Locators.OKOperationPageMolecola.NOTNOW).visibilityOfElement(10L).click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		WaitManager.get().waitMediumTime();
		try {
			page.getParticle(Locators.OKOperationPageMolecola.NOGRAZIE).visibilityOfElement(10L).click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		WaitManager.get().waitMediumTime();
		
		
		this.closeButton=page.getParticle(Locators.OKOperationPageMolecola.CLOSEBUTTON).getElement();


		this.closeButton.click();
		
		try {
			page.getParticle(Locators.OKOperationPageMolecola.NOGRAZIE).visibilityOfElement(10L).click();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void saveQOperation(QuickOperationCreationBean b) {
		// TODO Auto-generated method stub
		WaitManager.get().waitMediumTime();
		try {
			page.getParticle(Locators.OKOperationPageMolecola.NOGRAZIE).visibilityOfElement(10L).click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		this.quickOperationButton=page.getParticle(Locators.OKOperationPageMolecola.QUICKOPERATIONBUTTON).visibilityOfElement(15L);
		
		this.quickOperationButton.click();
		
		try {
			page.getParticle(Locators.OKOperationPageMolecola.NOGRAZIE).visibilityOfElement(10L).click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		WaitManager.get().waitMediumTime();
		
		SaveToQuickOperationPage save=(SaveToQuickOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SaveToQuickOperationPage", page.getDriver().getClass(), page.getLanguage());
		save.saveAsQuickOperation(b);
		
	}
	
	public void saveAsQuickOperation(QuickOperationCreationBean b) throws Exception 
	{
		WaitManager.get().waitMediumTime();
		try {
			page.getParticle(Locators.OKOperationPageMolecola.NOGRAZIE).visibilityOfElement(10L).click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		WaitManager.get().waitMediumTime();
		this.quickOperationButton=page.getParticle(Locators.OKOperationPageMolecola.QUICKOPERATIONBUTTON).visibilityOfElement(15L);
		

		this.quickOperationButton.click();
		
		try {
			page.getParticle(Locators.OKOperationPageMolecola.NOGRAZIE).visibilityOfElement(10L).click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		WaitManager.get().waitHighTime();
		
		//SaveToQuickOperationPage save=new SaveToQuickOperationPage(driver, driverType).getLight();
		SaveToQuickOperationPage save=(SaveToQuickOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SaveToQuickOperationPage", page.getDriver().getClass(), page.getLanguage());
		
		save.getLight();
		
		System.out.println(page.getDriver().getPageSource());
		
		save.saveAsQuickOperationLightMode(b);
		
		
//		if(b.getUseAdb() == null || b.getUseAdb().equals("false"))
//		{
//			SaveToQuickOperationPage save=new SaveToQuickOperationPage(driver, driverType).get();
//
//			save.saveAsQuickOperation(b);
//		}
//		else
//		{
//			SaveToQuickOperationPage save=new SaveToQuickOperationPage(driver, driverType);
//			save.saveAsQuickOperationAdb(b);
//		}

//		SaveToQuickOperationPage save=new SaveToQuickOperationPage(driver, driverType).get();
//
//		save.saveAsQuickOperation(b);

	}
 
	
	public boolean checkSaveOperationButton() 
	{
		WaitManager.get().waitMediumTime();
		
		try {
			this.quickOperationButton=page.getParticle(Locators.OKOperationPageMolecola.QUICKOPERATIONBUTTON).visibilityOfElement(10L);
			
		}catch(Exception e){
				   
			System.out.println("Pulsante per salvare l'operazione veloce correttamente assente nella pagina"); 
			
		}
		return true;
	}
	
	public void checkGirofondoOKPage() 
	{
		this.okicon =page.getParticle(Locators.OKOperationPageMolecola.OKICON).visibilityOfElement(10L);
		Assert.assertTrue(okicon.isDisplayed());
		
		this.header =page.getParticle(Locators.OKOperationPageMolecola.TRANSACTIONHEADER).getElement();
		headerTwo= header.getText().trim();
		
		Properties t=page.getLanguage();
		Assert.assertTrue(headerTwo.equals(t.getProperty("titoloGirofondo")) || headerTwo.equals(t.getProperty("titoloGirofondo2")));
		
		this.description =page.getParticle(Locators.OKOperationPageMolecola.TRANSACTIONDESCRIPTION).getElement();
		descriptionTwo= description.getText().trim();
		//System.out.println(descriptionTwo); 
		
		Assert.assertTrue(descriptionTwo.contains(t.getProperty("copyGirofondo1")));
		Assert.assertTrue(descriptionTwo.contains(t.getProperty("copyGirofondo2")));

		this.link =page.getParticle(Locators.OKOperationPageMolecola.LINK).getElement();
		Assert.assertTrue(link.isDisplayed()); 
		
		this.quickButton =page.getParticle(Locators.OKOperationPageMolecola.QUICKOPERATIONBUTTON).getElement();
		Assert.assertTrue(quickButton.isDisplayed()); 
		
		this.closeButton =page.getParticle(Locators.OKOperationPageMolecola.CLOSEBUTTON).getElement();
		Assert.assertTrue(closeButton.isDisplayed());

		System.out.println("Controllo dei campi terminato correttamente!");
		
	}
	
	public void closePage() 
	{
		WaitManager.get().waitMediumTime();
		try {
			page.getParticle(Locators.OKOperationPageMolecola.NOGRAZIE).visibilityOfElement(10L).click();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		this.closeButton=page.getParticle(Locators.OKOperationPageMolecola.CLOSEBUTTON).getElement();
		

		this.closeButton.click();
		
		try {
			page.getParticle(Locators.OKOperationPageMolecola.NOGRAZIE).visibilityOfElement(10L).click();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void check() 
	{
		load();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	@Override
	public void clickNoGrazie() {
		try {
			page.getParticle(Locators.OKOperationPageMolecola.NOGRAZIE).visibilityOfElement(10L).click();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Override
	public void closeFeedback() {
		
		try {
			UiParticle p = page.getParticle(Locators.OKOperationPageMolecola.NOTNOW);
			System.out.println(p);
			p.visibilityOfElement(5L).click();
			
		} catch (Exception r) {
		  r.printStackTrace();	
}
		
	}
	
	public void checkRicaricaQuestaOKPage() 
	{
		this.okicon =page.getParticle(Locators.OKOperationPageMolecola.OKICON).visibilityOfElement(10L);
		Assert.assertTrue(okicon.isDisplayed());
		
		this.header =page.getParticle(Locators.OKOperationPageMolecola.TRANSACTIONHEADER).getElement();
		headerTwo= header.getText().trim();
		
		Properties t=page.getLanguage();
		Assert.assertTrue("Titolo della Thank you page errata",headerTwo.equals(t.getProperty("titoloRicaricaQuestaPostePay")));
		
		this.description =page.getParticle(Locators.OKOperationPageMolecola.TRANSACTIONDESCRIPTION).getElement();
		descriptionTwo= description.getText().trim();
		//System.out.println(descriptionTwo); 
		
		Assert.assertTrue(descriptionTwo.contains(t.getProperty("copyRicaricaricaQuestaPostePay")));
		
		this.link =page.getParticle(Locators.OKOperationPageMolecola.LINK).getElement();
		Assert.assertTrue(link.isDisplayed()); 
		
		this.quickButton =page.getParticle(Locators.OKOperationPageMolecola.QUICKOPERATIONBUTTON).getElement();
		Assert.assertTrue(quickButton.isDisplayed()); 
		
		this.closeButton =page.getParticle(Locators.OKOperationPageMolecola.CLOSEBUTTON).getElement();
		Assert.assertTrue(closeButton.isDisplayed());
		System.out.println("Controllo dei campi terminato correttamente!");
		
	}

	


}
