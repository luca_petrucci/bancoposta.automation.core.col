package it.poste.bancoposta.pages.managers;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import it.poste.bancoposta.pages.Locators;
@PageManager(page="OKOperationPage")
@IOS
public class OKOperationPageManIOS extends OKOperationPageMan {
	
	private WebElement okicon;
	private WebElement description;
	private String descriptionTwo;
	private WebElement header;
	private String headerTwo;
	private WebElement link;
	private WebElement closeButton;
	private WebElement quickButton;
	
	public void checkGirofondoOKPage() 
	{
		try {
			page.getParticle(Locators.OKOperationPageMolecola.NOTNOW).getElement().click();
		}catch(Exception e) {
			
		}
		this.okicon =page.getParticle(Locators.OKOperationPageMolecola.OKICON).visibilityOfElement(10L);
		Assert.assertTrue(okicon.isEnabled());
		
		this.header =page.getParticle(Locators.OKOperationPageMolecola.TRANSACTIONHEADER).getElement();
		headerTwo= header.getText().trim();

		Properties t=page.getLanguage();
		System.out.println(headerTwo);
		String erHeader =t.getProperty("titoloGirofondo").trim();
		System.out.println(erHeader);
		String girofondoCarico="presa in carico!".trim();
		if(headerTwo.contains(girofondoCarico))
		{
			Assert.assertTrue("AR: "+headerTwo+ " ER: "+girofondoCarico, headerTwo.contains(girofondoCarico));
		}
		else {
			Assert.assertTrue("AR: "+headerTwo+ " ER: "+erHeader, headerTwo.contains(erHeader));
		}
		this.description =page.getParticle(Locators.OKOperationPageMolecola.TRANSACTIONDESCRIPTION).getElement();
		descriptionTwo= description.getText().trim();

		System.out.println(descriptionTwo);
		String erDescription1=t.getProperty("copyGirofondo1").trim();
		String erDescription2=t.getProperty("copyGirofondo2").trim();
		System.out.println("Description 1: "+erDescription1);
		System.out.println("Description 2: "+erDescription2);

//		Assert.assertTrue("AR: "+ descriptionTwo+ " ER: "+erDescription1,descriptionTwo.contains(erDescription1));
//		Assert.assertTrue("AR: "+ descriptionTwo+ " ER: "+erDescription2,descriptionTwo.contains(erDescription2));

		Assert.assertTrue("Descrizione vuota "+ descriptionTwo!=null);
		
		this.link =page.getParticle(Locators.OKOperationPageMolecola.LINK).getElement();
		Assert.assertTrue(link.isEnabled()); 
		
		this.quickButton =page.getParticle(Locators.OKOperationPageMolecola.QUICKOPERATIONBUTTON).getElement();
		Assert.assertTrue(quickButton.isEnabled()); 
		
		this.closeButton =page.getParticle(Locators.OKOperationPageMolecola.CLOSEBUTTON).getElement();
		Assert.assertTrue(closeButton.isEnabled());

		System.out.println("Controllo dei campi terminato correttamente!");
		
	}

}
