package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OnboardingChoosePage;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="OnboardingChoosePage")
@Android
@IOS
public class OnboardingChoosePageMan extends LoadableComponent<OnboardingChoosePageMan> implements OnboardingChoosePage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancelButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement title;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement enableDiv;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement disableDiv;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement enableDivTitle;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement enableDivButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement disableDivTitle;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement disableDIvButton;


	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
