package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OnboardingDisableModal;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="OnboardingDisableModal")
@Android
@IOS
public class OnboardingDisableModalMan extends LoadableComponent<OnboardingDisableModalMan> implements OnboardingDisableModal
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancelButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement continueButton;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
		}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
