package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.PagaConSubMenuBuoniELibretti;
import it.poste.bancoposta.pages.RicaricaLaTuaPostepayChooseOperationPage;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="PagaConSubMenuBuoniELibretti")
@Android
@IOS
public class PagaConSubMenuBuoniELibrettiMan extends LoadableComponent<PagaConSubMenuBuoniELibrettiMan> implements PagaConSubMenuBuoniELibretti
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement xCloseButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement sottoscrizioneBuoniButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement attivaOffertaSuperSmartButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement girofondoBuoniELibrettiButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ricaricaLaTuaPostepayButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement riceviBonificiButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();

	} 

	public RicaricaLaTuaPostepayChooseOperationPage gotoRicaricaLaTuaPostepay() 
	{
		this.ricaricaLaTuaPostepayButton=page.getParticle(Locators.PagaConSubMenuBuoniELibrettiMolecola.RICARICALATUAPOSTEPAYBUTTON).getElement();
		

		this.ricaricaLaTuaPostepayButton.click();

		RicaricaLaTuaPostepayChooseOperationPage p= (RicaricaLaTuaPostepayChooseOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaLaTuaPostepayChooseOperationPage", page.getDriver().getClass(), page.getLanguage());
		p.checkPage();

		return p;
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}