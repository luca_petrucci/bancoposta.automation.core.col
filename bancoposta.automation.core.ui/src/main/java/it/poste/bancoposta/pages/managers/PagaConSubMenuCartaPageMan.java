package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.BollettinoChooseOperationPage;
import it.poste.bancoposta.pages.BolloAutoPage;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.PagaConSubMenuCartaPage;
import it.poste.bancoposta.pages.RicaricaPostepayPage;
import it.poste.bancoposta.pages.SendBonificoPage;
import it.poste.bancoposta.pages.SendPostagiroPage;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="PagaConSubMenuCartaPage")
@Android
@IOS
public class PagaConSubMenuCartaPageMan extends LoadableComponent<PagaConSubMenuCartaPageMan> implements PagaConSubMenuCartaPage 
{
	UiPage page;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement closeButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */	
	private WebElement ricaricaAltraPostePayButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ricaricaTelefonicaButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */	

	private WebElement bonificoButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement postagiroButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement bollettinoButton;
	private WebElement bolloAutoButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public BollettinoChooseOperationPage gotoBollettino() 
	{
		this.bollettinoButton=page.getParticle(Locators.PagaConSubMenuCartaPageMolecola.BOLLETTINOBUTTON).getElement();
		
		WaitManager.get().waitMediumTime();

		BollettinoChooseOperationPage p=(BollettinoChooseOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BollettinoChooseOperationPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();

		return p;
	}
	
	
	
	
	
	public BollettinoChooseOperationPage gotoBollettinoCartaPage() 
	{
		return this.gotoBollettino();
	}

	public void clickClose() 
	{
		this.closeButton=page.getParticle(Locators.PagaConSubMenuCartaPageMolecola.CLOSEBUTTON).getElement();
		

		this.closeButton.click();
	}
	
	public SendBonificoPage gotoBonifico() 
	{
		this.bonificoButton=page.getParticle(Locators.PagaConSubMenuCartaPageMolecola.BONIFICOBUTTON).getElement();
		

		this.bonificoButton.click();
		
		SendBonificoPage  p=(SendBonificoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SendBonificoPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();

		return p;
	}

	public SendPostagiroPage gotoPostagiro() 
	{
		this.postagiroButton=page.getParticle(Locators.PagaConSubMenuCartaPageMolecola.POSTAGIROBUTTON).getElement();
		

		this.postagiroButton.click();
		
		SendPostagiroPage p=(SendPostagiroPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SendPostagiroPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();

		return p;
	}

	public RicaricaPostepayPage goToRicaricaAltraPostepay() {
		
		this.ricaricaAltraPostePayButton=page.getParticle(Locators.PagaConSubMenuCartaPageMolecola.RICARICAPOSTEPAYBUTTON).getElement();
		

		this.ricaricaAltraPostePayButton.click();
		
		RicaricaPostepayPage p=(RicaricaPostepayPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaPostepayPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();

		return p;
	}
	
	public BolloAutoPage goToBolloAuto() 
	{
		this.bolloAutoButton=page.getParticle(Locators.PagaConSubMenuCartaPageMolecola.BOLLOAUTOBUTTON).getElement();
		

		this.bolloAutoButton.click();
		
		BolloAutoPage  p=(BolloAutoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BolloAutoPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();

		return p;
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}