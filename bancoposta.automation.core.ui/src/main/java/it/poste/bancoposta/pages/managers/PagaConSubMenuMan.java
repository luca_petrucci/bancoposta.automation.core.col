package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BollettinoChooseOperationPage;
import it.poste.bancoposta.pages.BolloAutoPage;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.GirofondoFormPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.PagaConSubMenu;
import it.poste.bancoposta.pages.RicaricaPostepayPage;
import it.poste.bancoposta.pages.SelectBonificoTypePage;
import it.poste.bancoposta.pages.SendBonificoPage;
import it.poste.bancoposta.pages.SendPostagiroPage;
import it.poste.bancoposta.utils.PageMolecolaImgCreator;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="PagaConSubMenu")
@Android
@IOS
public class PagaConSubMenuMan extends LoadableComponent<PagaConSubMenuMan> implements PagaConSubMenu
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement closeButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ricaricaPostePayButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ricaricaTelefonicaButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement bonificoButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement postagiroButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement bollettinoButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement girofondoButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement buonoButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement riceviButton;
	private WebElement bolloAutoButton;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
		
		WaitManager.get().waitMediumTime();
		
//		byte[] b=((TakesScreenshot)page.getDriver()).getScreenshotAs(OutputType.BYTES);
//		PageMolecolaImgCreator.makeImg(b, page,
//				Locators.PagaConSubMenuMolecola.CLOSEBUTTON,
//				Locators.PagaConSubMenuMolecola.RICARICAPOSTEPAYBUTTON,
//				Locators.PagaConSubMenuMolecola.RICARICATELEFONICABUTTON,
//				Locators.PagaConSubMenuMolecola.BONIFICOBUTTON,
//				Locators.PagaConSubMenuMolecola.POSTAGIROBUTTON,
//				Locators.PagaConSubMenuMolecola.BOLLETTINOBUTTON,
//				Locators.PagaConSubMenuMolecola.GIROFONDOBUTTON
//				);
	}

	public BollettinoChooseOperationPage gotoBollettino()
	
	{
		this.bollettinoButton=page.getParticle(Locators.PagaConSubMenuMolecola.BOLLETTINOBUTTON).getElement();
		

		WaitManager.get().waitMediumTime();
		
		this.bollettinoButton.click();

		WaitManager.get().waitMediumTime(); 
		
		BollettinoChooseOperationPage p=(BollettinoChooseOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BollettinoChooseOperationPage", page.getDriver().getClass(), page.getLanguage());;

				WaitManager.get().waitMediumTime(); 
		p.checkPage();

		return p;
	}

	public GirofondoFormPage gotoGirofondo() 
	{
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 500);
		
		WaitManager.get().waitShortTime();
		
		this.girofondoButton=page.getParticle(Locators.PagaConSubMenuMolecola.GIROFONDOBUTTON).getElement();
		

		this.girofondoButton.click();

		GirofondoFormPage p=(GirofondoFormPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("GirofondoFormPage", page.getDriver().getClass(), page.getLanguage());;

		
		p.checkPage();

		return p;
	}

	public void clickClose() 
	{
		this.closeButton=page.getParticle(Locators.PagaConSubMenuMolecola.CLOSEBUTTON).getElement();
		

		this.closeButton.click();
	}

	public SendBonificoPage gotoBonifico() 
	{
		this.bonificoButton=page.getParticle(Locators.PagaConSubMenuMolecola.BONIFICOBUTTON).getElement();
		
		this.bonificoButton.click();
		WaitManager.get().waitMediumTime();
		
		SelectBonificoTypePage sel=(SelectBonificoTypePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SelectBonificoTypePage", page.getDriver().getClass(), page.getLanguage());
		
//		sel.checkPage();
		WaitManager.get().waitMediumTime();
		sel.clickOnBonificoSepa();
		WaitManager.get().waitMediumTime();
		
		SendBonificoPage p=(SendBonificoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SendBonificoPage", page.getDriver().getClass(), page.getLanguage());;


		p.checkPage();

		return p;
	}

	public SendPostagiroPage gotoPostagiro() 
	{
		this.postagiroButton=page.getParticle(Locators.PagaConSubMenuMolecola.POSTAGIROBUTTON).getElement();
		

		this.postagiroButton.click();
		
		WaitManager.get().waitMediumTime(); 
		
		SendPostagiroPage p=(SendPostagiroPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SendPostagiroPage", page.getDriver().getClass(), page.getLanguage());;

				WaitManager.get().waitMediumTime(); 
				
		p.checkPage();

		return p;
	}

	public RicaricaPostepayPage goToRicaricaPostepay() 
	{
		this.ricaricaPostePayButton=page.getParticle(Locators.PagaConSubMenuMolecola.RICARICAPOSTEPAYBUTTON).getElement();
		this.ricaricaPostePayButton.click();
		WaitManager.get().waitMediumTime(); 
		
		RicaricaPostepayPage p=(RicaricaPostepayPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaPostepayPage", page.getDriver().getClass(), page.getLanguage());;

		p.checkPage();

		return p;

	}

	public BolloAutoPage goToBolloAuto() 
	{
		this.bolloAutoButton=page.getParticle(Locators.PagaConSubMenuCartaPageMolecola.BOLLOAUTOBUTTON).getElement();
		

		this.bolloAutoButton.click();
		
		BolloAutoPage  p=(BolloAutoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BolloAutoPage", page.getDriver().getClass(), page.getLanguage());;


		p.checkPage();
		
		return p;
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	@Override
	public void clickOnRicaricaTelefonica() {
		WebElement ricaricaTelefonicaButton = page.getParticle(Locators.PagaConSubMenuMolecola.RICARICATELEFONICABUTTON).getElement();
		ricaricaTelefonicaButton.click();
		WaitManager.get().waitMediumTime(); 
	}
}

