package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.PagoPAPage;
import test.automation.core.UIUtils;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="PagoPAPage")
@Android
@IOS
public class PagoPAPageMan extends LoadableComponent<PagoPAPageMan> implements PagoPAPage
{
	UiPage page;
	
	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public void clickCompilaManualmente() 
	{
		WaitManager.get().waitMediumTime();
		System.out.println(page.getDriver().getPageSource());
		page.getParticle(Locators.PagoPAMolecola.COMPILA_MANUALMENTE).visibilityOfElement(10L).click();
	}

	public void checkPage() 
	{
		load();
	}

	public void clickAnnulla() 
	{
		page.getParticle(Locators.PagoPAMolecola.CLOSE_BUTTON).visibilityOfElement(10L).click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	

}
