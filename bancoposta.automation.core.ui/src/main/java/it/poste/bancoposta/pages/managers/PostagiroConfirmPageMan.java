package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.PosteIDSubAccessPage;
import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BonificoDataBean;
import bean.datatable.PaymentCreationBean;
import bean.datatable.QuickOperationCreationBean;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.BonificoConfirmationPages;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.Locators.PostagiroConfirmPageMolecola;
import it.poste.bancoposta.pages.OKOperationPage;
import it.poste.bancoposta.pages.PostagiroConfirmPage;
import it.poste.bancoposta.pages.QuickOperationEditable;
import it.poste.bancoposta.pages.SendPostagiroPage;
import it.poste.bancoposta.utils.PageMolecolaImgCreator;
import it.poste.bancoposta.utils.SoftAssertion;
import test.automation.core.UIUtils;

import java.util.Locale;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="PostagiroConfirmPage")
@Android
@IOS
public class PostagiroConfirmPageMan extends LoadableComponent<PostagiroConfirmPageMan> implements BonificoConfirmationPages, QuickOperationEditable,PostagiroConfirmPage
{
	private static final Object TITLE = "è tutto corretto?";
	private static final Object DESCRIPTION = "verifica i dati prima di procedere con il pagamento.";
	private static final Object HEADER = "riepilogo postagiro";
	
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement title;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement payWithInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ibanInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ownerInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement commissione;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement casuale;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement confirm;
	private BonificoDataBean bean;
	private WebElement modify;
	private WebElement modalOK;
	private PaymentCreationBean bean2;

	

	@Override
	protected void isLoaded() throws Error 
	{
		
	}

	@Override
	protected void load() 
	{
		WaitManager.get().waitMediumTime();
		page.readPageSource();
		System.out.println(page.getPageSource());
		page.get();
		
		this.title=page.getParticle(Locators.PostagiroConfirmPageMolecola.TITLE).getElement();
//		this.description=page.getParticle(Locators.PostagiroConfirmPageMolecola.DESCRIPTION).getElement();
//		this.header=page.getParticle(Locators.PostagiroConfirmPageMolecola.HEADER).getElement();
		
		Properties t=page.getLanguage();

		String txt=title.getText().trim();

		//Assert.assertTrue("controllo header È tutto corretto?",txt.equals(TITLE));
		
		SoftAssertion.get().getAssertion().assertThat(txt)
		.withFailMessage("Controllo header � tutto corretto Atteso:"+t.getProperty("eTuttoCorretto")+" - Attuale:"+txt)
		.isEqualTo(t.getProperty("eTuttoCorretto"));

//		txt=description.getText().trim();

		//TODO:RIPRISTINARE PER ANDROID
//		SoftAssertion.get().getAssertion().assertThat(txt)
//		.withFailMessage("Controllo header  Verifica i dati prima di procedere con il pagamento. AR:"+t.getProperty("verificaIdatiPrimaDiProcedere")+" - Attuale:"+txt)
//		.isEqualTo(t.getProperty("verificaIdatiPrimaDiProcedere"));
		
//		txt=this.header.getText().trim();
		
//		SoftAssertion.get().getAssertion().assertThat(txt)
//		.withFailMessage("controllo header riepilogo postagiro AR:"+t.getProperty("riepilogoPostagiro"))
//		.isEqualTo(t.getProperty("riepilogoPostagiro"));
		
		//Assert.assertTrue("controllo header Verifica i dati prima di procedere con il pagamento.",txt.equals(DESCRIPTION));

		//txt=header.getText().trim().toLowerCase();

		//Assert.assertTrue("controllo header riepilogo postagiro",txt.equals(HEADER));
		
//		WaitManager.get().waitMediumTime();
//		
//		byte[] b=((TakesScreenshot)page.getDriver()).getScreenshotAs(OutputType.BYTES);
//		PageMolecolaImgCreator.makeImg(b, page,
//				Locators.PostagiroConfirmPageMolecola.HEADER,
//				PostagiroConfirmPageMolecola.BACKBUTTON,
//				PostagiroConfirmPageMolecola.TITLE,
//				PostagiroConfirmPageMolecola.DESCRIPTION,
//				PostagiroConfirmPageMolecola.PAYWITHINFO,
//				PostagiroConfirmPageMolecola.IBANINFO,
//				PostagiroConfirmPageMolecola.OWNERINFO,
//				PostagiroConfirmPageMolecola.AMOUNT,
//				PostagiroConfirmPageMolecola.COMMISSIONE,
//				PostagiroConfirmPageMolecola.CONFIRM,
//				PostagiroConfirmPageMolecola.CAUSALE
//				);

	}

	@Override
	public void checkData(BonificoDataBean b) 
	{
		check(b.getPayWith(),b.getIban(),b.getOwner(),b.getDescription(),b.getAmount());
	}

	public void check(String payWith,String iban,String owner,String description,String am)
	{
		this.payWithInfo=page.getParticle(Locators.PostagiroConfirmPageMolecola.PAYWITHINFO).getElement();
		this.ibanInfo=page.getParticle(Locators.PostagiroConfirmPageMolecola.IBANINFO).getElement();
		this.ownerInfo=page.getParticle(Locators.PostagiroConfirmPageMolecola.OWNERINFO).getElement();
		//this.countryOwnerInfo=page.getParticle(Locators.PostagiroConfirmPageMolecola.COUNTRYOWNERINFO.getAndroidLocator()));
		this.amount=page.getParticle(Locators.PostagiroConfirmPageMolecola.AMOUNT).getElement();
		this.commissione=page.getParticle(Locators.PostagiroConfirmPageMolecola.COMMISSIONE).getElement();
		this.casuale=page.getParticle(Locators.PostagiroConfirmPageMolecola.CAUSALE).getElement();
		this.confirm=page.getParticle(Locators.PostagiroConfirmPageMolecola.CONFIRM).getElement();
		

		//controllo paga con
		String txt=this.payWithInfo.getText().trim().toLowerCase();
		String fourDigits = txt.substring(txt.length() - 4);
		String atteso = payWith.toLowerCase().replace(";", " ");
		String atteso4 = atteso.substring(atteso.length() - 4);
		Assert.assertTrue("controllo paga con;attuale:"+fourDigits+", atteso:"+atteso4,fourDigits.equals(atteso4));

		//controllo c/c iban
		txt=this.ibanInfo.getText().trim();

		Assert.assertTrue("controllo iban;attuale:"+txt+", atteso:"+iban,txt.equals(iban));

		//controllo intestato a
		txt=this.ownerInfo.getText().trim().toLowerCase();

		Assert.assertTrue("controllo intestato a;attuale:"+txt+", atteso:"+owner.toLowerCase(),txt.equals(owner.toLowerCase()));

		//controllo importo
		double amount=Utility.parseCurrency(this.amount.getText().trim(), Locale.ITALY);
		double tocheck=Double.parseDouble(am);

		Assert.assertTrue("controllo importo;attuale:"+amount+", atteso:"+tocheck,amount==tocheck);

		double commissioni=Utility.parseCurrency(this.commissione.getText().trim(), Locale.ITALY);

		txt=this.casuale.getText().trim().toLowerCase();

		Assert.assertTrue("controllo casuale;attuale:"+txt+", atteso:"+description.toLowerCase(),txt.equals(description.toLowerCase()));

		//controlla paga con
		String paga="paga \u20AC "+(Utility.toCurrency(amount+commissioni, Locale.ITALY));

		//TODO:RIPRISTINARE PER ANDROID
		//Assert.assertTrue("controllo paga;attuale:"+this.confirm.getText().trim().toLowerCase()+", atteso:"+paga,this.confirm.getText().trim().toLowerCase().contains(paga));

	}

	@Override
	public OKOperationPage submit(BonificoDataBean b) 
	{
		return submit(b.getPosteid());
	}

	public OKOperationPage submit(String posteid)
	{
		this.confirm=page.getParticle(Locators.PostagiroConfirmPageMolecola.CONFIRM).getElement();
		

		this.confirm.click();

		InsertPosteIDPage posteId=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
		
		posteId.checkPage();

		return posteId.insertPosteID(posteid);
	}

	@Override
	public void isEditable(QuickOperationCreationBean b) 
	{
		//clicco su modifica
		this.modify=page.getParticle(Locators.PostagiroConfirmPageMolecola.MODIFY).getElement();
		

		this.modify.click();

		SendPostagiroPage p=(SendPostagiroPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SendPostagiroPage", page.getDriver().getClass(), page.getLanguage());
		
		p.checkPage();

		if(bean != null)
			p.isEditable(bean);
		else
			p.isEditable(bean2);
	}

	@Override
	public OKOperationPage submit(QuickOperationCreationBean b, String campo, String value) 
	{
		if(campo != null)
		{
			//clicco su modifica
			this.modify=page.getParticle(Locators.PostagiroConfirmPageMolecola.MODIFY).getElement();
			

			this.modify.click();

			SendPostagiroPage p=(SendPostagiroPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("SendPostagiroPage", page.getDriver().getClass(), page.getLanguage());
			
			p.checkPage();

			if(bean != null)
				return p.eseguiPostagiro(bean,campo,value);
			else
				return p.eseguiPostagiro(bean2,campo,value);
		}
		else
		{
			//clicco su paga
			this.confirm=page.getParticle(Locators.PostagiroConfirmPageMolecola.CONFIRM).getElement();
			

			this.confirm.click();
			
			
			InsertPosteIDPage posteId=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
			
			posteId.checkPage();

			return posteId.insertPosteID(bean.getPosteid());
		}

	}

	@Override
	public void clickBack() 
	{
		page.getParticle(Locators.PostagiroConfirmPageMolecola.BACKBUTTON).visibilityOfElement(10L).click();
	}

	public void clickOKModal() 
	{
		try
		{
			this.modalOK=page.getParticle(Locators.PostagiroConfirmPageMolecola.MODALOK).visibilityOfElement(30L);
			this.modalOK.click();
			WaitManager.get().waitMediumTime();
		}
		catch(Exception err)
		{

		}
	}

	public void checkData(PaymentCreationBean b) 
	{
		check(b.getPayWith(),b.getIban(),b.getOwner(),b.getDescription(),b.getAmount());
	}

	public OKOperationPage submit(PaymentCreationBean bean) 
	{
		return submit(bean.getPosteid());
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	@Override
	public void setBean(QuickOperationCreationBean quickOperationCreationBean) {
		
	}

	@Override
	public void setBean(BonificoDataBean bonificoDataBean) {
		this.bean=bonificoDataBean;
	}

	@Override
	public void setBean(PaymentCreationBean paymentCreationBean) {
		this.bean2=paymentCreationBean;
	}

	@Override
	public void clikOnOperazioneEseguitaChiudi() {
		WebElement chiudiButton = page.getParticle(Locators.PostagiroConfirmPageMolecola.OPERAZIONEESEGUITACHIUDIBUTTON).getElement();
		chiudiButton.click();
	}

	
}
