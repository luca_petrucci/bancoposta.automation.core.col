package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import io.appium.java_client.HidesKeyboard;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.ChattaConNoiBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.PosteChatPage;
import test.automation.core.UIUtils;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="PosteChatPage")
@Android
@IOS
public class PosteChatPageMan extends LoadableComponent<PosteChatPageMan> implements PosteChatPage 
{
	UiPage page;
	
	private WebElement reserveCallButton;
	private WebElement endButton;
	private WebElement escButton;


	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() {
		// TODO Auto-generated method stub
		page.get();
	}



	public PosteChatPage insertPhoneNumber(ChattaConNoiBean b) 
	{
		WaitManager.get().waitMediumTime();
		
		reserveCallButton=page.getParticle(Locators.PosteChatPageMolecola.PHONENUMBER).getElement();
		
		System.out.println(b.getPhoneNumber());
		reserveCallButton.click();
		reserveCallButton.sendKeys(b.getPhoneNumber());
		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}
		return null;

	}



	public void clickOnRseserveCallButton () {
		reserveCallButton=page.getParticle(Locators.PosteChatPageMolecola.RESERVECALLBUTTON).getElement();
		
		reserveCallButton.click();

	}

	public void clickOnEndButton() {
		
		endButton=page.getParticle(Locators.PosteChatPageMolecola.ENDBUTTON).getElement();
		
		endButton.click();
		
		WaitManager.get().waitMediumTime();
		
		escButton=page.getParticle(Locators.PosteChatPageMolecola.POPUPESCBUTTON).getElement();
		

		escButton.click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}


	//	public void clickOnEsciButton () {
	//		
	//		
	//		switch(this.driverType)
	//		{
	//		case 0://android
	//			WebElement escButton=driver.findElement(By.xpath(Locators.PosteChatPageMolecola.ESCBUTTON.getAndroidLocator()));
	//			break;
	//		case 1://ios
	//			escButton=driver.findElement(By.xpath(Locators.PosteChatPageMolecola.ESCBUTTON.getLocator(driver)));
	//			break;
	//		}
	//		
	//		this.escButton.click();
	//		
	//	}


}


