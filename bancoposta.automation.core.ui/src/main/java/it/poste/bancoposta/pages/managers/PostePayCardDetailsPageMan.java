package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CheckTransactionBean;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.PostePayCardDetailsPage;
import test.automation.core.UIUtils;
import utils.Entry;
import utils.ObjectFinderLight;

import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


@PageManager(page="PostePayCardDetailsPage")
@Android
@IOS
public class PostePayCardDetailsPageMan extends LoadableComponent<PostePayCardDetailsPageMan> implements PostePayCardDetailsPage
{
	UiPage page;


	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		/*TOP("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_title' and @text='Dettaglio carta']","",""),
		CLOSE_BUTTON("//*[@resource-id='posteitaliane.posteapp.appbpol:id/ib_left']","",""),
		CARD_DIV("//*[@resource-id='posteitaliane.posteapp.appbpol:id/card']","",""),
		CARD_TYPE_IMG("//*[@resource-id='posteitaliane.posteapp.appbpol:id/iv_tipo_postepay']","",""),
		CARD_CIRC_IMG("//*[@resource-id='posteitaliane.posteapp.appbpol:id/iv_circuito']","",""),
		CARD_PAN("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_pan']","",""),
		AGGIORNATO_AL("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_updated_status']","",""),
		SALDO_DISPONIBILE("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_balance_available']","",""),
		SALDO_CONTABILE("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_balance_counting']","",""),
		COORDINATE_BANC_DIV("//*[@resource-id='posteitaliane.posteapp.appbpol:id/container_iban']","",""),
		COORDINATE_IBAN("//*@resource-id='posteitaliane.posteapp.appbpol:id/tv_iban']","",""),
		COORDINATE_BANC_INVIA("//*[@resource-id='posteitaliane.posteapp.appbpol:id/iv_share']","",""),
		SALVADANAIO_DIV("//*[@resource-id='posteitaliane.posteapp.appbpol:id/container_salvadanaio']","",""),
		SALVADANAIO_VERSAMENTI_ATTIVI("//*[@resource-id='posteitaliane.posteapp.appbpol:id/container_salvadanaio']//*[@resource-id='posteitaliane.posteapp.appbpol:id/customTextView40' and @text='Versamenti automatici attivi']","",""),
		SALVADANAIO_VERSAMENTI_ATTIVI_NUM("//*[@resource-id='posteitaliane.posteapp.appbpol:id/container_salvadanaio']//*[@resource-id='posteitaliane.posteapp.appbpol:id/customTextView41' and @text != '']","",""),
		SALVADANAIO_VERSAMENTI_ATTIVI_LINK("//*[@resource-id='posteitaliane.posteapp.appbpol:id/container_salvadanaio']//*[@resource-id='posteitaliane.posteapp.appbpol:id/imageView24']","",""),
		SCONTIPOSTE_DIV("//*[@resource-id='posteitaliane.posteapp.appbpol:id/container_sconti']","",""),
		SCONTIPOSTE_SCONTI_ACCUMULATI("//*[@resource-id='posteitaliane.posteapp.appbpol:id/container_sconti']//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_sconti_scopri']","",""),
		SCONTIPOSTE_SCONTI_LINK("//*[@resource-id='posteitaliane.posteapp.appbpol:id/container_sconti']//*[@resource-id='posteitaliane.posteapp.appbpol:id/iv_sconti_arrow']","",""),
		*/
		WaitManager.get().waitLongTime();
		String pageSource=page.getDriver().getPageSource();
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.TOP).getLocator()),"TOP"+"non presente");
		System.out.println("TOP "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.CLOSE_BUTTON).getLocator()),"CLOSE_BUTTON"+"non presente");
		System.out.println("CLOSE_BUTTON "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.CARD_DIV).getLocator()),"CARD_DIV"+"non presente");
		System.out.println("CARD_DIV "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.CARD_TYPE_IMG).getLocator()),"CARD_TYPE_IMG"+"non presente");
		System.out.println("CARD_TYPE_IMG "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.CARD_CIRC_IMG).getLocator()),"CARD_CIRC_IMG"+"non presente");
		System.out.println("CARD_CIRC_IMG "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.CARD_PAN).getLocator()),"CARD_PAN"+"non presente");
		System.out.println("CARD_PAN "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.AGGIORNATO_AL).getLocator()),"AGGIORNATO_AL"+"non presente");
		System.out.println("AGGIORNATO_AL "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.SALDO_DISPONIBILE).getLocator()),"SALDO_DISPONIBILE"+"non presente");
		System.out.println("SALDO_DISPONIBILE "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.SALDO_CONTABILE).getLocator()),"SALDO_CONTABILE"+"non presente");
		System.out.println("SALDO_CONTABILE "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.COORDINATE_BANC_DIV).getLocator()),"COORDINATE_BANC_DIV"+"non presente");
		System.out.println("COORDINATE_BANC_DIV "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.COORDINATE_IBAN).getLocator()),"COORDINATE_IBAN"+"non presente");
		System.out.println("COORDINATE_IBAN "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.COORDINATE_BANC_INVIA).getLocator()),"COORDINATE_BANC_INVIA"+"non presente");
		System.out.println("COORDINATE_BANC_INVIA "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.SALVADANAIO_DIV).getLocator()),"SALVADANAIO_DIV"+"non presente");
		System.out.println("SALVADANAIO_DIV "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.SALVADANAIO_VERSAMENTI_ATTIVI).getLocator()),"SALVADANAIO_VERSAMENTI_ATTIVI"+"non presente");
		System.out.println("SALVADANAIO_VERSAMENTI_ATTIVI "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.SALVADANAIO_VERSAMENTI_ATTIVI_NUM).getLocator()),"SALVADANAIO_VERSAMENTI_ATTIVI_NUM"+"non presente");
		System.out.println("SALVADANAIO_VERSAMENTI_ATTIVI_NUM "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.SALVADANAIO_VERSAMENTI_ATTIVI_LINK).getLocator()),"SALVADANAIO_VERSAMENTI_ATTIVI_LINK"+"non presente");
		System.out.println("SALVADANAIO_VERSAMENTI_ATTIVI_LINK "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.SCONTIPOSTE_DIV).getLocator()),"SCONTIPOSTE_DIV"+"non presente");
		System.out.println("SCONTIPOSTE_DIV "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.SCONTIPOSTE_SCONTI_ACCUMULATI).getLocator()),"SCONTIPOSTE_SCONTI_ACCUMULATI"+"non presente");
		System.out.println("SCONTIPOSTE_SCONTI_ACCUMULATI "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.SCONTIPOSTE_SCONTI_LINK).getLocator()),"SCONTIPOSTE_SCONTI_LINK"+"non presente");
		System.out.println("SCONTIPOSTE_SCONTI_LINK "+"ok");
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 300);
		WaitManager.get().waitMediumTime();
		/*
		 * BLOCCO_CARTA_DIV("//*[@resource-di='posteitaliane.posteapp.appbpol:id/carta_dettaglio_cv']","",""),
		BLOCCO_CARTA_SOSPENDI("//*[@resource-di='posteitaliane.posteapp.appbpol:id/carta_dettaglio_cv']//*[@resource-id='posteitaliane.posteapp.appbpol:id/switch_panic']","",""),
		MASSIMALI_DIV("//*[@resource-id='posteitaliane.posteapp.appbpol:id/container_take_limit']","",""),
		MASSIMALI_POS_DIV("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_budget' and @text='MASSIMALI DI PAGAMENTO (POS)']","",""),
		MASSIMALI_POS_GIORNO_MOD("//*[@resource-id='posteitaliane.posteapp.appbpol:id/bt_budget_limit_day']","",""),
		MASSIMALI_POS_GIORNO_VAL("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_budget_limit_day_value']","",""),
		MASSIMALI_POS_MESE_MOD("//*[@resource-id='posteitaliane.posteapp.appbpol:id/bt_budget_limit_month']","",""),
		MASSIMALI_POS_MESE_VAL("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_budget_limit_month_value']","",""),
		MASSIMALI_ATM_DIV("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_take_limit' and @text='MASSIMALI DI PRELIEVO (ATM)']","",""),
		MASSIMALI_ATM_GIORNO_MOD("//*[@resource-id='posteitaliane.posteapp.appbpol:id/bt_take_limit_day']","",""),
		MASSIMALI_ATM_GIORNO_VAL("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_take_limit_day_value']","",""),
		MASSIMALI_ATM_MESE_MOD("//*[@resource-id='posteitaliane.posteapp.appbpol:id/bt_take_limit_mounth']","",""),
		MASSIMALI_ATM_MESE_VAL("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_take_limit_mounth_value']","",""),
		AREA_GEOGRAFICA_DIV("//*[@resource-id='posteitaliane.posteapp.appbpol:id/container_geographic']","",""),
		AERA_GEOGRAFICA_EUROPA("//*[@resource-id='posteitaliane.posteapp.appbpol:id/rb_europe']","",""),
		AREA_GEOGRAFICA_MONDO("//*[@resource-id='posteitaliane.posteapp.appbpol:id/rb_world']","",""),
		ABILITA_CONTACTLES_DIV("//*[@resource-id='posteitaliane.posteapp.appbpol:id/container_more_service']","",""),
		ABILITA_CONTACTLES_SWITCH("//*[@resource-id='posteitaliane.posteapp.appbpol:id/switch_contactless']","",""),
		ACQUISTI_ONLINE_DIV("//*[@resource-id='posteitaliane.posteapp.appbpol:id/tv_acquisti_online']","",""),
		ACQUISTI_ONLINE_SWITCH("//*[@resource-id='posteitaliane.posteapp.appbpol:id/switch_shop_online']","",""),
		ACQUISTI_ONLINE_SELEZIONE_CAT("//*[@resource-id='posteitaliane.posteapp.appbpol:id/rl_selected_categories']","","");
		 */
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.BLOCCO_CARTA_DIV).getLocator()),"BLOCCO_CARTA_DIV"+"non presente");
		System.out.println("BLOCCO_CARTA_DIV "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.BLOCCO_CARTA_SOSPENDI).getLocator()),"BLOCCO_CARTA_SOSPENDI"+"non presente");
		System.out.println("BLOCCO_CARTA_SOSPENDI "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.MASSIMALI_DIV).getLocator()),"MASSIMALI_DIV"+"non presente");
		System.out.println("MASSIMALI_DIV "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.MASSIMALI_POS_DIV).getLocator()),"MASSIMALI_POS_DIV"+"non presente");
		System.out.println("MASSIMALI_POS_DIV "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.MASSIMALI_POS_GIORNO_MOD).getLocator()),"MASSIMALI_POS_GIORNO_MOD"+"non presente");
		System.out.println("MASSIMALI_POS_GIORNO_MOD "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.MASSIMALI_POS_GIORNO_VAL).getLocator()),"MASSIMALI_POS_GIORNO_VAL"+"non presente");
		System.out.println("MASSIMALI_POS_GIORNO_VAL "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.MASSIMALI_POS_MESE_MOD).getLocator()),"MASSIMALI_POS_MESE_MOD"+"non presente");
		System.out.println("MASSIMALI_POS_MESE_MOD "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.MASSIMALI_POS_MESE_VAL).getLocator()),"MASSIMALI_POS_MESE_VAL"+"non presente");
		System.out.println("MASSIMALI_POS_MESE_VAL "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.MASSIMALI_ATM_DIV).getLocator()),"MASSIMALI_ATM_DIV"+"non presente");
		System.out.println("MASSIMALI_ATM_DIV "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.MASSIMALI_ATM_GIORNO_MOD).getLocator()),"MASSIMALI_ATM_GIORNO_MOD"+"non presente");
		System.out.println("MASSIMALI_ATM_GIORNO_MOD "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.MASSIMALI_ATM_GIORNO_VAL).getLocator()),"MASSIMALI_ATM_GIORNO_VAL"+"non presente");
		System.out.println("MASSIMALI_ATM_GIORNO_VAL "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.MASSIMALI_ATM_MESE_MOD).getLocator()),"MASSIMALI_ATM_MESE_MOD"+"non presente");
		System.out.println("MASSIMALI_ATM_MESE_MOD "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.MASSIMALI_ATM_MESE_VAL).getLocator()),"MASSIMALI_ATM_MESE_VAL"+"non presente");
		System.out.println("MASSIMALI_ATM_MESE_VAL "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.AREA_GEOGRAFICA_DIV).getLocator()),"AREA_GEOGRAFICA_DIV"+"non presente");
		System.out.println("AREA_GEOGRAFICA_DIV "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.AERA_GEOGRAFICA_EUROPA).getLocator()),"AERA_GEOGRAFICA_EUROPA"+"non presente");
		System.out.println("AERA_GEOGRAFICA_EUROPA "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.AREA_GEOGRAFICA_MONDO).getLocator()),"AREA_GEOGRAFICA_MONDO"+"non presente");
		System.out.println("AREA_GEOGRAFICA_MONDO "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.ABILITA_CONTACTLES_DIV).getLocator()),"ABILITA_CONTACTLES_DIV"+"non presente");
		System.out.println("ABILITA_CONTACTLES_DIV "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.ABILITA_CONTACTLES_SWITCH).getLocator()),"ABILITA_CONTACTLES_SWITCH"+"non presente");
		System.out.println("ABILITA_CONTACTLES_SWITCH "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.ACQUISTI_ONLINE_DIV).getLocator()),"ACQUISTI_ONLINE_DIV"+"non presente");
		System.out.println("ACQUISTI_ONLINE_DIV "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.ACQUISTI_ONLINE_SWITCH).getLocator()),"ACQUISTI_ONLINE_SWITCH"+"non presente");
		System.out.println("ACQUISTI_ONLINE_SWITCH "+"ok");
		org.springframework.util.Assert.isTrue(ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.PostePayDetailsMolecola.ACQUISTI_ONLINE_SELEZIONE_CAT).getLocator()),"ACQUISTI_ONLINE_SELEZIONE_CAT"+"non presente");
		System.out.println("ACQUISTI_ONLINE_SELEZIONE_CAT "+"ok");
		
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.UP, 300);
		WaitManager.get().waitMediumTime();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
