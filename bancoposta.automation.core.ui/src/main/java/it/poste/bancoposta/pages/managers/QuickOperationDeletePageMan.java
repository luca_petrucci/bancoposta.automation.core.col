package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.QuickOperationCreationBean;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.HomePage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.QuickOperationDeletePage;
import test.automation.core.UIUtils;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="QuickOperationDeletePage")
@Android
@IOS
public class QuickOperationDeletePageMan extends LoadableComponent<QuickOperationDeletePageMan> implements QuickOperationDeletePage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement saveburton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement moveButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement selectOpCheckBox;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement quickOperationList;
	/**
	 * @android campo dinamico sostituire $(quickOpName) con il nome dell'operazione veloce da ricercare
	 * @ios 
	 * @web 
	 */
	private WebElement quickOperationElement;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement modalHeader;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement modalCancellButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement modalConfirmButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement deleteButton;


	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public void deleteQuickOperation(QuickOperationCreationBean b) 
	{
		String list="";
		String elements="";
		String descrizione="";
		String selectOp="";
		String delete="";

		elements=page.getParticle(Locators.QuickOperationDeletePageMolecola.QUICKOPERATIONELEMENT).getLocator();
		descrizione=page.getParticle(Locators.QuickOperationDeletePageMolecola.QUICKOPERATIONELEMENTDESCRIPTION).getLocator();
		selectOp=page.getParticle(Locators.QuickOperationDeletePageMolecola.SELECTOPCHECKBOX).getLocator();
		delete=page.getParticle(Locators.QuickOperationDeletePageMolecola.SAVEBURTON).getLocator();
		

		//ricerco l'elemento da eliminare
		List<WebElement> L=page.getDriver().findElements(By.xpath(elements));

		for(WebElement e : L)
		{
			String txt=e.findElement(By.xpath(descrizione)).getText().trim().toLowerCase();

			//una volta trovato clicco sul select box
			if(txt.equals(b.getQuickOperationName().toLowerCase()))
			{
				this.selectOpCheckBox=e.findElement(By.xpath(selectOp));
				this.selectOpCheckBox.click();

				this.deleteButton=page.getDriver().findElement(By.xpath(delete));
				

				this.deleteButton.click();

				//clicco su conferma da modale per Android
				this.modalConfirmButton=page.getParticle(Locators.QuickOperationDeletePageMolecola.MODALCONFIRMBUTTON).getElement();
				this.modalConfirmButton.click();
				
				WaitManager.get().waitShortTime();

				//clicco su salva
				this.saveburton=page.getParticle(Locators.QuickOperationDeletePageMolecola.SAVEBURTON).getElement();
				

				this.saveburton.click();

				break;
			}

		}
	}

	public HomePage gotoHomePage(String userHeader) 
	{
		this.backButton=page.getParticle(Locators.QuickOperationDeletePageMolecola.BACKBUTTON).getElement();
		

		this.backButton.click();
		
		HomePage e=(HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", page.getDriver().getClass(), page.getLanguage());
		
		e.setUserHeader(userHeader);
		
		e.checkPage();

		return e;
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
