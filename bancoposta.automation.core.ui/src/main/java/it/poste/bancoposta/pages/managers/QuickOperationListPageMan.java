package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BonificoDataBean;
import bean.datatable.GirofondoCreationBean;
import bean.datatable.PaymentCreationBean;
import bean.datatable.QuickOperationCheckBean;
import bean.datatable.QuickOperationCreationBean;
import bean.datatable.QuickOperationModifyBean;
import it.poste.bancoposta.pages.*;
import test.automation.core.UIUtils;
import utils.DinamicData;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="QuickOperationListPage")
@Android
@IOS
public class QuickOperationListPageMan extends LoadableComponent<QuickOperationListPageMan> implements QuickOperationListPage
{
	private static final String GIROFONDO = "girofondo";
	private static final String POSTAGIRO = "postagiro";
	private static final String BONIFICO = "bonifico";
	private static final String RICARICAPOSTEPAY = "ricarica postepay";
	private static final String RICARICA_POSTEPAY = "Ricarica Postepay";
	private static final String RICARICA_TELEFONICA = "Ricarica telefonica";
	private static final String BONIFICO_QUICK = "Bonifico";
	private static final String POSTAGIRO_QUICK = "Postagiro";
	private static final String BOLLETTINO = "Bollettino";
	private static final String PAGOPA = "PagoPA";
	private static final String GIROFONDO_QUICK = "Girofondo";
	//private static final String BOLLETTINO = "bollettino"; da implementare

	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement modifyButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement quickOperationList;
	/**
	 * @android campo dinamico sostituire $(quickOpName) con il nome dell'operazione veloce da ricercare
	 * @ios campo dinamico sostituire $(FastOperation) con il valore operazione veloce da ricercare
	 * @web 
	 */
	private WebElement quickOperationElement;
	private it.poste.bancoposta.pages.HomePage HomePage;
	private PayWithPage payWithPage;


	@Override
	protected void isLoaded() throws Error {

	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public void gotoQuickOperationPage(String quickOperationName, boolean saved) throws InterruptedException 
	{
		String elementsXpath="";
		String elementName="";

		if(!saved)
		{
			elementsXpath=page.getParticle(Locators.QuickOperationListPageMolecola.QUICKOPERATIONELEMENTDIN).getLocator(new Entry("text", quickOperationName));
			
			it.poste.bancoposta.utils.Utility.scrollVersoElemento(
					page.getDriver(), 
					elementsXpath, 
					UIUtils.SCROLL_DIRECTION.DOWN, 
					10);
			WaitManager.get().waitShortTime();
			
			WebElement element=page.getDriver().findElement(By.xpath(elementsXpath));
			element.click();
		}
		else
		{
			elementsXpath=page.getParticle(Locators.QuickOperationListPageMolecola.QUICKOPERATIONELEMENTDESCRIPTION).getLocator(new Entry("title", quickOperationName));
			
			it.poste.bancoposta.utils.Utility.scrollVersoElemento(
					page.getDriver(), 
					elementsXpath, 
					UIUtils.SCROLL_DIRECTION.DOWN, 
					10);
			WaitManager.get().waitShortTime();
			
			WebElement element=page.getDriver().findElement(By.xpath(elementsXpath));
			element.click();
		}
		

//		List<WebElement> list=page.getDriver().findElements(By.xpath(elementsXpath));
//		System.out.println(quickOperationName);
//		System.out.println(elementName);
//		System.out.println(list.size());
//		boolean find=false;
//		WebElement el = null;
//		System.out.println("quick operation to find:"+quickOperationName);
//		for(WebElement e : list)
//		{
//			String name=null;
//			if(!saved)
//			{
//				WebElement ee=e.findElement(page.getParticle(Locators.QuickOperationListPageMolecola.QUICKOPERATIONELEMENTNAME).getXPath());
//				name=ee.getText().trim();
//			}
//			else
//			{
//				name=e.getText().trim();
//			}
//			System.out.println("check "+name);
//			if(name.equals(quickOperationName))
//			{
//				find=true;
//				el=e;
//				break;
//			}
//		}
//		el.click();
//
//	
		//		this.quickOperationList=page.getParticle(Locators.QuickOperationListPageMolecola.QUICKOPERATIONLIST).getElement();
		//		elementXpath=Locators.QuickOperationListPageMolecola.QUICKOPERATIONELEMENT.getLocator(driver);
		//
		//		if(!saved) 
		//		{
		//			elementName=Utility.replacePlaceHolders(Locators.QuickOperationListPageMolecola.QUICKOPERATIONELEMENTNAME.getLocator(driver), new Entry("text",quickOperationName));
		//		}
		//		else
		//			elementName=Locators.QuickOperationListPageMolecola.QUICKOPERATIONELEMENTDESCRIPTION.getLocator(driver);
		//
		//		//ricerco l'item da cliccare
		//		List<WebElement> list=this.quickOperationList.findElements(By.xpath(elementXpath));
		//		System.out.println(quickOperationName);
		//		System.out.println(elementName);
		//
		//		for(WebElement e : list)
		//		{
		//			String name=e.findElement(By.xpath(elementName)).getText().trim().toLowerCase();
		//			Thread.sleep(1500);
		//			if(name.equals(quickOperationName.toLowerCase())) {
		//				e.click();
		//				try {
		//					WaitManager.get().waitMediumTime();
		//					WebElement OkButton = page.getParticle(Locators.BonificoConfirmPageLocator.MODALOK).getElement();
		//					OkButton.click();
		//				} catch (Exception er) {
		//				}
		//				break;
		//			}			
		//		}
	}

	public void checkModifyOperation(QuickOperationCreationBean b) 
	{
		QuickOperationEditable p=null;
		System.out.println(b.getTransactionType());

		if (b.getTransactionType().equals("RICARICAPOSTEPAY")) 
		{
			p=(QuickOperationEditable)(RicaricaPostepayConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("RicaricaPostepayConfirmPage", page.getDriver().getClass(), page.getLanguage());
			p.setBean((QuickOperationCreationBean)DinamicData.getIstance().get("RICARICAPOSTEPAY_DATA"));
			try {
				((RicaricaPostepayConfirmPage)p).clickOKModal();
				((RicaricaPostepayConfirmPage)p).checkPage();
				((RicaricaPostepayConfirmPage)p).checkData2((QuickOperationCreationBean) DinamicData.getIstance().get("RICARICAPOSTEPAY_DATA"));
			} catch (Exception e) {

			}

		} else if (b.getTransactionType().equals("GIROFONDO")) {
			GirofondoCreationBean girofondo = new GirofondoCreationBean();
			girofondo.setAmount(b.getAmount());
			girofondo.setTransferTo(b.getTransferTo());
			girofondo.setPayWith(b.getPayWith());

			p=(QuickOperationEditable)(GirofondoSubmitPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("GirofondoSubmitPage", page.getDriver().getClass(), page.getLanguage());
			((GirofondoSubmitPage)p).setBean(girofondo);
		} else if  (b.getTransactionType().equals("POSTAGIRO")) {

			if(DinamicData.getIstance().get("BONIFICO_DATA") != null)
			{
				p=(QuickOperationEditable)(PostagiroConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
						.getPageManager("PostagiroConfirmPage", page.getDriver().getClass(), page.getLanguage());

				((PostagiroConfirmPage)p).setBean((BonificoDataBean)DinamicData.getIstance().get("BONIFICO_DATA"));
			}
			else
			{
				p=(QuickOperationEditable)(PostagiroConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
						.getPageManager("PostagiroConfirmPage", page.getDriver().getClass(), page.getLanguage());
				((PostagiroConfirmPage)p).setBean((PaymentCreationBean) DinamicData.getIstance().get("POSTAGIRO_DATA"));
			}

			//visto che compare il modal di conferma esecuzione transazione per postagiro e bonifico
			//provo a cliccare su OK prima di procedere alla verifica della pagina
			((PostagiroConfirmPage)p).clickOKModal();
			((PostagiroConfirmPage)p).checkPage();

			if(DinamicData.getIstance().get("BONIFICO_DATA") != null)
				((BonificoConfirmationPages)p).checkData((BonificoDataBean) DinamicData.getIstance().get("BONIFICO_DATA"));
			else
				((PostagiroConfirmPage)p).checkData((PaymentCreationBean) DinamicData.getIstance().get("POSTAGIRO_DATA"));
		} else if (b.getTransactionType().equals("BONIFICO")) 
		{
			p=(QuickOperationEditable)(BonificoConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("BonificoConfirmPage", page.getDriver().getClass(), page.getLanguage());
			((BonificoConfirmPage)p).setBean((BonificoDataBean)DinamicData.getIstance().get("BONIFICO_DATA"));
			((BonificoConfirmPage)p).clickOKModal();
			((BonificoConfirmPage)p).checkPage();
			((BonificoConfirmPage)p).checkData((BonificoDataBean) DinamicData.getIstance().get("BONIFICO_DATA"));

		}




		//			switch(b.getTransactionType())
		//			{
		//			case GIROFONDO:
		//				page=new GirofondoSubmitPage(driver, driverType, (GirofondoCreationBean) DinamicData.getIstance().get("GIROFONDO_DATA")).get();
		//				break;
		//			case POSTAGIRO:
		//				if(DinamicData.getIstance().get("BONIFICO_DATA") != null)
		//					page=new PostagiroConfirmPage(driver, driverType,(BonificoDataBean)DinamicData.getIstance().get("BONIFICO_DATA"));
		//				else
		//					page=new PostagiroConfirmPage(driver, driverType,(PaymentCreationBean) DinamicData.getIstance().get("POSTAGIRO_DATA"));
		//				
		//				//visto che compare il modal di conferma esecuzione transazione per postagiro e bonifico
		//				//provo a cliccare su OK prima di procedere alla verifica della pagina
		//				((PostagiroConfirmPage)page).clickOKModal();
		//				((PostagiroConfirmPage)page).get();
		//				
		//				if(DinamicData.getIstance().get("BONIFICO_DATA") != null)
		//					((PostagiroConfirmPage)page).checkData((BonificoDataBean) DinamicData.getIstance().get("BONIFICO_DATA"));
		//				else
		//					((PostagiroConfirmPage)page).checkData((PaymentCreationBean) DinamicData.getIstance().get("POSTAGIRO_DATA"));
		//				
		//				break;
		//			case BONIFICO:
		//				page=new BonificoConfirmPage(driver, driverType,(BonificoDataBean)DinamicData.getIstance().get("BONIFICO_DATA"));
		//				((BonificoConfirmPage)page).clickOKModal();
		//				((BonificoConfirmPage)page).get();
		//				((BonificoConfirmPage)page).checkData((BonificoDataBean) DinamicData.getIstance().get("BONIFICO_DATA"));
		//				break;
		//			case RICARICAPOSTEPAY:
		//				page=new RicaricaPostepayConfirmPage(driver, driverType,(QuickOperationCreationBean)DinamicData.getIstance().get("RICARICAPOSTEPAY_DATA"));
		//				((RicaricaPostepayConfirmPage)page).clickOKModal();
		//				((RicaricaPostepayConfirmPage)page).get();
		//				((RicaricaPostepayConfirmPage)page).checkData2((QuickOperationCreationBean) DinamicData.getIstance().get("RICARICAPOSTEPAY_DATA"));
		//				break;
		//			}

		System.out.println(p);
		Assert.assertTrue("controllo generico su oggetto QuickOperationEditable", p != null);
		p.isEditable(b);

		//per via del bug in fase di analisi PBPAPP-2497 si atterra da ios sulla pagina di riepilogo
		//operazione veloce

		p.clickBack();
		this.HomePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", page.getDriver().getClass(), page.getLanguage());;
				HomePage.setUserHeader(b.getUserHeader());
				this.HomePage.checkPage();
	}

	public HomePage clickBack(String userHeader) 
	{
		this.backButton=page.getParticle(Locators.QuickOperationListPageMolecola.BACKBUTTON).getElement();


		this.backButton.click();
		this.HomePage = (HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", page.getDriver().getClass(), page.getLanguage());;
				HomePage.setUserHeader(userHeader);
				this.HomePage.checkPage();
				return HomePage;
	}

	public OKOperationPage submitTransaction(QuickOperationCreationBean b, String modify) 
	{
		QuickOperationEditable p=null;
		String campo=null;
		String value=null;

		switch(b.getTransactionType().toLowerCase())
		{
		case GIROFONDO:
			p=(QuickOperationEditable)(GirofondoSubmitPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
			.getPageManager("GirofondoSubmitPage", page.getDriver().getClass(), page.getLanguage());
			((GirofondoSubmitPage)p).setBean((GirofondoCreationBean) DinamicData.getIstance().get("GIROFONDO_DATA"));
			((GirofondoSubmitPage)p).checkPage();

			break;
		case POSTAGIRO:
			if(DinamicData.getIstance().get("BONIFICO_DATA") != null)
			{
				p=(QuickOperationEditable)(PostagiroConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
						.getPageManager("PostagiroConfirmPage", page.getDriver().getClass(), page.getLanguage());

				((PostagiroConfirmPage)p).setBean((BonificoDataBean)DinamicData.getIstance().get("BONIFICO_DATA"));
			}
			else
			{
				p=(QuickOperationEditable)(PostagiroConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
						.getPageManager("PostagiroConfirmPage", page.getDriver().getClass(), page.getLanguage());
				((PostagiroConfirmPage)p).setBean((PaymentCreationBean) DinamicData.getIstance().get("POSTAGIRO_DATA"));
			}
			//visto che compare il modal di conferma esecuzione transazione per postagiro e bonifico
			//provo a cliccare su OK prima di procedere alla verifica della pagina
			((PostagiroConfirmPage)p).clickOKModal();
			((PostagiroConfirmPage)p).checkPage();

			if(DinamicData.getIstance().get("BONIFICO_DATA") != null)
				((BonificoConfirmationPages)p).checkData((BonificoDataBean) DinamicData.getIstance().get("BONIFICO_DATA"));
			else
				((PostagiroConfirmPage)p).checkData((PaymentCreationBean) DinamicData.getIstance().get("POSTAGIRO_DATA"));

			break;
		case BONIFICO:
			p=(QuickOperationEditable)(BonificoConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
			.getPageManager("BonificoConfirmPage", page.getDriver().getClass(), page.getLanguage());
			((BonificoConfirmPage)p).setBean((BonificoDataBean)DinamicData.getIstance().get("BONIFICO_DATA"));
			((BonificoConfirmPage)p).clickOKModal();
			((BonificoConfirmPage)p).checkPage();

			break;
		}

		Assert.assertTrue("controllo generico su oggetto QuickOperationEditable", p != null);

		if(modify != null)
		{
			String[] v=modify.split(";");
			campo=v[0];
			value=v[1];
		}

		return p.submit(b,campo,value);
	}

	public QuickOperationDeletePage clickModify() 
	{
		this.modifyButton=page.getParticle(Locators.QuickOperationListPageMolecola.MODIFYBUTTON).getElement();
		

		this.modifyButton.click();

		QuickOperationDeletePage p=(QuickOperationDeletePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("QuickOperationDeletePage", page.getDriver().getClass(), page.getLanguage());
		
		p.checkPage();

		return p;
	}

	public void checkIfQuickOperationIsNotPresent(QuickOperationCreationBean b) 
	{
		String list="";
		String elements="";
		String descrizione="";
		String selectOp="";
		String delete="";

		elements=page.getParticle(Locators.QuickOperationListPageMolecola.QUICKOPERATIONELEMENT).getLocator();
		descrizione=page.getParticle(Locators.QuickOperationListPageMolecola.QUICKOPERATIONELEMENTDESCRIPTION).getLocator();
		

		if(page.getDriver().findElements(By.xpath(descrizione)).size() == 0)
			return;


		//ricerco l'elemento da eliminare
		List<WebElement> L=page.getDriver().findElements(By.xpath(elements));

		for(WebElement e : L)
		{
			String txt="";
			try
			{
				txt=e.findElement(By.xpath(descrizione)).getText().trim().toLowerCase();
			}
			catch(Exception err)
			{

			}

			//una volta trovato clicco sul select box
			if(txt.equals(b.getQuickOperationName().toLowerCase()))
			{
				Assert.assertTrue("Errore l'operazione veloce:"+b.getQuickOperationName()+" è ancora presente",false);
			}

		}
	}

	public void QuickOperationModifyBean(QuickOperationModifyBean b) {
		// TODO Auto-generated method stub

	}

	public void checkQuickOperationFunnel(QuickOperationCheckBean bean) 
	{
		WaitManager.get().waitMediumTime();
		String item=page.getParticle(Locators.QuickOperationListPageMolecola.QUICKOPERATIONELEMENTDIN).getLocator(new Entry("text",bean.getQuickOperation()));
		System.out.println(item);
		UIUtils.ui().waitForCondition(page.getDriver(), ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(item)));

		page.getDriver().findElement(By.xpath(item)).click();

		try {
			this.payWithPage = (PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PayWithPage", page.getDriver().getClass(), page.getLanguage());
			payWithPage.checkPage();

			this.payWithPage.clickOnConto(bean.getPayWith());
		} catch (Exception e) {
			// TODO: handle exception
		}

		switch(bean.getQuickOperation())
		{
		case RICARICA_POSTEPAY:
			RicaricaPostepayPage ricaricaPP=(RicaricaPostepayPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("RicaricaPostepayPage", page.getDriver().getClass(), page.getLanguage());
			ricaricaPP.checkPage();
			ricaricaPP.clickAnnulla();
			break;
		case RICARICA_TELEFONICA:
			RicaricaTelefonicaPage ricT=(RicaricaTelefonicaPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("RicaricaTelefonicaPage", page.getDriver().getClass(), page.getLanguage());
			ricT.checkPage();
			ricT.clickAnnulla();
			break;
		case BONIFICO_QUICK:
			SendBonificoPage bon=(SendBonificoPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("SendBonificoPage", page.getDriver().getClass(), page.getLanguage());
//			bon.checkPage();
//			bon.chechEntryPoint();
			bon.clickAnnulla();
			break;
		case POSTAGIRO_QUICK:
			SendPostagiroPage postG=(SendPostagiroPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("SendPostagiroPage", page.getDriver().getClass(), page.getLanguage());
			postG.checkPage();
			postG.clickAnnulla();
			break;
		case BOLLETTINO:
			BollettinoChooseOperationPage boll=(BollettinoChooseOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("BollettinoChooseOperationPage", page.getDriver().getClass(), page.getLanguage());
			boll.checkPage();
			try {
				SelectBolletinoTypesPage selBoll=boll.gotoCompilaManualmenteSection();
				//selBoll.clickAnnulla();
			} catch (Exception e) {

			}
			boll.clickAnnulla();
			break;
		case PAGOPA:
			PagoPAPage pagoP=(PagoPAPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PagoPAPage", page.getDriver().getClass(), page.getLanguage());
			try {
				pagoP.clickCompilaManualmente();
			} catch (Exception e) 
			{

			}
			pagoP.checkPage();
			pagoP.clickAnnulla();
			break;
		case GIROFONDO_QUICK:
			GirofondoFormPage gir=(GirofondoFormPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("GirofondoFormPage", page.getDriver().getClass(), page.getLanguage());
			gir.checkPage();
			gir.clickAnnulla();
			break;
		}
	}

	@Override
	public void init(UiPage page) {
		this.page=page;

	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	@Override
	public void clickOnOkPopupButton() {
		WebElement OkButton = page.getParticle(Locators.QuickOperationListPageMolecola.QUICKOPERATIONPOPUPOKBUTTON).getElement();
		OkButton.click();
	}

}
