package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BonificoDataBean;
import bean.datatable.QuickOperationCreationBean;
import bean.datatable.RicaricaPostepayBean;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OKOperationPage;
import it.poste.bancoposta.pages.QuickOperationEditable;
import it.poste.bancoposta.pages.RicaricaPostepayConfirmPage;
import it.poste.bancoposta.pages.RicaricaPostepayPage;
import it.poste.bancoposta.utils.SoftAssertion;
import test.automation.core.UIUtils;

import java.util.Locale;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="RicaricaPostepayConfirmPage")
@Android
public class RicaricaPostepayConfirmPageMan extends LoadableComponent<RicaricaPostepayConfirmPageMan> implements QuickOperationEditable,RicaricaPostepayConfirmPage
{
	private static final Object TITLE = "\u00E8 tutto corretto?";
	private static final Object DESCRIPTION = "verifica prima di procedere con il pagamento.";
	private static final Object HEADER = "riepilogo ricarica";
	private QuickOperationCreationBean quickOperation;
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement title;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement payWithInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cardNumberInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cardOwnerInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement commissione;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement confirm;
	private RicaricaPostepayBean bean;
	private WebElement modalOK;
	private WebElement modify;
	private WebElement headerDescription;


	@Override
	protected void isLoaded() throws Error 
	{
		
	}

	@Override
	protected void load() 
	{
		page.get();

		this.title=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.TITLE).getElement();
		System.out.println(title.getText());
		this.headerDescription=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.HEADER_DESCRIPTION).getElement();
		this.header=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.HEADER).getElement();
		
		Properties t=page.getLanguage();
		String txt=title.getText().trim().toLowerCase();

		SoftAssertion.get().getAssertion().assertThat(txt)
		.withFailMessage("controllo header � tutto corretto")
		.isEqualTo(t.getProperty("eTuttoCorretto").toLowerCase());
		
		System.out.println("check:"+txt.equals(t.getProperty("eTuttoCorretto").toLowerCase()));

		//Assert.assertTrue("controllo header � tutto corretto",txt.equals(TITLE));

		txt=headerDescription.getText().trim().toLowerCase();
		
		System.out.println("descr letto:"+txt);
		System.out.println("descr ER:"+t.getProperty("verificaIdatiPrimaDiProcedere").toLowerCase());
		
		SoftAssertion.get().getAssertion().assertThat(txt)
		.withFailMessage("ccontrollo header Verifica i dati prima di procedere con il pagamento.")
		.isEqualTo(t.getProperty("verificaIdatiPrimaDiProcedere").toLowerCase());

	}

	
	public void checkData(RicaricaPostepayBean b) 
	{
		check(b.getPayWith(),b.getTransferTo(),b.getOwner(),b.getDescription(),b.getAmount());
	}
	
	public void checkData2(QuickOperationCreationBean b) 
	{
		check(b.getPayWith(),b.getTransferTo(),b.getOwner(),b.getDescription(),b.getAmount());
	}

	public void checkData3(RicaricaPostepayBean b) 
	{
		check(b.getPayWith(),b.getTransferTo(),b.getOwner(),b.getDescription(),b.getAmount());
	}
	
	
	public void check(String payWith,String transferTo,String owner,String description,String am)
	{
		this.payWithInfo=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.PAYWITHINFO).getElement();
		this.cardNumberInfo=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.CARDNUMBERINFO).getElement();
		this.cardOwnerInfo=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.CARDOWNERINFO).getElement();
		this.amount=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.AMOUNT).getElement();
		this.commissione=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.COMMISSIONE).getElement();
		this.description=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.DESCRIPTION).getElement();
		this.confirm=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.CONFIRM).getElement();


		//controllo paga con
		String txt=this.payWithInfo.getText().trim().toLowerCase();
		String fourDigits2 = txt.substring(txt.length() - 4);
		String payWith4 = payWith.toLowerCase().replace(";", " ").trim().substring(payWith.length() - 4);
		

		Assert.assertTrue("controllo paga con;attuale:"+fourDigits2+", atteso:"+payWith4,fourDigits2.equals(payWith4));

		//controllo c/c iban
		String cardNumber = this.cardNumberInfo.getText();
		System.out.println(cardNumberInfo.getText());
		cardNumber.substring(this.cardNumberInfo.getText().trim().length() - 4);
		String fourDigits = cardNumber.substring(cardNumber.length() - 4);


		Assert.assertTrue("controllo numero carta;attuale:"+fourDigits+", atteso:"+cardNumber,fourDigits.equals(cardNumber.substring(cardNumber.length() - 4)));

		//controllo intestato a
		txt=this.cardOwnerInfo.getText().trim().toLowerCase();

		Assert.assertTrue("controllo intestato a;attuale:"+txt+", atteso:"+owner.toLowerCase(),txt.equals(owner.toLowerCase()));

		//controllo importo
		double amount=Utility.parseCurrency(this.amount.getText().trim(), Locale.ITALY);
		double tocheck=Double.parseDouble(am);

		Assert.assertTrue("controllo importo;attuale:"+amount+", atteso:"+tocheck,amount==tocheck);

		double commissioni=Utility.parseCurrency(this.commissione.getText().trim(), Locale.ITALY);

		txt=this.description.getText().trim().toLowerCase();
		System.out.println(txt);
		System.out.println(description);
		
		Assert.assertTrue("controllo causale;attuale:"+txt+", atteso:"+description.toLowerCase(),txt.equals(description.toLowerCase()));

		//controlla paga con
		String paga="paga \u20AC "+(Utility.toCurrency(amount+commissioni, Locale.ITALY));

		//Assert.assertTrue("controllo paga;attuale:"+this.confirm.getText().trim().toLowerCase()+", atteso:"+paga,this.confirm.getText().trim().toLowerCase().contains(paga));
		confirm.click();

	}
	
	
	public OKOperationPage submit(RicaricaPostepayBean b) 
	{
		return submit(b.getPosteId());
	}
	
	public OKOperationPage submit(String posteid)
	{
		this.confirm=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.CONFIRM).getElement();
		

		this.confirm.click();

		InsertPosteIDPage posteId=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
		
		posteId.checkPage();

		return posteId.insertPosteID(posteid);
	}

	public void clickOKModal() {
		// TODO Auto-generated method stub
		try
		{
			this.modalOK=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.MODALOK).visibilityOfElement(30L);
			this.modalOK.click();
			WaitManager.get().waitShortTime();
		}
		catch(Exception err)
		{
			
		}
	}

	@Override
	public void isEditable(QuickOperationCreationBean b) {
		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);
		
		//clicco su modifica
		this.modify=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.MODIFY).getElement();
		
		
		this.modify.click();
		
		RicaricaPostepayPage p=(RicaricaPostepayPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaPostepayPage", page.getDriver().getClass(), page.getLanguage());
		
		p.checkPage();
		
		p.isEditable(bean);
		
	}

	@Override
	public void clickBack() {
		try {
			WebElement annullaButton = page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.BACKBUTTON).visibilityOfElement(10L);
			annullaButton.click();
		} catch (Exception e) {

		}
		try {
			WebElement popupEsciButton = page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.MODALOK).visibilityOfElement(10L);
			popupEsciButton.click();
		} catch (Exception e) {

		}
		
		
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	@Override
	public OKOperationPage submit(QuickOperationCreationBean b, String campo, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setBean(QuickOperationCreationBean quickOperationCreationBean) {
		// TODO Auto-generated method stub
		
	}
	
	
}
