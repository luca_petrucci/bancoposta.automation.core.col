package it.poste.bancoposta.pages.managers;

import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import android.Utility;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import bean.datatable.RicaricaPostepayBean;
import it.poste.bancoposta.pages.Locators;

@PageManager(page="RicaricaPostepayConfirmPage")
@IOS
public class RicaricaPostepayConfirmPageManIOS extends RicaricaPostepayConfirmPageMan {
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement title;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement payWithInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cardNumberInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cardOwnerInfo;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement commissione;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement confirm;
	
	public void check(String payWith,String transferTo,String owner,String description,String am)
	{
		this.payWithInfo=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.PAYWITHINFO).getElement();
		this.cardNumberInfo=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.CARDNUMBERINFO).getElement();
		this.cardOwnerInfo=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.CARDOWNERINFO).getElement();
		this.amount=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.AMOUNT).getElement();
		this.commissione=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.COMMISSIONE).getElement();
		this.description=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.DESCRIPTION).getElement();
		this.confirm=page.getParticle(Locators.RicaricaPostepayConfirmPageMolecola.CONFIRM).getElement();


		//controllo paga con
		String txt=this.payWithInfo.getText().trim().toLowerCase();
		String fourDigits2 = txt.substring(txt.length() - 4);
		String payWith4 = payWith.toLowerCase().replace(";", " ").trim().substring(payWith.length() - 4);
		

		Assert.assertTrue("controllo paga con;attuale:"+fourDigits2+", atteso:"+payWith4,fourDigits2.equals(payWith4));

		//controllo c/c iban
		String cardNumber = this.cardNumberInfo.getText();
		System.out.println(cardNumberInfo.getText());
		cardNumber.substring(this.cardNumberInfo.getText().trim().length() - 4);
		String fourDigits = cardNumber.substring(cardNumber.length() - 4);


		Assert.assertTrue("controllo numero carta;attuale:"+fourDigits+", atteso:"+cardNumber,fourDigits.equals(cardNumber.substring(cardNumber.length() - 4)));

		//controllo intestato a
		txt=this.cardOwnerInfo.getText().trim().toLowerCase();

		Assert.assertTrue("controllo intestato a;attuale:"+txt+", atteso:"+owner.toLowerCase(),txt.equals(owner.toLowerCase()));

		//controllo importo
		double amount=Utility.parseCurrency(this.amount.getText().trim(), Locale.ITALY);
//		double tocheck=Double.parseDouble(am);
//		Assert.assertTrue("controllo importo;attuale:"+amount+", atteso:"+tocheck,amount==tocheck);
		
		String amountIOS=this.amount.getText().replace("€", "\u20AC").trim();
		String importo="\u20AC"+am;
		System.out.println("importo"+importo);
		System.out.println("amountIOS"+amountIOS);

		Assert.assertTrue("controllo importo;attuale:"+amountIOS+", atteso:"+importo,amountIOS.equals(importo));
		 

		double commissioni=Utility.parseCurrency(this.commissione.getText().trim(), Locale.ITALY);

		txt=this.description.getText().trim().toLowerCase();
		System.out.println(txt);
		System.out.println(description);
		
		Assert.assertTrue("controllo causale;attuale:"+txt+", atteso:"+description.toLowerCase(),txt.equals(description.toLowerCase()));

		//controlla paga con
		String paga="paga \u20AC "+(Utility.toCurrency(amount+commissioni, Locale.ITALY));

		//Assert.assertTrue("controllo paga;attuale:"+this.confirm.getText().trim().toLowerCase()+", atteso:"+paga,this.confirm.getText().trim().toLowerCase().contains(paga));
		confirm.click();

	}

}
