package it.poste.bancoposta.pages.managers;
import io.appium.java_client.HidesKeyboard;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.RicaricaPostepayBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OKOperationPage;
import it.poste.bancoposta.pages.PayWithPage;
import it.poste.bancoposta.pages.RicaricaPostepayConfirmPage;
import it.poste.bancoposta.pages.RicaricaPostepayPage;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.openqa.selenium.WebElement;

@PageManager(page="RicaricaPostepayPage")
@Android
@IOS
public class RicaricaPostepayPageMan extends LoadableComponent<RicaricaPostepayPageMan> implements RicaricaPostepayPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancellButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement paymentTypeSummary;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement changePaymentTypeButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement addressbookLink;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cardNumberInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ownerInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement saveOnAdressbook;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement confirm;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement changePaymentButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement codeInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ccpInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ricaricaAutomaticaButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private PayWithPage payWithPage;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancellaButton;
	private WebElement setsoglia;
	private WebElement saldominimo;
	private WebElement finoal;
	private WebElement okbutton;
	private WebElement commissione;
	private WebElement settempo;
	private WebElement selezionaRicorrenza;
	private WebElement ricobisettimanale;

	private WebElement descrizionePopup;

	private WebElement btnPopup;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public void check()
	{
		load();
	}
	public OKOperationPage submitRicarica(RicaricaPostepayBean b) 
	{
		this.confirm=page.getParticle(Locators.RicaricaPostepayPageMolecola.CONFIRM).getElement();
		
		this.confirm.click();


		//inserisco posteid
		InsertPosteIDPage insert=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
		insert.checkPage();
		
		OKOperationPage ok=insert.insertPosteID(b.getPosteId());
		
		ok.checkPage();
		
		return ok;
	}



	public OKOperationPage eseguiRicarica(RicaricaPostepayBean b) {
		
		WaitManager.get().waitMediumTime();
		try {
			page.getParticle(Locators.RicaricaPostepayPageMolecola.INIZIABUTTON).visibilityOfElement(20L).click();
			Properties t=page.getLanguage();
			System.out.println(page.getLanguage());
			System.out.println(t.getProperty("mese2"));
			page.getParticle(Locators.RicaricaPostepayPageMolecola.MESEANNOTEXTFIELD).visibilityOfElement(20L).click();
			page.getParticle(Locators.RicaricaPostepayPageMolecola.MESEANNOTEXTFIELD).visibilityOfElement(20L).sendKeys(t.getProperty("mese2"));
			try {
				((AppiumDriver)page.getDriver()).hideKeyboard();
			} catch (Exception r) {

			}		
			page.getParticle(Locators.RicaricaPostepayPageMolecola.CONTINUABUTTON).visibilityOfElement(20L).click();
			page.getParticle(Locators.RicaricaPostepayPageMolecola.CVVINSERTFIELD).visibilityOfElement(20L).sendKeys(t.getProperty("CVV2"));
			
			try {
				((AppiumDriver)page.getDriver()).hideKeyboard();
			} catch (Exception r) {

			}
			page.getParticle(Locators.RicaricaPostepayPageMolecola.CONFERMABUTTONCVV).visibilityOfElement(20L).click();
			page.getParticle(Locators.RicaricaPostepayPageMolecola.CONFERMAIDBUTTON).visibilityOfElement(20L).click();
			page.getParticle(Locators.RicaricaPostepayPageMolecola.CONFERMABUTTONABILITAPRODOTTO).visibilityOfElement(20L).click();
		}catch(Exception e) {
			
		}
		
		
//		try {
//			this.cancellaButton=page.getParticle(Locators.RicaricaPostepayPageMolecola.CANCELLABUTTON).getElement();
//		} catch (Exception e)  {
//
//		}
		WaitManager.get().waitShortTime();
		this.cardNumberInput=page.getParticle(Locators.RicaricaPostepayPageMolecola.CARDNUMBERINPUT).getElement();
		this.ownerInput=page.getParticle(Locators.RicaricaPostepayPageMolecola.ACCOUNTHOLDERINPUT).getElement();
		this.amount=page.getParticle(Locators.RicaricaPostepayPageMolecola.AMOUNT).getElement();
		this.description=page.getParticle(Locators.RicaricaPostepayPageMolecola.CAUSALEBUTTON).getElement();
		try {
			this.ricaricaAutomaticaButton=page.getParticle(Locators.RicaricaPostepayPageMolecola.RICARICAAUTOMATICABUTTON).getElement();
		} catch (Exception e) {

		}
		try {
			this.saveOnAdressbook=page.getParticle(Locators.RicaricaPostepayPageMolecola.SAVEONADRESSBOOK).getElement();
		} catch (Exception e)  {

		}
		


		//			this.addressbookLink.click();

		//	this.payWithPage=new PayWithPage(driver, driverType).get();
		//	this.payWithPage.clickOnConto(b.getPayWith());


		this.cardNumberInput.sendKeys(b.getTransferTo());
		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}

		this.ownerInput.sendKeys(b.getOwner());
		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}

		this.amount.sendKeys(b.getAmount());

		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}

		this.description.sendKeys(b.getDescription());
		System.out.println(description);
		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}

		Utility.swipe((AppiumDriver<?>)page.getDriver(), Utility.DIRECTION.DOWN, 300);

		WaitManager.get().waitMediumTime();
		page.getParticle(Locators.RicaricaPostepayPageMolecola.CONFIRM).getElement().click();
		
		try {
			page.getParticle(Locators.RicaricaPostepayPageMolecola.OKBUTTON).getElement().click();
		} catch(Exception e) {
			
		}

		RicaricaPostepayConfirmPage submit=(RicaricaPostepayConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaPostepayConfirmPage", page.getDriver().getClass(), page.getLanguage());
		
		submit.checkPage();
		
		submit.checkData(b);

		//		WebElement okButton = driver.findElement(By.xpath(Locators.RicaricaPostepayConfirmPageLocator.CONFIRM.getAndroidLocator()));
		//		okButton.click();
		//		
		InsertPosteIDPage insert=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());;
		insert.checkPage();
		//		
		OKOperationPage ok=insert.insertPosteID(b.getPosteId());
		
		ok.checkPage();

		return ok;
	}

	public void isEditable(RicaricaPostepayBean bean) 
	{
		try {
			this.cancellaButton=page.getParticle(Locators.RicaricaPostepayPageMolecola.CANCELLABUTTON).getElement();
			this.cancellaButton.click();
		} catch (Exception e) {

		}
		this.cardNumberInput=page.getParticle(Locators.RicaricaPostepayPageMolecola.CARDNUMBERINPUT).getElement();
		this.ownerInput=page.getParticle(Locators.RicaricaPostepayPageMolecola.ACCOUNTHOLDERINPUT).getElement();
		this.amount=page.getParticle(Locators.RicaricaPostepayPageMolecola.CAUSALEBUTTON).getElement();
		this.description=page.getParticle(Locators.RicaricaPostepayPageMolecola.CAUSALEBUTTON).getElement();
		try {
			this.ricaricaAutomaticaButton=page.getParticle(Locators.RicaricaPostepayPageMolecola.RICARICAAUTOMATICABUTTON).getElement();
		} catch (Exception e) {
			// TODO: handle exception
		}
		this.saveOnAdressbook=page.getParticle(Locators.RicaricaPostepayPageMolecola.SAVEONADRESSBOOK).getElement();
		this.addressbookLink=page.getParticle(Locators.RicaricaPostepayPageMolecola.ADDRESSBOOKLINK).getElement();			
		this.confirm=page.getParticle(Locators.RicaricaPostepayPageMolecola.CONFIRM).getElement();
		

		//modifico i dati

		WaitManager.get().waitMediumTime();

		this.addressbookLink.click();
		
		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}
		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}

	}


	public OKOperationPage eseguiRicaricaQuestaPostePay(RicaricaPostepayBean b) {
		
		WaitManager.get().waitMediumTime();

		this.cardNumberInput=page.getParticle(Locators.RicaricaPostepayPageMolecola.CARDNUMBERINPUT).getElement();
		this.ownerInput=page.getParticle(Locators.RicaricaPostepayPageMolecola.ACCOUNTHOLDERINPUT).getElement();
		this.amount=page.getParticle(Locators.RicaricaPostepayPageMolecola.AMOUNT).getElement();
		this.description=page.getParticle(Locators.RicaricaPostepayPageMolecola.CAUSALEBUTTON).getElement();
		this.ricaricaAutomaticaButton=page.getParticle(Locators.RicaricaPostepayPageMolecola.RICARICAAUTOMATICABUTTON).getElement();
		this.confirm=page.getParticle(Locators.RicaricaPostepayPageMolecola.CONFIRM).getElement();

		//			this.addressbookLink.click();

		//	this.payWithPage=new PayWithPage(driver, driverType).get();
		//	this.payWithPage.clickOnConto(b.getPayWith());
		String fourDigits = cardNumberInput.getText().trim();
		String fourDigits4 = fourDigits.substring(fourDigits.length() - 4);
		String fourDigitsTransferTo = b.getTransferTo().substring(b.getTransferTo().length() - 4);

		org.springframework.util.Assert.isTrue(fourDigits4.equals(fourDigitsTransferTo));

		String owner = ownerInput.getText().trim();
		String ownerFeature = b.getOwner2().trim();

		org.springframework.util.Assert.isTrue(owner.equals(ownerFeature));

		this.amount.sendKeys(b.getAmount());

		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}

		this.description.sendKeys(b.getDescription());
		System.out.println(description);
		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}


		WaitManager.get().waitShortTime();

		confirm.click();

		RicaricaPostepayConfirmPage submit=(RicaricaPostepayConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaPostepayConfirmPage", page.getDriver().getClass(), page.getLanguage());
		
		submit.checkPage();
		
		submit.checkData(b);

		InsertPosteIDPage q = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());;
		q.checkPage();
		OKOperationPage p = q.insertPosteID(b.getPosteId());
		p.checkPage();

		return p;
		//		return new OKOperationPage(driver, driverType).get();
	}

	public OKOperationPage eseguiRicaricaAutomatica(RicaricaPostepayBean b) throws Exception 
	{
		
		WaitManager.get().waitMediumTime();

		this.addressbookLink=page.getParticle(Locators.RicaricaPostepayPageMolecola.ADDRESSBOOKLINK).getElement();
		
		this.cardNumberInput=page.getParticle(Locators.RicaricaPostepayPageMolecola.CARDNUMBERINPUT).getElement();
		this.ownerInput=page.getParticle(Locators.RicaricaPostepayPageMolecola.ACCOUNTHOLDERINPUT).getElement();
		this.amount=page.getParticle(Locators.RicaricaPostepayPageMolecola.AMOUNT).getElement();
		this.description=page.getParticle(Locators.RicaricaPostepayPageMolecola.CAUSALEBUTTON).getElement();
		this.ricaricaAutomaticaButton=page.getParticle(Locators.RicaricaPostepayPageMolecola.RICARICAAUTOMATICABUTTON).getElement();			
		this.saveOnAdressbook=page.getParticle(Locators.RicaricaPostepayPageMolecola.SAVEONADRESSBOOK).getElement();
		this.confirm=page.getParticle(Locators.RicaricaPostepayPageMolecola.CONFIRM).getElement();

		//			this.addressbookLink.click();

		//	this.payWithPage=new PayWithPage(driver, driverType).get();
		//	this.payWithPage.clickOnConto(b.getPayWith());

		this.addressbookLink.click();
		WaitManager.get().waitShortTime();

		this.payWithPage = (PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PayWithPage", page.getDriver().getClass(), page.getLanguage());

		this.payWithPage.clickOnContodue(b.getTransferTo());


		//		this.cardNumberInput.sendKeys(b.getTransferTo());
		//		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}
		//
		//		this.ownerInput.sendKeys(b.getOwner());
		//		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		this.amount=page.getParticle(Locators.RicaricaPostepayPageMolecola.AMOUNT).getElement();
			
		this.amount.sendKeys(b.getAmount().trim());

		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}

		this.description.sendKeys(b.getDescription()); 
		System.out.println(description);

		this.ricaricaAutomaticaButton.click();

		WaitManager.get().waitMediumTime();

		this.setsoglia=page.getParticle(Locators.RicaricaPostepayPageMolecola.SETSOGLIA).getElement();	
		this.setsoglia.click();

		this.saldominimo=page.getParticle(Locators.RicaricaPostepayPageMolecola.MINIMUMBALANCE).getElement();
		org.springframework.util.Assert.isTrue(saldominimo.isDisplayed()); 
		this.saldominimo.sendKeys(b.getSaldominimo().trim()); 

		this.finoal=page.getParticle(Locators.RicaricaPostepayPageMolecola.UNTILTO).getElement();
		org.springframework.util.Assert.isTrue(finoal.isDisplayed()); 
		this.finoal.click();

		this.okbutton=page.getParticle(Locators.RicaricaPostepayPageMolecola.CALDENDAROKBUTTON).getElement();
		this.okbutton.click();

		WaitManager.get().waitMediumTime();

		SCROLL_DIRECTION x=SCROLL_DIRECTION.DOWN;
		//		Utility.swipe((AppiumDriver<?>)driver, Utility.DIRECTION.DOWN, 300);
		UIUtils.mobile().swipe((MobileDriver<?>)page.getDriver(), x, 0.9f, 0.15f, 0.9f, 0.05f, 200);

		WaitManager.get().waitMediumTime();

		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}

		WaitManager.get().waitMediumTime();

		//Utility.swipe((AppiumDriver<?>)driver, Utility.DIRECTION.DOWN, 300);
		this.confirm=page.getParticle(Locators.RicaricaPostepayPageMolecola.CONFIRM).getElement();
		confirm.click();

		RicaricaPostepayConfirmPage submit=(RicaricaPostepayConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaPostepayConfirmPage", page.getDriver().getClass(), page.getLanguage());;
		
		submit.checkPage();
		
		this.commissione=page.getParticle(Locators.RicaricaPostepayPageMolecola.COMMISSIONE).getElement();
		String c = this.commissione.getText().replace("�","").replace(",", ".").trim();
		org.springframework.util.Assert.isTrue(c.equals(b.getCommissione().trim())); 

		Utility.swipe((AppiumDriver<?>)page.getDriver(), Utility.DIRECTION.DOWN, 300);
		submit.checkData3(b);


		//			WebElement okButton = driver.findElement(By.xpath(Locators.RicaricaPostepayConfirmPageLocator.CONFIRM.getAndroidLocator()));
		//		    okButton.click();
		//		
		//		InsertPosteIDPage insert=new InsertPosteIDPage(driver, driverType);
		//		insert.get();
		//		
		//		insert.insertPosteID(b.getPosteId()).get();

		InsertPosteIDPage p = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
		p.checkPage();
		
		OKOperationPage ok = p.insertPosteID(b.getPosteId());
		ok.checkPage();

		return ok;
	}

	public OKOperationPage eseguiRicaricaAutomatica2(RicaricaPostepayBean b) throws Exception {
		
		WaitManager.get().waitMediumTime();

		this.addressbookLink=page.getParticle(Locators.RicaricaPostepayPageMolecola.ADDRESSBOOKLINK).getElement();
		
		this.cardNumberInput=page.getParticle(Locators.RicaricaPostepayPageMolecola.CARDNUMBERINPUT).getElement();
		this.ownerInput=page.getParticle(Locators.RicaricaPostepayPageMolecola.ACCOUNTHOLDERINPUT).getElement();
		this.amount=page.getParticle(Locators.RicaricaPostepayPageMolecola.AMOUNT).getElement();
		this.description=page.getParticle(Locators.RicaricaPostepayPageMolecola.CAUSALEBUTTON).getElement();
		this.ricaricaAutomaticaButton=page.getParticle(Locators.RicaricaPostepayPageMolecola.RICARICAAUTOMATICABUTTON).getElement();			
		this.saveOnAdressbook=page.getParticle(Locators.RicaricaPostepayPageMolecola.SAVEONADRESSBOOK).getElement();
		this.confirm=page.getParticle(Locators.RicaricaPostepayPageMolecola.CONFIRM).getElement();

		//			this.addressbookLink.click();

		//	this.payWithPage=new PayWithPage(driver, driverType).get();
		//	this.payWithPage.clickOnConto(b.getPayWith());


		this.addressbookLink.click();
		WaitManager.get().waitShortTime();

		this.payWithPage = (PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
		
		payWithPage.checkPage();

		this.payWithPage.clickOnContodue(b.getTransferTo());

		//		this.cardNumberInput.sendKeys(b.getTransferTo());
		//		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}
		//
		//		this.ownerInput.sendKeys(b.getOwner());
		//		try{((AppiumDriver<?>)driver).hideKeyboard();}catch(Exception e) {}

		this.amount=page.getParticle(Locators.RicaricaPostepayPageMolecola.AMOUNT).getElement();
			
		this.amount.sendKeys(b.getAmount().trim());

		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}

		this.description.sendKeys(b.getDescription()); 
		System.out.println(description);

		this.ricaricaAutomaticaButton.click();

		WaitManager.get().waitMediumTime();
		
		this.settempo=page.getParticle(Locators.RicaricaPostepayPageMolecola.SETTEMPO).getElement();	
		this.settempo.click();


		this.selezionaRicorrenza=page.getParticle(Locators.RicaricaPostepayPageMolecola.SELECTRICO).getElement();
		org.springframework.util.Assert.isTrue(selezionaRicorrenza.isDisplayed()); 
		this.selezionaRicorrenza.click();

		this.ricobisettimanale=page.getParticle(Locators.RicaricaPostepayPageMolecola.BIWEEK).getElement();
		org.springframework.util.Assert.isTrue(ricobisettimanale.isDisplayed()); 
		this.ricobisettimanale.click();

		this.finoal=page.getParticle(Locators.RicaricaPostepayPageMolecola.UNTILTO).getElement();
		org.springframework.util.Assert.isTrue(finoal.isDisplayed()); 
		this.finoal.click();

		this.okbutton=page.getParticle(Locators.RicaricaPostepayPageMolecola.CALDENDAROKBUTTON).getElement();
		this.okbutton.click();


		Utility.swipe((AppiumDriver<?>)page.getDriver(), Utility.DIRECTION.DOWN, 300);

		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}

		WaitManager.get().waitMediumTime();

		Utility.swipe((AppiumDriver<?>)page.getDriver(), Utility.DIRECTION.DOWN, 300);
		confirm.click();

		RicaricaPostepayConfirmPage submit=(RicaricaPostepayConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaPostepayConfirmPage", page.getDriver().getClass(), page.getLanguage());
		
		submit.checkPage();
		
		this.commissione=page.getParticle(Locators.RicaricaPostepayPageMolecola.COMMISSIONE).getElement();
		String c = this.commissione.getText().replace("�","").replace(",", ".").trim();
		org.springframework.util.Assert.isTrue(c.equals(b.getCommissione().trim())); 

		Utility.swipe((AppiumDriver<?>)page.getDriver(), Utility.DIRECTION.DOWN, 300);
		submit.checkData3(b);


		//			WebElement okButton = driver.findElement(By.xpath(Locators.RicaricaPostepayConfirmPageLocator.CONFIRM.getAndroidLocator()));
		//		    okButton.click();

		InsertPosteIDPage insert=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());;
		insert.checkPage();

		//		InsertPosteIDPage p = new InsertPosteIDPage(driver, driverType).get();
		//		p.insertPosteID(b.getPosteId());
		OKOperationPage ok = insert.insertPosteID(b.getPosteId());
		ok.check();

		return ok;
	}

	public void clickAnnulla() 
	{
		page.getParticle(Locators.RicaricaPostepayPageMolecola.CANCELLBUTTON).visibilityOfElement(10L).click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	@Override
	public OKOperationPage eseguiRicaricaAutomaticaFinoAl(RicaricaPostepayBean b) {
		this.cardNumberInput=page.getParticle(Locators.RicaricaPostepayPageMolecola.CARDNUMBERINPUT).getElement();
		this.ownerInput=page.getParticle(Locators.RicaricaPostepayPageMolecola.ACCOUNTHOLDERINPUT).getElement();
		this.amount=page.getParticle(Locators.RicaricaPostepayPageMolecola.AMOUNT).getElement();
		this.description=page.getParticle(Locators.RicaricaPostepayPageMolecola.CAUSALEBUTTON).getElement();
		this.ricaricaAutomaticaButton=page.getParticle(Locators.RicaricaPostepayPageMolecola.RICARICAAUTOMATICABUTTON).getElement();
		this.confirm=page.getParticle(Locators.RicaricaPostepayPageMolecola.CONFIRM).getElement();
		this.addressbookLink=page.getParticle(Locators.RicaricaPostepayPageMolecola.ADDRESSBOOKLINK).getElement();
		WaitManager.get().waitShortTime();

		this.payWithPage = (PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PayWithPage", page.getDriver().getClass(), page.getLanguage());
		this.addressbookLink.click();
		WaitManager.get().waitShortTime();
		payWithPage.checkPage();
		this.payWithPage.clickOnContodue(b.getTransferTo());

		//this.cardNumberInput.sendKeys(b.getTransferTo());
		
//		this.ownerInput.sendKeys(b.getOwner());
		
		this.amount=page.getParticle(Locators.RicaricaPostepayPageMolecola.AMOUNT).getElement();
		
		this.amount.sendKeys(b.getAmount().trim());

		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}

		this.description.sendKeys(b.getDescription()); 
		System.out.println(description);

		this.ricaricaAutomaticaButton.click();

		WaitManager.get().waitMediumTime();

		this.setsoglia=page.getParticle(Locators.RicaricaPostepayPageMolecola.SETSOGLIA).getElement();	
		this.setsoglia.click();

		this.saldominimo=page.getParticle(Locators.RicaricaPostepayPageMolecola.MINIMUMBALANCE).getElement();
		org.springframework.util.Assert.isTrue(saldominimo.isDisplayed()); 
		this.saldominimo.sendKeys(b.getSaldominimo().trim()); 

		this.finoal=page.getParticle(Locators.RicaricaPostepayPageMolecola.UNTILTO).getElement();
		org.springframework.util.Assert.isTrue(finoal.isDisplayed()); 
		WaitManager.get().waitMediumTime();

		SCROLL_DIRECTION x=SCROLL_DIRECTION.DOWN;
		UIUtils.mobile().swipe((MobileDriver<?>)page.getDriver(), x, 0.9f, 0.15f, 0.9f, 0.05f, 200);
		WaitManager.get().waitMediumTime();

		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}

		WaitManager.get().waitMediumTime();
		this.confirm=page.getParticle(Locators.RicaricaPostepayPageMolecola.CONFIRM).getElement();
		confirm.click();
		return null;
	}

	@Override
	public OKOperationPage checkPopupRicaricaAutomatica() {
		page.getParticle(Locators.RicaricaPostepayPageMolecola.TITLEPOPUPRICARICAAUTO).visibilityOfElement(10L);
		this.descrizionePopup=page.getParticle(Locators.RicaricaPostepayPageMolecola.DESCRIZIONEPOPUPRICARICAAUTO).visibilityOfElement(10L);
		this.btnPopup=page.getParticle(Locators.RicaricaPostepayPageMolecola.BTNPOPUPRICARICAAUTO).visibilityOfElement(10L);
		
		String erDescrizione="Servizio temporaneamente non disponibile";
		String arDescrizione=descrizionePopup.getText();
		
		assertTrue("ER: "+erDescrizione+"AR: "+ arDescrizione, arDescrizione.equals(erDescrizione));
		btnPopup.click();
		return null;
	}

}



