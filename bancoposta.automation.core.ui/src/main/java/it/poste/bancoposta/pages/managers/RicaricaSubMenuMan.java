package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.RicaricaSubMenu;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="RicaricaSubMenu")
@Android
@IOS
public class RicaricaSubMenuMan extends LoadableComponent<RicaricaSubMenuMan> implements RicaricaSubMenu
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement closeButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ricaricaUfficioButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ricaricaPostePayButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ibanButton;


	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();

	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}