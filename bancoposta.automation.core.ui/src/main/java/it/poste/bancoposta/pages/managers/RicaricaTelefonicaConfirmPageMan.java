package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import automation.core.ui.uiobject.UiParticle;
import bean.datatable.RicaricaTelefonicaBean;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.RicaricaTelefonicaConfirmPage;
import test.automation.core.UIUtils;

import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="RicaricaTelefonicaConfirmPage")
@Android
@IOS
public class RicaricaTelefonicaConfirmPageMan extends LoadableComponent<RicaricaTelefonicaConfirmPageMan> implements RicaricaTelefonicaConfirmPage
{
	private static final String TITLE = "tutto corretto?";
	private static final String DESCRIPTION = "verifica i dati prima di procedere con il pagamento.";
	UiPage page;
	private WebElement header;
	private WebElement backButton;
	private WebElement title;
	private WebElement description;
	private WebElement payWithInfo;
	private WebElement phoneNumberInfo;
	private WebElement phoneCompanyInfo;
	private WebElement amount;
	private WebElement confirm;



	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();

		String txt=page.getParticle(Locators.RicaricaTelefonicaConfirmPageMolecola.TITLE).getElement().getText().trim().toLowerCase();

		Assert.assertTrue("controllo header � tutto corretto",txt.contains(TITLE));

		txt=page.getParticle(Locators.RicaricaTelefonicaConfirmPageMolecola.DESCRIPTION).getElement().getText().trim().toLowerCase();

		Assert.assertTrue("controllo header Verifica i dati prima di procedere con il pagamento.",txt.equals(DESCRIPTION));
	}

	public void checkData(RicaricaTelefonicaBean b) 
	{
		WaitManager.get().waitMediumTime();

		this.header=page.getParticle(Locators.RicaricaTelefonicaConfirmPageMolecola.HEADER).getElement();
		this.backButton=page.getParticle(Locators.RicaricaTelefonicaConfirmPageMolecola.BACKBUTTON).getElement();
		this.title=page.getParticle(Locators.RicaricaTelefonicaConfirmPageMolecola.TITLE).getElement();
		this.description=page.getParticle(Locators.RicaricaTelefonicaConfirmPageMolecola.DESCRIPTION).getElement();
		this.payWithInfo=page.getParticle(Locators.RicaricaTelefonicaConfirmPageMolecola.PAYWITHINFO).getElement();
		this.phoneNumberInfo=page.getParticle(Locators.RicaricaTelefonicaConfirmPageMolecola.PHONENUMBERINFO).getElement();
		this.phoneCompanyInfo=page.getParticle(Locators.RicaricaTelefonicaConfirmPageMolecola.PHONECOMPANYINFO).getElement();
		this.amount=page.getParticle(Locators.RicaricaTelefonicaConfirmPageMolecola.AMOUNT).getElement();
		this.confirm=page.getParticle(Locators.RicaricaTelefonicaConfirmPageMolecola.CONFIRM).getElement();
		

		//controllo paga con
		System.out.println(payWithInfo);
		String txt=this.payWithInfo.getText().trim().toLowerCase();
		//		System.out.println(payWithInfo.getText().trim().toLowerCase());
		//System.out.println(b + "Questa � la b dopo");
		//		System.out.println("Questa � la stampa" + b.getPayWith().toLowerCase().replace(";", " ") + txt.equals(b.getPayWith().toLowerCase().replace(";", " ")));
		Assert.assertTrue("controllo paga con;attuale:"+txt+", atteso:"+b.getPayWith().toLowerCase().replace(";", " "),txt.equals(b.getPayWith().toLowerCase().replace(";", " ")));

		//controllo importo
//		double amount=Utility.parseCurrency(this.amount.getText().trim(), Locale.ITALY);
		String amount = this.amount.getText().replaceAll("\u20AC","").trim();
		System.out.println(amount);
		System.out.println(b.getAmount());
//		double tocheck=Double.parseDouble(b.getAmount());

		Assert.assertTrue("controllo importo;attuale:"+amount+", atteso:"+b.getAmount(),amount.equals(b.getAmount()));

		WaitManager.get().waitMediumTime();

		this.confirm=page.getParticle(Locators.RicaricaTelefonicaConfirmPageMolecola.CONFIRM).getElement();
		
//		//pagina presente e campi presenti procedo con la verifica dei dati mostrati
//		if(subPage.ok())
//		{
//			subPage.checkData(b);
//		}

		//controlla paga con
		String paga="paga \u20AC "+ amount;

		Assert.assertTrue("controllo paga;attuale:"+this.confirm.getText().trim().toLowerCase()+", atteso:"+paga,this.confirm.getText().trim().toLowerCase().contains(paga));
		confirm.click();
	}

	@Override
	public void init(UiPage page) 
	{
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
