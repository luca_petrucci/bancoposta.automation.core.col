package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import io.appium.java_client.HidesKeyboard;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.RicaricaPostepayBean;
import bean.datatable.RicaricaTelefonicaBean;
import io.appium.java_client.AppiumDriver;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OKOperationPage;
import it.poste.bancoposta.pages.RicaricaTelefonicaConfirmPage;
import it.poste.bancoposta.pages.RicaricaTelefonicaPage;
import test.automation.core.UIUtils;
import test.automation.core.cmd.adb.AdbCommandPrompt;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="RicaricaTelefonicaPage")
@Android
public class RicaricaTelefonicaPageMan extends LoadableComponent<RicaricaTelefonicaPageMan> implements RicaricaTelefonicaPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancellButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement paymentTypeSummary;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement changePaymentTypeButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement addressbookLink;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement phoneNumberInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement phoneCompaignInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement confirm;
	private WebElement cancelButton;

	private WebElement numeroTelefonoRubrica;

	private WebElement popUpConsentiButton;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();

	}

	public OKOperationPage eseguiRicarica(RicaricaTelefonicaBean b) {
		WaitManager.get().waitMediumTime();

		this.header=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.HEADER).getElement();
		this.cancelButton=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.CANCELLBUTTON).getElement();
		this.paymentTypeSummary=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.PAYMENTTYPESUMMARY).getElement();
		this.changePaymentTypeButton=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.CHANGEPAYMENTTYPEBUTTON).getElement();
		this.addressbookLink=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.ADDRESSBOOKLINK).getElement();
		this.phoneNumberInput=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.PHONENUMBERINPUT).getElement();		
	
		


        this.addressbookLink.click();
        try {
        page.getParticle(Locators.RicaricaTelefonicaPageMolecola.POPUPCONSENTIBUTTON).getElement().click();
		} catch (Exception e) {
			// TODO: handle exception
		}
        this.numeroTelefonoRubrica=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.NUMEROTELEFONORUBRICA).getElement();
        WaitManager.get().waitShortTime();
        this.numeroTelefonoRubrica.click();

    
        //		this.phoneNumberInput.sendKeys(b.getPhoneNumber());
//		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}
        this.phoneCompaignInput=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.PHONECOMPAIGNINPUT).getElement();
		this.phoneCompaignInput.sendKeys(b.getPhoneCompaign());
		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}
		this.amount=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.AMOUNT).getElement();
		this.amount.sendKeys(b.getAmount());

			
		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}

		WaitManager.get().waitShortTime();
		this.confirm=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.CONFIRM).getElement();
		confirm.click();

		RicaricaTelefonicaConfirmPage ricaricaTelefonicaConfirmPage = (RicaricaTelefonicaConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaTelefonicaConfirmPage", page.getDriver().getClass(), page.getLanguage());
		
		ricaricaTelefonicaConfirmPage.checkPage();
		
		ricaricaTelefonicaConfirmPage.checkData(b);
		InsertPosteIDPage insert=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
		
		insert.checkPage();

		OKOperationPage ok=insert.insertPosteID(b.getPosteid());
		
		ok.checkPage();
		//ok.closePage();
		return ok;
//		return null;
	}

	public void clickAnnulla() 
	{
		page.getParticle(Locators.RicaricaTelefonicaPageMolecola.CANCELLBUTTON).visibilityOfElement(10L).click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	@Override
	public OKOperationPage eseguiRicaricaConClose(RicaricaTelefonicaBean b) {
		WaitManager.get().waitMediumTime();

		this.header=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.HEADER).getElement();
		this.cancelButton=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.CANCELLBUTTON).getElement();
		this.paymentTypeSummary=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.PAYMENTTYPESUMMARY).getElement();
		this.changePaymentTypeButton=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.CHANGEPAYMENTTYPEBUTTON).getElement();
		this.addressbookLink=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.ADDRESSBOOKLINK).getElement();
		this.phoneNumberInput=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.PHONENUMBERINPUT).getElement();
		this.phoneCompaignInput=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.PHONECOMPAIGNINPUT).getElement();
		this.confirm=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.CONFIRM).getElement();
		this.amount=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.AMOUNT).getElement();

		 this.addressbookLink.click();
	        try {
	        page.getParticle(Locators.RicaricaTelefonicaPageMolecola.POPUPCONSENTIBUTTON).getElement().click();
			} catch (Exception e) {
				// TODO: handle exception
			}
	        this.numeroTelefonoRubrica=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.NUMEROTELEFONORUBRICA).getElement();
	        WaitManager.get().waitShortTime();
	        this.numeroTelefonoRubrica.click();


//		this.phoneNumberInput.sendKeys(b.getPhoneNumber());
	//	this.phoneNumberInput.click();
//		Properties devName = UIUtils.ui().getCurrentProperties();
//		String adbExe = devName.getProperty("adb.path");
//		AdbCommandPrompt adbCmd = new AdbCommandPrompt(adbExe, devName.getProperty("adb.device.udid"));
//		
//		for (int i=0 ; i< b.getPhoneNumber().length(); i++) {
//			char c = b.getPhoneNumber().charAt(i);
//			adbCmd.typeText(""+c);
//	
//		}
//		
		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}

//		this.phoneCompaignInput.sendKeys(b.getPhoneCompaign());
		this.phoneCompaignInput.click();
		WaitManager.get().waitShortTime();
		
		page.getParticle(Locators.RicaricaTelefonicaPageMolecola.OPERATORE_VALUE).getElement(new Entry("value",b.getPhoneCompaign())).click();
		
		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}

		this.amount.click();
		
		
		page.getParticle(Locators.RicaricaTelefonicaPageMolecola.AMOUNT_VALUE).visibilityOfElement(5L).click();
			
		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}

		WaitManager.get().waitShortTime();

		confirm.click();

		RicaricaTelefonicaConfirmPage ricaricaTelefonicaConfirmPage = (RicaricaTelefonicaConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaTelefonicaConfirmPage", page.getDriver().getClass(), page.getLanguage());
		
		ricaricaTelefonicaConfirmPage.checkPage();
		
		ricaricaTelefonicaConfirmPage.checkData(b);
		InsertPosteIDPage insert=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
		
		insert.checkPage();

		OKOperationPage ok=insert.insertPosteID(b.getPosteid());
		
		ok.checkPage();
		ok.closePage();
		return ok;
//		return null;
	}
}
