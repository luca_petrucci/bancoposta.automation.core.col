package it.poste.bancoposta.pages.managers;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import bean.datatable.RicaricaTelefonicaBean;
import io.appium.java_client.HidesKeyboard;
import io.appium.java_client.MobileBy;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OKOperationPage;
import it.poste.bancoposta.pages.RicaricaTelefonicaConfirmPage;
import test.automation.core.UIUtils;

@PageManager(page="RicaricaTelefonicaPage")
@IOS


public class RicaricaTelefonicaPageManIOS extends RicaricaTelefonicaPageMan {
	private WebElement paymentTypeSummary;
	private WebElement phoneNumberInput;
	private WebElement phoneCompaignInput;
	private WebElement amount;
	private WebElement confirm;
	private By pickers = MobileBy.className("XCUIElementTypePickerWheel"); 
	
	public OKOperationPage eseguiRicarica(RicaricaTelefonicaBean b) {
		WaitManager.get().waitShortTime();

		this.paymentTypeSummary=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.PAYMENTTYPESUMMARY).getElement();
		this.phoneNumberInput=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.PHONENUMBERINPUT).getElement();
		
		

		this.phoneNumberInput.sendKeys(b.getPhoneNumber());
//		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}
		
		this.phoneCompaignInput=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.PHONECOMPAIGNINPUT).getElement();
		this.phoneCompaignInput.click();
		List<WebElement> pickerEls = UIUtils.ui().waitForCondition(page.getDriver(),ExpectedConditions.presenceOfAllElementsLocatedBy(pickers));
        System.out.println(pickerEls.size());
        pickerEls.get(0).sendKeys(b.getPhoneCompaign());
        
//		this.phoneCompaignInput.sendKeys(b.getPhoneCompaign());
//		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}
        
        /*serve per chiudere la tastiera */
		this.paymentTypeSummary.click();
		this.amount=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.AMOUNT).getElement();
		this.amount.click();
		pickerEls.get(0).sendKeys(b.getAmount());
//		this.amount.sendKeys(b.getAmount());
			
//		try{((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception e) {}

		WaitManager.get().waitShortTime();
		/*serve per chiudere la tastiera */
		this.paymentTypeSummary.click();
		WaitManager.get().waitShortTime();
		this.confirm=page.getParticle(Locators.RicaricaTelefonicaPageMolecola.CONFIRM).getElement();
		confirm.click();

		RicaricaTelefonicaConfirmPage ricaricaTelefonicaConfirmPage = (RicaricaTelefonicaConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaTelefonicaConfirmPage", page.getDriver().getClass(), page.getLanguage());
		
		ricaricaTelefonicaConfirmPage.checkPage();
		
		ricaricaTelefonicaConfirmPage.checkData(b);
		InsertPosteIDPage insert=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
		
		insert.checkPage();

		OKOperationPage ok=insert.insertPosteID(b.getPosteid());
		
		ok.checkPage();
		//ok.closePage();
		return ok;
//		return null;
	}

}
