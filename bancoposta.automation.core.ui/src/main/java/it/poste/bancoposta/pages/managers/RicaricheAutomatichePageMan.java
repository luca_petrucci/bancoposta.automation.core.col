package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CheckTransactionBean;
import bean.datatable.CredentialAccessBean;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.RicaricheAutomatichePage;
import test.automation.core.UIUtils;
import utils.Entry;
import utils.ObjectFinderLight;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="RicaricheAutomatichePage")
@Android
@IOS
public class RicaricheAutomatichePageMan extends LoadableComponent<RicaricheAutomatichePageMan> implements RicaricheAutomatichePage
{
	UiPage page;
	
	private List<WebElement> record2;
	private WebElement deleteButton;
	private WebElement popupOkButton;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		
	}

	public void checkEliminazioneRicarica(CredentialAccessBean s) throws Exception 
	{
		this.record2=(List<WebElement>) page.getParticle(Locators.AccountDetailPageMolecola.AUTOMATICRECHARGERESULT).getListOfElements();

		if (this.record2.size() > 0) {
			int currentSize= record2.size();

			WebElement movement = this.record2.get(0);
			try {
				// Coordinate dell'oggetto
				int coordinateX; int coordinateY; int coordinateXnew;
				org.openqa.selenium.Dimension d = movement.getSize();

				coordinateX=movement.getLocation().getX() + d.width/2;

				//					System.out.println("Elemento 1 Trovato "+coordinateX);
				coordinateXnew=movement.getLocation().getX();
				//					
				System.out.println("Elemento 2 Trovato "+coordinateXnew);
				coordinateY=movement.getLocation().getY()+d.height/2;
				//					System.out.println("Elemento 3 Trovato "+coordinateY);

				// TouchAction e imposto il driver
				TouchAction touchAction = new TouchAction((MobileDriver<?>)page.getDriver());
				//					System.out.println("Prova lo Swipe");
				touchAction.longPress(PointOption.point(coordinateX,coordinateY)).moveTo(PointOption.point(coordinateXnew,coordinateY)).release().perform();
				//					System.out.println("Lo Swipe � fatto!");
				
				automation.core.ui.WaitManager.get().waitMediumTime();

				this.deleteButton=page.getParticle(Locators.AccountDetailPageMolecola.DELETEBUTTON).getElement();
				deleteButton.click(); 

				this.popupOkButton=page.getParticle(Locators.AccountDetailPageMolecola.POPUPOKBUTTON).getElement();
				popupOkButton.click(); 


			} catch (Exception e) {
				System.out.println("Effettua prima il test PBPAPP-2959 per creare una lista ricariche automatiche");
				throw new Exception("Effettua prima il test PBPAPP-2959 per creare una lista ricariche automatiche");
			}

			WaitManager.get().waitShortTime();
			
			InsertPosteIDPage i = (InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
			i.checkPage();
			i.insertPosteID(s.getPosteid());
			
			WaitManager.get().waitHighTime();
			
			this.record2=(List<WebElement>) page.getParticle(Locators.AccountDetailPageMolecola.AUTOMATICRECHARGERESULT).getListOfElements();
			int newSize = record2.size();
			Assert.assertTrue(newSize<currentSize);

		}
	}

	public boolean RicaricaIsPresent() 
	{
		try {
			page.getParticle(Locators.AccountDetailPageMolecola.RICARICABUTTON).visibilityOfElement(10L);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void clickOnRicarica() 
	{
		page.getParticle(Locators.AccountDetailPageMolecola.RICARICABUTTON).getElement().click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
