package it.poste.bancoposta.pages.managers;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.RicaricaLaTuaPostepayChooseOperationPage;
import it.poste.bancoposta.pages.RiceviSubMenuCartaPage;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="RiceviSubMenuCartaPage")
@Android
@IOS
public class RiceviSubMenuCartaPageMan extends LoadableComponent<RiceviSubMenuCartaPageMan> implements RiceviSubMenuCartaPage
{
	UiPage page;
	
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement closeButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */	
	private WebElement ricaricaInUfficioPostale;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ricaricaQuestaPostePay;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */	

	private WebElement condividiIban;
	private WebElement ricaricaQuestaPostepayButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	
	@Override
	protected void isLoaded() throws Error {
		
	}
	
	@Override
	protected void load() 
	{
		page.get();

	}
	
	
	
	
	public RicaricaLaTuaPostepayChooseOperationPage goToRicaricaQuestaPostepay() {
		this.ricaricaQuestaPostepayButton=page.getParticle(Locators.RiceviSubMenuCartaPageMolecolas.RICARICAQUESTAPOSTEPAYBUTTON).getElement();
		

		this.ricaricaQuestaPostepayButton.click();
		
		RicaricaLaTuaPostepayChooseOperationPage p=(RicaricaLaTuaPostepayChooseOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("RicaricaLaTuaPostepayChooseOperationPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();
		return p;
		
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
	
}






