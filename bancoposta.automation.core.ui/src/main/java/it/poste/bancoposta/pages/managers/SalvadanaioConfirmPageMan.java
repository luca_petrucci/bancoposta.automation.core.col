package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import bean.datatable.QuickOperationCreationBean;
import bean.datatable.SalvadanaioDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.FAQPage;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SalvadanaioConfirmPage;
import it.poste.bancoposta.pages.SalvadanaioPage;
import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SalvadanaioConfirmPage")
@Android
@IOS
public class SalvadanaioConfirmPageMan extends LoadableComponent<SalvadanaioConfirmPageMan> implements SalvadanaioConfirmPage
{

	UiPage page;
	
	private WebElement tuttoCorretto;
	private WebElement versaConCardNumber;
	private WebElement owner;
	private WebElement amount;
	private WebElement confirmButton;
	private WebElement salvadanaioQuickOperationName;
	private WebElement annullaButton;
	private WebElement header;
	private WebElement modifyButtonQuickOperation;
	private WebElement frequency;
	private WebElement startDate;
	private WebElement endDate;
	private WebElement helpButton;
	private WebElement assistance;
	private String assistance2;
	private WebElement xbutton;
	private WebElement cancelButton;
	private WebElement h;
	private WebElement subdescription;
	private WebElement informationIcon;
	private WebElement informationMessage; 

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	@Override
	protected void isLoaded() throws Error {
		;
	} 

	@Override
	protected void load() 
	{
		page.get();

	}





	public void checkData(SalvadanaioDataBean bean) {

		//this.changePaymentButton=page.getParticle(Locators.RicaricaPostepayPageLocator.CHANGEPAYMENTTYPEBUTTON.getAndroidLocator()));
		this.tuttoCorretto=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.TUTTOCORRETTOTEXT).getElement();
		this.versaConCardNumber=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.VERSACONCARDNUMBER).getElement();
		this.owner=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.OWNER).getElement();
		this.amount=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.AMOUNT).getElement();
		this.confirmButton=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.CONFIRMBUTTON).getElement();
		
		Assert.assertTrue(tuttoCorretto.getText().trim().equals("� tutto corretto?"));

		String number = versaConCardNumber.getText().trim().toLowerCase();
		String fourDigits = number.substring(number.length() - 4);
		String featureNumber = bean.getPayWith().trim();
		String featureNumberFour = featureNumber.substring(featureNumber.length() - 4);
		Assert.assertTrue(fourDigits.equals(featureNumberFour));
		Assert.assertTrue(owner.getText().trim().equals(bean.getOwner()));
		Assert.assertTrue(amount.getText().replace( "�","").replace(",", ".").trim().equals(bean.getAmount()));

		this.confirmButton.click();	

	}

	public void checkData2(QuickOperationCreationBean bean) {

		QuickOperationCreationBean b;
		
		this.annullaButton=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.ANNULLABUTTON).getElement();
		this.header=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.HEADER).getElement();
		//this.changePaymentButton=page.getParticle(Locators.RicaricaPostepayPageLocator.CHANGEPAYMENTTYPEBUTTON.getAndroidLocator()));
		this.tuttoCorretto=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.TUTTOCORRETTOTEXT).getElement();
		this.versaConCardNumber=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.VERSACONCARDNUMBER).getElement();
		this.salvadanaioQuickOperationName=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.SALVADANAIONAMEQUICKOPERATION).getElement();
		this.amount=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.AMOUNT).getElement();
		this.confirmButton=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.CONFIRMBUTTON).getElement();
		this.modifyButtonQuickOperation=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.MODIFYBUTTONQUICKOPERATION).getElement();
		

		String s = bean.getObjectiveName().trim().toLowerCase();
		System.out.println(s);

		String c = salvadanaioQuickOperationName.getText().trim().toLowerCase();
		System.out.println(c);

		String amountPoint = amount.getText().replace("�","").replace(",", ".").trim();
		System.out.println(amountPoint);

		String amountBean = bean.getAmount().trim();
		System.out.println(amountBean);

		Assert.assertTrue(header.getText().trim().equals("accantonamento"));
		Assert.assertTrue(tuttoCorretto.getText().trim().equals("� tutto corretto?"));

		String number = versaConCardNumber.getText().trim().toLowerCase();
		String fourDigits = number.substring(number.length() - 4);
		String featureNumber = bean.getPayWith().trim();
		String featureNumberFour = featureNumber.substring(featureNumber.length() - 4);
		Assert.assertTrue(fourDigits.equals(featureNumberFour));
		Assert.assertTrue(c.equals(s));
		Assert.assertTrue(amountPoint.contentEquals(amountBean));
		Assert.assertTrue(confirmButton.getText().trim().equals("VERSA"));

		this.annullaButton.click();	
	}


	public void checkData3(SalvadanaioDataBean bean) {

		QuickOperationCreationBean b;
		this.annullaButton=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.ANNULLABUTTON).getElement();
		this.header=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.HEADER).getElement();
		//this.changePaymentButton=page.getParticle(Locators.RicaricaPostepayPageLocator.CHANGEPAYMENTTYPEBUTTON.getAndroidLocator()));
		this.tuttoCorretto=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.TUTTOCORRETTOTEXT).getElement();
		this.versaConCardNumber=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.VERSACONCARDNUMBER).getElement();
		//			this.salvadanaioQuickOperationName=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.SALVADANAIONAMEQUICKOPERATION.getAndroidLocator()));
		this.amount=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.AMOUNT).getElement();
		this.owner=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.OWNER).getElement();
		this.confirmButton=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.CONFIRMBUTTON).getElement();
		this.frequency = page.getParticle(Locators.SalvadanaioConfirmPageMolecola.FREQUENCY).getElement();
		this.startDate = page.getParticle(Locators.SalvadanaioConfirmPageMolecola.STARTDATE).getElement();
		this.endDate=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.RAGGIUNGIMENTO).getElement();
		//			this.modifyButtonQuickOperation=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.MODIFYBUTTONQUICKOPERATION.getAndroidLocator()));
		
		Assert.assertTrue(header.getText().trim().equals("Versa sull'obiettivo"));
		Assert.assertTrue(tuttoCorretto.getText().trim().equals("� tutto corretto?"));

		String number = versaConCardNumber.getText().trim().toLowerCase();
		String fourDigits = number.substring(number.length() - 4);
		String featureNumber = bean.getPayWith().trim();
		String featureNumberFour = featureNumber.substring(featureNumber.length() - 4);
		Assert.assertTrue(fourDigits.equals(featureNumberFour));

		String ownerApp = owner.getText().trim().toLowerCase();
		String ownerFeature = bean.getOwner().trim().toLowerCase();
		Assert.assertTrue("controllo owner "+ownerApp+" - feature:"+ownerFeature,ownerApp.equals(ownerFeature));

		String amountPoint = amount.getText().replace("�","").replace(",", ".").trim();		
		String amountBean = bean.getAmount().trim();
		Assert.assertTrue(amountPoint.equals(amountBean));

		String frequenza = frequency.getText().trim().toLowerCase();
		String frequenzaFeature = bean.getFrequency().trim().toLowerCase();
		Assert.assertTrue(frequenza.equals(frequenzaFeature));

		String startDateApp = startDate.getText().trim().toLowerCase();
		String startDateFeature = bean.getStartDate().trim().toLowerCase();
		Assert.assertTrue(startDateApp.equals(startDateFeature));

		String raggiungimentApp = endDate.getText().trim().toLowerCase();
		SimpleDateFormat formattedDate = new SimpleDateFormat("dd/MM/yyyy");       
		Calendar c = Calendar.getInstance();  
		try {
			c.add(Calendar.DATE, 2);  // number of days to add      
			String tomorrow = (String)(formattedDate.format(c.getTime()));
			System.out.println("Tomorrow date is " + tomorrow);
			//			String raggiungimentoFeature = bean.getEndDate().trim().toLowerCase();
			if (raggiungimentApp.equals(tomorrow)) {
				System.out.println("Le date sono uguali");
			} else {
				c.add(Calendar.DATE, 1);  // number of days to add      
				String tomorrow2 = (String)(formattedDate.format(c.getTime()));
				System.out.println("Tomorrow2 date is " + tomorrow2);
				//				String raggiungimentoFeature = bean.getEndDate().trim().toLowerCase();
				Assert.assertTrue(raggiungimentApp.equals(tomorrow2));
			}
			//			Assert.assertTrue(raggiungimentApp.equals(tomorrow));
		} catch (Exception e) {

		}




		Assert.assertTrue(confirmButton.getText().trim().equals("CONFERMA"));

		this.confirmButton.click();	
	}	



	public void checkData4(SalvadanaioDataBean bean) {

		QuickOperationCreationBean b;
		this.annullaButton=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.ANNULLABUTTON).getElement();
		this.header=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.HEADER).getElement();
		//this.changePaymentButton=page.getParticle(Locators.RicaricaPostepayPageLocator.CHANGEPAYMENTTYPEBUTTON.getAndroidLocator()));
		this.tuttoCorretto=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.TUTTOCORRETTOTEXT).getElement();
		this.versaConCardNumber=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.VERSACONCARDNUMBER).getElement();
		//	this.salvadanaioQuickOperationName=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.SALVADANAIONAMEQUICKOPERATION.getAndroidLocator()));
		this.amount=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.AMOUNT).getElement();
		this.owner=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.OWNER).getElement();
		this.confirmButton=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.CONFIRMBUTTON).getElement();
		this.frequency = page.getParticle(Locators.SalvadanaioConfirmPageMolecola.FREQUENCY).getElement();
		this.startDate = page.getParticle(Locators.SalvadanaioConfirmPageMolecola.STARTDATE).getElement();
		this.endDate=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.RAGGIUNGIMENTO).getElement();
		//	this.modifyButtonQuickOperation=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.MODIFYBUTTONQUICKOPERATION.getAndroidLocator()));



		Assert.assertTrue("Modifica automatismo",header.getText().trim().equals("Modifica automatismo"));
		Assert.assertTrue("� tutto corretto?",tuttoCorretto.getText().trim().equals("� tutto corretto?"));

		//	String number = versaConCardNumber.getText().trim().toLowerCase();
		//	String fourDigits = number.substring(number.length() - 4);
		//	String featureNumber = bean.getPayWith().trim();
		//	String featureNumberFour = featureNumber.substring(featureNumber.length() - 4);
		//	Assert.assertTrue(fourDigits.equals(featureNumberFour));

		String ownerApp = owner.getText().trim().toLowerCase();
		String ownerFeature = bean.getOwner().trim().toLowerCase();
		Assert.assertTrue("owner",ownerApp.equals(ownerFeature));

		String amountPoint = amount.getText().replace("�","").replace(",", ".").trim();		
		String amountBean = bean.getAmount().trim();
		Assert.assertTrue("amount",amountPoint.equals(amountBean));

		String frequenza = frequency.getText().trim().toLowerCase();
		String frequenzaFeature = bean.getModifiedFrequency().trim().toLowerCase();
		Assert.assertTrue("frequenza",frequenza.equals(frequenzaFeature));

		String startDateApp = startDate.getText().trim().toLowerCase();
		String startDateFeature = bean.getStartDate().trim().toLowerCase();
		Assert.assertTrue("start date",startDateApp.equals(startDateFeature));


		String raggiungimentApp = endDate.getText().trim().toLowerCase();
		//	Date today = new Date();               
		//	SimpleDateFormat formattedDate = new SimpleDateFormat("dd/MM/yyyy");       
		//	Calendar c = Calendar.getInstance();        
		//	c.add(Calendar.DATE, 2);  // number of days to add      
		//	String tomorrow = (String)(formattedDate.format(c.getTime()));
		//	System.out.println("Tomorrows date is " + tomorrow);
		////	String raggiungimentoFeature = bean.getEndDate().trim().toLowerCase();
		//	Assert.assertTrue(raggiungimentApp.equals(tomorrow));


		Assert.assertTrue("confirm button",confirmButton.getText().trim().equals("CONFERMA"));

		this.confirmButton.click();	
	}	


	public void clicksOnButtons2(){
//		SalvadanaioConfirmPage s = new SalvadanaioConfirmPage(driver, driverType);
//		s.get();
		checkPage();

		this.helpButton = page.getParticle(Locators.SalvadanaioConfirmPageMolecola.ASSISTENZABUTTON).getElement();
		Assert.assertTrue(helpButton.isDisplayed());
		helpButton.click();

		FAQPage f = (FAQPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("FAQPage", page.getDriver().getClass(), page.getLanguage());
		f.checkPage();


		WaitManager.get().waitMediumTime();

		f.clickCancelButton();

		checkPage();
		

		this.cancelButton = page.getParticle(Locators.SalvadanaioConfirmPageMolecola.ANNULLABUTTON).getElement();
		cancelButton.click();

		SalvadanaioPage salvadanaio = (SalvadanaioPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SalvadanaioPage", page.getDriver().getClass(), page.getLanguage());
		salvadanaio.checkPage();

	}


	public void checkData5(SalvadanaioDataBean bean) {

		this.tuttoCorretto=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.TUTTOCORRETTOTEXT2).getElement();
		this.subdescription=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.SUBDESCRIPTION).getElement();
		this.versaConCardNumber=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.VERSACONCARDNUMBER2).getElement();
		this.owner=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.OWNER2).getElement();
		this.amount=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.AMOUNT2).getElement();
		this.confirmButton=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.CONFIRMBUTTON2).getElement();
		this.informationIcon=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.INFORMATIONICON).getElement();
		this.informationMessage=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.INFORMATIONMESSAGE).getElement();
	

		Assert.assertTrue(tuttoCorretto.getText().trim().equals("� tutto corretto?"));
		Assert.assertTrue(subdescription.getText().trim().equals("Verifica i dati prima di procedere."));
		String number = versaConCardNumber.getText().trim().toLowerCase();
		String fourDigits = number.substring(number.length() - 4);
		String featureNumber = bean.getPayWith().trim();
		String featureNumberFour = featureNumber.substring(featureNumber.length() - 4);
		Assert.assertTrue(fourDigits.equals(featureNumberFour));
		Assert.assertTrue(owner.getText().trim().equals(bean.getOwner()));
		Assert.assertTrue(amount.getText().replace( "�","").replace(",", ".").trim().equals(bean.getAmount()));
		Assert.assertTrue(informationIcon.isDisplayed());
		String informationMessage2 = informationMessage.getText().trim();
		Assert.assertTrue(informationMessage2.equals("Confermando l�operazione, l�importo visualizzato sar� immediatamente "
				+ "disponibile sullo strumento di accredito selezionato."));

		//this.confirmButton.click();	

	}

	public void clickConferma() 
	{
		this.confirmButton=page.getParticle(Locators.SalvadanaioConfirmPageMolecola.CONFIRMBUTTON).getElement();
		confirmButton.click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

}
