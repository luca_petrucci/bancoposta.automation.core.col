package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import automation.core.ui.uiobject.UiPageRepository;
import bean.datatable.CredentialAccessBean;
import bean.datatable.SalvadanaioDataBean;
import it.poste.bancoposta.pages.*;
import it.poste.bancoposta.utils.SoftAssertion;
import test.automation.core.UIUtils;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SalvadanaioCreateObjNome")
@Android
@IOS
public class SalvadanaioCreateObjNomeMan extends LoadableComponent<SalvadanaioCreateObjNomeMan> implements SalvadanaioCreateObjNome
{

	protected UiPage page;
	private WebElement toggle;
	@Override
	protected void load() {
		page.get();
	}
	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void init(UiPage page) {
		this.page=page;
	}
	@Override
	public void checkPage(Object... params) 
	{	
		load();
		Properties t=page.getLanguage();
		
		SoftAssertion.get().getAssertion().assertThat(page.getParticle(Locators.SalvadanaioCreateObjNomeMolecola.HEADER).getElement().getText())
		.withFailMessage("header errato")
		.isEqualTo(t.getProperty("salvadanaioCreateHeaderImporto"));
		
		SoftAssertion.get().getAssertion().assertThat(page.getParticle(Locators.SalvadanaioCreateObjNomeMolecola.PERSONALIZZAOBIETTIVO).getElement().getText())
		.withFailMessage("titolo errato")
		.isEqualTo(t.getProperty("salvadanaioCreateTitleNome"));
		
		int s=page.getParticle(Locators.SalvadanaioCreateObjNomeMolecola.CATEGORYELEMENT).getListOfElements().size();
		
		org.springframework.util.Assert.isTrue(s > 0, "categorie assenti");
		
		
	}
	@Override
	public void insertNome(SalvadanaioDataBean c) 
	{
		page.getParticle(Locators.SalvadanaioCreateObjNomeMolecola.NOMEOBIETTIVO).getElement().sendKeys(c.getNomeObiettivo());
	}
	@Override
	public void clickAvanti() {
		page.getParticle(Locators.SalvadanaioCreateObjNomeMolecola.AVANTI).getElement().click();
	}
	
	
	

}
