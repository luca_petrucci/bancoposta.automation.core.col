package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import automation.core.ui.uiobject.UiPageRepository;
import bean.datatable.CredentialAccessBean;
import bean.datatable.SalvadanaioDataBean;
import it.poste.bancoposta.pages.*;
import test.automation.core.UIUtils;
import utils.DinamicData;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SalvadanaioCreateObjRiepilogo")
@Android
@IOS
public class SalvadanaioCreateObjRiepilogoMan extends LoadableComponent<SalvadanaioCreateObjRiepilogoMan> implements SalvadanaioCreateObjRiepilogo
{

	protected UiPage page;
	private WebElement toggle;
	
	@Override
	public void init(UiPage page) {
		this.page=page;
	}
	@Override
	public void checkPage(Object... params) {
		load();
		
		if(params.length > 0)
		{
			SalvadanaioDataBean b=(SalvadanaioDataBean) params[0];
			String nome=page.getParticle(Locators.SalvadanaioCreateObjRiepilogoMolecola.NOMETEXT).getElement().getText();
			
			org.springframework.util.Assert.isTrue(nome.equals(b.getNomeObiettivo()), "nome obiettivo errato nella pagina di riepilogo creazione");
			
			nome=page.getParticle(Locators.SalvadanaioCreateObjRiepilogoMolecola.LIBRETTOTEXT).getElement().getText();
			org.springframework.util.Assert.isTrue(StringUtils.isNotBlank(nome), "libretto associato errato nella pagina di riepilogo creazione");
			
			nome=page.getParticle(Locators.SalvadanaioCreateObjRiepilogoMolecola.PERIODOTEXT).getElement().getText();
			org.springframework.util.Assert.isTrue(StringUtils.isNotBlank(nome), "periodo errato nella pagina di riepilogo creazione");
			
			nome=page.getParticle(Locators.SalvadanaioCreateObjRiepilogoMolecola.CATEGORIATEXT).getElement().getText();
			org.springframework.util.Assert.isTrue(StringUtils.isNotBlank(nome), "categoria errato nella pagina di riepilogo creazione");
			
			nome=page.getParticle(Locators.SalvadanaioCreateObjRiepilogoMolecola.IMPORTOTEXT).getElement().getText();
			org.springframework.util.Assert.isTrue(nome.contains(b.getImporto2()), "importo errato nella pagina di riepilogo creazione");
			
			SimpleDateFormat f=new SimpleDateFormat("dd/MM/yyyy");
			String da=f.format(Calendar.getInstance().getTime());
			String a=(String) DinamicData.getIstance().get("SALVADANAIO_DATE_SELECTED");
			String periodo=da+" - "+a;
			
			nome=page.getParticle(Locators.SalvadanaioCreateObjRiepilogoMolecola.PERIODOTEXT).getElement().getText();
			org.springframework.util.Assert.isTrue(nome.equals(periodo), "periodo errato nella pagina di riepilogo creazione");
			
			nome=page.getParticle(Locators.SalvadanaioCreateObjRiepilogoMolecola.LIBRETTOTEXT).getElement().getText();
			org.springframework.util.Assert.isTrue(nome.equals(b.getIban()), "libretto associato errato nella pagina di riepilogo creazione");
			
		}
	}
	@Override
	protected void load() {
		page.get();
	}
	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void clickConferma() {
		page.getParticle(Locators.SalvadanaioCreateObjRiepilogoMolecola.CONFERMA).getElement().click();
	}
	@Override
	public void controlloPopupImportoMassimo() 
	{
		Properties t=page.getLanguage();
		String txt=page.getParticle(Locators.SalvadanaioCreateObjRiepilogoMolecola.POPUPERROREIMPORTOMAX).getElement().getText();
		System.out.println(t.getProperty("popupCreazioneSalvadanaioLimiteMax"));
		System.out.println(txt);
		org.springframework.util.Assert.isTrue(txt.equals(t.getProperty("popupCreazioneSalvadanaioLimiteMax")), "popup Importo massimo ragiunto errato "+txt);
	}


}
