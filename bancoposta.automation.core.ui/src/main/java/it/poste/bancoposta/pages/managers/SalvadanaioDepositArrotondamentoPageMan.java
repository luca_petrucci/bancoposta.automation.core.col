package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.SalvadanaioDataBean;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.PayWithPage;
import it.poste.bancoposta.pages.SalvadanaioDepositArrotondamentoPage;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SalvadanaioDepositArrotondamentoPage")
@Android
@IOS
public class SalvadanaioDepositArrotondamentoPageMan extends LoadableComponent<SalvadanaioDepositArrotondamentoPageMan> implements SalvadanaioDepositArrotondamentoPage
{
	UiPage page;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.getParticle(Locators.SalvadanaioDepositArrotondamentoPage.HEADER).visibilityOfElement(30L);
		page.get();
	}

	public void checkFields(SalvadanaioDataBean b) 
	{
		//versamento singolo
		page.getParticle(Locators.SalvadanaioDepositArrotondamentoPage.VERSA_DA).visibilityOfElement(10L)
		.click();
		
		WaitManager.get().waitMediumTime();
		
		PayWithPage pay=(PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PayWithPage", page.getDriver().getClass(), page.getLanguage());
		pay.checkPage();
		pay.clickBack();
		
		WaitManager.get().waitMediumTime();
		
		page.getParticle(Locators.SalvadanaioDepositArrotondamentoPage.EDIT_STRUMENTO_PAGAMENTO).visibilityOfElement(10L)
		.click();
		
		WaitManager.get().waitMediumTime();
		
		pay.checkPage();
		pay.clickBack();
		WaitManager.get().waitMediumTime();
		
		//versamento dinamico
		page.getParticle(Locators.SalvadanaioDepositArrotondamentoPage.ARROTONDAMENTO_DINAMICO).visibilityOfElement(10L)
		.click();
		
		WaitManager.get().waitMediumTime();
		
		page.getParticle(Locators.SalvadanaioDepositArrotondamentoPage.EDIT_STRUMENTO_PAGAMENTO).visibilityOfElement(10L)
		.click();
		
		WaitManager.get().waitMediumTime();
		
		pay.checkPage();
		pay.clickBack();
		WaitManager.get().waitMediumTime();
		
		page.getParticle(Locators.SalvadanaioDepositArrotondamentoPage.REGOLA_ARROTONDAMENTO).visibilityOfElement(10L)
		.click();
		
		checkArrotondamentoPopup();
		
		page.getParticle(Locators.SalvadanaioDepositArrotondamentoPage.ARROTONDA_X3).visibilityOfElement(10L)
		.click();
		
		WaitManager.get().waitShortTime();
		
		
		page.getParticle(Locators.SalvadanaioDepositArrotondamentoPage.EDIT_REGOLA_ARROTONDAMENTO).visibilityOfElement(10L)
		.click();
		
		checkArrotondamentoPopup();
		
		page.getParticle(Locators.SalvadanaioDepositArrotondamentoPage.ARROTONDA_X4).visibilityOfElement(10L)
		.click();
		
		WaitManager.get().waitShortTime();
		
	}

	public void checkArrotondamentoPopup() 
	{
		page.getParticle(Locators.SalvadanaioDepositArrotondamentoPage.ARROTONDA_X2).visibilityOfElement(10L);
		page.getParticle(Locators.SalvadanaioDepositArrotondamentoPage.ARROTONDA_X3).visibilityOfElement(10L);
		page.getParticle(Locators.SalvadanaioDepositArrotondamentoPage.ARROTONDA_X4).visibilityOfElement(10L);
		page.getParticle(Locators.SalvadanaioDepositArrotondamentoPage.ARROTONDA_X5).visibilityOfElement(10L);
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
