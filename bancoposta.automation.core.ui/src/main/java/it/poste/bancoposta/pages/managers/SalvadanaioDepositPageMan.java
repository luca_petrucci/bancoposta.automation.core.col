package it.poste.bancoposta.pages.managers;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import bean.datatable.SalvadanaioDataBean;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SalvadanaioDepositPage;
import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SalvadanaioDepositPage")
@Android
@IOS
public class SalvadanaioDepositPageMan extends LoadableComponent<SalvadanaioDepositPageMan> implements SalvadanaioDepositPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement annullaButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement assistenzaButton;


	private WebElement payWithCardNumber;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement veramentoRicorrenteTrigger;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */


	private WebElement frequencyLabel;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement startDate;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement raggiungimentoButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement dataSpecificaButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement raggiungimentoTextLabel;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement timeForEndObjectiveTextLabel;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement suggestionTextLabel;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement confirmButton;
	private WebElement versamentoRicorrente;
	private WebElement dataSpecificaLabelText;
	private WebElement dateSelection;
	private WebElement infoTextLabel;
	private WebElement everyDay;
	private WebElement everySevenDays;
	private WebElement everyFifteenDays;
	private WebElement everyThirtyDays;
	private WebElement datePopupOkButton;
	private String dateText;
	private WebElement datePopupAnnullaButton;
	private WebElement deleteButton;
	private WebElement popupTextMessage;
	private WebElement popupDeleteButton;
	private WebElement domenicaPopupButton;
	private WebElement domenicaPopupOkButton;
	private WebElement domenicaPopupText;
	private WebElement domenicaPopupAvvisoText;

	@Override
	protected void isLoaded() throws Error {

	} 


	@Override
	protected void load() 
	{
		page.get();

	}

	public void checkFields(String payWith) {
		this.payWithCardNumber=page.getParticle(Locators.SalvadanaioDepositPageMolecola.PAYWITHCARDNUMBER).getElement();
		this.amount=page.getParticle(Locators.SalvadanaioDepositPageMolecola.AMOUNT).getElement();
		WaitManager.get().waitShortTime();

		this.versamentoRicorrente=page.getParticle(Locators.SalvadanaioDepositPageMolecola.VERSAMENTORICORRENTETRIGGER).getElement();	

		WaitManager.get().waitShortTime();


		this.versamentoRicorrente.click();

		WaitManager.get().waitMediumTime();

		this.frequencyLabel=page.getParticle(Locators.SalvadanaioDepositPageMolecola.FREQUENCYLABEL).getElement();
		String frequenza = frequencyLabel.getText().trim();
		String giorno = "Ogni giorno";
		Assert.assertTrue(frequenza.equals(giorno));
		System.out.println("Il testo visualizzato come predefinito di FREQUENZA � giusto");
		this.frequencyLabel.click();

		WaitManager.get().waitShortTime();

		this.everyDay=page.getParticle(Locators.SalvadanaioDepositPageMolecola.EVERYDAYPICKEROPTION).getElement();	
		this.everySevenDays=page.getParticle(Locators.SalvadanaioDepositPageMolecola.EVERYSEVENDAYSPICKEROPTION).getElement();
		this.everyFifteenDays=page.getParticle(Locators.SalvadanaioDepositPageMolecola.EVERYFIFTEENDAYSPICKEROPTION).getElement();
		this.everyThirtyDays=page.getParticle(Locators.SalvadanaioDepositPageMolecola.EVERYTHIRTYDAYSPICKEROPTION).getElement();
		this.everyDay.click();

		WaitManager.get().waitShortTime();

		this.startDate=page.getParticle(Locators.SalvadanaioDepositPageMolecola.STARTDATE).getElement();
		this.dateText = startDate.getText();
		String oggi = "Oggi";
		Assert.assertTrue(dateText.equals(oggi));
		System.out.println("Il testo visualizzato come predefinito di A PARTIRE DA � giusto");
		this.startDate.click();

		WaitManager.get().waitShortTime();

		this.datePopupOkButton = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATEPOPUPOKBUTTON).getElement();
		this.datePopupOkButton.click();

		WaitManager.get().waitShortTime();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
		LocalDateTime now = LocalDateTime.now(); 
		String date = dtf.format(now);
		//			   System.out.println(dtf.format(now));  
		//			   System.out.println(dtf);
		//			   System.out.println(now);
		this.startDate=page.getParticle(Locators.SalvadanaioDepositPageMolecola.STARTDATE).getElement();
		dateText = startDate.getText();
		Assert.assertTrue(date.equals(dateText));

		this.raggiungimentoButton=page.getParticle(Locators.SalvadanaioDepositPageMolecola.RAGGIUNGIMENTOBUTTON).getElement();
		this.raggiungimentoTextLabel=page.getParticle(Locators.SalvadanaioDepositPageMolecola.RAGGIUNGIMENTOTEXTLABEL).getElement();
		//			this.infoTextLabel=page.getParticle(Locators.SalvadanaioDepositPageMolecola.INFOTEXTLABEL.getAndroidLocator()));
		if  (raggiungimentoTextLabel.isDisplayed()){
			this.dataSpecificaButton=page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATASPECIFICABUTTON).getElement();
			this.dataSpecificaButton.click();
		}

		WaitManager.get().waitShortTime();

		this.dataSpecificaLabelText = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATASPECIFICATEXTLABEL).getElement(); 
		this.dateSelection = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATESELECTION).getElement();	
		this.dateSelection.click();    

		WaitManager.get().waitShortTime();

		this.datePopupOkButton = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATEPOPUPOKBUTTON).getElement();
		this.datePopupOkButton.click();

		WaitManager.get().waitShortTime();

		String dateSelectionText = dateSelection.getText().trim();
		Assert.assertTrue(date.equals(dateSelectionText));
		System.out.println("La data visualizzata per l'interruzione del versamento ricorrente � giusta");

	}


	public void checkFieldsAccantonamentoRicorrente(String payWith) {
		//SalvadanaioDepositPage r = new SalvadanaioDepositPage(driver, driverType).get();

		this.payWithCardNumber=page.getParticle(Locators.SalvadanaioDepositPageMolecola.PAYWITHCARDNUMBER).getElement();
		this.amount=page.getParticle(Locators.SalvadanaioDepositPageMolecola.AMOUNT).getElement();
		WaitManager.get().waitShortTime();

		this.versamentoRicorrente=page.getParticle(Locators.SalvadanaioDepositPageMolecola.VERSAMENTORICORRENTETRIGGER).getElement();	

		WaitManager.get().waitShortTime();


		this.versamentoRicorrente.click();

		WaitManager.get().waitMediumTime();

		this.frequencyLabel=page.getParticle(Locators.SalvadanaioDepositPageMolecola.FREQUENCYLABEL).getElement();
		String frequenza = frequencyLabel.getText().trim();
		String giorno = "Ogni giorno";
		Assert.assertTrue(frequenza.equals(giorno));
		System.out.println("Il testo visualizzato come predefinito di FREQUENZA � giusto");
		this.frequencyLabel.click();

		WaitManager.get().waitShortTime();

		this.everyDay=page.getParticle(Locators.SalvadanaioDepositPageMolecola.EVERYDAYPICKEROPTION).getElement();	
		this.everySevenDays=page.getParticle(Locators.SalvadanaioDepositPageMolecola.EVERYSEVENDAYSPICKEROPTION).getElement();
		this.everyFifteenDays=page.getParticle(Locators.SalvadanaioDepositPageMolecola.EVERYFIFTEENDAYSPICKEROPTION).getElement();
		this.everyThirtyDays=page.getParticle(Locators.SalvadanaioDepositPageMolecola.EVERYTHIRTYDAYSPICKEROPTION).getElement();
		this.everyDay.click();

		WaitManager.get().waitShortTime();

		this.startDate=page.getParticle(Locators.SalvadanaioDepositPageMolecola.STARTDATE).getElement();
		this.dateText = startDate.getText();
		String oggi = "Oggi";
		Assert.assertTrue(dateText.equals(oggi));
		System.out.println("Il testo visualizzato come predefinito di A PARTIRE DA � giusto");
		this.startDate.click();

		WaitManager.get().waitShortTime();

		this.datePopupOkButton = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATEPOPUPOKBUTTON).getElement();
		this.datePopupOkButton.click();

		WaitManager.get().waitShortTime();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
		LocalDateTime now = LocalDateTime.now();
		String date2 = dtf.format(now); 
		this.startDate=page.getParticle(Locators.SalvadanaioDepositPageMolecola.STARTDATE).getElement();
		this.dateText = startDate.getText();
		//			String oggi2 = "Oggi";
		Assert.assertTrue(dateText.equals(date2));
		System.out.println("Il testo visualizzato come predefinito di A PARTIRE DA � giusto");

		this.raggiungimentoButton=page.getParticle(Locators.SalvadanaioDepositPageMolecola.RAGGIUNGIMENTOBUTTON).getElement();
		this.raggiungimentoTextLabel=page.getParticle(Locators.SalvadanaioDepositPageMolecola.RAGGIUNGIMENTOTEXTLABEL).getElement();
		//			this.infoTextLabel=page.getParticle(Locators.SalvadanaioDepositPageMolecola.INFOTEXTLABEL.getAndroidLocator()));
		if  (raggiungimentoTextLabel.isDisplayed()){
			this.dataSpecificaButton=page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATASPECIFICABUTTON).getElement();
			this.dataSpecificaButton.click();
		}

		WaitManager.get().waitShortTime();

		this.dataSpecificaLabelText = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATASPECIFICATEXTLABEL).getElement(); 
		this.dateSelection = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATESELECTION).getElement();	
		this.dateSelection.click();    

		WaitManager.get().waitShortTime();

		WebElement selectDate;
		int i;	
		List<WebElement> dateDisponibili = page.getParticle(Locators.SalvadanaioPageMolecola.SELECTDATE).getListOfElements();
		if (dateDisponibili.size() >= 2) {
			selectDate =page.getParticle(Locators.SalvadanaioPageMolecola.SELECTDATE).getListOfElements().get(1);
			selectDate.click();

			this.datePopupOkButton = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATEPOPUPOKBUTTON).getElement();
			this.datePopupOkButton.click();
		}
		else {
			WebElement nextArrow = page.getParticle(Locators.SalvadanaioPageMolecola.NEXTARROW).getElement();
			nextArrow.click();

			WaitManager.get().waitMediumTime();

			selectDate = page.getParticle(Locators.SalvadanaioPageMolecola.SELECTDATE).getListOfElements().get(1);
			selectDate.click();	

			this.datePopupOkButton = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATEPOPUPOKBUTTON).getElement();
			this.datePopupOkButton.click();
		}

		SimpleDateFormat formattedDate = new SimpleDateFormat("dd/MM/yyyy");       
		Calendar c = Calendar.getInstance();        
		c.add(Calendar.DATE, 2);  // number of days to add      
		String tomorrow = (String)(formattedDate.format(c.getTime()));
		System.out.println("Tomorrows date is " + tomorrow);


		this.dateSelection = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATESELECTION).visibilityOfElement(3L);					
		String dateSelectionModified =dateSelection.getText().trim().toLowerCase();
		Assert.assertTrue(dateSelectionModified.equals(tomorrow));

		//				this.datePopupAnnullaButton = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATEPOPUPOKBUTTON.getAndroidLocator()));
		//				this.datePopupAnnullaButton.click();
		//				
		//				WaitManager.get().waitShortTime();
		//				this.raggiungimentoButton.click();
		//				
		////			String dateSelectionText = dateSelection.getText().trim();
		////			Assert.assertTrue(date.equals(dateSelectionText));
		//			System.out.println("La data visualizzata per l'interruzione del versamento ricorrente � giusta");



		//		String cardNumber = payWithCardNumber.getText().trim().toLowerCase();
		//		String cardNumber4 = cardNumber.substring(cardNumber.length() - 4);
		//		String pay = payWith;
		//		System.out.println(cardNumber4);		
	}



	public void modifyAccantonamentoRicorrente(SalvadanaioDataBean bean) {
		this.payWithCardNumber=page.getParticle(Locators.SalvadanaioDepositPageMolecola.PAYWITHCARDNUMBER).getElement();
		this.amount=page.getParticle(Locators.SalvadanaioDepositPageMolecola.AMOUNT).getElement();
		String ammontare = amount.getText().trim().toLowerCase().replace(",", ".");
		Assert.assertTrue(ammontare.equals(bean.getAmount()));

		this.amount.sendKeys(bean.getAmount2());
		WaitManager.get().waitShortTime();
		this.amount.sendKeys(bean.getAmount());

		this.frequencyLabel=page.getParticle(Locators.SalvadanaioDepositPageMolecola.FREQUENCYLABEL).getElement();
		String frequenza = frequencyLabel.getText().trim();
		String giorno = "Ogni giorno";
		Assert.assertTrue(frequenza.equals(giorno));
		System.out.println("Il testo visualizzato come predefinito di FREQUENZA � giusto");
		this.frequencyLabel.click();

		WaitManager.get().waitShortTime();

		this.everyDay=page.getParticle(Locators.SalvadanaioDepositPageMolecola.EVERYDAYPICKEROPTION).getElement();	
		this.everySevenDays=page.getParticle(Locators.SalvadanaioDepositPageMolecola.EVERYSEVENDAYSPICKEROPTION).getElement();
		this.everyFifteenDays=page.getParticle(Locators.SalvadanaioDepositPageMolecola.EVERYFIFTEENDAYSPICKEROPTION).getElement();
		this.everyThirtyDays=page.getParticle(Locators.SalvadanaioDepositPageMolecola.EVERYTHIRTYDAYSPICKEROPTION).getElement();
		this.everySevenDays.click();

		WaitManager.get().waitShortTime();

		this.startDate=page.getParticle(Locators.SalvadanaioDepositPageMolecola.STARTDATE).getElement();
		this.dateText = startDate.getText();
		String oggi = "Oggi";
		Assert.assertTrue(dateText.equals(oggi));
		System.out.println("Il testo visualizzato come predefinito di A PARTIRE DA � giusto");
		this.startDate.click();

		WaitManager.get().waitShortTime();


		this.datePopupOkButton = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATEPOPUPOKBUTTON).getElement();
		this.datePopupOkButton.click();

		WaitManager.get().waitShortTime();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
		LocalDateTime now = LocalDateTime.now();
		String date2 = dtf.format(now); 
		this.startDate=page.getParticle(Locators.SalvadanaioDepositPageMolecola.STARTDATE).getElement();
		this.dateText = startDate.getText();
		//			String oggi2 = "Oggi";
		Assert.assertTrue(dateText.equals(date2));
		System.out.println("Il testo visualizzato come predefinito di A PARTIRE DA � giusto");

		//			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
		//			   LocalDateTime now = LocalDateTime.now(); 
		//			   String date = dtf.format(now);
		////			   System.out.println(dtf.format(now));  
		////			   System.out.println(dtf);
		////			   System.out.println(now);
		//			   this.startDate=page.getParticle(Locators.SalvadanaioDepositPageMolecola.STARTDATE.getAndroidLocator()));
		//			   dateText = startDate.getText();
		//			   Assert.assertTrue(date.equals(dateText));

		this.raggiungimentoButton=page.getParticle(Locators.SalvadanaioDepositPageMolecola.RAGGIUNGIMENTOBUTTON).getElement();
		try {
			this.raggiungimentoTextLabel=page.getParticle(Locators.SalvadanaioDepositPageMolecola.RAGGIUNGIMENTOTEXTLABEL).getElement();
			this.dataSpecificaButton=page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATASPECIFICABUTTON).getElement();
			this.dataSpecificaButton.click();
		} catch (Exception e ) {
			this.raggiungimentoButton.click();
			WaitManager.get().waitShortTime();
			this.raggiungimentoTextLabel=page.getParticle(Locators.SalvadanaioDepositPageMolecola.RAGGIUNGIMENTOTEXTLABEL).getElement();				
			System.out.println("Il testo del raggiungimento � presente");
			this.dataSpecificaButton=page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATASPECIFICABUTTON).getElement();
			this.dataSpecificaButton.click();
		}
		//			this.infoTextLabel=page.getParticle(Locators.SalvadanaioDepositPageMolecola.INFOTEXTLABEL.getAndroidLocator()));
		//			if  (raggiungimentoTextLabel.isDisplayed()){
		//				
		//			} else {
		//				
		//			}

		WaitManager.get().waitShortTime();

		this.dataSpecificaLabelText = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATASPECIFICATEXTLABEL).getElement(); 
		this.dateSelection = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATESELECTION).getElement();	
		this.dateSelection.click();    

		WaitManager.get().waitShortTime();

		WebElement selectDate;
		int i;

		List<WebElement> dateDisponibili = page.getParticle(Locators.SalvadanaioPageMolecola.SELECTDATE).getListOfElements();

		//se la prossima data non � di domenica la seleziono
		LocalDateTime plus2=now.plusDays(2);
		DayOfWeek d = plus2.getDayOfWeek();

		if (dateDisponibili.size() < 2) 
		{
			WebElement nextArrow = page.getParticle(Locators.SalvadanaioPageMolecola.NEXTARROW).getElement();
			nextArrow.click();

			WaitManager.get().waitMediumTime();

		}


		if(d != DayOfWeek.SUNDAY)
		{
			selectDate =page.getParticle(Locators.SalvadanaioPageMolecola.SELECTDATE).getListOfElements().get(1);
			selectDate.click();
		}
		else
		{
			selectDate =page.getParticle(Locators.SalvadanaioPageMolecola.SELECTDATE).getListOfElements().get(0);
			selectDate.click();
		}

		this.datePopupOkButton = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATEPOPUPOKBUTTON).getElement();
		this.datePopupOkButton.click();


		SimpleDateFormat formattedDate = new SimpleDateFormat("dd/MM/yyyy");       
		Calendar c = Calendar.getInstance();        
		//c.add(Calendar.DATE, 2);  // number of days to add      
		String tomorrow = (String)(formattedDate.format(c.getTime()));
		//System.out.println("Tomorrows date is " + tomorrow);

		this.dateSelection = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATESELECTION).getElement();					
		String dateSelectionModified =dateSelection.getText().trim().toLowerCase();
		Assert.assertTrue(!dateSelectionModified.equals(tomorrow));

		WaitManager.get().waitShortTime();

		page.getParticle(Locators.SalvadanaioDepositPageMolecola.RAGGIUNGIMENTOBUTTON).getElement()
		.click();

		WaitManager.get().waitShortTime();
		//				this.raggiungimentoButton.click();

		//			String dateSelectionText = dateSelection.getText().trim();
		//			Assert.assertTrue(date.equals(dateSelectionText));
		System.out.println("La data visualizzata per l'interruzione del versamento ricorrente � giusta");



		//		String cardNumber = payWithCardNumber.getText().trim().toLowerCase();
		//		String cardNumber4 = cardNumber.substring(cardNumber.length() - 4);
		//		String pay = bean.getPayWith();
		//		System.out.println(cardNumber4);	

		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 1000);

		//		switch(this.driverType)
		//		{
		//		case 0://android driver
		//			this.confirmButton=page.getParticle(Locators.SalvadanaioDepositPageMolecola.CONFIRMBUTTON).getElement();
		//
		//			break;
		//		case 1://ios driver
		//			break;
		//		case 2://web driver
		//			break;
		//		}
		this.confirmButton=page.getParticle(Locators.SalvadanaioDepositPageMolecola.CONFIRMBUTTON).getElement();
		this.confirmButton.click();
	}


	public void modifyAccantonamentoRicorrenteDeleteAccantonamento(SalvadanaioDataBean bean) {
		this.payWithCardNumber=page.getParticle(Locators.SalvadanaioDepositPageMolecola.PAYWITHCARDNUMBER).getElement();
		this.amount=page.getParticle(Locators.SalvadanaioDepositPageMolecola.AMOUNT).getElement();
		//			String ammontare = amount.getText().trim().toLowerCase().replace(",", ".");
		//			Assert.assertTrue(ammontare.equals(bean.getAmount()));

		this.amount.sendKeys(bean.getAmount2());

		this.frequencyLabel=page.getParticle(Locators.SalvadanaioDepositPageMolecola.FREQUENCYLABEL).getElement();
		String frequenza = frequencyLabel.getText().trim();
		String giorno = "Ogni giorno";
		String setteGiorni = "Ogni 7 giorni";
		if (frequenza.equals(giorno)) {
			this.timeForEndObjectiveTextLabel = page.getParticle(Locators.SalvadanaioDepositPageMolecola.TIMEFORENDOBJECTIVETEXTLABEL).getElement();
			String endObjText = timeForEndObjectiveTextLabel.getText().trim();
			String endObjText2 = "Raggiungerai il tuo obiettivo fra 358 settimane, dopo la data di scadenza che hai impostato!";
			//				Assert.assertTrue(endObjText.equals(endObjText2));

			this.suggestionTextLabel = page.getParticle(Locators.SalvadanaioDepositPageMolecola.SUGGESTIONTEXTLABEL).getElement();
			String suggestionText = suggestionTextLabel.getText().trim().replace(" ", "");
			//				String suggestionTextToCompare = "Vuoiraggiungerloprima?Aumental'importoolafrequenzadelversamentoricorrente,oppurefaiversamentiaggiuntivisultuoobiettivo.";
			//				Assert.assertTrue(suggestionText.equals(suggestionTextToCompare));

			this.amount.sendKeys(bean.getAmount3());

			this.timeForEndObjectiveTextLabel = page.getParticle(Locators.SalvadanaioDepositPageMolecola.TIMEFORENDOBJECTIVETEXTLABEL).getElement();
			String endObjText3 = timeForEndObjectiveTextLabel.getText().trim();
			String endObjText4 = "Ottima scelta! Raggiungerai il tuo obiettivo fra 1 giorno.";
			Assert.assertTrue(endObjText3.equals(endObjText4));

			this.suggestionTextLabel = page.getParticle(Locators.SalvadanaioDepositPageMolecola.SUGGESTIONTEXTLABEL).getElement();
			String suggestionText3 = suggestionTextLabel.getText().trim().replace(" ", "");
			//				String suggestionTextToCompare3 = "Vuoiraggiungerloprima?Aumental'importoolafrequenzadelversamentoricorrente,oppurefaiversamentiaggiuntivisultuoobiettivo.";
			//				Assert.assertTrue(suggestionText3.equals(suggestionTextToCompare3));

		} else if (frequenza.equals(setteGiorni)) {
			this.timeForEndObjectiveTextLabel = page.getParticle(Locators.SalvadanaioDepositPageMolecola.TIMEFORENDOBJECTIVETEXTLABEL).getElement();
			String endObjTextModified = timeForEndObjectiveTextLabel.getText().trim();
			String endObjText2Modified = "Raggiungerai il tuo obiettivo fra 2500 settimane, dopo la data di scadenza che hai impostato!";
			Assert.assertTrue(endObjTextModified.equals(endObjText2Modified));

			this.suggestionTextLabel = page.getParticle(Locators.SalvadanaioDepositPageMolecola.SUGGESTIONTEXTLABEL).getElement();
			String suggestionText = suggestionTextLabel.getText().trim().replace(" ", "");
			String suggestionTextToCompare = "Vuoiraggiungerloprima?Aumental'importoolafrequenzadelversamentoricorrente,oppurefaiversamentiaggiuntivisultuoobiettivo.";
			Assert.assertTrue(suggestionText.equals(suggestionTextToCompare));

			this.amount.sendKeys(bean.getAmount3());


			this.timeForEndObjectiveTextLabel = page.getParticle(Locators.SalvadanaioDepositPageMolecola.TIMEFORENDOBJECTIVETEXTLABEL).getElement();
			String endObjTextModified2 = timeForEndObjectiveTextLabel.getText().trim();
			String endObjText2Modified2 = "Raggiungerai il tuo obiettivo fra 4 giorni, dopo la data di scadenza che hai impostato!";
			Assert.assertTrue(endObjTextModified2.equals(endObjText2Modified2));

			this.suggestionTextLabel = page.getParticle(Locators.SalvadanaioDepositPageMolecola.SUGGESTIONTEXTLABEL).getElement();
			String suggestionText3 = suggestionTextLabel.getText().trim().replace(" ", "");
			String suggestionTextToCompare3 = "Vuoiraggiungerloprima?Aumental'importoolafrequenzadelversamentoricorrente,oppurefaiversamentiaggiuntivisultuoobiettivo.";
			Assert.assertTrue(suggestionText3.equals(suggestionTextToCompare3));

		}



		//			this.frequencyLabel=page.getParticle(Locators.SalvadanaioDepositPageMolecola.FREQUENCYLABEL.getAndroidLocator()));
		//			String frequenza = frequencyLabel.getText().trim();
		//			String giorno = "Ogni giorno";
		//			try {
		//				Assert.assertTrue(frequenza.equals(giorno));
		//				System.out.println("Il testo visualizzato come predefinito di FREQUENZA � giusto OGNI GIORNO");
		//			} catch (Exception e) {
		//				String setteGiorni = "Ogni giorno";
		//			}


		//			this.frequencyLabel.click();
		//			
		//			WaitManager.get().waitShortTime();
		//			
		//			this.everyDay=page.getParticle(Locators.SalvadanaioDepositPageMolecola.EVERYDAYPICKEROPTION.getAndroidLocator()));	
		//			this.everySevenDays=page.getParticle(Locators.SalvadanaioDepositPageMolecola.EVERYSEVENDAYSPICKEROPTION.getAndroidLocator()));
		//			this.everyFifteenDays=page.getParticle(Locators.SalvadanaioDepositPageMolecola.EVERYFIFTEENDAYSPICKEROPTION.getAndroidLocator()));
		//			this.everyThirtyDays=page.getParticle(Locators.SalvadanaioDepositPageMolecola.EVERYTHIRTYDAYSPICKEROPTION.getAndroidLocator()));
		//			this.everySevenDays.click();
		//			
		//			WaitManager.get().waitShortTime();

		this.startDate=page.getParticle(Locators.SalvadanaioDepositPageMolecola.STARTDATE).getElement();
		this.dateText = startDate.getText();
		String oggi = "Oggi";
		//			Assert.assertTrue(dateText.equals(oggi));
		System.out.println("Il testo visualizzato come predefinito di A PARTIRE DA � giusto");
		this.startDate.click();

		WaitManager.get().waitShortTime();



		this.datePopupOkButton = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATEPOPUPOKBUTTON).getElement();
		this.datePopupOkButton.click();

		WaitManager.get().waitShortTime();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
		LocalDateTime now = LocalDateTime.now();
		String date2 = dtf.format(now); 
		this.startDate=page.getParticle(Locators.SalvadanaioDepositPageMolecola.STARTDATE).getElement();
		this.dateText = startDate.getText();
		String oggi2 = "Oggi";
		Assert.assertTrue(dateText.equals(date2));
		System.out.println("Il testo visualizzato come predefinito di A PARTIRE DA � giusto");

		//			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
		//			   LocalDateTime now = LocalDateTime.now(); 
		//			   String date = dtf.format(now);
		////			   System.out.println(dtf.format(now));  
		////			   System.out.println(dtf);
		////			   System.out.println(now);
		//			   this.startDate=page.getParticle(Locators.SalvadanaioDepositPageMolecola.STARTDATE.getAndroidLocator()));
		//			   dateText = startDate.getText();
		//			   Assert.assertTrue(date.equals(dateText));

		this.raggiungimentoButton=page.getParticle(Locators.SalvadanaioDepositPageMolecola.RAGGIUNGIMENTOBUTTON).getElement();
		//			this.raggiungimentoTextLabel=page.getParticle(Locators.SalvadanaioDepositPageMolecola.RAGGIUNGIMENTOTEXTLABEL.getAndroidLocator()));
		//			this.infoTextLabel=page.getParticle(Locators.SalvadanaioDepositPageMolecola.INFOTEXTLABEL.getAndroidLocator()));
		this.raggiungimentoButton.click();
		try {
			this.raggiungimentoTextLabel=page.getParticle(Locators.SalvadanaioDepositPageMolecola.RAGGIUNGIMENTOTEXTLABEL).getElement();
			if  (raggiungimentoTextLabel.isDisplayed()){
				this.dataSpecificaButton=page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATASPECIFICABUTTON).getElement();
				this.dataSpecificaButton.click();
			}
		} catch (Exception e) {
			this.raggiungimentoButton.click();
			this.dataSpecificaButton.click();
		}

		//			this.infoTextLabel=page.getParticle(Locators.SalvadanaioDepositPageMolecola.INFOTEXTLABEL.getAndroidLocator()));

		WaitManager.get().waitShortTime();

		this.dataSpecificaLabelText = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATASPECIFICATEXTLABEL).getElement(); 
		this.dateSelection = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATESELECTION).getElement();	
		this.dateSelection.click();    

		WaitManager.get().waitShortTime();

		WebElement selectDate;
		int i;
		try {	
			List<WebElement> dateDisponibili = page.getParticle(Locators.SalvadanaioPageMolecola.SELECTDATE).getListOfElements();
			if (dateDisponibili.size() >= 2) {
				selectDate =page.getParticle(Locators.SalvadanaioPageMolecola.SELECTDATE).getListOfElements().get(1);
				selectDate.click();
			}

		} catch (Exception e) {
			WebElement nextArrow = page.getParticle(Locators.SalvadanaioPageMolecola.NEXTARROW).getElement();
			nextArrow.click();

			WaitManager.get().waitMediumTime();

			selectDate = page.getParticle(Locators.SalvadanaioPageMolecola.SELECTDATE).getListOfElements().get(1);
			selectDate.click();	
		}

		this.datePopupOkButton = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATEPOPUPOKBUTTON).getElement();
		this.datePopupOkButton.click();



		SimpleDateFormat formattedDate = new SimpleDateFormat("dd/MM/yyyy");       
		Calendar c = Calendar.getInstance();        
		c.add(Calendar.DATE, 2);  // number of days to add      
		String tomorrow = (String)(formattedDate.format(c.getTime()));
		System.out.println("Tomorrows date is " + tomorrow);

		this.dateSelection = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATESELECTION).getElement();					
		String dateSelectionModified =dateSelection.getText().trim().toLowerCase();
		Assert.assertTrue(dateSelectionModified.equals(tomorrow));

		WaitManager.get().waitShortTime();
		//				this.raggiungimentoButton.click();

		//			String dateSelectionText = dateSelection.getText().trim();
		//			Assert.assertTrue(date.equals(dateSelectionText));
		System.out.println("La data visualizzata per l'interruzione del versamento ricorrente � giusta");



		String cardNumber = payWithCardNumber.getText().trim().toLowerCase();
		String cardNumber4 = cardNumber.substring(cardNumber.length() - 4);
		String pay = bean.getPayWith();
		System.out.println(cardNumber4);	

		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 1000);

		this.deleteButton=page.getParticle(Locators.SalvadanaioDepositPageMolecola.DELETEBUTTON).getElement();

		this.deleteButton.click();

		try {		
			domenicaPopupOkButton = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DOMENICAPOPUPOKBUTTON).getElement();
			domenicaPopupText = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DOMENICAPOPUPTEXT).getElement();
			domenicaPopupAvvisoText = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DOMENICAPOPUPAVVISOTEXT).getElement();
			if (domenicaPopupText.getText().trim().toLowerCase().equals("Domenica non selezionabile")) {
				this.domenicaPopupOkButton.click();
			}


			this.dateSelection = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATESELECTION).getElement();	
			this.dateSelection.click();    

			WaitManager.get().waitShortTime();


			try {	
				List<WebElement> dateDisponibili = page.getParticle(Locators.SalvadanaioPageMolecola.SELECTDATE).getListOfElements();
				if (dateDisponibili.size() >= 3) {
					selectDate =page.getParticle(Locators.SalvadanaioPageMolecola.SELECTDATE).getListOfElements().get(2);
					selectDate.click();
				}

			} catch (Exception e) {
				WebElement nextArrow = page.getParticle(Locators.SalvadanaioPageMolecola.NEXTARROW).getElement();
				nextArrow.click();

				WaitManager.get().waitMediumTime();

				selectDate = page.getParticle(Locators.SalvadanaioPageMolecola.SELECTDATE).getListOfElements().get(1);
				selectDate.click();	
			}

			this.datePopupOkButton = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATEPOPUPOKBUTTON).getElement();
			this.datePopupOkButton.click();


			formattedDate = new SimpleDateFormat("dd/MM/yyyy");       
			c = Calendar.getInstance();        
			c.add(Calendar.DATE, 3);  // number of days to add      
			tomorrow = (String)(formattedDate.format(c.getTime()));
			System.out.println("Tomorrows date is " + tomorrow);

			this.dateSelection = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATESELECTION).getElement();					
			dateSelectionModified =dateSelection.getText().trim().toLowerCase();
			Assert.assertTrue(dateSelectionModified.equals(tomorrow));
		} catch (Exception t) {


		}

		WaitManager.get().waitShortTime();

		this.popupTextMessage=page.getParticle(Locators.SalvadanaioDepositPageMolecola.POPUPTEXTMESSAGE).getElement();
		this.popupDeleteButton=page.getParticle(Locators.SalvadanaioDepositPageMolecola.POPUPDELETEBUTTON).getElement();


		String popUpTextMessage2 = popupTextMessage.getText().trim();
		String popUpTextCompare = "Sei sicuro di voler eliminare il versamento ricorrente?";
		Assert.assertTrue(popUpTextMessage2.equals(popUpTextCompare));

		this.popupDeleteButton.click();
	}



	public void effettuaAccantonamento(String amount) {
		this.payWithCardNumber=page.getParticle(Locators.SalvadanaioDepositPageMolecola.PAYWITHCARDNUMBER).getElement();
		this.amount=page.getParticle(Locators.SalvadanaioDepositPageMolecola.AMOUNT).getElement();			
		this.confirmButton=page.getParticle(Locators.SalvadanaioDepositPageMolecola.CONFIRMBUTTON).getElement();

		this.amount.sendKeys(amount);
		this.confirmButton.click();
	}


	public void effettuaAccantonamentoRicorrente(String amount, String payWith, String objectiveName) {

		checkFieldsAccantonamentoRicorrente(payWith);

		this.payWithCardNumber=page.getParticle(Locators.SalvadanaioDepositPageMolecola.PAYWITHCARDNUMBER).getElement();
		this.amount=page.getParticle(Locators.SalvadanaioDepositPageMolecola.AMOUNT).getElement();			
		this.confirmButton=page.getParticle(Locators.SalvadanaioDepositPageMolecola.CONFIRMBUTTON).getElement();



		this.amount.sendKeys(amount);

		WaitManager.get().waitMediumTime();

		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 1000);
		this.confirmButton.click();
		boolean popupEsistente = false;

		try {
			this.domenicaPopupOkButton=page.getParticle(Locators.SalvadanaioDepositPageMolecola.POPUPDOMENICAOKBUTTON).getElement();
			this.domenicaPopupOkButton.click();
			popupEsistente = true;


		} catch (Exception e) {
			popupEsistente = false;		
		}

		if (popupEsistente == true) {
			WebElement selectDate;
			int i;
			this.dateSelection = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATESELECTION).getElement();	
			this.dateSelection.click(); 


			try {	
				List<WebElement> dateDisponibili = page.getParticle(Locators.SalvadanaioPageMolecola.SELECTDATE).getListOfElements();
				if (dateDisponibili.size() >= 3) {
					selectDate =page.getParticle(Locators.SalvadanaioPageMolecola.SELECTDATE).getListOfElements().get(2);
					selectDate.click();
				}

			} catch (Exception e) {
				WebElement nextArrow = page.getParticle(Locators.SalvadanaioPageMolecola.NEXTARROW).getElement();
				nextArrow.click();

				WaitManager.get().waitMediumTime();

				selectDate = page.getParticle(Locators.SalvadanaioPageMolecola.SELECTDATE).getListOfElements().get(1);
				selectDate.click();	
			}

			this.datePopupOkButton = page.getParticle(Locators.SalvadanaioDepositPageMolecola.DATEPOPUPOKBUTTON).getElement();
			this.datePopupOkButton.click();
			UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 1000);
			this.confirmButton.click();		
		}

	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}



}
