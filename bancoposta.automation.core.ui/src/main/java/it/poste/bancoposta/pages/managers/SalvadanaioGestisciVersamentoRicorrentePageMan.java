package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import bean.datatable.SalvadanaioDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OKOperationPage;
import it.poste.bancoposta.pages.SalvadanaioGestisciVersamentoRicorrentePage;

import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SalvadanaioGestisciVersamentoRicorrentePage")
@Android
@IOS
public class SalvadanaioGestisciVersamentoRicorrentePageMan extends LoadableComponent<SalvadanaioGestisciVersamentoRicorrentePageMan> implements SalvadanaioGestisciVersamentoRicorrentePage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement assistenzaButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement payWith;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement frequency;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement startDate;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement endDate;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement modifyDeleteButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement ricorrenzaAttivaText;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement prossimoVersamento;
	

	@Override
	protected void isLoaded() throws Error {
		
	} 

	@Override
	protected void load() 
	{
		WaitManager.get().waitShortTime();
		
		page.get();
	}

	public void checkData(SalvadanaioDataBean bean) 
	{
		
		this.backButton=page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.BACKBUTTON).getElement();
		this.header=page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.HEADER).getElement();
		this.assistenzaButton=page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.ASSISTENZABUTTON).getElement();
		this.payWith=page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.PAYWITH).getElement();
		//			this.amount=page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.VERSACONCARDNUMBER.getAndroidLocator()));
		//			this.salvadanaioQuickOperationName=page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.SALVADANAIONAMEQUICKOPERATION.getAndroidLocator()));
		this.amount=page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.AMOUNT).getElement();
		//			this.owner=page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.OWNER.getAndroidLocator()));
		this.modifyDeleteButton=page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.MODIFICAELIMINABUTTON).getElement();
		this.frequency = page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.FREQUENCY).getElement();
//		this.startDate = page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.STARTDATE).getElement();
//		this.endDate=page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.ENDDATE).getElement();
//		//			this.ricorrenzaAttivaText=page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.RICORRENZAATTIVA.getAndroidLocator()));
//		this.prossimoVersamento=page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.PROSSIMOVERSAMENTO).getElement();
//		

		String headerText = header.getText().trim();
		Assert.assertTrue(headerText.equals("Gestisci automatismi"));

//		String number = payWith.getText().trim().toLowerCase();
//		String fourDigits = number.substring(number.length() - 4);
//		String featureNumber = bean.getPayWith().trim();
//		String featureNumberFour = featureNumber.substring(featureNumber.length() - 4);
//		Assert.assertTrue(fourDigits.equals(featureNumberFour));

		String amountText = amount.getText().replace("�","").replace(",", ".").trim();		
		String amountBean = bean.getAmount().trim();
		Assert.assertTrue("amount automatismo errato "+amountText+" <> "+amountBean,amountText.equals(amountBean));

		String frequenza = frequency.getText().trim().toLowerCase();
		String frequenzaFeature = bean.getFrequency().trim().toLowerCase();
		Assert.assertTrue("frequenza automatismo errata "+frequenza+" <> "+frequenzaFeature,frequenza.equals(frequenzaFeature));


//		String data = startDate.getText().trim().toLowerCase();
//
////		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
////		LocalDateTime now = LocalDateTime.now();
////		String date2 = dtf.format(now); 
////		Assert.assertTrue(data.equals(date2));
//		//		Assert.assertTrue(data.equals(date2));
//		////		String data2 = data + 01/00/00;
//		////		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd" );
//		//		Calendar cal = Calendar.getInstance();
//		////		cal.setTime( dtf );
//		//		cal.add( Calendar.DATE, 1 );
//
//		//		Calendar cal = Calendar.getInstance(); 
//		//		cal.setTime(); 
//		//		cal.add(Calendar.DATE, 1);
//		//		yourDate = cal.getTime();
//
//		Date today = new Date(driverType);               
//		SimpleDateFormat formattedDate = new SimpleDateFormat("dd/MM/yyyy");       
//
//		Calendar c = Calendar.getInstance();        
//		c.add(Calendar.DATE, 2);  // number of days to add      
//		String tomorrow = (String)(formattedDate.format(c.getTime()));
//		System.out.println("Tomorrows date is " + tomorrow);
//
//		Calendar g = Calendar.getInstance();        
//		g.add(Calendar.DATE, 1);  // number of days to add      
//		String tomorrowOne = (String)(formattedDate.format(g.getTime()));
//		System.out.println("Tomorrows date is " + tomorrowOne);
//
//		String alRaggiungimento = endDate.getText().trim().toLowerCase();
//		//		String alRaggiungimentoFeature = bean.getEndDate().trim().toLowerCase();
//		Assert.assertTrue(alRaggiungimento.equals(tomorrow));
//
//		//		String ricorrenza = ricorrenzaAttivaText.getText().trim();
//		//		Assert.assertTrue(ricorrenza.equals("Ricorrenza attiva"));
//
//		String prossimoVersamentoText = prossimoVersamento.getText().toLowerCase().replace("prossimo versamento ", "").trim();
//		System.out.println(prossimoVersamentoText);
//		Assert.assertTrue(prossimoVersamentoText.equals(tomorrowOne));

		this.modifyDeleteButton.click();


	}

	public void eliminaVersamentoRicorrente(String posteId) 
	{
		page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.MODIFICA_ELIMINA_AUTOMATISMO).visibilityOfElement(10L)
		.click();
		
		WaitManager.get().waitShortTime();
		
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 500);
		
		WaitManager.get().waitShortTime();
		
		page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.ELIMINA_AUTOMATISMO).visibilityOfElement(10L)
		.click();
		
		WaitManager.get().waitShortTime();
		
		//click su conferma da popup
		
		page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.OK_POPUP).visibilityOfElement(10L)
		.click();
		
		InsertPosteIDPage p=(InsertPosteIDPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("InsertPosteIDPage", page.getDriver().getClass(), page.getLanguage());
		
		p.checkPage();
		
		p.insertPosteId(posteId);
		
		OKOperationPage ok=(OKOperationPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("OKOperationPage", page.getDriver().getClass(), page.getLanguage());
		
		ok.close();
	}

	public void clickModificaElimina() {
		page.getParticle(Locators.SalvadanaioGestisciVersamentoRicorrentePageMolecola.MODIFICA_ELIMINA_AUTOMATISMO).visibilityOfElement(10L)
		.click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}








}





