package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import bean.datatable.SalvadanaioDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SalvadanaioSelectVersamentoPage;

import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SalvadanaioSelectVersamentoPage")
@Android
@IOS
public class SalvadanaioSelectVersamentoPageMan extends LoadableComponent<SalvadanaioSelectVersamentoPageMan> implements SalvadanaioSelectVersamentoPage
{

	UiPage page;

	@Override
	protected void isLoaded() throws Error {
		
	} 

	@Override
	protected void load() 
	{
		page.get();
	}

	public void clickVersamentoSingolo() 
	{
		page.getParticle(Locators.SalvadanaioSelectVersamentoPageMolecola.VERSAMENTI_DIV).visibilityOfElement(10L).click();
		WaitManager.get().waitMediumTime();
	}

	public void clickVersamentoRicorrente() 
	{
		page.getParticle(Locators.SalvadanaioSelectVersamentoPageMolecola.VERSAMENTO_RICORRENTE).visibilityOfElement(10L).click();
		WaitManager.get().waitMediumTime();
	}

	public void clickVersamentoArrotondamento() 
	{
		page.getParticle(Locators.SalvadanaioSelectVersamentoPageMolecola.VERSAMENTO_ARROTONDAMENTO).visibilityOfElement(10L).click();
		WaitManager.get().waitMediumTime();
	}

	public void clickVersamentoLeTueSpese() 
	{
		page.getParticle(Locators.SalvadanaioSelectVersamentoPageMolecola.VERSAMENTO_LETUESPESE).visibilityOfElement(10L).click();
		WaitManager.get().waitMediumTime();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

}
