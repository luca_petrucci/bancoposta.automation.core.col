package it.poste.bancoposta.pages.managers;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import bean.datatable.SalvadanaioDataBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SalvadanaioVersamentiListPage;
import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SalvadanaioVersamentiListPage")
@Android
@IOS
public class SalvadanaioVersamentiListPageMan extends LoadableComponent<SalvadanaioVersamentiListPageMan> implements SalvadanaioVersamentiListPage {

	UiPage page;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement assistenzaButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement date;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement versamentoName;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement amount;
	private WebElement message;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	@Override
	protected void isLoaded() throws Error {

	} 

	@Override
	protected void load() 
	{
		page.get();
	}

	public void checkVersamento(SalvadanaioDataBean bean) {


		//	String date = "";
		String message = "";
		//	String amount = "";
		//		
		//		switch(this.driverType)
		//		{
		//			case 0://android driver
		message= page.getParticle(Locators.SalvadanaioVersamentiListPageMolecola.VERSAMENTONAME).getLocator();
		//				date= Locators.SalvadanaioVersamentiListPageMolecola.DATE.getAndroidLocator();
		//				amount= Locators.SalvadanaioVersamentiListPageMolecola.AMOUNT.getAndroidLocator();
		//			break;
		//			case 1://ios driver
		////				this.message= page.getParticle(Locators.SalvadanaioVersamentiListPageMolecola.VERSAMENTONAME).getElement();
		////				this.date= page.getParticle(Locators.SalvadanaioVersamentiListPageMolecola.DATE).getElement();
		////				this.amount= page.getParticle(Locators.SalvadanaioVersamentiListPageMolecola.AMOUNT).getElement();
		//			break;
		//			case 2://web driver
		//				
		//			break;
		//		}

		//		System.out.println(date);
		//		System.out.println(amount);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MMM-yyyy");  
		LocalDateTime now = LocalDateTime.now();
		String date2 = dtf.format(now); 
		//		String dataNotifica = date.
		//				
		//		String obiettivo= message.getText().trim().toLowerCase();
		String featureMessage = bean.getMessageText().trim().toLowerCase();
		//				
		//		String amountTxt =amount.getText().trim().toLowerCase().replace("�", "").replace(",", ".");
		String featureAmount = bean.getAmount().trim().toLowerCase();

		List<WebElement> list=page.getDriver().findElements(By.xpath(message));
		//		String dataNotificaTxt= findElement(By.xpath(date)).getText().trim().toLowerCase();

		for(WebElement e : list)
		{
			this.date= page.getParticle(Locators.SalvadanaioVersamentiListPageMolecola.DATE).getElement();
			this.amount= page.getParticle(Locators.SalvadanaioVersamentiListPageMolecola.AMOUNT).getElement();
			
			String amountTxt =amount.getText().trim().toLowerCase().replace("�", "").replace(",", ".");
			String dataNotificaTxt= date.getText().trim().toLowerCase();
			String messageTxt=e.findElement(By.xpath(message)).getText().trim().toLowerCase();
			System.out.println(date2);
			System.out.println(dataNotificaTxt);
			//			String amountTxt=e.findElement(By.xpath(amount)).getText().trim().toLowerCase();
			if(messageTxt.equals(featureMessage) 
					&& date2.equals(dataNotificaTxt) 
					&& amountTxt.equals(featureAmount) 
					)
			{
				System.out.println("La notifica del versamento � presente");
				break;
			}
		}





	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}













}





