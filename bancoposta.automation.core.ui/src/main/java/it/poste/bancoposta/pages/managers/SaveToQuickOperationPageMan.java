package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.w3c.dom.Node;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.QuickOperationCreationBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SaveToQuickOperationPage;
import test.automation.core.UIUtils;
import test.automation.core.cmd.adb.AdbCommandPrompt;
import utils.ObjectFinderLight;

import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SaveToQuickOperationPage")
@Android
public class SaveToQuickOperationPageMan extends LoadableComponent<SaveToQuickOperationPageMan> implements SaveToQuickOperationPage{
	
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement description;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement quickOpNameInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement confirm;

	protected WebElement overrideQuickOperationYesButton;
	protected String pageSource;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{

		System.out.println("Start load SAVE TO QUICK OPERATION");
		
		page.get();
		
		System.out.println("Stop load SAVE TO QUICK OPERATION");

	}

	public void saveAsQuickOperation(QuickOperationCreationBean b) 
	{
		this.quickOpNameInput=page.getParticle(Locators.SaveToQuickOperationPageMolecola.QUICKOPNAMEINPUT).getElement();
		

		//setto il nome
		this.quickOpNameInput.sendKeys(b.getQuickOperationName());
		try {((AppiumDriver<?>) page.getDriver()).hideKeyboard();}catch(Exception err) {};

		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);
		WaitManager.get().waitShortTime();

		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);
		WaitManager.get().waitShortTime();
		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);

		this.confirm=page.getParticle(Locators.SaveToQuickOperationPageMolecola.CONFIRM).getElement();
		

		//salvo
		this.confirm.click();

		try
		{
			this.overrideQuickOperationYesButton=page.getParticle(Locators.SaveToQuickOperationPageMolecola.OVERRIDEQUICKOPERATIONYESBUTTON).getElement();
			
			this.overrideQuickOperationYesButton.click();
		}
		catch(Exception err)
		{

		}
	}


	public void saveAsQuickOperationAdb(QuickOperationCreationBean b) 
	{
		Properties p=UIUtils.ui().getProperties(b.getDevice());
		AdbCommandPrompt adb=new AdbCommandPrompt(p.getProperty("adb.path"));
		String devName=null;

		if(p.containsKey("android.device.name"))
			devName=p.getProperty("android.device.name");
		else
			devName=p.getProperty("ios.udid");

		adb.setDeviceName(devName);

		//provo ad inserire il nome direttamente nell'input
		WaitManager.get().waitMediumTime();
		adb.typeText(b.getQuickOperationName());
		WaitManager.get().waitShortTime();

		try {
			((AppiumDriver<?>) page.getDriver()).hideKeyboard();
		} catch (Exception err) {
		}

		//faccio uno scroll del device
		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);


		Dimension d=page.getDriver().manage().window().getSize();

		WaitManager.get().waitShortTime();
		//provo un click da adb
		System.out.println(d.height);
		adb.tap((int)(d.width * 0.5), (int)(d.height * 0.99));
		WaitManager.get().waitMediumTime();

		//			switch(this.driverType)
		//			{
		//			case 0://android driver
		//				//this.quickOpNameInput=page.getParticle(Locators.SaveToQuickOperationPageMolecola.QUICKOPNAMEINPUT.getAndroidLocator()));
		//				this.confirm=page.getParticle(Locators.SaveToQuickOperationPageMolecola.CONFIRM.getAndroidLocator()));
		//				break;
		//			case 1://ios driver
		//				//this.quickOpNameInput=page.getParticle(Locators.SaveToQuickOperationPageMolecola.QUICKOPNAMEINPUT).getElement();
		//				this.confirm=page.getParticle(Locators.SaveToQuickOperationPageMolecola.CONFIRM).getElement();
		//				break;
		//			}
		//			
		//			//salvo
		//			this.confirm.click();

		try
		{
			this.overrideQuickOperationYesButton=page.getParticle(Locators.SaveToQuickOperationPageMolecola.OVERRIDEQUICKOPERATIONYESBUTTON).getElement();
			
			this.overrideQuickOperationYesButton.click();
		}
		catch(Exception err)
		{

		}
	}

	public SaveToQuickOperationPage getLight() throws Exception 
	{
		pageSource=page.getDriver().getPageSource();

		if(!ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.SaveToQuickOperationPageMolecola.HEADER).getLocator()))
			throw new Exception("element not find in light mode "+page.getParticle(Locators.SaveToQuickOperationPageMolecola.HEADER).getName());

		if(!ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.SaveToQuickOperationPageMolecola.BACKBUTTON).getLocator()))
			throw new Exception("element not find in light mode "+page.getParticle(Locators.SaveToQuickOperationPageMolecola.BACKBUTTON).getName());

		if(!ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.SaveToQuickOperationPageMolecola.DESCRIPTION).getLocator()))
			throw new Exception("element not find in light mode "+page.getParticle(Locators.SaveToQuickOperationPageMolecola.DESCRIPTION).getName());

		if(!ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.SaveToQuickOperationPageMolecola.QUICKOPNAMEINPUT).getLocator()))
			throw new Exception("element not find in light mode "+page.getParticle(Locators.SaveToQuickOperationPageMolecola.QUICKOPNAMEINPUT).getName());

		return this;
	}

	public void saveAsQuickOperationLightMode(QuickOperationCreationBean b) 
	{
		Node e=ObjectFinderLight.getElement(pageSource, page.getParticle(Locators.SaveToQuickOperationPageMolecola.QUICKOPNAMEINPUT).getLocator());

//		ObjectFinderLight.clickOnElement(e);
//
//		Properties p=UIUtils.ui().getCurrentProperties();
//		System.out.println(p.toString());
//		AdbCommandPrompt adb=new AdbCommandPrompt(p.getProperty("adb.path"),p.getProperty("ios.udid"));
//
//		adb.typeText(b.getQuickOperationName());
//
//		WaitManager.get().waitShortTime();

		page.getParticle(Locators.SaveToQuickOperationPageMolecola.QUICKOPNAMEINPUT).getElement().click();
		page.getParticle(Locators.SaveToQuickOperationPageMolecola.QUICKOPNAMEINPUT).getElement().sendKeys(b.getQuickOperationName());
		WaitManager.get().waitShortTime();
		
		try {
			((AppiumDriver<?>) page.getDriver()).hideKeyboard();
		} catch (Exception err) {
		}

		WaitManager.get().waitShortTime();

		page.getParticle(Locators.SaveToQuickOperationPageMolecola.CONFIRM).getElement().click();
		WaitManager.get().waitShortTime();

//		e=ObjectFinderLight.getElement(pageSource, page.getParticle(Locators.SaveToQuickOperationPageMolecola.DESCRIPTION).getLocator());
//		
//		ObjectFinderLight.clickOnElement(e);
//		WaitManager.get().waitShortTime();
//		e=ObjectFinderLight.getElement(pageSource, page.getParticle(Locators.SaveToQuickOperationPageMolecola.CONFIRM).getLocator());
//
//		if(e == null)
//		{
//			Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);
//
//			WaitManager.get().waitShortTime();
//
//			pageSource=page.getDriver().getPageSource();
//		}
//
//		e=ObjectFinderLight.getElement(pageSource, page.getParticle(Locators.SaveToQuickOperationPageMolecola.CONFIRM).getLocator());
//
//		ObjectFinderLight.clickOnElement(e);
//
//		WaitManager.get().waitShortTime();
//
		try {
			this.overrideQuickOperationYesButton=page.getParticle(Locators.SaveToQuickOperationPageMolecola.OVERRIDEQUICKOPERATIONYESBUTTON).getElement();
			overrideQuickOperationYesButton.click();
		} catch (Exception e2) {
			// TODO: handle exception
		}

		try {
			e=ObjectFinderLight.getElement(page.getDriver().getPageSource(), page.getParticle(Locators.SaveToQuickOperationPageMolecola.CONFIRM).getLocator());
			ObjectFinderLight.clickOnElement(e);
		} catch (Exception e2) {
			// TODO: handle exception
		}
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}


}
