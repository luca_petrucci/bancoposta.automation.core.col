package it.poste.bancoposta.pages.managers;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.ScontiPostePage;
import test.automation.core.UIUtils;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="ScontiPostePage")
@Android
@IOS
public class ScontiPostePageMan extends LoadableComponent<ScontiPostePageMan> implements ScontiPostePage 
{
	UiPage page;

	private List<WebElement> child;
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement hamburgerMenu;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement nearMeTab;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement onlineTab;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement searchBar;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement shopBubble;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement shopName;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement shopPercentage;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 	
	{
		page.get();
	}

	public boolean searchForShopsOnTheMap() {
		this.shopBubble=page.getParticle(Locators.SalesPostePageMolecola.SHOPONMAPSTRUE).getElement();
		
		try {
			Point loc=this.shopBubble.getLocation();
			Rectangle rect=this.shopBubble.getRect();
			int py=rect.y + (rect.height / 2);
			int px=rect.x + (rect.width / 2);
			TouchAction act=new TouchAction((PerformsTouchActions) page.getDriver());
			act.tap(PointOption.point(px, py)).perform();

			WaitManager.get().waitShortTime();

			this.shopName=page.getParticle(Locators.SalesPostePageMolecola.SHOPNAMEDISPLAYED).getElement();
			this.shopPercentage=page.getParticle(Locators.SalesPostePageMolecola.SHOPDISCOUNTDISPLAYED).getElement();
			System.out.println("Informations Found");
			

			return true;
		} catch (Exception e) {
			return false;
		}



	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

}
