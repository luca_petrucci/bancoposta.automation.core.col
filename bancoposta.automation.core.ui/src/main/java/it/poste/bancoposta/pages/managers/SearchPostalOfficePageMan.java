package it.poste.bancoposta.pages.managers;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SearchPostalOfficePage;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


@PageManager(page="SearchPostalOfficePage")
@Android
public class SearchPostalOfficePageMan extends LoadableComponent<SearchPostalOfficePageMan> implements SearchPostalOfficePage
{
	UiPage page;

	protected WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement listButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement mapButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement searchBar;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement postalOfficeBubble;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement postalOfficeCityName;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement postalOfficeAddress;

	@Override
	protected void isLoaded() throws Error {

	}

	@Override
	protected void load() 	{

		page.get();

	}

	public boolean searchForPostalOfficeOnTheMap() {

		WaitManager.get().waitMediumTime();
		try {
			this.postalOfficeBubble=page.getParticle(Locators.SearchPostalOfficePageMolecola.POSTALOFFICEONMAPSTRUE).getElement();

			Point loc=this.postalOfficeBubble.getLocation();
			Rectangle rect=this.postalOfficeBubble.getRect();
			int py=rect.y + (rect.height / 2);
			int px=rect.x + (rect.width / 2);
			TouchAction act=new TouchAction((PerformsTouchActions) page.getDriver());
			act.tap(PointOption.point(px, py)).perform();

			WaitManager.get().waitShortTime();

			this.postalOfficeCityName=page.getParticle(Locators.SearchPostalOfficePageMolecola.POSTALOFFICECITYNAMEDISPLAYED).getElement();
			this.postalOfficeAddress=page.getParticle(Locators.SearchPostalOfficePageMolecola.POSTALOFFICEADDRESSDISPLAYED).getElement();
			System.out.println("Informations Found");
			return true;
		} catch (Exception e) {

			return false;
		}


	}

	public void search(String search) 
	{
		page.getParticle(Locators.SearchPostalOfficePageMolecola.SEARCHBAR).visibilityOfElement(10L);
		WebElement s=page.getParticle(Locators.SearchPostalOfficePageMolecola.SEARCHBAR).getElement();
		s.clear();
		s.sendKeys(search);
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	@Override
	public void clickOkPoup() {
		page.getParticle(Locators.SearchPostalOfficePageMolecola.POPUPOKBUTTON).visibilityOfElement(10L).click();
	}
}


