package it.poste.bancoposta.pages.managers;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.Point;
import org.openqa.selenium.Rectangle;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import it.poste.bancoposta.pages.Locators;

@PageManager(page="SearchPostalOfficePage")
@IOS
public class SearchPostalOfficePageManIOS extends SearchPostalOfficePageMan {

	public boolean searchForPostalOfficeOnTheMap() {
		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.LEFT, 800);
		WaitManager.get().waitMediumTime();
		WaitManager.get().waitMediumTime();
	
			this.postalOfficeBubble=page.getParticle(Locators.SearchPostalOfficePageMolecola.POSTALOFFICEONMAPSTRUE).getElement();

			Point loc=this.postalOfficeBubble.getLocation();
			Rectangle rect=this.postalOfficeBubble.getRect();
			int py=rect.y + (rect.height / 2);
			int px=rect.x + (rect.width / 2);
			TouchAction act=new TouchAction((PerformsTouchActions) page.getDriver());
			act.tap(PointOption.point(px, py)).perform();

			WaitManager.get().waitShortTime();
			this.postalOfficeCityName=page.getParticle(Locators.SearchPostalOfficePageMolecola.POSTALOFFICECITYNAMEDISPLAYED).getElement();
			this.postalOfficeAddress=page.getParticle(Locators.SearchPostalOfficePageMolecola.POSTALOFFICEADDRESSDISPLAYED).getElement();
			System.out.println("Citta': "+ this.postalOfficeCityName.getText());
			System.out.println("Indirizzo: "+ this.postalOfficeAddress.getText());
			assertTrue("Non è stato aperto nessun Ufficio Postale",this.postalOfficeCityName.isEnabled());
			return true;

	}
}
