package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.AbstractPageManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BollettinoDataBean;
import it.poste.bancoposta.pages.*;
import test.automation.core.UIUtils;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SelectBolletinoTypesPage")
@Android
@IOS
public class SelectBolletinoTypesPageMan extends LoadableComponent<SelectBolletinoTypesPageMan> implements SelectBolletinoTypesPage{

	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancellButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement title;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement bollettinoList;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement bollettinoElement;
	/**
	 * @android 
	 * @ios campo dinamico sostituire $(bollettinoTitle) con il nome del bollettino da ricercare
	 * @web 
	 */
	private WebElement bollettinoTitle;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();

		}

	public void creaBollettino(BollettinoDataBean b,boolean operazioneVeloce) throws InterruptedException 
	{
		String itemXpath="";
		String titleXpath="";
		List<WebElement> items=null;
		try {
			NewLoginPreLoginPage p=(NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("NewLoginPreLoginPage", page.getDriver().getClass(), null);
			p.clickOnConsentiUnaVolta();
		}catch(Exception e) {

		}
		
		try {
			NewLoginPreLoginPage p=(NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("NewLoginPreLoginPage", page.getDriver().getClass(), null);
			p.clickOnConsenti();
		}catch(Exception e) {

		}
		
		try {
			NewLoginPreLoginPage p=(NewLoginPreLoginPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("NewLoginPreLoginPage", page.getDriver().getClass(), null);
			p.clickOnConsentiAccessoContatti();
		}catch(Exception e) {

		}
		
		this.bollettinoList=page.getParticle(Locators.SelectBolletinoTypesPageMolecola.BOLLETTINOLIST).getElement();
		itemXpath=page.getParticle(Locators.SelectBolletinoTypesPageMolecola.BOLLETTINOELEMENT).getLocator();
		titleXpath=page.getParticle(Locators.SelectBolletinoTypesPageMolecola.BOLLETTINOTITLE).getLocator();
		System.out.println(titleXpath);
		System.out.println(itemXpath);
		//prelevo tutti gli item dalla lista
		items=this.bollettinoList.findElements(By.xpath(itemXpath));

		for(WebElement e : items)
		{
			//ricavo il titolo e controllo che sia uguale al desiderato, se si clicco sull'item
			String title=e.findElement(By.xpath(titleXpath)).getText();

			if(title.equals(b.getBollettinoType()))
			{
				e.click();
				WaitManager.get().waitMediumTime();
				break;
			}
		}

		if(operazioneVeloce)
			this.chooseConto(b.getPayWith());

		IBollettino bollettino=null;

		//switccio sul tipo di bollettino e vado a caricare la pagina desiderata
		WaitManager.get().waitMediumTime();
		switch(b.getBollettinoType())
		{
		case BollettinoDataBean.BOLLETTINO_896:
		case BollettinoDataBean.BOLLETTINO_674:
			bollettino =(IBollettino)(Bollettino896FormPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("Bollettino896FormPage", page.getDriver().getClass(), page.getLanguage());
			break;
		case BollettinoDataBean.BOLLETTINO_BIANCO:
			bollettino = (IBollettino)(BollettinoBiancoFormPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("BollettinoBiancoFormPage", page.getDriver().getClass(), page.getLanguage());
			((BollettinoBiancoFormPage)bollettino).setBean(b);
			break;
		case BollettinoDataBean.MAV:
			bollettino = (IBollettino)(MAVFormPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("MAVFormPage", page.getDriver().getClass(), page.getLanguage());
			break;
		case BollettinoDataBean.BOLLETTINO_PA:
			bollettino = (IBollettino)(BollettinoPAFormPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("BollettinoPAFormPage", page.getDriver().getClass(), page.getLanguage());
			break;
		}
		
		((AbstractPageManager)bollettino).checkPage();


		Thread.sleep(2000);
		try {
			PayWithPage payWithPage = (PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PayWithPage", page.getDriver().getClass(), page.getLanguage());
			
			WaitManager.get().waitMediumTime();
			
			payWithPage.clickOnConto(b.getPayWith());
		} catch (Exception e) {}


		bollettino.checkData(b);

		//			if(operazione.equals("annullamento"))
		//			{
		//				bollettino.checkData(b);
		//			}
		//			else if (operazione.contentEquals("esecuzione"))
		//			{
		//				bollettino.checkData(b);
		//				
		//			} else {
		//				bollettino.submit(b);
		//			}
	}

	public void chooseConto(String payWith) 
	{
		//seleziono il conto
		PayWithPage p=(PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PayWithPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();

		p.clickOnConto(payWith);

	}

	public void clickAnnulla() 
	{
		page.getParticle(Locators.SelectBolletinoTypesPageMolecola.CANCELLBUTTON).visibilityOfElement(10L);
		page.getParticle(Locators.SelectBolletinoTypesPageMolecola.CANCELLBUTTON).getElement().click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;

	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
