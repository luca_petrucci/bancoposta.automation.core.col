package it.poste.bancoposta.pages.managers;

import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SelectBonificoTypePage;
import it.poste.bancoposta.utils.SoftAssertion;

@PageManager(page="SelectBonificoTypePage")
@Android
@IOS
public class SelectBonificoTypePageMan extends LoadableComponent<SelectBonificoTypePageMan> implements SelectBonificoTypePage 
{
	UiPage page;

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		page.readPageSource();
		System.out.println(page.getPageSource());
		load();
	}

	@Override
	protected void load() {
		page.get();
		SoftAssertion.get().getAssertion()
		.assertThat(page.getParticle(Locators.SelectBonificoTypeMolecola.BONIFICOSEPATITLE).getElement().getText().trim())
		.withFailMessage("Controllo titolo Bonifico SEPA/Postagiro")
		.isEqualTo("Bonifico SEPA o Postagiro");
		
//		SoftAssertion.get().getAssertion()
//		.assertThat(page.getParticle(Locators.SelectBonificoTypeMolecola.BONIFICOSEPADESCRIPTION).getElement().getText().trim())
//		.withFailMessage("Controllo titolo Per i tuoi pagamenti")
//		.isEqualTo("Per i tuoi pagamenti");
	
		SoftAssertion.get().getAssertion()
		.assertThat(page.getParticle(Locators.SelectBonificoTypeMolecola.BONIFICOISTANTANEOTITLE).getElement().getText().trim())
		.withFailMessage("Controllo titolo Bonifico SEPA istantaneo")
		.isEqualTo("Bonifico istantaneo");
		
//		SoftAssertion.get().getAssertion()
//		.assertThat(page.getParticle(Locators.SelectBonificoTypeMolecola.BONIFICOISTANTANEODESCRIPTION).getElement().getText().trim())
//		.withFailMessage("Controllo titolo Per pagare in pochi secondi")
//		.isEqualTo("Per pagare in pochi secondi");
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clickOnBonificoSepa() {
		WaitManager.get().waitMediumTime();
		page.getParticle(Locators.SelectBonificoTypeMolecola.BONIFICOPOSTAGIROITEM).visibilityOfElement(10L).click();
		WaitManager.get().waitShortTime();
	}

}
