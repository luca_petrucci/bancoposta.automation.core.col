package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SelectProvinciaModal;
import test.automation.core.UIUtils;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SelectProvinciaModal")
@Android
public class SelectProvinciaModalMan extends LoadableComponent<SelectProvinciaModalMan> implements SelectProvinciaModal
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement provincieList;
	/**
	 * @android 
	 * @ios campo dinamico sostituire $(provincia) con il nome della provincia da ricercare
	 * @web 
	 */
	protected WebElement provinciaElement;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public void selectModalElement(String senderProv, int repeats) 
	{
		List<WebElement> province=new ArrayList<WebElement>();

		for(int i=0;i<repeats;i++)
		{
			province=page.getParticle(Locators.SelectProvinciaModalMolecola.PROVINCIAELEMENT).getListOfElements();

			for(WebElement e : province)
			{
				String txt=e.getText();

				if(txt.equals(senderProv))
				{
					e.click();
					return;
				}
			}

			Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.4f, 500);
		}

		for(WebElement e : province)
		{
			String txt=e.getText();

			if(txt.equals(senderProv))
			{
				e.click();
				return;
			}
		}

	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
