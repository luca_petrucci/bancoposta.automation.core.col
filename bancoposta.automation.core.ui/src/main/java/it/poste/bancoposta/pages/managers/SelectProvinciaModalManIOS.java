package it.poste.bancoposta.pages.managers;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import io.appium.java_client.MobileBy;
import test.automation.core.UIUtils;

@PageManager(page="SelectProvinciaModal")
@IOS
public class SelectProvinciaModalManIOS extends SelectProvinciaModalMan {
	
	private By pickers = MobileBy.className("XCUIElementTypePickerWheel");
	
	public void selectModalElement(String senderProv, int repeats) 
	{
		List<WebElement> pickerEls = UIUtils.ui().waitForCondition(page.getDriver(),ExpectedConditions.presenceOfAllElementsLocatedBy(pickers));
        System.out.println(pickerEls.size());
        pickerEls.get(0).sendKeys("Rimini");
        pickerEls.get(0).sendKeys("Roma");
	
	}


}
