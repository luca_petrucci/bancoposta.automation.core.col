package it.poste.bancoposta.pages.managers;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.RicaricaPostepayBean;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SelezionaCartaPage;
import test.automation.core.UIUtils;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SelezionaCartaPage")
@Android
@IOS
public class SelezionaCartaPageMan extends LoadableComponent<SelezionaCartaPageMan> implements SelezionaCartaPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */	
	private WebElement searchBar;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */	
	private WebElement searchFilterText;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */	
	private WebElement headerCarte;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */	
	private WebElement productList;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement productElement;
	/**
	 * @android 
	 * @ios campo dinamico sostituire $(index) con l'indice dell'elemento da ricercare
	 * @web 
	 */
	private WebElement productDetailType;
	/**
	 * @android 
	 * @ios campo dinamico sostituire $(index) con l'indice dell'elemento da ricercare
	 * @web 
	 */
	private WebElement productType;

	private String cardelement;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
			
	}

	public void clickOnConto(String payWith) 
	{
		String element="";
		String title="";
		String detail="";

		element=page.getParticle(Locators.SelezionaCartaPageMolecola.CARDELEMENT).getLocator();
		title=page.getParticle(Locators.SelezionaCartaPageMolecola.PRODUCTTYPE).getLocator();
		detail=page.getParticle(Locators.SelezionaCartaPageMolecola.PRODUCTDETAILTYPE).getLocator();

		String[] payWithInfo=payWith.split(";");
		System.out.println(payWithInfo);

		List<WebElement> list=page.getDriver().findElements(By.xpath(element));

		for(WebElement e : list)
		{
			System.out.println(e);
			String titleTxt=e.findElement(By.xpath(title)).getText().trim().toLowerCase();
			String descrTxt=e.findElement(By.xpath(detail)).getText().trim().toLowerCase(); 
			String fourDigits = descrTxt.substring(descrTxt.length() - 4);
			if(payWithInfo[0].equals(titleTxt) && payWithInfo[1].contains(fourDigits))
			{descrTxt.substring(descrTxt.length() - 4);
			e.click();
			break;
			}
		}
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	@Override
	public void clickOnFirstItem() {
		cardelement=page.getParticle(Locators.SelezionaCartaPageMolecola.CARDELEMENT).getLocator();	
		List<WebElement> list=(List<WebElement>) page.getDriver().findElements(By.xpath(cardelement));	
		WaitManager.get().waitMediumTime();	
		list.get(0).click();		
	}



}
