package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import io.appium.java_client.HidesKeyboard;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.AbstractPageManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BonificoDataBean;
import bean.datatable.QuickOperationCreationBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BonificoConfirmPage;
import it.poste.bancoposta.pages.BonificoConfirmationPages;
import it.poste.bancoposta.pages.InsertPosteIDPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OKOperationPage;
import it.poste.bancoposta.pages.PayWithPage;
import it.poste.bancoposta.pages.PostagiroConfirmPage;
import it.poste.bancoposta.pages.SelectProvinciaModal;
import it.poste.bancoposta.pages.SendBonificoPage;
import it.poste.bancoposta.utils.PageMolecolaImgCreator;
import test.automation.core.UIUtils;

import java.util.Locale;

import org.apache.poi.xwpf.usermodel.TOC;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SendBonificoPage")
@Android
public class SendBonificoPageMan extends LoadableComponent<SendBonificoPageMan> implements SendBonificoPage
{
	protected static final String IMPORTO = "importo";
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement cancellButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement paymentTypeSummary;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement changePaymentTypeButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement addressbookLink;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement ibanInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement ccInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement countryInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement accountholderInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement saveAccountCheckBox;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement casual;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement showOtherInformations;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement addressInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement cityInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement referenceInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement actualReferenceInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement confirm;
	protected WebElement exitModalButton;

	@Override
	protected void isLoaded() throws Error 
	{

	}

	@Override
	protected void load() 
	{
		page.getParticle(Locators.SendBonificoPageMolecola.HEADER).visibilityOfElement(10L);
		page.getParticle(Locators.SendBonificoPageMolecola.CANCELLBUTTON).visibilityOfElement(10L);
		//		page.getParticle(Locators.SendBonificoPageMolecola.PAYMENTTYPESUMMARY).visibilityOfElement(10L);
		//		page.getParticle(Locators.SendBonificoPageMolecola.CHANGEPAYMENTTYPEBUTTON).visibilityOfElement(10L);
		page.getParticle(Locators.SendBonificoPageMolecola.ADDRESSBOOKLINK).visibilityOfElement(10L);
		page.getParticle(Locators.SendBonificoPageMolecola.IBANINPUT).visibilityOfElement(10L);
		page.getParticle(Locators.SendBonificoPageMolecola.COUNTRYINPUT).visibilityOfElement(10L);
		page.getParticle(Locators.SendBonificoPageMolecola.ACCOUNTHOLDERINPUT).visibilityOfElement(10L);
		//		page.getParticle(Locators.SendBonificoPageMolecola.SAVEACCOUNTCHECKBOX).visibilityOfElement(10L);
		//		page.getParticle(Locators.SendBonificoPageMolecola.AMOUNT).visibilityOfElement(10L);
		//		page.getParticle(Locators.SendBonificoPageMolecola.CASUAL).visibilityOfElement(10L);
		//		page.getParticle(Locators.SendBonificoPageMolecola.ENABLEBONIFICOINSTANTANEO).visibilityOfElement(10L);

		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);

		page.getParticle(Locators.SendBonificoPageMolecola.SHOWOTHERINFORMATIONS).visibilityOfElement(10L);

		try
		{
			page.getParticle(Locators.SendBonificoPageMolecola.ADDRESSINPUT).visibilityOfElement(10L);
		}
		catch(Exception err)
		{
			this.showOtherInformations=page.getParticle(Locators.SendBonificoPageMolecola.SHOWOTHERINFORMATIONS).getElement();
			this.showOtherInformations.click();

		}

		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);

		page.getParticle(Locators.SendBonificoPageMolecola.ADDRESSINPUT).visibilityOfElement(10L);
		page.getParticle(Locators.SendBonificoPageMolecola.CITYINPUT).visibilityOfElement(10L);
		page.getParticle(Locators.SendBonificoPageMolecola.REFERENCEINPUT).visibilityOfElement(10L);
		page.getParticle(Locators.SendBonificoPageMolecola.ACTUALREFERENCEINPUT).visibilityOfElement(10L);
		page.getParticle(Locators.SendBonificoPageMolecola.CONFIRM).visibilityOfElement(10L);

		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.UP, 200);
		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.UP, 200);

		//		System.out.println("inizio get element");
		//		
		//		byte[] b=((TakesScreenshot)page.getDriver()).getScreenshotAs(OutputType.BYTES);
		//		PageMolecolaImgCreator.makeImg(b, page,
		//				Locators.SendBonificoPageMolecola.HEADER,
		//				Locators.SendBonificoPageMolecola.CANCELLBUTTON,
		//				Locators.SendBonificoPageMolecola.PAYMENTTYPESUMMARY,
		//				Locators.SendBonificoPageMolecola.CHANGEPAYMENTTYPEBUTTON,
		//				Locators.SendBonificoPageMolecola.ADDRESSBOOKLINK,
		//				Locators.SendBonificoPageMolecola.IBANINPUT,
		//				Locators.SendBonificoPageMolecola.COUNTRYINPUT,
		//				Locators.SendBonificoPageMolecola.ACCOUNTHOLDERINPUT,
		//				Locators.SendBonificoPageMolecola.SAVEACCOUNTCHECKBOX,
		//				Locators.SendBonificoPageMolecola.AMOUNT,
		//				Locators.SendBonificoPageMolecola.CASUAL
		//				);
	}

	public OKOperationPage sendBonifico(BonificoDataBean b) 
	{
		//		this.paymentTypeSummary=page.getParticle(Locators.SendBonificoPageMolecola.PAYMENTTYPESUMMARY).getElement();
		try 
		{this.ibanInput=page.getParticle(Locators.SendBonificoPageMolecola.IBANINPUT).getElement();}
		catch (Exception err) 
		{this.ccInput=page.getParticle(Locators.SendBonificoPageMolecola.CCINPUT).getElement();}
		try 
		{this.countryInput=page.getParticle(Locators.SendBonificoPageMolecola.COUNTRYINPUT).getElement();}
		catch (Exception err) {}


		//controllo che il valore mostrato sia uguale a payWith
		//		String payWith=this.paymentTypeSummary.getText().trim().toLowerCase();
		//		String toCheck=b.getPayWith().replace(";", " ").toLowerCase();
		//		String fourDigits = payWith.substring(payWith.length() - 4);
		//		String toCheck4 = toCheck.substring(toCheck.length() - 4);
		//
		//		Assert.assertTrue("controllo paga con;attuale:"+fourDigits+", atteso:"+toCheck4,fourDigits.equals(toCheck4));

		//inserisco iban
		try
		{this.ibanInput.sendKeys(b.getIban());}
		catch (Exception err)
		{this.ccInput.sendKeys(b.getCc());}

		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}

	

		try {
			//seleziono nazione solo per Bonifico
			this.accountholderInput.sendKeys(b.getCity());
			//			this.countryInput.click();
			//
			//			SelectProvinciaModal modal=(SelectProvinciaModal) UiPageManager.get("it.poste.bancoposta.pages.managers")
			//					.getPageManager("SelectProvinciaModal", page.getDriver().getClass(), page.getLanguage());
			//			modal.checkPage();
			//			modal.selectModalElement(b.getCity(),30);
		}catch(Exception err) {}

		WaitManager.get().waitShortTime();

		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}
		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}

		//inserisco intestato a
		page.getParticle(Locators.SendBonificoPageMolecola.ACCOUNTHOLDERINPUT).getElement().sendKeys(b.getOwner());
//		this.accountholderInput.sendKeys(b.getOwner());
		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}

		try 
		{
			//Le informazioni relative all'indirizzo sono presenti solo per il bonifico
			try 
			{
				this.addressInput=page.getParticle(Locators.SendBonificoPageMolecola.ADDRESSINPUT).getElement();
			} catch (Exception err) 
			{
				this.showOtherInformations=page.getParticle(Locators.SendBonificoPageMolecola.SHOWOTHERINFORMATIONS).getElement();
				this.showOtherInformations.click();
				try 
				{
					Thread.sleep(1500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block

				}
			}
		}catch (Exception err) {}

		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 500);

		WaitManager.get().waitShortTime();


		try 
		{
			// Gli indirizzi sono presenti solo per il bonifico
			this.addressInput=page.getParticle(Locators.SendBonificoPageMolecola.ADDRESSINPUT).getElement();
			this.cityInput=page.getParticle(Locators.SendBonificoPageMolecola.CITYINPUT).getElement();
			this.referenceInput=page.getParticle(Locators.SendBonificoPageMolecola.REFERENCEINPUT).getElement();
			this.actualReferenceInput=page.getParticle(Locators.SendBonificoPageMolecola.ACTUALREFERENCEINPUT).getElement();
		}catch (Exception err) {}
		this.confirm=page.getParticle(Locators.SendBonificoPageMolecola.CONFIRM).getElement();

		try
		{

			//inserisco indirizzo
			this.addressInput.sendKeys(b.getAddress());
			try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}

			//inserisco citta
			this.cityInput.sendKeys(b.getCitySender());
			try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}

			//inserisco riferimento
			this.referenceInput.sendKeys(b.getReference());
			try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}

			//inserisco riferimento effettivo
			this.actualReferenceInput.sendKeys(b.getRefecenceEffective());
			try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}

		}catch(Exception err) {}
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 400);

		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}
		//inserisco clicco conferma
		this.confirm.click();

		//controllo se viene mostrato il modal di conferma
		//nel caso di pi� operazioni sullo stesso destinatario
		try
		{
			System.out.println(page.getParticle(Locators.SendBonificoPageMolecola.MODALCONFIRMBUTTON).getLocator());
			page.getParticle(Locators.SendBonificoPageMolecola.MODALCONFIRMBUTTON).visibilityOfElement(3L)
			.click();

		}
		catch(Exception err)
		{

		}

		WaitManager.get().waitShortTime();

		//this.accountholderInput=page.getParticle(Locators.SendBonificoPageMolecola.ACCOUNTHOLDERINPUT).getElement();
		this.amount=page.getParticle(Locators.SendBonificoPageMolecola.AMOUNT).getElement();
		this.casual=page.getParticle(Locators.SendBonificoPageMolecola.CASUAL).getElement();




		//inserisco importo
		insertAmount(b.getAmount());

		//inserisco casuale
		this.casual.sendKeys(b.getDescription());
		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}


		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN,200);

		WaitManager.get().waitShortTime();



		this.confirm=page.getParticle(Locators.SendBonificoPageMolecola.CONFIRM).getElement();
		this.confirm.click();

		WaitManager.get().waitShortTime();

		this.confirm=page.getParticle(Locators.SendBonificoPageMolecola.CONFIRM).getElement();
		this.confirm.click();

		//controllo che i dati siano corretti
		BonificoConfirmationPages confirm=null;

		switch(b.getBonificoType())
		{
		case "postagiro":
			confirm=(BonificoConfirmationPages)(PostagiroConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
			.getPageManager("PostagiroConfirmPage", page.getDriver().getClass(), page.getLanguage());
			break;
		case "bonifico":
			confirm=(BonificoConfirmationPages)(BonificoConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
			.getPageManager("BonificoConfirmPage", page.getDriver().getClass(), page.getLanguage());
			break;
		}

		((AbstractPageManager)confirm).checkPage();

		try {
			UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 1000);

			WaitManager.get().waitShortTime();

			UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 1000);

		} catch (Exception e) {
			// TODO: handle exception
		}

		confirm.checkData(b);

		//eseguo il bonifico
		return confirm.submit(b);
	}

	void insertAmount(String amount) {
		this.amount.sendKeys(amount);
		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}
	}

	public void isEditable(BonificoDataBean bean) 
	{
		this.ibanInput=page.getParticle(Locators.SendBonificoPageMolecola.IBANINPUT).getElement();
		this.countryInput=page.getParticle(Locators.SendBonificoPageMolecola.COUNTRYINPUT).getElement();


		//modifico i dati
		this.ibanInput.sendKeys(bean.getIban()+"#");
		this.countryInput.click();

		SelectProvinciaModal modal=(SelectProvinciaModal) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SelectProvinciaModal", page.getDriver().getClass(), page.getLanguage());

		modal.checkPage();

		modal.selectModalElement("Angola", 30);

		WaitManager.get().waitShortTime();

		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}
		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}


		this.accountholderInput=page.getParticle(Locators.SendBonificoPageMolecola.ACCOUNTHOLDERINPUT).getElement();
		this.amount=page.getParticle(Locators.SendBonificoPageMolecola.AMOUNTMODIFY).getElement();
		this.casual=page.getParticle(Locators.SendBonificoPageMolecola.CASUAL).getElement();


		this.accountholderInput.sendKeys(bean.getOwner()+"#");

		double amount=Double.parseDouble(bean.getAmount())*50;
		String importo=""+amount;

		this.amount.sendKeys(importo);

		this.casual.sendKeys(bean.getDescription()+"#");

		//controllo i valori
		this.ibanInput=page.getParticle(Locators.SendBonificoPageMolecola.IBANINPUT).getElement();
		this.countryInput=page.getParticle(Locators.SendBonificoPageMolecola.COUNTRYINPUT).getElement();
		this.accountholderInput=page.getParticle(Locators.SendBonificoPageMolecola.ACCOUNTHOLDERINPUT).getElement();
		this.amount=page.getParticle(Locators.SendBonificoPageMolecola.AMOUNTMODIFY).getElement();
		this.casual=page.getParticle(Locators.SendBonificoPageMolecola.CASUAL).getElement();


		String txt,toCheck;

		txt=this.ibanInput.getText().trim();
		toCheck=bean.getIban()+"#";
		Assert.assertTrue("controllo nuovo valore iban;attuale:"+txt+", atteso:"+toCheck,txt.equals(toCheck));

		txt=this.countryInput.getText().trim();
		toCheck="Angola";
		Assert.assertTrue("controllo nuovo valore Paese residenza;attuale:"+txt+", atteso:"+toCheck,txt.equals(toCheck));

		txt=this.accountholderInput.getText().trim();
		toCheck=bean.getOwner()+"#";
		Assert.assertTrue("controllo nuovo valore intestato a;attuale:"+txt+", atteso:"+toCheck,txt.equals(toCheck));

		txt=this.amount.getText().trim().replace("\u20AC", "").trim();
		toCheck=bean.getAmount();
		double a1,a2;
		a1=Utility.parseCurrency(txt, Locale.ITALY);
		a2=Double.parseDouble(toCheck)*50;
		Assert.assertTrue("controllo nuovo valore importo;attuale:"+a1+", atteso:"+a2,a1==a2);

		txt=this.casual.getText().trim();
		toCheck=bean.getDescription()+"#";
		Assert.assertTrue("controllo nuovo valore casuale;attuale:"+txt+", atteso:"+toCheck,txt.equals(toCheck));

		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);

		WaitManager.get().waitShortTime();

		//modifico i valori
		this.addressInput=page.getParticle(Locators.SendBonificoPageMolecola.ADDRESSINPUT).getElement();
		this.cityInput=page.getParticle(Locators.SendBonificoPageMolecola.CITYINPUTMODIFY).getElement();
		this.referenceInput=page.getParticle(Locators.SendBonificoPageMolecola.REFERENCEINPUT).getElement();
		this.actualReferenceInput=page.getParticle(Locators.SendBonificoPageMolecola.ACTUALREFERENCEINPUT).getElement();


		this.addressInput.sendKeys(bean.getAddress()+"'");
		this.cityInput.sendKeys(bean.getCitySender()+"#");
		this.referenceInput.sendKeys(bean.getReference()+"�");
		this.actualReferenceInput.sendKeys(bean.getRefecenceEffective()+"!");

		//controllo i valori
		this.addressInput=page.getParticle(Locators.SendBonificoPageMolecola.ADDRESSINPUT).getElement();
		this.cityInput=page.getParticle(Locators.SendBonificoPageMolecola.CITYINPUTMODIFY).getElement();
		this.referenceInput=page.getParticle(Locators.SendBonificoPageMolecola.REFERENCEINPUT).getElement();
		this.actualReferenceInput=page.getParticle(Locators.SendBonificoPageMolecola.ACTUALREFERENCEINPUT).getElement();


		txt=this.addressInput.getText().trim();
		toCheck=bean.getAddress()+"'";
		Assert.assertTrue("controllo nuovo valore indirizzo;attuale:"+txt+", atteso:"+toCheck,txt.equals(toCheck));

		txt=this.cityInput.getText().trim();
		toCheck=bean.getCitySender()+"#";
		Assert.assertTrue("controllo nuovo valore citt�;attuale:"+txt+", atteso:"+toCheck,txt.equals(toCheck));

		txt=this.referenceInput.getText().trim();
		toCheck=bean.getReference()+"�";
		Assert.assertTrue("controllo nuovo valore riferimento;attuale:"+txt+", atteso:"+toCheck,txt.equals(toCheck));

		txt=this.actualReferenceInput.getText().trim();
		toCheck=bean.getRefecenceEffective()+"!";
		Assert.assertTrue("controllo nuovo valore riferimento effettivo;attuale:"+txt+", atteso:"+toCheck,txt.equals(toCheck));


		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.UP, 200);

		WaitManager.get().waitShortTime();

		this.changePaymentTypeButton=page.getParticle(Locators.SendBonificoPageMolecola.CHANGEPAYMENTTYPEBUTTON).getElement();


		this.changePaymentTypeButton.click();

		PayWithPage p=(PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PayWithPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();

		p.clickBack();

		//faccio un ricco click back
		this.cancellButton=page.getParticle(Locators.SendBonificoPageMolecola.CANCELLBUTTON).getElement();


		this.cancellButton.click();

		//clicco su ESCI
		page.getParticle(Locators.SendBonificoPageMolecola.EXITMODALBUTTON).visibilityOfElement(10L);
		this.exitModalButton=page.getParticle(Locators.SendBonificoPageMolecola.EXITMODALBUTTON).getElement();


		this.exitModalButton.click();
	}

	public OKOperationPage sendBonifico(BonificoDataBean bean, String campo, String value) 
	{
		String oldValue=null;

		//memorizzo temporaneamente il vecchio valore
		switch(campo)
		{
		case IMPORTO:
			oldValue=bean.getAmount();
			bean.setAmount(value);

			this.amount=page.getParticle(Locators.SendBonificoPageMolecola.AMOUNTMODIFY).getElement();


			this.amount.sendKeys(bean.getAmount());
			try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}
			try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}

			break;
		}

		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);

		WaitManager.get().waitShortTime();

		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}
		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}


		//click su conferma
		this.confirm=page.getParticle(Locators.SendBonificoPageMolecola.CONFIRM).getElement();


		this.confirm.click();

		BonificoConfirmPage p =(BonificoConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("BonificoConfirmPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();
		p.checkData(bean);

		return ((BonificoConfirmationPages)p).submit(bean);
	}

	public void checkPaeseDiResidenza(String city) 
	{
		String c=page.getParticle(Locators.SendBonificoPageMolecola.COUNTRYINPUT).getElement().getText().trim();

		org.springframework.util.Assert.isTrue(c.equals(city), "Paese di residenza KO attuale:"+c+", atteso:"+city);
	}

	public void clickAnnulla() 
	{
		page.getParticle(Locators.SendBonificoPageMolecola.CANCELLBUTTON).visibilityOfElement(10L);
		page.getParticle(Locators.SendBonificoPageMolecola.CANCELLBUTTON).getElement().click();

		//		page.getParticle(Locators.SendBonificoPageMolecola.POPUPCONTINUABUTTON).visibilityOfElement(10L);
		//		page.getParticle(Locators.SendBonificoPageMolecola.POPUPCONTINUABUTTON).getElement().click();

	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	@Override
	public void chiudiPopup() {
		WebElement annullaButton = page.getParticle(Locators.SendBonificoPageMolecola.CANCELLBUTTON).getElement();
		annullaButton.click();
		WebElement popupEsci = page.getParticle(Locators.SendBonificoPageMolecola.ESCBUTTON).getElement();
		popupEsci.click();
	}

	@Override
	public void chechEntryPoint() {
		// TODO Auto-generated method stub
		page.getParticle(Locators.SelectBonificoTypeMolecola.BONIFICOSEPATITLE).getElement();
	}
}
