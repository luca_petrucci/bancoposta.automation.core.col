package it.poste.bancoposta.pages.managers;

import static io.appium.java_client.MobileCommand.setSettingsCommand;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.util.Assert;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.AbstractPageManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.QuickObjectFinder;
import bean.datatable.BonificoDataBean;
import io.appium.java_client.CommandExecutionHelper;
import io.appium.java_client.ExecutesMethod;
import io.appium.java_client.HidesKeyboard;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BonificoConfirmPage;
import it.poste.bancoposta.pages.BonificoConfirmationPages;
import it.poste.bancoposta.pages.DeviceNative;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OKOperationPage;
import it.poste.bancoposta.pages.PostagiroConfirmPage;
import it.poste.bancoposta.pages.SelectProvinciaModal;
import test.automation.core.UIUtils;

@PageManager(page="SendBonificoPage")
@IOS
public class SendBonificoPageManiOS extends SendBonificoPageMan 
{
	void insertAmount(String amount2) {
		amount2=amount2.replace(".", ",");
		
		DeviceNative d=(DeviceNative) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("DeviceNative", page.getDriver().getClass(), null);
		
		for(int i=0;i<amount2.length();i++)
		{
			char c=amount2.charAt(i);
			d.clickOnNumPadiOS(c);
			System.out.println("tap on ios pad:"+c);
			WaitManager.get().waitFor(TimeUnit.MILLISECONDS, 500);
		}
		
		d.clickOnFineFromKeyboard();
	}
	
	protected void load() 
	{
		WaitManager.get().waitMediumTime();
		page.readPageSource();
		String pageSource=page.getPageSource();
		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.HEADER).getLocator()));
		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.CANCELLBUTTON).getLocator()));
		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.PAYMENTTYPESUMMARY).getLocator()));
		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.CHANGEPAYMENTTYPEBUTTON).getLocator()));
		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.ADDRESSBOOKLINK).getLocator()));
		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.IBANINPUT).getLocator()));
		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.COUNTRYINPUT).getLocator()));
		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.ACCOUNTHOLDERINPUT).getLocator()));
		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.SAVEACCOUNTCHECKBOX).getLocator()));
		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.AMOUNT).getLocator()));
		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.CASUAL).getLocator()));
		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.ENABLEBONIFICOINSTANTANEO).getLocator()));

		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);
		
		WaitManager.get().waitShortTime();
		page.readPageSource();
		pageSource=page.getPageSource();

		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.SHOWOTHERINFORMATIONS).getLocator()));

		try
		{
			page.getParticle(Locators.SendBonificoPageMolecola.ADDRESSINPUT).visibilityOfElement(3L);
		}
		catch(Exception err)
		{
			page.getParticle(Locators.SendBonificoPageMolecola.SHOWOTHERINFORMATIONS).clickOffline(pageSource);

		}

		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 200);
		
		WaitManager.get().waitShortTime();
		page.readPageSource();
		pageSource=page.getPageSource();

		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.ADDRESSINPUT).getLocator()));
		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.CITYINPUT).getLocator()));
		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.REFERENCEINPUT).getLocator()));
		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.ACTUALREFERENCEINPUT).getLocator()));
		Assert.isTrue(QuickObjectFinder.elementExists(pageSource, page.getParticle(Locators.SendBonificoPageMolecola.CONFIRM).getLocator()));

		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.UP, 200);
		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.UP, 200);
		
//		System.out.println("inizio get element");
//		
//		byte[] b=((TakesScreenshot)page.getDriver()).getScreenshotAs(OutputType.BYTES);
//		PageMolecolaImgCreator.makeImg(b, page,
//				Locators.SendBonificoPageMolecola.HEADER,
//				Locators.SendBonificoPageMolecola.CANCELLBUTTON,
//				Locators.SendBonificoPageMolecola.PAYMENTTYPESUMMARY,
//				Locators.SendBonificoPageMolecola.CHANGEPAYMENTTYPEBUTTON,
//				Locators.SendBonificoPageMolecola.ADDRESSBOOKLINK,
//				Locators.SendBonificoPageMolecola.IBANINPUT,
//				Locators.SendBonificoPageMolecola.COUNTRYINPUT,
//				Locators.SendBonificoPageMolecola.ACCOUNTHOLDERINPUT,
//				Locators.SendBonificoPageMolecola.SAVEACCOUNTCHECKBOX,
//				Locators.SendBonificoPageMolecola.AMOUNT,
//				Locators.SendBonificoPageMolecola.CASUAL
//				);
	}
	
	private void restoreDefaultSettings() {
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("customSnapshotTimeout",15));
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("snapshotMaxDepth",50));
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("useFirstMatch",false));
	}

	private void setCustomSettings() {
		System.out.println("Set Custom Setting for conti e carte");
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("customSnapshotTimeout",0));
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("snapshotMaxDepth",10));
		CommandExecutionHelper.execute((ExecutesMethod) page.getDriver(), setSettingsCommand("useFirstMatch",true));
	}
	
	public OKOperationPage sendBonifico(BonificoDataBean b) 
	{
		setCustomSettings();
		
		this.paymentTypeSummary=page.getParticle(Locators.SendBonificoPageMolecola.PAYMENTTYPESUMMARY).getElement();
		try 
		{this.ibanInput=page.getParticle(Locators.SendBonificoPageMolecola.IBANINPUT).getElement();}
		catch (Exception err) 
		{this.ccInput=page.getParticle(Locators.SendBonificoPageMolecola.CCINPUT).getElement();}
		try 
		{this.countryInput=page.getParticle(Locators.SendBonificoPageMolecola.COUNTRYINPUT).getElement();}
		catch (Exception err) {}


		//controllo che il valore mostrato sia uguale a payWith
		String payWith=this.paymentTypeSummary.getText().trim().toLowerCase();
		String toCheck=b.getPayWith().replace(";", " ").toLowerCase();
		String fourDigits = payWith.substring(payWith.length() - 4);
		String toCheck4 = toCheck.substring(toCheck.length() - 4);

		Assert.isTrue(fourDigits.equals(toCheck4),"controllo paga con;attuale:"+fourDigits+", atteso:"+toCheck4);

		//inserisco iban
		try
		{this.ibanInput.sendKeys(b.getIban());}
		catch (Exception err)
		{this.ccInput.sendKeys(b.getCc());}

		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}

		try {
			//seleziono nazione solo per Bonifico
			this.countryInput.click();

			SelectProvinciaModal modal=(SelectProvinciaModal) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("SelectProvinciaModal", page.getDriver().getClass(), page.getLanguage());
			modal.checkPage();
			modal.selectModalElement(b.getCity(),30);
		}catch(Exception err) {}

		WaitManager.get().waitShortTime();
		
//		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}
		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}

		this.accountholderInput=page.getParticle(Locators.SendBonificoPageMolecola.ACCOUNTHOLDERINPUT).getElement();
		this.amount=page.getParticle(Locators.SendBonificoPageMolecola.AMOUNT).getElement();
		this.casual=page.getParticle(Locators.SendBonificoPageMolecola.CASUAL).getElement();


		//inserisco intestato a
		this.accountholderInput.sendKeys(b.getOwner());
//		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}

		this.paymentTypeSummary.click();
		this.amount.click();
		//inserisco importo
		insertAmount(b.getAmount());
		page.getParticle(Locators.SendBonificoPageMolecola.LABELRUBRICA).getElement().click();	
		//inserisco casuale
		this.casual.sendKeys(b.getDescription());
		page.getParticle(Locators.SendBonificoPageMolecola.LABELRUBRICA).getElement().click();
//		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}


		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN,200);

		WaitManager.get().waitShortTime();

		try 
		{
			//Le informazioni relative all'indirizzo sono presenti solo per il bonifico
			try 
			{
				this.addressInput=page.getParticle(Locators.SendBonificoPageMolecola.ADDRESSINPUT).getElement();
			} catch (Exception err) 
			{
				this.showOtherInformations=page.getParticle(Locators.SendBonificoPageMolecola.SHOWOTHERINFORMATIONS).getElement();
				this.showOtherInformations.click();
				try 
				{
					Thread.sleep(1500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block

				}
			}
		}catch (Exception err) {}

		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 500);

		WaitManager.get().waitShortTime();


		try 
		{
			// Gli indirizzi sono presenti solo per il bonifico
			this.addressInput=page.getParticle(Locators.SendBonificoPageMolecola.ADDRESSINPUT).getElement();
			this.cityInput=page.getParticle(Locators.SendBonificoPageMolecola.CITYINPUT).getElement();
			this.referenceInput=page.getParticle(Locators.SendBonificoPageMolecola.REFERENCEINPUT).getElement();
			this.actualReferenceInput=page.getParticle(Locators.SendBonificoPageMolecola.ACTUALREFERENCEINPUT).getElement();
		}catch (Exception err) {}
		this.confirm=page.getParticle(Locators.SendBonificoPageMolecola.CONFIRM).getElement();
		
		try
		{

			//inserisco indirizzo
			this.addressInput.sendKeys(b.getAddress());
			try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}

			//inserisco citta
			this.cityInput.sendKeys(b.getCitySender());
			try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}

			//inserisco riferimento
			this.referenceInput.sendKeys(b.getReference());
			try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}

			//inserisco riferimento effettivo
			this.actualReferenceInput.sendKeys(b.getRefecenceEffective());
			try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}

		}catch(Exception err) {}
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 400);
		try {((HidesKeyboard) page.getDriver()).hideKeyboard();} catch(Exception err) {}
		//inserisco clicco conferma
		this.confirm.click();
		
		restoreDefaultSettings();
		WaitManager.get().waitShortTime();
		//controllo se viene mostrato il modal di conferma
		//nel caso di pi� operazioni sullo stesso destinatario
		try
		{
			WaitManager.get().waitMediumTime();
			System.out.println(page.getParticle(Locators.SendBonificoPageMolecola.MODALCONFIRMBUTTON).getLocator());
			page.getParticle(Locators.SendBonificoPageMolecola.MODALCONFIRMBUTTON).visibilityOfElement(10L).click();
			
		}
		catch(Exception err)
		{

		}


		//controllo che i dati siano corretti
		BonificoConfirmationPages confirm=null;

		switch(b.getBonificoType())
		{
		case "postagiro":
			confirm=(BonificoConfirmationPages)(PostagiroConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("PostagiroConfirmPage", page.getDriver().getClass(), page.getLanguage());
			break;
		case "bonifico":
			confirm=(BonificoConfirmationPages)(BonificoConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
			.getPageManager("BonificoConfirmPage", page.getDriver().getClass(), page.getLanguage());
			break;
		}
		
		((AbstractPageManager)confirm).checkPage();

		try {
			UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 1000);

			WaitManager.get().waitShortTime();

			UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.8f, 0.5f, 0.5f, 1000);

		} catch (Exception e) {
			// TODO: handle exception
		}

		confirm.checkData(b);

		//eseguo il bonifico
		return confirm.submit(b);
	}
}
