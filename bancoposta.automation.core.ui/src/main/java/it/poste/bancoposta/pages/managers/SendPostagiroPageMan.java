package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import io.appium.java_client.HidesKeyboard;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BonificoDataBean;
import bean.datatable.PaymentCreationBean;
import io.appium.java_client.AppiumDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.BonificoConfirmationPages;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.OKOperationPage;
import it.poste.bancoposta.pages.PayWithPage;
import it.poste.bancoposta.pages.PostagiroConfirmPage;
import it.poste.bancoposta.pages.SendPostagiroPage;
import it.poste.bancoposta.utils.PageMolecolaImgCreator;
import test.automation.core.UIUtils;
import utils.Entry;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SendPostagiroPage")
@Android
public class SendPostagiroPageMan extends LoadableComponent<SendPostagiroPageMan> implements SendPostagiroPage
{
	protected static final String IMPORTO = "importo";
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement cancellButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement paymentTypeSummary;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement changePaymentTypeButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement addressbookLink;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement ibanInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement accountholderInput;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement saveOnadressbook;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement casual;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	protected WebElement confirm;
	protected WebElement cancellaDati;
	protected WebElement exitModalButton;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
		WaitManager.get().waitMediumTime();
		
//		byte[] b=((TakesScreenshot)page.getDriver()).getScreenshotAs(OutputType.BYTES);
//		PageMolecolaImgCreator.makeImg(b, page,
//				Locators.SendPostagiroPageMolecola.HEADER,
//				Locators.SendPostagiroPageMolecola.CANCELLBUTTON,
//				Locators.SendPostagiroPageMolecola.PAYMENTTYPESUMMARY,
//				Locators.SendPostagiroPageMolecola.CHANGEPAYMENTTYPEBUTTON,
//				Locators.SendPostagiroPageMolecola.ADDRESSBOOKLINK,
//				Locators.SendPostagiroPageMolecola.IBANINPUT,
//				Locators.SendPostagiroPageMolecola.ACCOUNTHOLDERINPUT,
//				Locators.SendPostagiroPageMolecola.SAVEONADRESSBOOK,
//				Locators.SendPostagiroPageMolecola.AMOUNT,
//				Locators.SendPostagiroPageMolecola.CASUAL,
//				Locators.SendPostagiroPageMolecola.CONFIRM
//				);
		}

	public void isEditable(BonificoDataBean bean) 
	{
		isEditable(bean.getPayWith(),bean.getIban(),bean.getOwner(),bean.getAmount(),bean.getDescription());
	}

	public void isEditable(String payWith,String iban,String owner,String importo,String description)
	{
		this.paymentTypeSummary=page.getParticle(Locators.SendPostagiroPageMolecola.PAYMENTTYPESUMMARY).getElement();
		//this.changePaymentTypeButton=page.getParticle(Locators.SendPostagiroPageMolecola.CHANGEPAYMENTTYPEBUTTON.getAndroidLocator()));
		this.ibanInput=page.getParticle(Locators.SendPostagiroPageMolecola.IBANINPUT).getElement();
		this.accountholderInput=page.getParticle(Locators.SendPostagiroPageMolecola.ACCOUNTHOLDERINPUT).getElement();
		this.amount=page.getParticle(Locators.SendPostagiroPageMolecola.AMOUNT).getElement();
		this.casual=page.getParticle(Locators.SendPostagiroPageMolecola.CASUAL).getElement();
		this.confirm=page.getParticle(Locators.SendPostagiroPageMolecola.CONFIRM).getElement();
		this.cancellaDati=page.getParticle(Locators.SendPostagiroPageMolecola.CANCELLADATI).getElement();
		

		//controllo paga con
		String txt=this.paymentTypeSummary.getText().trim().toLowerCase();
		String toCheck=payWith.replace(";", " ").toLowerCase();

		Assert.assertTrue("controllo paga con;attuale:"+txt+",atteso:"+toCheck,txt.equals(toCheck));

		txt=this.ibanInput.getText().trim();
		toCheck=iban;
		Assert.assertTrue("controllo iban;attuale:"+txt+",atteso:"+toCheck,txt.equals(toCheck));

		txt=this.accountholderInput.getText().trim();
		toCheck=owner;
		Assert.assertTrue("controllo intestato a;attuale:"+txt+",atteso:"+toCheck,txt.equals(toCheck));

		txt=this.amount.getText().replace("\u20AC", "").trim();
		toCheck=importo.replace(".", ",");
		Assert.assertTrue("controllo importo;attuale:"+txt+",atteso:"+toCheck,txt.equals(toCheck));

		txt=this.casual.getText().trim();
		toCheck=description;
		Assert.assertTrue("controllo casuale;attuale:"+txt+",atteso:"+toCheck,txt.equals(toCheck));


		//clicco su cancella
		this.cancellaDati.click();

		WaitManager.get().waitShortTime();

		this.ibanInput=page.getParticle(Locators.SendPostagiroPageMolecola.IBANINPUTMODIFY).getElement();
		this.accountholderInput=page.getParticle(Locators.SendPostagiroPageMolecola.ACCOUNTHOLDERINPUT).getElement();
		

		//controllo che iban e intestato a siano vuoti
		Assert.assertTrue("controllo iban vuoto;attuale:"+this.ibanInput.getText().trim(),!this.ibanInput.getText().trim().equals(iban));
		Assert.assertTrue("controllo intestato a vuoto;attuale:"+this.accountholderInput.getText().trim(),!this.accountholderInput.getText().trim().equals(description));

		//invio le stringe ai campi
		this.ibanInput=page.getParticle(Locators.SendPostagiroPageMolecola.IBANINPUTMODIFY).getElement();
		this.accountholderInput=page.getParticle(Locators.SendPostagiroPageMolecola.ACCOUNTHOLDERINPUTMODIFY).getElement();
		this.amount=page.getParticle(Locators.SendPostagiroPageMolecola.AMOUNTMODIFY).getElement();
		this.casual=page.getParticle(Locators.SendPostagiroPageMolecola.CASUALMODIFY).getElement();
		

		this.ibanInput.sendKeys(iban+"!");
		try {((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception err) {}

		this.accountholderInput.sendKeys(owner+"!");
		try {((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception err) {}

		double amount=Utility.parseCurrency(this.amount.getText().trim(), Locale.ITALY)*2;
		String newAmount=Utility.toCurrency(amount, Locale.ITALY);


		//this.amount.sendKeys(newAmount);
		insertAmount(newAmount);

		this.casual.sendKeys(description+"!");

		//controllo i campi
		this.ibanInput=page.getParticle(Locators.SendPostagiroPageMolecola.IBANINPUTMODIFY).getElement();
		this.accountholderInput=page.getParticle(Locators.SendPostagiroPageMolecola.ACCOUNTHOLDERINPUTMODIFY).getElement();
		this.amount=page.getParticle(Locators.SendPostagiroPageMolecola.AMOUNTMODIFY).getElement();
		this.casual=page.getParticle(Locators.SendPostagiroPageMolecola.CASUALMODIFY).getElement();
		

		txt=this.ibanInput.getText().trim();
		toCheck=iban;
		Assert.assertTrue("controllo iban nuovo;attuale:"+txt+",atteso:"+toCheck,txt.equals(toCheck+"!"));

		txt=this.accountholderInput.getText().trim();
		toCheck=owner;
		Assert.assertTrue("controllo intestato a nuovo;attuale:"+txt+",atteso:"+toCheck,txt.equals(toCheck+"!"));

		txt=this.amount.getText().trim();
		//toCheck=amount.replace(".", ",");
		Assert.assertTrue("controllo importo nuovo;attuale:"+txt+",atteso:"+newAmount,txt.equals(newAmount));

		txt=this.casual.getText().trim();
		toCheck=description;
		Assert.assertTrue("controllo casuale nuovo;attuale:"+txt+",atteso:"+toCheck,txt.equals(toCheck+"!"));

		//controllo che sia mostrata la form di selezione prodotti dopo click su modifica
		this.changePaymentTypeButton=page.getParticle(Locators.SendPostagiroPageMolecola.CHANGEPAYMENTTYPEBUTTON).getElement();
		

		this.changePaymentTypeButton.click();

		WaitManager.get().waitShortTime();

		PayWithPage p=(PayWithPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PayWithPage", page.getDriver().getClass(), page.getLanguage());
		
		p.checkPage();

		p.clickBack();

		//faccio un ricco click back
		this.cancellButton=page.getParticle(Locators.SendPostagiroPageMolecola.CANCELLBUTTON).getElement();
		

		this.cancellButton.click();

		//clicco su ESCI
		page.getParticle(Locators.SendPostagiroPageMolecola.EXITMODALBUTTON).visibilityOfElement(10L);
		this.exitModalButton=page.getParticle(Locators.SendPostagiroPageMolecola.EXITMODALBUTTON).getElement();
		

		this.exitModalButton.click();
	}

	public OKOperationPage eseguiPostagiro(BonificoDataBean bean, String campo, String value) 
	{
		String oldValue=null;

		//memorizzo temporaneamente il vecchio valore
		switch(campo)
		{
		case IMPORTO:
			oldValue=bean.getAmount();
			bean.setAmount(value);

			this.amount=page.getParticle(Locators.SendPostagiroPageMolecola.AMOUNTMODIFY).getElement();
			insertAmount(bean.getAmount());

			break;
		}

		//click su conferma
		this.confirm=page.getParticle(Locators.SendPostagiroPageMolecola.CONFIRM).getElement();
		

		this.confirm.click();

		PostagiroConfirmPage confirm=(PostagiroConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PostagiroConfirmPage", page.getDriver().getClass(), page.getLanguage());
		
		confirm.checkPage();

		((BonificoConfirmationPages)confirm).checkData(bean);

		return ((BonificoConfirmationPages)confirm).submit(bean);
	}

	protected void insertAmount(String amount2) 
	{
		this.amount.sendKeys(amount2);
	}

	public OKOperationPage sendPostagiro(PaymentCreationBean b) 
	{
		this.paymentTypeSummary=page.getParticle(Locators.SendPostagiroPageMolecola.PAYMENTTYPESUMMARY).getElement();
		this.ibanInput=page.getParticle(Locators.SendPostagiroPageMolecola.IBANINPUT).getElement();
		this.accountholderInput=page.getParticle(Locators.SendPostagiroPageMolecola.ACCOUNTHOLDERINPUT).getElement();
		this.amount=page.getParticle(Locators.SendPostagiroPageMolecola.AMOUNT).getElement();
		this.casual=page.getParticle(Locators.SendPostagiroPageMolecola.CASUAL).getElement();
		this.confirm=page.getParticle(Locators.SendPostagiroPageMolecola.CONFIRM).getElement();
		

		//controllo campo paga con
		String txt=this.paymentTypeSummary.getText().trim().toLowerCase();
		String fourDigits = txt.substring(txt.length() - 4);
		System.out.println(fourDigits);
		String toCheck=b.getPayWith().replace(";", " ").toLowerCase();
		System.out.println(toCheck.substring(toCheck.length() - 4));
		String toCheck4 = toCheck.substring(toCheck.length() - 4);
		Assert.assertTrue("controllo paga con;attuale:"+fourDigits+",atteso:"+toCheck4,fourDigits.equals(toCheck4));

		//invio i dati
		this.ibanInput.sendKeys(b.getIban());
		try {((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception err) {}

		this.accountholderInput.sendKeys(b.getOwner());
		try {((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception err) {}
		

		//this.amount.sendKeys(b.getAmount());
		this.insertAmount(b.getAmount());
		try {((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception err) {}

		this.casual.sendKeys(b.getDescription());
		try {((HidesKeyboard) page.getDriver()).hideKeyboard();}catch(Exception err) {}

		this.confirm.click();

		try
		{
			WaitManager.get().waitMediumTime();

			this.exitModalButton=page.getParticle(Locators.SendPostagiroPageMolecola.EXITMODALBUTTON).getElement();
			this.exitModalButton.click();

			WaitManager.get().waitMediumTime();

		}
		catch(Exception err)
		{

		}

		PostagiroConfirmPage p=(PostagiroConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PostagiroConfirmPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();
		
		p.checkData(b);

		return ((PostagiroConfirmPage)p).submit(b);
	}

	public void isEditable(PaymentCreationBean bean) 
	{
		isEditable(bean.getPayWith(),bean.getIban(),bean.getOwner(),bean.getAmount(),bean.getDescription());
	}

	public OKOperationPage eseguiPostagiro(PaymentCreationBean bean, String campo, String value)
	{
		String oldValue=null;

		//memorizzo temporaneamente il vecchio valore
		switch(campo)
		{
		case IMPORTO:
			oldValue=bean.getAmount();
			bean.setAmount(value);

			this.amount=page.getParticle(Locators.SendPostagiroPageMolecola.AMOUNTMODIFY).getElement();
			

			//this.amount.sendKeys(bean.getAmount());
			insertAmount(bean.getAmount());

			break;
		}




		//click su conferma
		this.confirm=page.getParticle(Locators.SendPostagiroPageMolecola.CONFIRM).getElement();
		

		this.confirm.click();

		WaitManager.get().waitShortTime();

		try {
			WebElement OkOkButton = page.getParticle(Locators.PayWithPageMolecola.OKKOBUTTON).visibilityOfElement(5L);
			OkOkButton.click();
		} catch (Exception e) {

		}

		PostagiroConfirmPage confirm=(PostagiroConfirmPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("PostagiroConfirmPage", page.getDriver().getClass(), page.getLanguage());
		
		confirm.checkPage();

		confirm.checkData(bean);

		return confirm.submit(bean);
	}

	public void clickAnnulla() 
	{
		page.getParticle(Locators.SendPostagiroPageMolecola.CANCELLBUTTON).visibilityOfElement(10L);
		page.getParticle(Locators.SendPostagiroPageMolecola.CANCELLBUTTON).getElement().click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
