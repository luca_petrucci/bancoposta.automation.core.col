package it.poste.bancoposta.pages.managers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;

import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import it.poste.bancoposta.pages.DeviceNative;
import it.poste.bancoposta.pages.Locators;

@PageManager(page="SendPostagiroPage")
@IOS
public class SendPostagiroPageManiOS extends SendPostagiroPageMan 
{
	protected void insertAmount(String amount2) 
	{
		amount2=amount2.replace(".", ",");
		
		DeviceNative d=(DeviceNative) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("DeviceNative", page.getDriver().getClass(), null);
		
		for(int i=0;i<amount2.length();i++)
		{
			char c=amount2.charAt(i);
			d.clickOnNumPadiOS(c);
			System.out.println("tap on ios pad:"+c);
			WaitManager.get().waitFor(TimeUnit.MILLISECONDS, 500);
		}
		
		d.clickOnFineFromKeyboard();
		
		//this.amount.sendKeys(amount2.replace(".",","));
	}
}
