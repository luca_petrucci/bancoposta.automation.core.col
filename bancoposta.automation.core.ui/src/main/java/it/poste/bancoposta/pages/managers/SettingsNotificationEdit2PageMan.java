package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.NotificationSettingBean;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SettingsNotificationEdit2Page;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SettingsNotificationEdit2Page")
@Android
@IOS
public class SettingsNotificationEdit2PageMan extends LoadableComponent<SettingsNotificationEdit2PageMan> implements SettingsNotificationEdit2Page
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement leftMenu;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement switchButton;
	private NotificationSettingBean bean;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
		
		this.header=page.getParticle(Locators.SettingsNotificationEdit2PageMolecola.HEADER).getElement();
		
		String txt=this.header.getText();
		Assert.assertTrue("controllo header",txt.equals(bean.getOperationType()));
	}

	public void setValue(boolean clickSwicth) 
	{
		this.switchButton=page.getParticle(Locators.SettingsNotificationEdit2PageMolecola.SWITCHBUTTON).getElement();
		this.leftMenu=page.getParticle(Locators.SettingsNotificationEdit2PageMolecola.LEFTMENU).getElement();
		
		//controllo che lo switch sia gia selezionato, se no lo clicco qualora clickSwitch sia true
		boolean checked=this.switchButton.getAttribute("checked").equalsIgnoreCase("true");

		if(!checked && clickSwicth)
		{
			this.switchButton.click();
		}

		//torno indietro
		this.leftMenu.click();
	}

	public void checkValues(NotificationSettingBean bean) 
	{
		this.switchButton=page.getParticle(Locators.SettingsNotificationEdit2PageMolecola.SWITCHBUTTON).getElement();
		this.leftMenu=page.getParticle(Locators.SettingsNotificationEdit2PageMolecola.LEFTMENU).getElement();
		

		//controllo che lo switch sia gia selezionato, se no lo clicco qualora clickSwitch sia true
		boolean checked=this.switchButton.getAttribute("checked").equalsIgnoreCase("true");
		boolean toCheck=bean.getValue().equals("si");

		Assert.assertTrue(checked == toCheck);


		//torno indietro
		this.leftMenu.click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
