package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SettingsNotificationMinAmountPage;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SettingsNotificationMinAmountPage")
@Android
@IOS
public class SettingsNotificationMinAmountPageMan extends LoadableComponent<SettingsNotificationMinAmountPageMan> implements SettingsNotificationMinAmountPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancelButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement saveButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement amount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement decreaseAmount;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement increaseAmount;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();

		}

	public void setSoglia(Integer currentVal, Integer toVal) throws Exception 
	{
		this.saveButton=page.getParticle(Locators.SettingsNotificationMinAmountPageMolecola.SAVEBUTTON).getElement();
		this.amount=page.getParticle(Locators.SettingsNotificationMinAmountPageMolecola.AMOUNT).getElement();
		this.decreaseAmount=page.getParticle(Locators.SettingsNotificationMinAmountPageMolecola.DECREASEAMOUNT).getElement();
		this.increaseAmount=page.getParticle(Locators.SettingsNotificationMinAmountPageMolecola.INCREASEAMOUNT).getElement();
		

		//controllo che il valore mostrato sia uguale alla pagina precedente
		String current=this.amount.getText().trim().replace("\u20AC", "").replace(" ", "");
		Integer c=Integer.parseInt(current);

		//Assert.assertTrue(c == currentVal);
		int numOfClicks=Math.abs(toVal - currentVal) / 50;

		//se currentVal < toVal clicco su increaseAmount fino all raggiungimento del valore atteso
		if(currentVal < toVal)
		{

			//effettuo il numero di click necessari
			for(int i=0;i<numOfClicks;i++)
			{
				this.increaseAmount.click();
				WaitManager.get().waitShortTime();
			}


		}
		else
		{
			//effettuo il numero di click necessari
			for(int i=0;i<numOfClicks;i++)
			{
				this.decreaseAmount.click();
				WaitManager.get().waitShortTime();
			}

		}

		//controllo che il valore sia uguale a quello desiderato
		current=this.amount.getText().trim().replace("\u20AC", "").replace(" ", "");
		c=Integer.parseInt(current);

		Assert.assertTrue("controllo valore settato uguale a quello desiderato; current:"+c+", atteso:"+toVal,c == toVal);

		//clicco su salva
		this.saveButton.click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
