package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import bean.datatable.NotificationSettingBean;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.InsertPosteIDPage2;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SettingsNotificationEdit1Page;
import it.poste.bancoposta.pages.SettingsNotificationEdit2Page;
import it.poste.bancoposta.pages.SettingsNotificationPage;
import it.poste.bancoposta.pages.SettingsPage;
import test.automation.core.UIUtils;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SettingsNotificationPage")
@Android
@IOS
public class SettingsNotificationPageMan extends LoadableComponent<SettingsNotificationPageMan> implements SettingsNotificationPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement backButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement notificationList;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement notificationElement;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement notificationName;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement notificationStatus;

	private String thsElements="Movimenti";

	private static final String HEADER_ANDROID="Notifiche";

	private SettingsNotificationEdit1Page sogliaPage;
	private SettingsNotificationEdit2Page noSogliaPage;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.getParticle(Locators.SettingsNotificationPageMolecola.HEADER).visibilityOfElement(30L);
		page.get();
		this.header=page.getParticle(Locators.SettingsNotificationPageMolecola.HEADER).getElement();

		String txt=this.header.getText();
		Assert.assertTrue("controllo header",txt.equals(HEADER_ANDROID));

	}

	public void checkNotificationSetting(NotificationSettingBean bean) 
	{
		//ricavo la lista delle notifiche
		List<WebElement> notificationList=null;
		String notName="";
		String status="";
		boolean soglia=(bean.getOperationType().contains(thsElements)) ? true : false;
		boolean clickSwicth=false;

		notificationList=page.getParticle(Locators.SettingsNotificationPageMolecola.NOTIFICATIONELEMENT).getListOfElements();
		notName=page.getParticle(Locators.SettingsNotificationPageMolecola.NOTIFICATIONNAME).getLocator();
		status=page.getParticle(Locators.SettingsNotificationPageMolecola.NOTIFICATIONSTATUS).getLocator();
		
		//ricavo la lista associata all'operation type e clicco
		for(WebElement e : notificationList)
		{
			WebElement txt=e.findElement(By.xpath(notName));
			WebElement stat=e.findElement(By.xpath(status));

			String text=txt.getText();
			String st=stat.getText();

			clickSwicth=st.toLowerCase().equals(bean.getValue());

			Assert.assertTrue("controllo stato notifica "+bean.getOperationType(),clickSwicth);

			if(text.equals(bean.getOperationType()))
			{
				e.click();
				break;
			}
		}

		if(soglia)
		{
			//prendo in esame pagina edit notification con soglia
			sogliaPage=(SettingsNotificationEdit1Page) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("SettingsNotificationEdit1Page", page.getDriver().getClass(), page.getLanguage());
			sogliaPage.checkPage();

			sogliaPage.checkValues(bean);
		}
		else
		{
			//prendo in esame pagina edit notification senza soglia
			noSogliaPage=(SettingsNotificationEdit2Page) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("SettingsNotificationEdit2Page", page.getDriver().getClass(), page.getLanguage());
			noSogliaPage.checkPage();
			noSogliaPage.checkValues(bean);
		}
	}

	public void setNotification(NotificationSettingBean bean) throws Exception 
	{
		//ricavo la lista delle notifiche
		List<WebElement> notificationList=null;
		String notName="";
		String status="";
		boolean soglia=(bean.getOperationType().contains(thsElements)) ? true : false;
		boolean clickSwicth=false;

		notificationList=page.getParticle(Locators.SettingsNotificationPageMolecola.NOTIFICATIONELEMENT).getListOfElements();
		notName=page.getParticle(Locators.SettingsNotificationPageMolecola.NOTIFICATIONNAME).getLocator();
		status=page.getParticle(Locators.SettingsNotificationPageMolecola.NOTIFICATIONSTATUS).getLocator();
		

		//ricavo la lista associata all'operation type e clicco
		for(WebElement e : notificationList)
		{
			WebElement txt=e.findElement(By.xpath(notName));
			WebElement stat=e.findElement(By.xpath(status));

			String text=txt.getText();
			String st=stat.getText();

			clickSwicth=!st.toLowerCase().equals(bean.getValue());

			if(text.equals(bean.getOperationType()))
			{
				e.click();
				break;
			}
		}

		if(soglia)
		{
			//prendo in esame pagina edit notification con soglia
			sogliaPage=(SettingsNotificationEdit1Page) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("SettingsNotificationEdit1Page", page.getDriver().getClass(), page.getLanguage());
			sogliaPage.checkPage();

			sogliaPage.setValue(clickSwicth);
		}
		else
		{
			//prendo in esame pagina edit notification senza soglia
			noSogliaPage=(SettingsNotificationEdit2Page) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("SettingsNotificationEdit2Page", page.getDriver().getClass(), page.getLanguage());
			noSogliaPage.checkPage();
			noSogliaPage.setValue(clickSwicth);
		}
	}

	public SettingsPage gotoSettingsPage() 
	{
		this.backButton=page.getParticle(Locators.SettingsNotificationPageMolecola.BACKBUTTON).getElement();
		

		this.backButton.click();

		SettingsPage p=(SettingsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("SettingsPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();

		return p;



	}

	//-------------------------------ANNUNZIATA------------------------------------------
	public void setPreferito(CredentialAccessBean b) 
	{
		try
		{
			WebElement impostaPreferito=page.getParticle(Locators.SettingsNotificationPageMolecola.SETPREFERITO).visibilityOfElement(5L);
			impostaPreferito.click();

			InsertPosteIDPage2 posteId = (InsertPosteIDPage2) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("InsertPosteIDPage2", page.getDriver().getClass(), page.getLanguage());
			posteId.checkPage();
			posteId.insertPosteID(b.getPosteid());
		}
		catch(Exception err)
		{

		}
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

	//-------------------------------ANNUNZIATA------------------------------------------



}
