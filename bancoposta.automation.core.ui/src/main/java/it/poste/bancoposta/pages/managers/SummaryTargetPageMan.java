package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.CredentialAccessBean;
import io.appium.java_client.android.AndroidDriver;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.SummaryTargetPage;
import test.automation.core.UIUtils;

import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="SummaryTargetPage")
@Android
@IOS
public class SummaryTargetPageMan extends LoadableComponent<SummaryTargetPageMan> implements SummaryTargetPage
{

	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement headerTarget;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancelButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement helpButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */ 

	private WebElement summaryTargetTitle;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement categoryLabel;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement categoryContent;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement amountTargetSubDescription;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement summaryTargetCategoryIcon;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement periodLabel;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement periodContent;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement amountLabel;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement amountContent;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement targetName;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement generalCondition;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	private WebElement summaryTargetButton;
	private WebElement amountTargetName;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override 
	protected void load() 
	{
		page.get();
	}



	public void InsertObjectiveName(String objectiveName)
	{
		summaryTargetTitle = page.getParticle(Locators.SummaryTargetPageMolecola.SUMMARYTARGETTITLE).getElement();
		org.springframework.util.Assert.isTrue(summaryTargetTitle.getText().trim().equals("Riepilogo obiettivo")); 

		String sum = summaryTargetTitle.getText();
		System.out.println(sum);

		amountContent = page.getParticle(Locators.SummaryTargetPageMolecola.AMOUNTCONTENT).getElement();
		org.springframework.util.Assert.isTrue(amountContent.getText().trim().equals("� 50,00")); 

		this.amountTargetName = page.getParticle(Locators.SummaryTargetPageMolecola.AMOUNTTARGETNAME).getElement();
		amountTargetName.sendKeys(objectiveName);

		amountContent = page.getParticle(Locators.SummaryTargetPageMolecola.AMOUNTCONTENT).getElement();

		summaryTargetButton = page.getParticle(Locators.SummaryTargetPageMolecola.SUMMARYTARGETBUTTON).getElement();
		summaryTargetButton.click();

	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}



}
