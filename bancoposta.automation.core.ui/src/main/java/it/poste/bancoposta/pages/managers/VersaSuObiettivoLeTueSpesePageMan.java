package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.SalvadanaioDataBean;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.VersaSuObiettivoLeTueSpesePage;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="VersaSuObiettivoLeTueSpesePage")
@Android
@IOS
public class VersaSuObiettivoLeTueSpesePageMan extends LoadableComponent<VersaSuObiettivoLeTueSpesePageMan> implements VersaSuObiettivoLeTueSpesePage
{
	UiPage page;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.getParticle(Locators.VersaSuObiettivoLeTueSpesePage.HEADER).visibilityOfElement(30L);
		page.getParticle(Locators.VersaSuObiettivoLeTueSpesePage.LEFT).visibilityOfElement(10L);
	}

	public void checkFields(SalvadanaioDataBean b) 
	{
		try {
			page.getParticle(Locators.VersaSuObiettivoLeTueSpesePage.HEADER).visibilityOfElement(30L);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
