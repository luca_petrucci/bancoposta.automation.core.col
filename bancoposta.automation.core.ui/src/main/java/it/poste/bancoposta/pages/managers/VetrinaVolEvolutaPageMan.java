package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.w3c.dom.Node;

import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BiscottoDataBean;
import bean.datatable.TutorialPopUpBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.ContoVolSubPage;
import it.poste.bancoposta.pages.LibrettoVolSubPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.VetrinaVolEvolutaPage;
import test.automation.core.UIUtils;
import utils.ObjectFinderLight;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="VetrinaVolEvolutaPage")
@Android
@IOS
public class VetrinaVolEvolutaPageMan extends LoadableComponent<VetrinaVolEvolutaPageMan> implements VetrinaVolEvolutaPage
{
	UiPage page;
	String pageSource;

	@Override
	protected void isLoaded() throws Error {

	}

	@Override
	protected void load() 
	{
		page.get();
	}

	public void checkSubPage(BiscottoDataBean b) throws Exception 
	{
		System.out.println("check subpage "+b.getBiscotto());
		if(b.getBiscotto().equals("Libretto"))
		{
			LibrettoVolSubPage p=(LibrettoVolSubPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("LibrettoVolSubPage", page.getDriver().getClass(), page.getLanguage());
			p.checkPage();
		}
		if(b.getBiscotto().equals("Conto"))
		{
			ContoVolSubPage p=(ContoVolSubPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
					.getPageManager("ContoVolSubPage", page.getDriver().getClass(), page.getLanguage());
			p.checkPage();
		}
	}

	public void clickOnScopriOfferta() 
	{
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.5f, 0.5f, 0.2f, 300);
		WaitManager.get().waitHighTime();

		String pageSource=page.getDriver().getPageSource();

		Node e=ObjectFinderLight.getElement(pageSource, page.getParticle(Locators.ContoVolSubPageMolecola.INTERA_GAMMA_LINK).getLocator());

		page.getParticle(Locators.ContoVolSubPageMolecola.INTERA_GAMMA_LINK).visibilityOfElement(10L)
		.click();
		WaitManager.get().waitMediumTime();
	}

	public VetrinaVolEvolutaPage getLight(String pageSource) throws Exception 
	{
		WaitManager.get().waitHighTime();

		this.pageSource=pageSource;

		if(!ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.VetrinaVolEvolutaMolecola.CLOSE_BUTTON).getLocator()))
			throw new Exception("Element not find in light mode "+page.getParticle(Locators.VetrinaVolEvolutaMolecola.CLOSE_BUTTON).getName());

		if(!ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.VetrinaVolEvolutaMolecola.CONTO_TAB).getLocator()))
			throw new Exception("Element not find in light mode "+page.getParticle(Locators.VetrinaVolEvolutaMolecola.CONTO_TAB).getName());

		if(!ObjectFinderLight.elementExists(pageSource, page.getParticle(Locators.VetrinaVolEvolutaMolecola.LIBRETTO_TAB).getLocator()))
			throw new Exception("Element not find in light mode "+page.getParticle(Locators.VetrinaVolEvolutaMolecola.LIBRETTO_TAB).getName());

		//controllo bearer assente
		if(ObjectFinderLight.getElement(pageSource, page.getParticle(Locators.VetrinaVolEvolutaMolecola.BEARER).getLocator()) != null)
		{
			throw new Exception("Bearer Presente nella vetrina vol "+ page.getParticle(Locators.VetrinaVolEvolutaMolecola.BEARER).getName());
		}

		return this;
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		System.out.println(page.getDriver().getPageSource());
		load();
	}

	@Override
	public void clickOnTab(String biscotto) {
		switch(biscotto)
		{
		case "Libretto":
			WaitManager.get().waitLongTime();
			page.getParticle(Locators.VetrinaVolEvolutaMolecola.LIBRETTO_TAB).clickOffline(page.getDriver().getPageSource());
			break;
		}
	}



}
