package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.BiscottoDataBean;
import bean.datatable.TutorialPopUpBean;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.VolProductDetailsPage;
import it.poste.bancoposta.utils.Utility;
import test.automation.core.UIUtils;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="VolProductDetailsPage")
@Android
public class VolProductDetailsPageMan extends LoadableComponent<VolProductDetailsPageMan> implements VolProductDetailsPage
{
	UiPage page;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
//		try {
//			page.getParticle(Locators.VolProductDetailsMolecola.RICHIEDI_BUTTON).visibilityOfElement(10L);
//		} catch (Exception e) {
//			UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.5f, 0.5f, 0.2f, 400);
//		    WaitManager.get().waitMediumTime();
//		    page.getParticle(Locators.VolProductDetailsMolecola.RICHIEDI_BUTTON).visibilityOfElement(10L);
//		    UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.UP, 400);
//		    WaitManager.get().waitMediumTime();
//		}
		Utility.scrollVersoElemento(
				page.getDriver(), 
				page.getParticle(Locators.VolProductDetailsMolecola.RICHIEDI_BUTTON), 
				UIUtils.SCROLL_DIRECTION.DOWN, 
				5);
		WaitManager.get().waitMediumTime();
		try {
			Utility.scrollVersoElemento(
					page.getDriver(), 
					page.getParticle(Locators.VolProductDetailsMolecola.IMAGE_HEADER), 
					UIUtils.SCROLL_DIRECTION.UP, 
					5);
		} catch (Exception e) {
			// TODO: handle exception
		}
		WaitManager.get().waitMediumTime();
	}

	public void checkTitlePage(BiscottoDataBean b) throws Exception 
	{
		page.getParticle(Locators.VolProductDetailsMolecola.CONTENT_LIST).visibilityOfElement(10L);
		
		List<WebElement> el=page.getParticle(Locators.VolProductDetailsMolecola.CONTENT_LIST).getListOfElements();
		
		for(WebElement e : el)
		{
			String txt=e.getText().trim().toLowerCase();
			System.out.println(txt);
			if(txt.contains(b.getProdotto().toLowerCase()))
				return;
		}
		
		throw new Exception("titolo "+b.getProdotto()+" non trovato");
	}

	public void clickOnFID() 
	{
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 500);
		WaitManager.get().waitShortTime();
		page.getParticle(Locators.VolProductDetailsMolecola.DOCUMENTO_FID_LINK).visibilityOfElement(10L);
		WebElement fid=page.getParticle(Locators.VolProductDetailsMolecola.DOCUMENTO_FID_LINK).getElement();
		fid.click();
	}

	public void checkFID(BiscottoDataBean b) 
	{
		page.getParticle(Locators.VolProductDetailsMolecola.FID_DOC).visibilityOfElement(10L,new Entry("txt", b.getFidCode()));
		
	}

	public void clickOnInteraGamma() 
	{
		UIUtils.mobile().swipe((MobileDriver) page.getDriver(), UIUtils.SCROLL_DIRECTION.DOWN, 0.5f, 0.5f, 0.5f, 0.2f, 300);
	    WaitManager.get().waitMediumTime();
	    System.out.println("############################################");
	    System.out.println(page.getDriver().getPageSource());
	    System.out.println("############################################");
	    page.getParticle(Locators.VolProductDetailsMolecola.INTERA_GAMMA).visibilityOfElement(10L)
	    .click();
	    
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}

}
