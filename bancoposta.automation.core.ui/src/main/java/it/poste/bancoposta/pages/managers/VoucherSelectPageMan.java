package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import bean.datatable.VoucherVerificationBean;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.VoucherSelectAmoutPage;
import it.poste.bancoposta.pages.VoucherSelectPage;
import test.automation.core.UIUtils;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="VoucherSelectPage")
@Android
@IOS
public class VoucherSelectPageMan extends LoadableComponent<VoucherSelectPageMan> implements VoucherSelectPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement cancelButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement voucherTypeHeader;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement voucherTypeHeaderDescr;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement voucherDiv;
	/**
	 * @android campo dinamico sostituire $(voucherTitle) con il nome del buono da ricercare
	 * @ios 
	 * @web 
	 */
	private WebElement voucherTitle;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement voucherTypeButton;

	private VoucherSelectAmoutPage selectAmount;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();

	}

	public void subScribeBuono(VoucherVerificationBean b, boolean annulla) 
	{
		String vouchers="";
		String voucherTitle="";

		vouchers=page.getParticle(Locators.VoucherSelectPageMolecola.VOUCHERDIV).getLocator();
		voucherTitle=page.getParticle(Locators.VoucherSelectPageMolecola.VOUCHERTITLE).getLocator();
		this.cancelButton=page.getParticle(Locators.VoucherSelectPageMolecola.CANCELBUTTON).getElement();
		

		//ricerco il buono desiderato
		List<WebElement> voucherList=page.getDriver().findElements(By.xpath(vouchers));

		for(WebElement v : voucherList)
		{
			String title=v.findElement(By.xpath(voucherTitle)).getText();

			//ok clicco sul campo
			if(title.equals(b.getVoucherType()))
			{
				v.click();
				break;
			}
		}

		selectAmount=(VoucherSelectAmoutPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("VoucherSelectAmoutPage", page.getDriver().getClass(), page.getLanguage());

		selectAmount.checkPage();

		selectAmount.selectAmount(annulla,b.getPosteId());

		if(annulla)
		{
			this.cancelButton.click();
		}

	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
