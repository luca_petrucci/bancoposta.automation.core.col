package it.poste.bancoposta.pages.managers;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import android.Utility;
import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import bean.datatable.TransactionDetailBean;
import io.appium.java_client.InteractsWithApps;
import it.poste.bancoposta.pages.ExclutedOperationsPage;
import it.poste.bancoposta.pages.Locators;
import test.automation.core.UIUtils;
import test.automation.core.cmd.adb.AdbCommandPrompt;
import test.automation.core.cmd.adb.AdbKeyEvent;

@PageManager(page="YourExpencesDetailsPage")
@IOS
public class YourExpencesDetailsPageManIOS extends YourExpencesDetailsPageMan {

	public List<String[]> readSpeseDettaglio() 
	{
		WaitManager.get().waitLongTime();
		List<WebElement> items= page.getParticle(Locators.YourExpencesDetailsPageMolecola.DETAILSTITLE).getListOfElements();
		System.out.println(items.size());
		List<WebElement> values= page.getParticle(Locators.YourExpencesDetailsPageMolecola.DETAILS_VALUE).getListOfElements();
		System.out.println(values.size());
		List<WebElement> percentances= page.getParticle(Locators.YourExpencesDetailsPageMolecola.DETAILS_PERCENTANCE).getListOfElements();
		System.out.println(percentances.size());
		org.springframework.util.Assert.isTrue(items.size() == values.size() && items.size() == percentances.size(), "spese dettaglio valori non della stessa lunghezza");


		ArrayList<String[]> L=new ArrayList<>();

		for(int i=0;i<items.size();i++)
		{
			String[] v=new String[3];
			v[0]=items.get(i).getText().trim();
			v[1]=values.get(i).getText().trim().replace(",", ".");
			v[2]=percentances.get(i).getText().trim();

			L.add(v);

			System.out.println(Arrays.toString(v));
		}

		return L;
	}
	
	public void esportaCSV() 
	{
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.ESPORTA_CSV).visibilityOfElement(10L);
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.ESPORTA_CSV).getElement().click();
		
		WaitManager.get().waitMediumTime();
		
		int m=Calendar.getInstance().get(Calendar.MONTH);
		int y=Calendar.getInstance().get(Calendar.YEAR);
		
		String csvFile="Uscite_"+mesi[m]+"_"+y+".csv";
		System.out.println(csvFile);
		
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.salvaSuFileBtn).getElement().click();
		WaitManager.get().waitShortTime();
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.btnIPhone).getElement().click();
		WaitManager.get().waitShortTime();
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.btnSalvaCsv).getElement().click();
		WaitManager.get().waitShortTime();
		try {
			page.getParticle(Locators.YourExpencesDetailsPageMolecola.btnSostituisciCsv).getElement().click();
			WaitManager.get().waitShortTime();
		}catch(Exception e)
		{
			//TODO
		}
	
	}
	
	public void readandCheckCsv(List<String[]> speseDettaglioList) throws Exception 
	{
		((InteractsWithApps) page.getDriver()).activateApp("com.apple.DocumentsApp");
		WaitManager.get().waitShortTime();
		
		int m=Calendar.getInstance().get(Calendar.MONTH);
		int y=Calendar.getInstance().get(Calendar.YEAR);
		String csvFile="Uscite_"+mesi[m].toLowerCase()+"_"+y;
		try {
			page.getParticle(Locators.YourExpencesDetailsPageMolecola.btnFine).getElement().click();;
		}catch(Exception e)
		{
			
		}
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.labelCerca).getElement().sendKeys(csvFile);
		page.getParticle(Locators.YourExpencesDetailsPageMolecola.csvFile).getElement(new Entry("title",csvFile)).click();
		WaitManager.get().waitShortTime();
		
		String locator= page.getParticle(Locators.YourExpencesDetailsPageMolecola.csvTable).getLocator(new Entry("title",csvFile));
		List<WebElement> table= page.getDriver().findElements(By.xpath(locator));
		for(int i=2;i<table.size();i++)
		{
			System.out.println(table.get(i).getText());
		}
		Properties pro = UIUtils.ui().getCurrentProperties();
		((InteractsWithApps) page.getDriver()).activateApp(pro.getProperty("ios.bundle.id"));
	}
	
	public ExclutedOperationsPage gotoCategory(TransactionDetailBean b) 
	{
		String listXpath="";
		String title="";
		WaitManager.get().waitLongTime();
		WaitManager.get().waitLongTime();

		listXpath=page.getParticle(Locators.YourExpencesDetailsPageMolecola.DETAILSELEMENT).getLocator();
		title=page.getParticle(Locators.YourExpencesDetailsPageMolecola.DETAILSTITLE).getLocator();
		

		List<WebElement> list=page.getDriver().findElements(By.xpath(title));

		//for(WebElement e : list)
		for(int i=0;i<list.size();i++)
		{

			String elementTitle=null;

			elementTitle=list.get(i).getText().trim().toLowerCase();
			

			if(elementTitle.equals(b.getCategoryTransation().toLowerCase()))
			{
				list.get(i).click();
				break;
			}
		}

		ExclutedOperationsPage p=(ExclutedOperationsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("ExclutedOperationsPage", page.getDriver().getClass(), page.getLanguage());

		p.setBean(b);
		p.checkPage();

		return p;
	}
}
