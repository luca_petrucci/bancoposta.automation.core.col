package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import android.Utility;
import automation.core.ui.Entry;
import automation.core.ui.WaitManager;
import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.pagemanager.UiPageManager;
import automation.core.ui.uiobject.UiPage;
import io.appium.java_client.MobileDriver;
import it.poste.bancoposta.pages.BonificoConfirmSubPage;
import it.poste.bancoposta.pages.HomePage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.YourExpencesDetailsPage;
import it.poste.bancoposta.pages.YourExpencesPage;
import test.automation.core.UIUtils;


import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="YourExpencesPage")
@Android
@IOS
public class YourExpencesPageMan extends LoadableComponent<YourExpencesPageMan> implements YourExpencesPage
{
	UiPage page;

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement header;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement leftmenu;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement MonthList;
	/**
	 * @android campo dinamico sostituire $(month) con il nome del mese da ricercare
	 * @ios campo dinamico sostituire $(month) con il mese da ricercare
	 * @web 
	 */
	private WebElement MonthElement;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement widgetContainer;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement widget;
	/**
	 * @android 
	 * @ios campo dinamico sostituire $(index) con l'indice dell'elemento da ricercare
	 * @web 
	 */
	private WebElement widgetHeader;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement seeYourProgress;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement seeAllCategories;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement seeYourThresholds;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement setBudget;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement yourProductTab;
	
	private static final String[] mesi= new String[] {
			"GENNAIO",
			"FEBBRAIO",
			"MARZO",
			"APRILE",
			"MAGGIO",
			"GIUGNO",
			"LUGLIO",
			"AGOSTO",
			"SETTEMBRE",
			"OTTOBRE",
			"NOVEMBRE",
			"DICEMBRE"
	};

	

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.getParticle(Locators.YourExpencesPageMolecola.HEADER).visibilityOfElement(40L);
		
		page.get();

		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 300);

		//page.getParticle(Locators.YourExpencesPageMolecola.SEEALLCATEGORIES).visibilityOfElement(10L);
		//page.getParticle(Locators.YourExpencesPageMolecola.SEEYOURTHRESHOLDS.getAndroidLocator())));
		//page.getParticle(Locators.YourExpencesPageMolecola.SETBUDGET.getAndroidLocator())));

		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.UP,0.50f,0.50f,0.50f,0.70f, 300);
		page.getParticle(Locators.YourExpencesPageMolecola.SEEYOURPROGRESS).visibilityOfElement(10L);

		}

	public void tapMeseCorrente() 
	{
		String mese=Utility.capitalize(Utility.getCurrentMonth());
		String meseTab="";

		try
		{
			meseTab=page.getParticle(Locators.YourExpencesPageMolecola.MONTHELEMENT).getLocator(new Entry("month", mese));
			this.MonthElement=page.getDriver().findElement(By.xpath(meseTab));
		}
		catch(Exception err)
		{
			meseTab=page.getParticle(Locators.YourExpencesPageMolecola.MONTHELEMENT).getLocator(new Entry("month", mese.toUpperCase()));
			this.MonthElement=page.getDriver().findElement(By.xpath(meseTab));
		}

		this.MonthElement.click();
	}

	public YourExpencesDetailsPage clickVediTutteLeCategorie() 
	{
		WaitManager.get().waitLongTime();
		WaitManager.get().waitLongTime();
		page.getParticle(Locators.YourExpencesPageMolecola.SEEYOURPROGRESS).visibilityOfElement(40L);
		//page.getParticle(Locators.YourExpencesPageMolecola.SEEALLCATEGORIES.getAndroidLocator())),60);


		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.UP,0.50f,0.50f,0.50f,0.70f, 500);

		try {Thread.sleep(1000);} catch (InterruptedException e) {}

		//Utility.swipe((MobileDriver) driver, Utility.DIRECTION.DOWN,0.50f,0.50f,0.50f,0.70f, 300);
		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.DOWN, 500);

		try {Thread.sleep(1000);} catch (InterruptedException e) {}

		this.seeAllCategories=page.getParticle(Locators.YourExpencesPageMolecola.SEEALLCATEGORIES).getElement();


		this.seeAllCategories.click();

		WaitManager.get().waitLongTime();
		YourExpencesDetailsPage p=(YourExpencesDetailsPage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("YourExpencesDetailsPage", page.getDriver().getClass(), page.getLanguage());

		p.checkPage();
		WaitManager.get().waitLongTime();
		return p;
		
	}

	public HomePage clickITuoiProdotti(String userHeader) 
	{
		this.yourProductTab=page.getParticle(Locators.YourExpencesPageMolecola.YOURPRODUCTTAB).getElement();
		
		this.yourProductTab.click();

		WaitManager.get().waitMediumTime();
		
		Utility.swipe((MobileDriver) page.getDriver(), Utility.DIRECTION.UP,0.50f,0.50f,0.50f,0.70f, 300);

		
		HomePage p=(HomePage) UiPageManager.get("it.poste.bancoposta.pages.managers")
				.getPageManager("HomePage", page.getDriver().getClass(), page.getLanguage());
		
		p.setUserHeader(userHeader);
		p.checkPage();

		return p;
	}

	public void clickOnVediIltuoAndamento() 
	{
		page.getParticle(Locators.YourExpencesPageMolecola.SEEYOURPROGRESS).visibilityOfElement(40L).click();
	}

	public void clickOnMesePrecedente() 
	{
		//Date d=Calendar.getInstance().getTime();
		int m=Calendar.getInstance().get(Calendar.MONTH);
		
		String mesePrecedente=null;
		
		if(m == 0)
			mesePrecedente=mesi[11];
		else
			mesePrecedente=mesi[m-1];
		
		System.out.println("mese precedente:"+mesePrecedente);
		
		String mese=page.getParticle(Locators.YourExpencesPageMolecola.MONTHELEMENT).getLocator(new Entry("month",mesePrecedente));
		
		page.getDriver().findElement(By.xpath(mese)).click();
		
	}

	public void checkRiepilogoDiv() 
	{
		page.getParticle(Locators.YourExpencesPageMolecola.RIEPILOGO_HEADER).visibilityOfElement(40L);
		page.getParticle(Locators.YourExpencesPageMolecola.ENTRATE_HEADER).visibilityOfElement(10L);
		page.getParticle(Locators.YourExpencesPageMolecola.ENTRATE_VALUE).visibilityOfElement(10L);
		page.getParticle(Locators.YourExpencesPageMolecola.USCITE_HEADER).visibilityOfElement(10L);
		page.getParticle(Locators.YourExpencesPageMolecola.USCITE_VALUE).visibilityOfElement(10L);
		page.getParticle(Locators.YourExpencesPageMolecola.DIFFERENZA_HEADER).visibilityOfElement(10L);
		page.getParticle(Locators.YourExpencesPageMolecola.DIFFERENZA_VALUE).visibilityOfElement(10L);
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
	
	public void clickOnMesePrecedente(int mesePre) 
	{
		//Date d=Calendar.getInstance().getTime();
		int m=Calendar.getInstance().get(Calendar.MONTH);
		
		String mesePrecedente=null;
		
		if(m == 0)
			mesePrecedente=mesi[11];
		else
			mesePrecedente=mesi[m-mesePre];
		
		System.out.println("mese precedente:"+mesePrecedente);
		
		String mese=page.getParticle(Locators.YourExpencesPageMolecola.MONTHELEMENT).getLocator(new Entry("month",mesePrecedente));
		
		page.getDriver().findElement(By.xpath(mese)).click();
		
	}
}
