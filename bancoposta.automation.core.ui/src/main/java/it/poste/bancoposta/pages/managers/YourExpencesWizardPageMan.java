package it.poste.bancoposta.pages.managers;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;

import automation.core.ui.pagemanager.Android;
import automation.core.ui.pagemanager.IOS;
import automation.core.ui.pagemanager.PageManager;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.Locators;
import it.poste.bancoposta.pages.YourExpencesWizardPage;
import test.automation.core.UIUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@PageManager(page="YourExpencesWizardPage")
@Android
@IOS
public class YourExpencesWizardPageMan extends LoadableComponent<YourExpencesWizardPageMan> implements YourExpencesWizardPage
{
	UiPage page;
	

	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement saltaButton;
	/**
	 * @android 
	 * @ios 
	 * @web 
	 */
	private WebElement continuaButton;

	@Override
	protected void isLoaded() throws Error {
		
	}

	@Override
	protected void load() 
	{
		page.get();
		
		}

	public void clickContinua() 
	{
		page.getParticle(Locators.YourExpencesWizardPageMolecola.CONTINUABUTTON).visibilityOfElement(5L);
		this.continuaButton=page.getParticle(Locators.YourExpencesWizardPageMolecola.CONTINUABUTTON).getElement();

		this.continuaButton.click();
	}

	public void clickSalta() 
	{
		page.getParticle(Locators.YourExpencesWizardPageMolecola.SALTABUTTON).visibilityOfElement(5L);
		this.saltaButton=page.getParticle(Locators.YourExpencesWizardPageMolecola.SALTABUTTON).getElement();
		

		this.saltaButton.click();
	}

	@Override
	public void init(UiPage page) {
		this.page=page;
	}

	@Override
	public void checkPage(Object... params) {
		load();
	}
}
