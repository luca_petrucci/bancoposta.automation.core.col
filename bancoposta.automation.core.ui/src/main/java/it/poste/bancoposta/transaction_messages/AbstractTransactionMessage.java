package it.poste.bancoposta.transaction_messages;

public abstract class AbstractTransactionMessage 
{
	protected String inputMessage;

	public AbstractTransactionMessage(String inputMessage) {
		super();
		this.inputMessage = inputMessage;
	}
	
	public abstract boolean checkMessage();
	
}
