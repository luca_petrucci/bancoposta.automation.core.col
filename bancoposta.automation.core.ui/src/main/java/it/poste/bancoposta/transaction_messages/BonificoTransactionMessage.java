package it.poste.bancoposta.transaction_messages;

import org.junit.Assert;

public class BonificoTransactionMessage extends AbstractTransactionMessage 
{
	private static final String HEADER="Gentile <header>,";
	private static final String DATA="ti riportiamo gli estremi del Bonifico SEPA da te effettuato il <data>";
	private static final String PAYWITH="<paywith>";
	private static final String INTESTAZIONE="Intestazione: <intestazione>";
	private static final String IBAN="IBAN: <iban>";
	private static final String INTESTAZIONE_IBAN="Intestazione: <iban_int>";
	private static final String INDIRIZZO="Indirizzo: <indirizzo>";
	private static final String RIFERIMENTO_BENEFICIARIO="Riferimento beneficiario: <riferimento>";
	private static final String RIFERIMENTO_BENEFICIARIO_EFFETTIVO="Riferimento Beneficiario effettivo: <riferimento_beneficiario_effettivo>";
	private static final String RIFERIMENTO_EFFETTIVO="Beneficiario effettivo: <riferimento_effettivo>";
	private static final String DATA_ADDEBITO="Data valuta addebito: <data>";
	private static final String IMPORTO="Importo bonifico: Euro <importo>";
	private static final String COMMISSIONI="Commissioni: Euro <commissioni>";
	private static final String TOTALE="Totale: Euro <totale>";
	private static final String CASUALE="Comunicazioni al Beneficiario: <casuale>";
	private static final String GRAZIE="Grazie per aver utilizzato i nostri servizi online.";
	private static final String SALUTI="Saluti";
	private static final String BANCOPOSTA="BancoPosta";
	private static final String LOCALITA="Localita': <localita>";
	private static final String PAESE_RESIDENZA="Paese di residenza: <nazione>";
	
	private String header;
	private String data;
	private String paywith;
	private String intestazione;
	private String iban;
	private String ibanInt;
	private String indirizzo;
	private String riferimento;
	private String riferimentoEffettivo;
	private String riferimentoBeneficiarioEffetivo;
	private String importo;
	private String commissioni;
	private String totale;
	private String casuale;
	private String localita;
	private String nazione;

	public BonificoTransactionMessage(String inputMessage) {
		super(inputMessage);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean checkMessage() 
	{
		String toCheck=HEADER.replace("<header>", header);
		Assert.assertTrue("controllo header messaggio:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		toCheck=DATA.replace("<data>", data);
		Assert.assertTrue("controllo data messaggio:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		toCheck=PAYWITH.replace("<paywith>", paywith);
		Assert.assertTrue("controllo paga con:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		toCheck=INTESTAZIONE.replace("<intestazione>", intestazione);
		Assert.assertTrue("controllo intestazione:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		toCheck=IBAN.replace("<iban>", iban);
		Assert.assertTrue("controllo iban:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		toCheck=INTESTAZIONE_IBAN.replace("<iban_int>", ibanInt);
		Assert.assertTrue("controllo intestazione iban:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		toCheck=INDIRIZZO.replace("<indirizzo>", indirizzo);
		Assert.assertTrue("controllo indirizzo:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		toCheck=RIFERIMENTO_BENEFICIARIO.replace("<riferimento>", riferimento);
		Assert.assertTrue("controllo riferimento beneficiario:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		toCheck=RIFERIMENTO_BENEFICIARIO_EFFETTIVO.replace("<riferimento_beneficiario_effettivo>", riferimentoBeneficiarioEffetivo);
		Assert.assertTrue("controllo riferimento beneficiario effettivo:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		toCheck=RIFERIMENTO_EFFETTIVO.replace("<riferimento_effettivo>", riferimentoEffettivo);
		Assert.assertTrue("controllo beneficiario effettivo:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		toCheck=DATA_ADDEBITO.replace("<data>", data);
		Assert.assertTrue("controllo data addebito:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		toCheck=IMPORTO.replace("<importo>", importo);
		Assert.assertTrue("controllo importo:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		toCheck=COMMISSIONI.replace("<commissioni>", commissioni);
		Assert.assertTrue("controllo commissioni:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		toCheck=TOTALE.replace("<totale>", totale);
		Assert.assertTrue("controllo totale:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		toCheck=CASUALE.replace("<casuale>", casuale);
		Assert.assertTrue("controllo casuale:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		toCheck=LOCALITA.replace("<localita>", localita);
		Assert.assertTrue("controllo localit�:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		toCheck=PAESE_RESIDENZA.replace("<nazione>", nazione);
		Assert.assertTrue("controllo paese di residenza:"+toCheck+"\nmessaggio originale:\n"+inputMessage,inputMessage.contains(toCheck));
		
		Assert.assertTrue("controllo thank you messaggio:"+GRAZIE,inputMessage.contains(GRAZIE));
		Assert.assertTrue("controllo bye messaggio:"+SALUTI,inputMessage.contains(SALUTI));
		Assert.assertTrue("controllo BANCOPOSTA messaggio:"+BANCOPOSTA,inputMessage.contains(BANCOPOSTA));
		
		
		return true;
	}
	


	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getPaywith() {
		return paywith;
	}

	public void setPaywith(String paywith) {
		this.paywith = paywith;
	}

	public String getIntestazione() {
		return intestazione;
	}

	public void setIntestazione(String intestazione) {
		this.intestazione = intestazione;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getIbanInt() {
		return ibanInt;
	}

	public void setIbanInt(String ibanInt) {
		this.ibanInt = ibanInt;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getRiferimento() {
		return riferimento;
	}

	public void setRiferimento(String riferimento) {
		this.riferimento = riferimento;
	}

	public String getRiferimentoEffettivo() {
		return riferimentoEffettivo;
	}

	public void setRiferimentoEffettivo(String riferimentoEffettivo) {
		this.riferimentoEffettivo = riferimentoEffettivo;
	}

	public String getImporto() {
		return importo;
	}

	public void setImporto(String importo) {
		this.importo = importo;
	}

	public String getCommissioni() {
		return commissioni;
	}

	public void setCommissioni(String commissioni) {
		this.commissioni = commissioni;
	}

	public String getTotale() {
		return totale;
	}

	public void setTotale(String totale) {
		this.totale = totale;
	}

	public String getCasuale() {
		return casuale;
	}

	public void setCasuale(String casuale) {
		this.casuale = casuale;
	}

	public String getRiferimentoBeneficiarioEffetivo() {
		return riferimentoBeneficiarioEffetivo;
	}

	public void setRiferimentoBeneficiarioEffetivo(String riferimentoBeneficiarioEffetivo) {
		this.riferimentoBeneficiarioEffetivo = riferimentoBeneficiarioEffetivo;
	}

	public String getLocalita() {
		return localita;
	}

	public void setLocalita(String localita) {
		this.localita = localita;
	}

	public String getNazione() {
		return nazione;
	}

	public void setNazione(String nazione) {
		this.nazione = nazione;
	}

}
