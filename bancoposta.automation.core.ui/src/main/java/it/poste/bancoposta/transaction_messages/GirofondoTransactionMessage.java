package it.poste.bancoposta.transaction_messages;

import org.junit.Assert;

public class GirofondoTransactionMessage extends AbstractTransactionMessage 
{
	private static final String HEADER="Gentile <header>,";
	private static final String INFO_DATA="ti riportiamo gli estremi del Girofondo da te effettuato online in data <data>:";
	private static final String IMPORTO="Importo Euro: <importo>";
	private static final String FAVORE_DI="A favore di: <favore_di>";
	private static final String FROM_CONTO="N. <tipologia_from> dell'Ordinante: <from_conto>";
	private static final String TO_CONTO="N. <tipologia_to> del Beneficiario: <to_conto>";
	private static final String COMMISSIONI="Commissioni: Euro <commissioni>";
	private static final String TOTALE="Importo Complessivo: Euro <totale>";
	private static final String FOOTER="Grazie per aver utilizzato i nostri servizi online."; 
	private static final String FOOTER2="Saluti,";
	private static final String FOOTER3="BancoPosta";
	
	private String header;
	private String data;
	private String importo;
	private String favore_di;
	private String fromConto;
	private String fromTipologiaConto;
	private String toConto;
	private String toTipologiaConto;
	private String commissioni;
	private String totale;
	
	public GirofondoTransactionMessage(String inputMessage) 
	{
		super(inputMessage);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean checkMessage() 
	{
		String toCheck=HEADER.replace("<header>", header);
		Assert.assertTrue("controllo header messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=INFO_DATA.replace("<data>", data);
		Assert.assertTrue("controllo messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=IMPORTO.replace("<importo>", importo);
		Assert.assertTrue("controllo messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=FAVORE_DI.replace("<favore_di>", favore_di);
		Assert.assertTrue("controllo messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=FROM_CONTO.replace("<tipologia_from>", fromTipologiaConto).replace("<from_conto>", fromConto);
		Assert.assertTrue("controllo messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=TO_CONTO.replace("<tipologia_to>", toTipologiaConto).replace("<to_conto>", toConto);
		Assert.assertTrue("controllo messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=COMMISSIONI.replace("<commissioni>", commissioni);
		Assert.assertTrue("controllo messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=TOTALE.replace("<totale>", totale);
		Assert.assertTrue("controllo messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=FOOTER;
		Assert.assertTrue("controllo messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=FOOTER2;
		Assert.assertTrue("controllo messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=FOOTER3;
		Assert.assertTrue("controllo messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		return true;
	}

	public String getFromTipologiaConto() {
		return fromTipologiaConto;
	}

	public void setFromTipologiaConto(String fromTipologiaConto) {
		this.fromTipologiaConto = fromTipologiaConto;
	}

	public String getToTipologiaConto() {
		return toTipologiaConto;
	}

	public void setToTipologiaConto(String toTipologiaConto) {
		this.toTipologiaConto = toTipologiaConto;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getImporto() {
		return importo;
	}

	public void setImporto(String importo) {
		this.importo = importo;
	}

	public String getFavore_di() {
		return favore_di;
	}

	public void setFavore_di(String favore_di) {
		this.favore_di = favore_di;
	}

	public String getFromConto() {
		return fromConto;
	}

	public void setFromConto(String fromConto) {
		this.fromConto = fromConto;
	}

	public String getToConto() {
		return toConto;
	}

	public void setToConto(String toConto) {
		this.toConto = toConto;
	}

	public String getCommissioni() {
		return commissioni;
	}

	public void setCommissioni(String commissioni) {
		this.commissioni = commissioni;
	}

	public String getTotale() {
		return totale;
	}

	public void setTotale(String totale) {
		this.totale = totale;
	}

	
}
