package it.poste.bancoposta.transaction_messages;

import org.junit.Assert;

public class PostagiroTransactionMessage extends AbstractTransactionMessage 
{
	private static final String HEADER="Gentile <intestatario>,";
	private static final String DATA="ti riportiamo gli estremi del Postagiro da te effettuato in data <data>";
	private static final String PAYWITH="<paywith>";
	private static final String ORDINANTE="Intestazione: <intestatario>";
	private static final String IBAN="<iban>";
	private static final String BENEFICIARIO="Intestazione: <intestatarioBeneficiario>";
	private static final String DATA_ADDEBITO="Data valuta addebito: <data>";
	private static final String IMPORTO="Importo del postagiro: Euro <importo>";
	private static final String COMMISSIONI="Spese: Euro <commissioni>";
	private static final String TOTALE="Totale: Euro <totale>";
	private static final String CASUALE="Causale: <casuale>";
	private static final String THANKYOU="Grazie per aver utilizzato i nostri servizi online.";
	private static final String BYE="Saluti";
	private static final String BANCOPOSTA="BancoPosta";
	
	private String header;
	private String data;
	private String payWith;
	private String intestazione;
	private String intestatarioBeneficiario;
	private String iban;
	private String dataAddebito;
	private String importo;
	private String commissioni;
	private String totale;
	private String casuale;

	public PostagiroTransactionMessage(String inputMessage) 
	{
		super(inputMessage);
		// TODO Auto-generated constructor stub
	}



	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getPayWith() {
		return payWith;
	}

	public void setPayWith(String payWith) {
		this.payWith = payWith;
	}

	public String getIntestazione() {
		return intestazione;
	}

	public void setIntestazione(String intestazione) {
		this.intestazione = intestazione;
	}

	public String getIntestatarioBeneficiario() {
		return intestatarioBeneficiario;
	}

	public void setIntestatarioBeneficiario(String intestatarioBeneficiario) {
		this.intestatarioBeneficiario = intestatarioBeneficiario;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getDataAddebito() {
		return dataAddebito;
	}

	public void setDataAddebito(String dataAddebito) {
		this.dataAddebito = dataAddebito;
	}

	public String getImporto() {
		return importo;
	}

	public void setImporto(String importo) {
		this.importo = importo;
	}

	public String getCommissioni() {
		return commissioni;
	}

	public void setCommissioni(String commissioni) {
		this.commissioni = commissioni;
	}

	public String getTotale() {
		return totale;
	}

	public void setTotale(String totale) {
		this.totale = totale;
	}

	public String getCasuale() {
		return casuale;
	}
	public void setCasuale(String casuale) {
		this.casuale = casuale;
	}
	
	@Override
	public boolean checkMessage() 
	{
		String toCheck=HEADER.replace("<intestatario>", header);
		Assert.assertTrue("controllo header messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=DATA.replace("<data>", data);
		Assert.assertTrue("controllo data messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=ORDINANTE.replace("<intestatario>", header);
		Assert.assertTrue("controllo data messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		
		toCheck=PAYWITH.replace("<paywith>", payWith);
		Assert.assertTrue("controllo payWith messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=IBAN.replace("<iban>", iban);
		Assert.assertTrue("controllo iban messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=BENEFICIARIO.replace("<intestatarioBeneficiario>", intestatarioBeneficiario);
		Assert.assertTrue("controllo Intestazione beneficiario messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=DATA_ADDEBITO.replace("<data>", data);
		Assert.assertTrue("controllo data addebito messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=IMPORTO.replace("<importo>", importo);
		Assert.assertTrue("controllo importo messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=COMMISSIONI.replace("<commissioni>", commissioni);
		Assert.assertTrue("controllo commissioni messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=TOTALE.replace("<totale>", totale);
		Assert.assertTrue("controllo totale messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		toCheck=CASUALE.replace("<casuale>", casuale);
		Assert.assertTrue("controllo casuale messaggio:"+toCheck,inputMessage.contains(toCheck));
		
		Assert.assertTrue("controllo thank you messaggio:"+THANKYOU,inputMessage.contains(THANKYOU));
		Assert.assertTrue("controllo bye messaggio:"+BYE,inputMessage.contains(BYE));
		Assert.assertTrue("controllo BANCOPOSTA messaggio:"+BANCOPOSTA,inputMessage.contains(BANCOPOSTA));
		
		return true;
	}
	

}
