package it.poste.bancoposta.utils;

import java.io.IOException;

import org.springframework.util.Assert;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.connection.ConnectionState;
import io.appium.java_client.android.connection.ConnectionStateBuilder;

public interface ConnectionUtils 
{
	public static void disableWiFi(AndroidDriver<?> driver) {
		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
				.withWiFiDisabled()
				.build());
		Assert.isTrue(!state.isWiFiEnabled());
		try {Thread.sleep(3000);} catch (Exception err )  {}
	}
	
	public static  void disableData(AndroidDriver<?> driver) {
		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
				.withDataDisabled()
				.build());
		Assert.isTrue(!state.isDataEnabled());
		try {Thread.sleep(3000);} catch (Exception err )  {}
	}
	
	public static  void disableAirMode(AndroidDriver<?> driver) {
		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
				.withAirplaneModeDisabled()
				.build());
		Assert.isTrue(!state.isAirplaneModeEnabled());
		try {Thread.sleep(3000);} catch (Exception err )  {}
	}
	
	public static  void enableWiFi(AndroidDriver<?> driver) {
		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
				.withWiFiEnabled()
				.build());
		Assert.isTrue(state.isWiFiEnabled());
		try {Thread.sleep(6000);} catch (Exception err )  {}
	}
	
	public static  void enableData(AndroidDriver<?> driver) {
		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
				//				.withWiFiEnabled()
				.withDataEnabled()
				.build());
		Assert.isTrue(state.isDataEnabled());
		try {Thread.sleep(6000);} catch (Exception err )  {}
	}
	
	public static  void enableAirMode(AndroidDriver<?> driver) {
		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
				//				.withWiFiEnabled()
				.withAirplaneModeEnabled()
				.build());
		Assert.isTrue(state.isAirplaneModeEnabled());
		try {Thread.sleep(6000);} catch (Exception err )  {}
	}
	
	public static  void disableWiFiADB(String emuName) {
		try {
			Runtime.getRuntime().exec("adb -s "+emuName+" shell svc wifi disable").waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {Thread.sleep(1000);} catch (Exception err )  {}
	}
	
	public static  void disableDataADB(String emuName) {
		try {
			Runtime.getRuntime().exec("adb -s "+emuName+" shell svc data disable").waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {Thread.sleep(1000);} catch (Exception err )  {}
	}
	
	public static  void disableAirModeADB(String emuName) {
		try {
			Runtime.getRuntime().exec("adb -s "+emuName+" shell settings put global airplane_mode_on 0").waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {Thread.sleep(5000);} catch (Exception err )  {}
	}
	
	public static  void enableWiFiADB(String emuName) {
		try {
			Runtime.getRuntime().exec("adb -s "+emuName+" shell svc wifi enable").waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {Thread.sleep(4000);} catch (Exception err )  {}
	}
	
	public static  void enableDataADB(String emuName) {
		try {
			Runtime.getRuntime().exec("adb -s "+emuName+" shell svc data enable").waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {Thread.sleep(4000);} catch (Exception err )  {}
	}
	
	public static  void enableAirModeADB(String emuName) {
		try {
			Runtime.getRuntime().exec("adb -s "+emuName+" shell settings put global airplane_mode_on 1").waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {Thread.sleep(3000);} catch (Exception err )  {}
	}
	
	public static boolean airPlaneModeEnable(AndroidDriver<?> driver)
	{
		return driver.getConnection().isAirplaneModeEnabled();
	}
	
	public static boolean mobileDataEnable(AndroidDriver<?> driver)
	{
		return driver.getConnection().isDataEnabled();
	}
	
	public static boolean wifiEnable(AndroidDriver<?> driver)
	{
		return driver.getConnection().isWiFiEnabled();
	}
	
	/*
	 *  ESEMPI VARI
	 */
//	AppiumDriver<?> d = (AppiumDriver<?>) TestEnvironment.get().getDriver(setting.getDriver());
	
	
//	NetworkConnection mobileDriver = (NetworkConnection) driver;
//	 if (mobileDriver.getNetworkConnection() != ConnectionType.AIRPLANE_MODE) {
//	   // enabling Airplane mode
//	   mobileDriver.setNetworkConnection(ConnectionType.AIRPLANE_MODE);
//	 }
//	ConnectionState state=driver.setConnection(new ConnectionStateBuilder().withWiFiDisabled().build());
	
}
