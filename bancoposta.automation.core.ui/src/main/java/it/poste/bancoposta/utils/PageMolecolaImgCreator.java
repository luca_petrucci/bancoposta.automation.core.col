package it.poste.bancoposta.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.openqa.selenium.Rectangle;

import automation.core.ui.uiobject.UiObjectElementLocator;
import automation.core.ui.uiobject.UiPage;
import it.poste.bancoposta.pages.Locators;

public class PageMolecolaImgCreator 
{
	public static void makeImg(byte[] scr,UiPage page,UiObjectElementLocator...elements)
	{
		ByteArrayInputStream bis = new ByteArrayInputStream(scr);
		try {
			BufferedImage img=ImageIO.read(bis);
			ImageIO.write(img, "png", new File("C:\\Users\\ACOTEM\\Documents\\UI\\"+elements[0].getMolecolaId()+".png"));
			
			for(UiObjectElementLocator loc : elements)
			{
				Rectangle rect=page.getParticle(loc).getElement().getRect();
				BufferedImage i=cropImage(img, rect);
				ImageIO.write(i, "png", new File("C:\\Users\\ACOTEM\\Documents\\UI\\"+loc.getMolecolaId()+"_"+loc.getParticleId()+".png"));
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static BufferedImage cropImage(BufferedImage src, Rectangle rect) {
	      BufferedImage dest = src.getSubimage(rect.x, rect.y, rect.width, rect.height);
	      return dest; 
	   }
}
