package it.poste.bancoposta.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class Parser {

	public static void parsePageSourceToUix(String pageSource,String uixName) 
	{
		// Instantiate the Factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Document doc=null;
		try {

			// optional, but recommended
			// process XML securely, avoid attacks like XML External Entities (XXE)
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

			// parse XML file
			DocumentBuilder db = dbf.newDocumentBuilder();

			doc = db.parse(new InputSource(new StringReader(pageSource)));

			// optional, but recommended
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();

			System.out.println("Root Element :" + doc.getDocumentElement().getNodeName());
			System.out.println("------");

			Element root=doc.getDocumentElement();

			renameTree(doc,root);

			root.removeAttribute("index");
			root.removeAttribute("class");
			root.removeAttribute("width");
			root.removeAttribute("height");
			
			doc.renameNode(root, "", "hierarchy");
			
			Transformer tr = TransformerFactory.newInstance().newTransformer();
            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "xml");
            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            // send DOM to file
            tr.transform(new DOMSource(doc), 
                                 new StreamResult(new FileOutputStream(new File(uixName+".uix"))));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void renameTree(Document doc, Node root) 
	{
		NodeList l=root.getChildNodes();

		for(int i=0;i<l.getLength();i++)
		{
			Node node = l.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
		        renameTree(doc,node);
		    }
		}
		if(root.getNodeName().equals("hierarchy")) return;
		
		doc.renameNode(root, "", "node");
	}

}
