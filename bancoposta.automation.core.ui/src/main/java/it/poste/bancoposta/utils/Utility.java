package it.poste.bancoposta.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang.text.StrSubstitutor;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import automation.core.ui.WaitManager;
import automation.core.ui.uiobject.UiParticle;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import test.automation.core.UIUtils;
import test.automation.core.UIUtils.SCROLL_DIRECTION;

public class Utility {
	
	private static final String SCROLL_TO_ELEMENT = "arguments[0].scrollIntoView({behavior: \"smooth\",block: \"end\",inline: \"end\"});";
	
	public static String getAlphaNumericString(int n) 
    { 
  
        // length is bounded by 256 Character 
        byte[] array = new byte[256]; 
        new Random().nextBytes(array); 
  
        String randomString 
            = new String(array, Charset.forName("UTF-8")); 
  
        // Create a StringBuffer to store the result 
        StringBuffer r = new StringBuffer(); 
  
        // remove all spacial char 
        String  AlphaNumericString 
            = randomString 
                  .replaceAll("[^A-Za-z0-9]", ""); 
  
        // Append first 20 alphanumeric characters 
        // from the generated random String into the result 
        for (int k = 0; k < AlphaNumericString.length(); k++) { 
  
            if (Character.isLetter(AlphaNumericString.charAt(k)) 
                    && (n > 0) 
                || Character.isDigit(AlphaNumericString.charAt(k)) 
                       && (n > 0)) { 
  
                r.append(AlphaNumericString.charAt(k)); 
                n--; 
            } 
        } 
  
        // return the resultant string 
        return r.toString(); 
    }
	
	public static String generateRandomDigits(int n) 
	{
		 // length is bounded by 256 Character 
        byte[] array = new byte[256]; 
        new Random().nextBytes(array); 
  
        String randomString 
            = new String(array, Charset.forName("UTF-8")); 
  
        // Create a StringBuffer to store the result 
        StringBuffer r = new StringBuffer(); 
  
        // remove all spacial char 
        String  AlphaNumericString 
            = randomString 
                  .replaceAll("[^A-Za-z0-9]", ""); 
  
        // Append first 20 alphanumeric characters 
        // from the generated random String into the result 
        for (int k = 0; k < AlphaNumericString.length(); k++) { 
  
            if (Character.isDigit(AlphaNumericString.charAt(k)) 
                       && (n > 0)) { 
  
                r.append(AlphaNumericString.charAt(k)); 
                n--; 
            } 
        } 
  
        // return the resultant string 
        return r.toString(); 
	}
	
	public static void scrollVersoElemento(WebDriver webDriver,String xpath,SCROLL_DIRECTION dir,int repeats) 
	{
		for(int i=0;i<repeats;i++)
		{
			try
			{
				WaitManager.get().waitShortTime();

				String pageSource=webDriver.getPageSource();
				
				System.out.println(pageSource);
				
				Document page=null;
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
				DocumentBuilder builder;  
				try {
					builder = factory.newDocumentBuilder();  
					page = builder.parse(new InputSource(new StringReader(pageSource)));
				} catch (Exception e) {
					e.printStackTrace();
				}

				XPath xPath = XPathFactory.newInstance().newXPath();
				NodeList n=(NodeList) xPath.compile(xpath).evaluate(page,XPathConstants.NODESET);

				if(n != null && n.getLength() > 0) return;

				UIUtils.mobile().swipe((MobileDriver) webDriver, dir, 2000);
			}
			catch(Exception err)
			{

			}
		}
	}
	
	public static void scrollVersoElemento(WebDriver driver,UiParticle element,SCROLL_DIRECTION dir,int repeats) 
	{
		for(int i=0;i<repeats;i++)
		{
			try
			{
				WaitManager.get().waitShortTime();

				String pageSource=driver.getPageSource();
				
				System.out.println(pageSource);
				
				Document page=null;
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
				DocumentBuilder builder;  
				try {
					builder = factory.newDocumentBuilder();  
					page = builder.parse(new InputSource(new StringReader(pageSource)));
				} catch (Exception e) {
					e.printStackTrace();
				}

				XPath xPath = XPathFactory.newInstance().newXPath();
				NodeList n=(NodeList) xPath.compile(element.getLocator()).evaluate(page,XPathConstants.NODESET);

				if(n != null && n.getLength() > 0) return;

				UIUtils.mobile().swipe((MobileDriver) driver, dir, 2000);
			}
			catch(Exception err)
			{

			}
		}
	}
	
	public static WebElement scrollToWebElement(WebDriver driver,WebElement element)
	{
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		
		jse.executeScript(SCROLL_TO_ELEMENT, element);
		return UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOf(element));
	}
	
	public static Properties getLanguageLocators(String country) 
	{
		InputStream authConfigStream = Utility.class.getClassLoader().getResourceAsStream("com/bata/language/locators-"+country+".properties");

	    if (authConfigStream != null) {
	      Properties prov = new Properties();
	      try 
	      {
	        prov.load(authConfigStream);
	        
	        return prov;
	        
	      } catch (Exception e) 
	      {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	        try {
				authConfigStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	      }
	    }
	    
		return null;
	}
	
	public static double parseCurrency(final String amount, final String country)
	{  
	    try {
	    	final NumberFormat format = NumberFormat.getNumberInstance(getLocale(country));
		    if (format instanceof DecimalFormat) {
		        ((DecimalFormat) format).setParseBigDecimal(true);
		    }
			return format.parse(amount.replaceAll("[^\\d.,]","")).doubleValue();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    return -1;
	}
	
	private static Locale getLocale(String country) 
	{
		switch(country.toLowerCase())
		{
		case "ita":
			return Locale.ITALY;
			default:
				return Locale.ITALY;
		}
	}

	public static String toCurrency(double d, Locale locale) 
	{
		NumberFormat format = NumberFormat.getNumberInstance(locale);
		
		return format.format(d);
	}
	
	public static double round(double value, int places) 
	{
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public static void closeEditPageMenu(WebDriver driver)
	{
		try
		{
			FluentWait wait=new FluentWait(driver);
			wait.withTimeout(Duration.ofSeconds(5));
			wait.pollingEvery(Duration.ofSeconds(1));
			
			wait.until(new Function<WebDriver,Boolean>(){

				@Override
				public Boolean apply(WebDriver arg0) 
				{
					return arg0.findElement(By.xpath("//iframe[@id='DW-SFToolkit']")) != null;
				}
				
			});
			
			driver.switchTo().frame("DW-SFToolkit");
			driver.findElement(By.id("ext-gen16")).click();
			driver.switchTo().defaultContent();
		}
		catch(Exception err)
		{
			err.printStackTrace();
			driver.switchTo().defaultContent();
		}
	}
	
	public static void IOSStagingAccess(IOSDriver<?> driver,String username,String password)
	{
		driver.context("NATIVE_APP");
		
		try
		{
			MobileElement e=(MobileElement) driver.findElement(By.xpath("//*[@placeholder='Nome utente' and ./parent::*[@placeholder='Nome utente']]"));
			
			e.sendKeys(username);
			
			Thread.sleep(1000);
			
			e=(MobileElement) driver.findElement(By.xpath("//*[@placeholder='Password' and ./parent::*[@placeholder='Password']]"));
			
			e.sendKeys(password);
			
			Thread.sleep(1000);
			
			driver.findElement(By.xpath("//*[@text='Accedi']")).click();
			
			
		}
		catch(Exception err)
		{
			
		}
		
		driver.context("WEBVIEW_1");
	}

	public static void IOSCloseSafariPanel(IOSDriver<?> driver) 
	{
		driver.context("NATIVE_APP");
		
		driver.findElement(By.xpath("//*[@text='Pannelli' or @text='Fine']")).click();
		
		try {Thread.sleep(2000);} catch (InterruptedException e) {}
		
		try
		{
			driver.findElement(By.xpath("//*[@name='closeTabButton']")).click();
		}catch(Exception err)
		{
			driver.findElement(By.xpath("//*[@text='Fine']")).click();
		}
		
		driver.context("WEBVIEW_1");
	}
	
	
}
